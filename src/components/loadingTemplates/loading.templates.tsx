import React from 'react';
import SvgAnimatedLinearGradient from 'react-native-svg-animated-linear-gradient';
import { Rect, Circle } from 'react-native-svg'



export const PostTemplateLoading = ({ width, height = 250, duration = 800 }) =>

    <SvgAnimatedLinearGradient height={height} duration={duration} width={width}>
        <Circle cx="30" cy="30" r="30" />
        <Rect x="75" y="13" rx="4" ry="4" width="150" height="13" />
        <Rect x="75" y="37" rx="4" ry="4" width="50" height="8" />
        <Rect x="0" y="110" rx="5" ry="5" width="400" height="132" />
        <Rect x="0" y="75" rx="0" ry="0" width="400" height="13" />
        <Rect x="0" y="90" rx="0" ry="0" width="400" height="13" />
    </SvgAnimatedLinearGradient>

export const ActivityTemplateLoading = ({ width, height = 150, duration = 800 }) =>

    <SvgAnimatedLinearGradient height={height} duration={duration} width={width}>
        <Rect x="0" y="21" rx="4" ry="4" width="150" height="6" />
        <Rect x="0" y="44" rx="3" ry="3" width="390" height="8" />
        <Rect x="0" y="66" rx="3" ry="3" width="390" height="8" />
        <Rect x="0" y="90" rx="0" ry="0" width="194" height="30" />
        <Rect x="195" y="90" rx="0" ry="0" width="194" height="30" />
    </SvgAnimatedLinearGradient>

export const SocialPeopleTemplateLoading = ({ width, height = 60, duration = 800 }) =>

    <SvgAnimatedLinearGradient height={height} duration={duration} width={width}>
        <Circle cx="30" cy="30" r="30" />
        <Rect x="75" y="13" rx="4" ry="4" width="390" height="13" />
        <Rect x="75" y="37" rx="4" ry="4" width="50" height="8" />
    </SvgAnimatedLinearGradient>

export const ListLoadingTemplateLoading = ({ width, height = 60, duration = 800 }) =>
    <SvgAnimatedLinearGradient height={height} duration={duration} width={width}>
        <Rect x="0" y="21" rx="4" ry="4" width="150" height="6" />
        <Rect x="0" y="44" rx="3" ry="3" width="390" height="8" />
        <Rect x="0" y="66" rx="3" ry="3" width="390" height="8" />
        <Rect x="0" y="90" rx="0" ry="0" width="194" height="30" />
        <Rect x="195" y="90" rx="0" ry="0" width="194" height="30" />
    </SvgAnimatedLinearGradient>

export const SocialChatsTemplateLoading = ({ width, height = 60, duration = 800 }) =>
    <SvgAnimatedLinearGradient height={height} duration={duration} width={width}>
        <Circle cx="30" cy="30" r="30" /> 
        <Rect x="75" y="13" rx="4" ry="4" width="400" height="13" /> 
        <Rect x="75" y="43" rx="4" ry="4" width="300" height="10" />
    </SvgAnimatedLinearGradient>