import React from 'react';
import { FlatList, View } from 'react-native';
import { EventsComponent } from '../EventsComponents/EventsComponent';
import TextView from '../TextView/TextView';
import { COLORS } from '../../config/colors/colors';
import { FaIcons } from '../../utils/Icons';
import { FaIconsEnum } from '../../utils/IconsEnum';
import Button from '../Button/Button';
import PropTypes from 'prop-types';


export class CustomListComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            noData: props.noData,
            error: props.error
        }
    }

    private currentOffset: number = 0;

    shouldComponentUpdate(nextProps) {

        if (
            (this.props.showLoadingTemplate !== nextProps.showLoadingTemplate) ||
            (this.props.loadingItems !== nextProps.loadingItems) ||
            (this.props.data !== nextProps.data) ||
            (this.props.stateType !== nextProps.stateType) ||
            (this.props.stateLegend !== nextProps.stateLegend)
        ) {
            return true;
        }
        return false;
    }

    render() {

        let data = this._data()
        return (
            <FlatList
                ref='lista'
                {...this.props}
                contentContainerStyle={[{
                    flexGrow: 1,
                }, this.props.contentContainerStyle]}
                data={data}
                renderItem={this._renderItem}
                keyExtractor={this._keyExtractor}
                onScroll={this._onScroll}
                ListEmptyComponent={this._showStateComponent()}
            />
        )
    }

    _keyExtractor = (item, index) =>
        this.props.showLoadingTemplate ?
            `Item${index}` :
            this.props.keyExtractor(item, index)

    _data = () =>
        this.props.showLoadingTemplate ?
            new Array(this.props.loadingItems || 5) :
            this.props.data

    _renderItem = (props) =>
        this.props.showLoadingTemplate ?
            this.props.loadingTemplate(props) :
            this.props.renderItem(props)

    _onScroll = (event) => {
        let currentOffset = event.nativeEvent.contentOffset.y;

        let direction = currentOffset > this.currentOffset ? 'down' : 'up';
        if (direction === 'down') {
            this.props.onScrollDown &&
                this.props.onScrollDown(event, currentOffset, this.currentOffset);
        } else if (direction === 'up') {
            this.props.onScrollUp &&
                this.props.onScrollUp(event, currentOffset, this.currentOffset);
        }


        this.currentOffset = currentOffset;

        this.props.onScroll &&
            this.props.onScroll(event)

    }

    _showStateComponent = () => {
        if (this.props.showLoadingTemplate || !this.props.stateType) {
            return null;
        }

        return this.props.stateType === 'ERROR' ?
            this._stateErrorComponent :
            this._stateNoDataComponent;
    }

    _stateErrorComponent = () =>
        <EventsComponent
            type='ERROR'
            legend={
                <View style={{
                    alignItems: 'center'
                }}>

                    <TextView textValue={this.props.stateLegend || 'Ups! algo sucedió.'}
                        styles={{
                            fontSize: 15,
                            fontWeight: 'bold',
                            color: COLORS.secondaryColor
                        }} />
                    {
                        !!this.props.refreshButton &&
                        <Button type={'light'}
                            onPress={() => {
                                this.props.refreshAction &&
                                    this.props.refreshAction()
                            }}
                            text={'Toca para refrescar'}
                            icon={FaIcons.getIcon(FaIconsEnum.REFRESH, 10, COLORS.midGrey)}
                        />
                    }

                </View>
            }
        />

    _stateNoDataComponent = () =>
        <EventsComponent
            type='NODATA'
            legend={
                <View style={{
                    alignItems: 'center'
                }}>

                    <TextView textValue={this.props.stateLegend || 'No se encontraron datos.'}
                        styles={{
                            fontSize: 15,
                            fontWeight: 'bold',
                            color: COLORS.secondaryColor
                        }} />
                    {
                        !!this.props.refreshButton &&
                        <Button type={'light'}
                            onPress={() => {
                                this.props.refreshAction &&
                                    this.props.refreshAction()
                            }}
                            text={'Toca para refrescar'}
                            icon={FaIcons.getIcon(FaIconsEnum.REFRESH, 10, COLORS.midGrey)}
                        />
                    }

                </View>
            }
        />

    _getRef = () => this.refs.lista;

    static propTypes = {
        showLoadingTemplate: PropTypes.bool,
        refreshButton: PropTypes.bool,
        refreshAction: PropTypes.func,
        loadingTemplate: PropTypes.func,
        stateLegend: PropTypes.string,
        onScrollUp: PropTypes.func,
        onScrollDown: PropTypes.func,
        stateType: PropTypes.oneOf(['ERROR', 'NODATA'])
    }

}