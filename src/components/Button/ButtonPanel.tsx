import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import TextView from '../TextView/TextView';
import PropTypes from 'prop-types';


export default class ButtonPanel extends React.Component {

    constructor(props) {
        super(props);
    }


    _button = (key: string, button: any, styles: any, textStyle: any) => {
        const { text, action, icon, iconPosition = 'left', disabled } = button;

        return <View style={{
            flex: 1
        }}
            key={key}>
            <TouchableOpacity style={[
                button_panel_styles.button_style,
                styles,
                { flexDirection: iconPosition === 'top' ? 'column' : 'row' },
                { opacity: disabled ? 0.5 : 1 }
            ]}
                disabled={disabled}
                onPress={() => { action && action() }}>
                {(iconPosition && ['top', 'left'].indexOf(iconPosition) !== -1) ? icon : null}
                <TextView textValue={text} styles={
                    [
                        button_panel_styles.text_styles,
                        textStyle,
                        {
                            marginRight: (iconPosition === 'right' && text) ? 10 : 0,
                            marginLeft: (iconPosition === 'left' && text) ? 10 : 0
                        }
                    ]
                } />
                {(iconPosition && ['bottom', 'right'].indexOf(iconPosition) !== -1) ? icon : null}
            </TouchableOpacity>
        </View>
    }

    render() {

        const { useIcon, buttons, style, buttonStyle, buttonTextStyle, orientation } = this.props;
        return (
            <View style={[
                button_panel_styles.main_panel,
                style,
                { flexDirection: orientation === 'horizontal' ? 'row' : 'column' },
                orientation === 'vertical' && { height: '100%' }
            ]}>

                {/* Botones */}
                {(useIcon && buttons && buttons.length) ?
                    buttons.map((button, index) => {
                        return this._button(button.key || ('button_' + index),
                            button,
                            buttonStyle,
                            buttonTextStyle
                        );
                    }) :
                    null
                }

            </View>

        )
    }

    static propTypes = {
        icon: PropTypes.node,
        useIcon: PropTypes.bool,
        buttons: PropTypes.array,
        orientation: PropTypes.string,
        style: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.shape({}),
        ]),
        buttonStyle: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.shape({}),
        ]),
        buttonTextStyle: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.shape({}),
        ])
    }
    static defaultProps = {
        useIcon: true,
        orientation: 'horizontal'
    }

}

const button_panel_styles = StyleSheet.create({

    main_panel: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        justifyContent: 'center',
        width: 100 + '%',
        height: 50,
        borderColor: '#9c9c9c',
        borderWidth: 0.5
    },
    text_styles: {
        marginLeft: 5,
        color: '#cdcdcd',
    },
    button_style: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        height: 100 + '%',
    }

})