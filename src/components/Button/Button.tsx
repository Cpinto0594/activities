import React from 'react';
import { TouchableOpacity, View, StyleSheet, Text } from "react-native";
import PropTypes from 'prop-types';

export default class Button extends React.Component {

    render() {

        const { text, onPress, buttonStyles, textStyles, buttonColorStyles, type, enabled, icon, outlined } = this.props;
        let typeName = (type || '').toLowerCase();
        let btntype = this.buttonTypes.indexOf(typeName) !== -1;
        if (!btntype) typeName = 'default';
        let btnColors = this.buttonColors[typeName];
        let btnColorStyle = {
            backgroundColor: btnColors.color,
            borderColor: btnColors.border,
            borderWidth: 1
        }


        return (
            <TouchableOpacity

                onPress={onPress ? onPress.bind(this) : () => { }} 
                style={
                    [
                        btnColorStyle,
                        stylesButton.button,
                        buttonStyles,
                        { opacity: !enabled ? 0.5 : 1 },
                        {
                            backgroundColor: outlined ? 'transparent' : btnColors.color,

                        },
                        buttonColorStyles,

                    ]
                }
                disabled={!enabled} >
                <View
                   
                    style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                    {icon}
                    <Text 
                     ref='button'
                     style={
                        [
                            stylesButton.text,
                            { color: btnColors.textColor },
                            textStyles,
                            icon ? { marginLeft: 15 } : null,
                            { color: outlined ? btnColors.color : btnColors.textColor }

                        ]
                    } >{text}</Text>
                </View>
            </TouchableOpacity>
        )
    }



    getRef = () => this.refs.button
    focus = () => this.getRef().focus();


    buttonTypes = ['info', 'danger', 'warning', 'success', 'default', 'light'];

    buttonColors: any = {
        success: { color: '#5cb85c', border: '#4cae4c', textColor: '#fff' },
        warning: { color: '#f0ad4e', border: '#eea236', textColor: '#fff' },
        default: { color: '#0457AD', border: '#ccc', textColor: '#fff' },
        danger: { color: '#d9534f', border: '#d43f3a', textColor: '#fff' },
        info: { color: '#5bc0de', border: '#46b8da', textColor: '#fff' },
        light: { color: '#f8f9fa', border: '#f8f9fa', textColor: '#212529' },
    }

    static defaultProps = {
        type: 'default',
        enabled: true,
        buttonStyles: [],
        textStyles: [],
        outlined: false
    }
    static propTypes = {
        buttonStyles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        textStyles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        buttonColorStyles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        text: PropTypes.string.isRequired,
        onPress: PropTypes.func,
        type: PropTypes.string,
        enabled: PropTypes.bool,
        icon: PropTypes.node,
        outlined: PropTypes.bool
    }


}
const stylesButton = StyleSheet.create({

    button: {
        padding: 10,
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: '#000',

    }

});