import React from 'react';
import { TextInput, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';


export default class InputText extends React.Component {

    defaulState = {
        height: 40,
        contentSize: {}
    }

    state = Object.assign({}, this.defaulState);



    render() {
        const { value, onChange, placeholder, styles, enabled, type, onKeyPress,
            placeHolderTextColor, capitalize, toUpper, autofocus, onSubmit,
            multilineMaxHeight, onFocus, onBlur } = this.props;

        return (
            <TextInput
                ref={'input'}
                autoFocus={autofocus}
                onFocus={onFocus}
                onBlur={onBlur}
                style={[
                    stylesInput.inputStyles,
                    styles,
                    { height: this.state.height },

                ]}
                onChangeText={(value) => { onChange && onChange(this._transformText(value, toUpper)) }}
                value={
                    (toUpper && value) ? value.toUpperCase() : value}
                placeholder={placeholder}
                autoCapitalize={toUpper ? 'characters' : capitalize}
                onKeyPress={onKeyPress}
                editable={enabled}
                secureTextEntry={type.toLowerCase() === 'password'}
                multiline={type.toLowerCase() === 'textarea'}
                keyboardType={['password', 'textarea', 'text'].indexOf(type.toLowerCase()) === -1 ? type : 'default'}
                placeholderTextColor={placeHolderTextColor}
                {
                ...(onSubmit && typeof onSubmit === 'function' ?
                    {
                        returnKeyType: 'next',
                        onSubmitEditing: onSubmit
                    } :
                    {})
                }
                onContentSizeChange={(e) => {
                    this.state.contentSize = e.nativeEvent.contentSize;
                    if (e.nativeEvent.contentSize.height < multilineMaxHeight)
                        type.toLowerCase() === 'textarea' && this._updateSize(e.nativeEvent.contentSize.height)
                }}

            >
            </TextInput>
        )
    }

    _updateSize = (height) => {
        this.setState({
            height
        });
    }

    _transformText(value, toUpper) {
        return (toUpper && value) ? value.toUpperCase() : value;
    }

    getContentSize = () => this.state.contentSize;
    getRef = () => this.refs.input
    focus = () => !this.refs.input.isFocused() && this.refs.input.focus();
    blur = () => this.refs.input.blur();

    static propTypes = {
        styles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        textColor: PropTypes.string,
        type: PropTypes.string,
        onChange: PropTypes.func,
        onKeyPress: PropTypes.func,
        value: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.string,
            PropTypes.number,
            PropTypes.object,
            PropTypes.shape({}),
        ]),
        placeholder: PropTypes.string,
        enabled: PropTypes.bool,
        placeHolderTextColor: PropTypes.string,
        toUpper: PropTypes.bool,
        capitalize: PropTypes.string,
        autofocus: PropTypes.bool,
        onSubmit: PropTypes.func,
        onFocus: PropTypes.func,
        onBlur: PropTypes.func,
        multilineMaxHeight: PropTypes.number

    }

    static defaultProps = {
        enabled: true,
        styles: [],
        type: 'text',
        toUpper: false,
        placeHolderTextColor: '#cdcdcd',
        capitalize: 'sentences',
        autofocus: false,
        multilineMaxHeight: 70
    }

}

const stylesInput = StyleSheet.create({

    inputStyles: {
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 5,
        backgroundColor: '#fff',
    }

});

