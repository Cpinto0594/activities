import React from 'react';
import { View, TouchableOpacity, Animated, Dimensions, Easing } from "react-native";
import { COLORS } from "../../config/colors/colors";
import TextView from '../TextView/TextView';
import { boxShadowTopTabs } from '../../utils/Styles';
import { SimpleLineIcons } from '../../utils/Icons';
import { SimpleLineIconsEnum } from '../../utils/IconsEnum';
import PropTypes from 'prop-types';
import { NavigationEvents, ScrollView } from 'react-navigation';

export class AnimatedSlideHeader extends React.Component {

    private animatedWidth = new Animated.Value(0);
    private windowWidth = Dimensions.get('window').width;
    private BAR_WIDTH = this.windowWidth;
    private BAR_HEIGHT = 50;
    private iconContainerWidth = this.BAR_HEIGHT;
    private barIsAnimated = false;
    private tabSpaceAvailable = (this.windowWidth - this.iconContainerWidth);

    constructor(props) {
        super(props);
        this.state = {
            icon: SimpleLineIconsEnum.OPTS_HORIZONTAL,
            headerTextComponent: props.initialTitle,
            headerLeft: props.headerLeft,
            headerRight: props.headerRight
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (
            (nextState.headerTextComponent !== this.state.headerTextComponent) ||
            (nextState.headerLeft !== this.state.headerLeft) ||
            (nextState.headerRight !== this.state.headerRight) ||
            (nextState.icon !== this.state.icon)
        ) {
            return true;
        }
        return false
    }

    render() {
        const { textColor = COLORS.secondaryColor,
            icons = [],
            tabButtonOptions = {
                activeTintColor: COLORS.secondaryColor,
                inactiveTintColor: COLORS.secondaryColor,
            }
        } = this.props;


        const { headerLeft, headerRight, headerTextComponent } = this.state;

        const inactiveTint = tabButtonOptions.inactiveTintColor;
        const activeTint = tabButtonOptions.activeTintColor;

        let iconsWidth = this.tabSpaceAvailable / icons.length;
        iconsWidth = iconsWidth < 80 ? 80 : (this.tabSpaceAvailable / icons.length)
        return (
            <View style={{
                height: this.BAR_HEIGHT,
                backgroundColor: COLORS.white,
                borderBottomColor: COLORS.softGrey,
                borderBottomWidth: 0.5,
                ...boxShadowTopTabs.box_shadow,
                flexDirection: 'row'
            }}>
                <View style={{
                    padding: 5,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    {
                        headerLeft
                    }
                </View>
                <View style={{
                    marginLeft: 10,
                    justifyContent: 'center',
                    flex: 1
                }}>
                    {
                        React.isValidElement(headerTextComponent) ?
                            headerTextComponent :
                            <TextView textValue={headerTextComponent} styles={{
                                fontSize: 20,
                                color: textColor,
                                fontWeight: 'bold'
                            }} />
                    }
                </View>
                <View style={{
                    padding: 5,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: this.iconContainerWidth
                }}>
                    {
                        headerRight
                    }
                </View>
                <Animated.View style={{
                    marginLeft: 10,
                    position: 'absolute',
                    right: 0,
                    height: 99.8 + '%',
                    backgroundColor: COLORS.white,
                    width: this.animatedWidth.interpolate({
                        inputRange: [0, 1],
                        outputRange: [this.iconContainerWidth, this.BAR_WIDTH]
                    }),
                    flexDirection: 'row'

                }}>
                    <View style={{
                        width: this.iconContainerWidth,
                    }}>
                        <TouchableOpacity style={{
                            //padding: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: 100 + '%',
                            borderRightColor: COLORS.softGrey,
                            borderRightWidth: 0.3
                        }}
                            onPress={() => {
                                this.barIsAnimated = !this.barIsAnimated;
                                this.setState({
                                    icon: this.barIsAnimated ?
                                        SimpleLineIconsEnum.ARROW_RIGHT :
                                        SimpleLineIconsEnum.OPTS_HORIZONTAL
                                }, () => {

                                    Animated.timing(this.animatedWidth, {
                                        toValue: !this.barIsAnimated ? 0 : 1,
                                        duration: 160,
                                        easing: Easing.circle
                                    }).start()
                                })
                            }}>
                            {
                                SimpleLineIcons.getIcon(this.state.icon, 20,
                                    this.barIsAnimated ? COLORS.red :
                                        COLORS.secondaryColor)
                            }
                        </TouchableOpacity>

                    </View>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{
                            flexGrow: 1,
                        }}>
                        <View style={{
                            flex: 1,
                            height: this.BAR_HEIGHT,
                            flexDirection: 'row',
                        }}>
                            {
                                icons.map(item =>
                                    <TouchableOpacity
                                        key={`navigation${item.text}`}
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            height: 100 + '%',
                                            width: iconsWidth
                                        }}
                                        onPress={() => {
                                            Animated.timing(this.animatedWidth, {
                                                toValue: 0,
                                                duration: 120
                                            }).start(() => {
                                                this.barIsAnimated = false;
                                                this.setState({
                                                    icon: this.barIsAnimated ?
                                                        SimpleLineIconsEnum.REDO :
                                                        SimpleLineIconsEnum.OPTS_HORIZONTAL,
                                                    headerTextComponent: item.text
                                                })
                                            });
                                            item.onClick();
                                        }}
                                    >
                                        {
                                            item.icon(item.activated ? 25 : 20,
                                                item.activated ? activeTint : inactiveTint)
                                        }
                                    </TouchableOpacity>
                                )
                            }
                        </View>
                    </ScrollView>
                </Animated.View>

            </View >
        )
    }

    _showHeaderLeft = (leftButtons) => {
        //this.props.headerLeft = leftButton;
        this.setState({
            headerLeft: leftButtons
        })
    }

    _showHeaderRight = (rightButtons) => {
        //this.props.headerLeft = leftButton;
        this.setState({
            headerRight: rightButtons
        })
    }

    _changeTabTitle = (title) => {
        this.setState({
            headerTextComponent: title
        })
    }

    _setProperties = (properties) => {
        this.setState(properties)
    }

    static propTypes = {
        icons: PropTypes.array,
        navigation: PropTypes.object,
        initialTitle: PropTypes.string,
        tabButtonOptions: PropTypes.object
    }

}
