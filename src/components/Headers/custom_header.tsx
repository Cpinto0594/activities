import React from 'react';
import { View } from "react-native";
import { COLORS } from "../../config/colors/colors";
import TextView from '../TextView/TextView';
export const headerTitleStyle = {
  fontSize: 17,
  fontWeight: 'bold'
}
const DefaultCustomHeader = (props) => {

  const { headerLeft, headerRight, headerTitle, style, textColor } = props;
  return (

    <View style={[
      {
        flexDirection: 'row',
        backgroundColor: COLORS.primaryColor,
        height: 50,
        justifyContent: 'flex-start',
        width: '100%',
      },
      style
    ]}>
      <View style={{
        width: 50
      }}>
        {
          headerLeft
        }
      </View>
      {
        headerTitle !== undefined && React.isValidElement(headerTitle) ?
          headerTitle :
          <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <TextView textValue={headerTitle} styles={[
              headerTitleStyle, {
                color: textColor || COLORS.secondaryColor
              }
            ]} />

          </View>
      }
      <View style={{
        width: 50
      }}>
        {
          headerRight
        }
      </View>
    </View>
  );
};

export default DefaultCustomHeader;