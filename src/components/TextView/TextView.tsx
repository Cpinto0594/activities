import React from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { COLORS } from '../../config/colors/colors';




export default class TextView extends React.Component {

    render() {

        const { textValue, styles, numberOfLines } = this.props;

        return (
            <Text style={[
                stylesInput.text_view,
                stylesInput.input_content_align,
                styles
            ]}
                numberOfLines={numberOfLines}
            >
            {textValue}
            {this.props.children}
            </Text>
        )
    }


    static propTypes = {
        styles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        textColor: PropTypes.string,
        textValue: PropTypes.string,
        numberOfLines: PropTypes.number
    }

}

const stylesInput = StyleSheet.create({
    text_view: {
        color: 'black'
    },
    input_content_align: {
        textAlignVertical: "center"
    }
});
