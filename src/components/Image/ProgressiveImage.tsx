import React from 'react';
import { View, StyleSheet, Animated, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { COLORS } from '../../config/colors/colors';

const styles = StyleSheet.create({
  imageOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
  },
  container: {
  },
});

class ProgressiveImage extends React.Component {
  thumbnailAnimated = new Animated.Value(0);
  loadingAnimated = null;
  imageAnimated = new Animated.Value(0);

  constructor(props) {
    super(props);
    this.loadingAnimated = new Animated.Value(props.loading ? 1 : 0)
  }

  handleThumbnailUrlLoad = () => {
    Animated.timing(this.thumbnailAnimated, {
      toValue: 1,
    }).start();
  }

  onImageLoad = () => {
    Animated.timing(this.imageAnimated, {
      toValue: 1,
    }).start();

  }

  componentDidMount() {
    const { source } = this.props;
    if (!source.uri) {
      Animated.timing(this.loadingAnimated, {
        toValue: 0,
        duration: 80,
        delay: 10
      }).start();
    }
  }

  render() {
    const {
      thumbnailSource,
      source,
      style,
      loading,
      loadingImage,
      loadingIndicatorProps,
      ...props
    } = this.props;



    return (
      <View style={styles.container}>
        {
          (React.isValidElement(thumbnailSource)) && !loading ?
            thumbnailSource :
            <Animated.Image
              {...props}
              source={thumbnailSource}
              style={[style, { opacity: this.thumbnailAnimated }]}
              onLoad={null}
              blurRadius={1}
            />
        }
        {
          loading &&
          <Animated.View style={[
            style,
            { backgroundColor: 'rgba(0,0,0, 0.8)' },
            { justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0 },
            { opacity: this.loadingAnimated }
          ]}>
            {
              !!(loadingImage) ?
                loadingImage :
                <ActivityIndicator {...loadingIndicatorProps} />
            }
          </Animated.View>
        }
        <Animated.Image
          {...props}
          source={source}
          style={[styles.imageOverlay, { opacity: this.imageAnimated }, style]}
          onLoad={this.onImageLoad}
        />
      </View >
    );
  }

  static propTypes = {
    thumbnailSource: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.node,
      PropTypes.number
    ]),
    source: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    loading: PropTypes.bool,
    loadingImage: PropTypes.node,
    loadingIndicatorProps: PropTypes.object,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array
    ])
  }

  static defaultProps = {
    loadingIndicatorProps: {
      size: 'small',
      color: COLORS.softGrey
    }
  }

}

export default ProgressiveImage;