import React from 'react';
import Loading from 'react-native-whc-loading';
import PropTypes from 'prop-types';
import TextView from '../TextView/TextView';
import { View } from 'react-native';
import { COLORS } from '../../config/colors/colors';

export class LoadingIndicator extends React.Component {

    render() {

        return (
            <View>
                <Loading
                    ref='loading'
                    {...this.props}
                >
                </Loading>
            </View>
        )
    }

    show() {
        this.refs.loading.show();
    }

    hide() {
        this.refs.loading.close();
    }


    static propTypes = {
        backgroundColor: PropTypes.string,
        borderRadius: PropTypes.number,
        size: PropTypes.number,
        imageSize: PropTypes.number,
        indicatorColor: PropTypes.string,
        easing: PropTypes.func,
        show: PropTypes.bool
    }

    static defaultProps = {
        backgroundColor: 'white',
        borderRadius: 50,
        size: 50,
        imageSize: 30,
        indicatorColor: COLORS.secondaryColor,
        easing: Loading.EasingType.ease,
    }

}