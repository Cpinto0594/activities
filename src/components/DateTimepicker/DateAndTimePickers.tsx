
import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-native-datepicker';
import { StyleSheet } from 'react-native';


export class DatePickers extends React.Component {


    constructor(props) {
        super(props);
    }


    render() {
        const { value, dateFormat, minDate, maxDate, onChange, enabled,
            styles, inputStyles, placeholder, textStyles } = this.props;
        let input_styles = Object.assign({}, date_styles.inputStyles, inputStyles);
        let dateTouchstyles = Object.assign({}, date_styles.dateTouch, styles);
        return (
            <DatePicker
                date={value}
                showIcon={false}
                mode="date"
                placeholder={placeholder}
                format={dateFormat}
                minDate={minDate}
                maxDate={maxDate}
                style={dateTouchstyles}
                customStyles={{
                    dateInput: input_styles,
                    dateText: textStyles
                }}
                confirmBtnText="Confirmar"
                cancelBtnText="Cancelar"
                disabled={!enabled}
                onDateChange={(value) => { onChange(value) }}
            />
        )

    }
    static propTypes = {
        styles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        onChange: PropTypes.func,
        enabled: PropTypes.bool,
        dateFormat: PropTypes.string,
        value: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.string
        ]),
        inputStyles: PropTypes.object,
        minDate: PropTypes.string,
        maxDate: PropTypes.string,
        textStyles: PropTypes.object
    }

    static defaultProps = {
        onChange: () => { },
        enabled: true,
        placeholder: 'Seleccionar',
        mode: 'clock',
        dateFormat: 'YYYY-MM-DD'
    }


}

export class TimePicker extends React.Component {


    constructor(props) {
        super(props);
    }


    render() {
        const { value, onChange, is24, enabled, placeholder, inputStyles, styles, textStyles } = this.props;
        let input_styles = Object.assign({}, date_styles.inputStyles, inputStyles);
        let dateTouchstyles = Object.assign({}, date_styles.dateTouch, styles);
        return (
            <DatePicker
                date={value}
                mode="time"
                showIcon={false}
                confirmBtnText="Confirmar"
                cancelBtnText="Cancelar"
                disabled={!enabled}
                onDateChange={(value) => { onChange(value) }}
                is24Hour={is24}
                style={dateTouchstyles}
                placeholder={placeholder}
                customStyles={{
                    dateInput: input_styles,
                    dateText: textStyles,
                    dateTouchBody: {
                        flexDirection: 'row',
                        height: 40,
                        paddingTop: 0,
                        paddingBottom: 0
                    }
                }}
            />
        )

    }


    static propTypes = {
        styles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        onChange: PropTypes.func,
        is24: PropTypes.bool,
        dateFormat: PropTypes.string,
        value: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.string
        ]),
        inputStyles: PropTypes.object,
        enabled: PropTypes.bool,
        textStyles: PropTypes.object
    }


    static defaultProps = {
        onChange: () => { },
        enabled: true,
        placeholder: 'Seleccionar',
        is24: false,
        mode: 'clock'
    }
}

const date_styles = StyleSheet.create({
    inputStyles: {
        paddingLeft: 10,
        paddingRight: 10,
        borderWidth: 1,
        borderColor: '#fff',
        borderRadius: 5,
        backgroundColor: '#fff',
        marginLeft: 0,
        alignItems: 'flex-start',
        width: 100 + '%'
    },
    dateTouch: {
        width: '100%'
    }
})