import React from 'react';
import {
    View, DatePickerAndroid,
    StyleSheet, TouchableWithoutFeedback,
    TimePickerAndroid
} from 'react-native';
import PropTypes from 'prop-types';
import InputText from '../InputText/InputText';
import moment  from 'moment';


export default class AndroidDatePicker extends React.Component {
    state = {
        selectedDate: null,
        stringDate: null
    }
    dateFormat: string = '';

    _onChangePicker(func: any) {
        func = !func ? () => { } : func;
        return this._openDatePicker(func);
    }

    _formatStringDate(date: any) {
        var aux = this.dateFormat;

        var year = date instanceof Date ? date.getFullYear() : date.year;
        var month = date instanceof Date ? (date.getMonth() ) : date.month;
        var day = date instanceof Date ? date.getDate() : date.day;

        return moment(new Date(year , month, day)).format(this.props.dateFormat);

    }

    _openDatePicker(func: any) {
        DatePickerAndroid.open({ date: this.state.selectedDate || new Date() })
            .then((date) => {
                if (date.action !== DatePickerAndroid.dismissedAction) {
                    let dateConverted = this._convertDate(date);
                    let stringConverted = this._formatStringDate(date);
                    console.log(dateConverted , stringConverted)
                    this.setState({
                        selectedDate: dateConverted,
                        stringDate: stringConverted
                    });

                    func.call(this, dateConverted, date);
                }
            });
    }

    _convertDate(dateObject: any) {
        var year = dateObject instanceof Date ? dateObject.getFullYear() : dateObject.year;
        var month = dateObject instanceof Date ? (dateObject.getMonth() ) : dateObject.month;
        var day = dateObject instanceof Date ? dateObject.getDate() : dateObject.day;
        return new Date(year, month, day)
        //return new Date(dateObject.year + '/' + this._convertNumber(dateObject.month) + '/' + this._convertNumber(dateObject.day));
    }

    _convertNumber(num: any) {
        return num <= 9 ? ('0' + num) : num;
    }

    render() {
        const { onChange, placeholder, enabled, dateFormat, value, inputStyles } = this.props;

        this.dateFormat = dateFormat;
        var _value = this._formatStringDate(new Date(value));
        this.state.stringDate = _value || this.state.stringDate;

        var input_styles = Object.assign({}, stylesDatePicker.inputText, inputStyles || {});

        return (

            <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
                <TouchableWithoutFeedback onPress={() => { enabled ? this._onChangePicker.call(this, onChange) : null; }}>
                    <View style={{ flex: 1 }} >
                        <InputText value={this.state.stringDate}
                            styles={input_styles}
                            enabled={false}
                            placeholder={placeholder}
                        />
                    </View>
                </TouchableWithoutFeedback>

            </View>
        )
    }

    static propTypes = {
        styles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        onChange: PropTypes.func,
        enabled: PropTypes.bool,
        dateFormat: PropTypes.string,
        value: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.string
        ]),
        inputStyles: PropTypes.object
    }

    static defaultProps = {
        onChange: () => { },
        enabled: true,
        placeholder: 'Seleccionar',
        dateFormat: 'YYYY-MM-DD'
    }
}

//TIMEPICKER//

export class AndroidTimePicker extends React.Component {
    state = {
        selectedTime: null
    }
    is24: boolean = false;
    mode: string = 'clock';

    _onChangePicker(func: any) {
        func = !func ? () => { } : func;
        return this._openTimePicker(func);
    }

    _openTimePicker(func: any) {
        TimePickerAndroid.open({ is24Hour: this.is24, mode: this.mode })
            .then((time) => {
                if (time.action !== TimePickerAndroid.dismissedAction) {
                    let timeConverted = this._convertDate(time);
                    this.setState({
                        selectedTime: timeConverted
                    });

                    func.call(this, timeConverted, time);
                }
            });
    }

    _convertDate(timeObject: any) {
        return this._convertNumber(timeObject.hour) + ':' + this._convertNumber(timeObject.minute) + '';
    }

    _convertNumber(num: any) {
        return num <= 9 ? ('0' + num) : num;
    }

    render() {
        const { onChange, placeholder, enabled, is24, mode, inputStyles, value } = this.props;
        this.is24 = is24;
        this.mode = mode;
        this.state.selectedTime = value || '';

        var input_styles = Object.assign({}, stylesDatePicker.inputText, inputStyles || {});

        return (

            <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
                <TouchableWithoutFeedback onPress={() => { enabled ? this._onChangePicker.call(this, onChange) : null; }}>
                    <View style={{ flex: 1 }} >
                        <InputText value={this.state.selectedTime}
                            styles={input_styles}
                            enabled={enabled}
                            placeholder={placeholder}
                        />
                    </View>
                </TouchableWithoutFeedback>

            </View>
        )
    }


    static propTypes = {
        styles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        mode: PropTypes.string,
        onChange: PropTypes.func,
        enabled: PropTypes.bool,
        is24: PropTypes.bool,
        inputStyles: PropTypes.object,
        value: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.string
        ]),
    }

    static defaultProps = {
        onChange: () => { },
        enabled: true,
        placeholder: 'Seleccionar',
        is24: false,
        mode: 'clock'
    }
}



const stylesDatePicker = StyleSheet.create({

    button: {
        borderRadius: 10,
        padding: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputText: {
        color:'black'
    }

});


