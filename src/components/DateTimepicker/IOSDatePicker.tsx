import React from 'react';
import { DatePickerIOS, View, TouchableWithoutFeedback, StyleSheet, Text } from "react-native";
import PropTypes from 'prop-types';
import InputText from '../InputText/InputText';
import moment from 'moment';


export default class IOSDatePicker extends React.Component {

    constructor(props) {
        super(props);
    }

    state = {
        stringDate: null,
        showPicker: false,
        value: null
    }

    render() {

        const { onChange, placeholder, enabled, value, inputStyles } = this.props;
        this.state.value = value;
        this.state.stringDate = this._formatStringDate(new Date(this.state.value));
        var input_styles = Object.assign({}, stylesDatePicker.inputText, inputStyles || {});


        var showDatePicker = this.state.showPicker && enabled ?
            <DatePickerIOS
                style={{ height: 150 }}
                date={this.state.value}
                onDateChange={(value) => {
                    this._onChangeDate(value, onChange);
                }}
                mode="date" /> : <View />

        return (

            <View style={[input_styles, { borderRadius: 5 }]}>
                <TouchableWithoutFeedback onPress={() => { enabled ? this._togglePicker.call(this) : null; }}>
                    <Text
                    >{this.state.stringDate}</Text>
                </TouchableWithoutFeedback>
                {showDatePicker}
            </View>
        )
    }

    _formatStringDate(date: any) {
        if (!date) return null;
        var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();

        return moment(new Date(year, month, day)).format('YYYY-MM-DD');

    }

    _onChangeDate = (date, callback) => {
        let stringValue = this._formatStringDate(date);
        this.setState({ value: date, stringValue: stringValue });
        callback(stringValue, date);
        this._togglePicker();
    }

    _togglePicker() {
        this.setState({ showPicker: !this.state.showPicker });
    }

    static propTypes = {
        styles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        onChange: PropTypes.func,
        enabled: PropTypes.bool,
        dateFormat: PropTypes.string,
        value: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.string
        ]),
        inputStyles: PropTypes.object
    }

    static defaultProps = {
        onChange: () => { },
        enabled: true,
        placeholder: 'Seleccionar',
        dateFormat: 'YYYY-MM-DD'
    }




}

export class IOSTimePicker extends React.Component {

    constructor(props) {
        super(props);
    }

    state = {
        stringDate: null,
        showPicker: false,
        value: null,
        is24: false
    }

    render() {

        const { onChange, placeholder, enabled, value, inputStyles, is24 } = this.props;

        this.state.value = value;
        this.state.is24 = is24;

        this.state.stringDate = this._formatStringDate(new Date(this.state.value));
        var input_styles = Object.assign({}, stylesDatePicker.inputText, inputStyles || {});


        var showDatePicker = this.state.showPicker && enabled ?
            <DatePickerIOS
                style={{ height: 150 }}
                date={this.state.value}
                onDateChange={(value) => {
                    this._onChangeDate(value, onChange);
                }}
                mode="time" /> : <View />

        return (

            <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
                <TouchableWithoutFeedback onPress={() => { !enabled ? this._togglePicker.call(this) : null; }}>
                    <View style={{ flex: 1 }} >
                        <InputText value={this.state.stringDate}
                            styles={input_styles}
                            enabled={enabled}
                            placeholder={placeholder}
                        />
                    </View>
                </TouchableWithoutFeedback>
                {showDatePicker}
            </View>
        )
    }

    _formatStringDate(date: any) {
        let format = 'HH:MM';
        format = this.state.is24 ? format : format.toLowerCase();

        return moment(date).format(format);

    }

    _onChangeDate = (date, callback) => {
        let stringValue = this._formatStringDate(date);
        this.setState({ value: date, stringValue: stringValue });
        callback(stringValue, date);
        this._togglePicker();
    }

    _togglePicker() {
        this.setState({ showPicker: !this.state.showPicker });
    }

    static propTypes = {
        styles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        onChange: PropTypes.func,
        enabled: PropTypes.bool,
        is24: PropTypes.bool,
        value: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.string
        ]),
        inputStyles: PropTypes.object
    }

    static defaultProps = {
        onChange: () => { },
        enabled: false,
        placeholder: 'Seleccionar',
        dateFormat: 'YYYY-MM-DD'
    }



}


const stylesDatePicker = StyleSheet.create({

    button: {
        borderRadius: 10,
        padding: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputText: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        borderWidth: 1,
        borderColor: '#fff',
        borderRadius: 5,
        backgroundColor: '#fff'
    }

});