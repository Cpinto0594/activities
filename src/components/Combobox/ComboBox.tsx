import React from 'react';
import { Picker, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';


export default class ComboBox extends React.Component {




    render() {

        const { onChange, items, itemLabel, itemValue, enabled, value } = this.props;
        let arrComponents = [];
        let arrData = items || []

        arrComponents = arrData.map((row: any, index) =>
            <Picker.Item
                key={index}
                label={row[itemLabel]}
                value={row[itemValue]}
                color='black'
                
            />
        );

        return (
            <Picker
                itemStyle={{borderBottomColor:'#fff' , color:'black' , borderBottomWidth:1, padding:0, ...comboStyles.items }}
                onValueChange={onChange.bind(this)}
                enabled={enabled}
                selectedValue={value}
            >
                {arrComponents}
            </Picker>
        )
    }

    static propTypes = {
        styles: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.number,
            PropTypes.shape({}),
        ]),
        onChange: PropTypes.func,
        items: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.object
        ]),
        itemLabel: PropTypes.string.isRequired,
        itemValue: PropTypes.string.isRequired,
    }

    static defaultProps = {
        onChange: () => { }
    }


}

const comboStyles = StyleSheet.create({

    items: {
        fontSize: 10,
        color:'red'
    }

});


