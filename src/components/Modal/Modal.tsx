import React from 'react';
import { Modal, Dimensions, View, StyleSheet } from 'react-native';

export class ModalWindow extends React.Component {
    state = {
        isVisible: false
    }

    constructor() {
        super();

    }

    componentWillMount() {
        this.setState({ isVisible: false });
    }

    componentWillUnMount() {
        this.setState({ isVisible: false });
    }

    toggleModal() {
        this.setState({ isVisible: !this.state.isVisible })
    }


    closeModal() {
        this.setState({ isVisible: false })
    }

    openModal() {
        this.setState({ isVisible: true })
    }



    render() {
        const { onModalClose, onModalHide, onModalShow, backDropStyles, containerStyles,
            onBackDropClick } = this.props;

        const children = this.props.children;
        return (this.state.isVisible && 
            <View style={stylesModal.container}>
                <Modal 
                    animationType="slide"
                    visible={this.state.isVisible}
                    transparent={true}
                    onRequestClose={() => {
                        let shouldClose = onModalClose ? !!onModalClose.call(this) : true;
                        if (shouldClose) this.closeModal.call(this);

                    }}
                    onDismiss={onModalHide ? onModalHide.bind(this) : () => { }}
                    onShow={onModalShow ? onModalShow.bind(this) : () => { }}
                >
                    <View style={[stylesModal.containerBackDrop, backDropStyles]}  >
                        <View style={[stylesModal.containerContent, containerStyles]}>
                            {children}
                        </View>
                    </View>

                </Modal>
            </View>
        )
    }
}

export class ModalFooter extends React.Component {
    render() {
        const { children, styles } = this.props;
        let attrs = [stylesModal.footer];
        attrs = attrs.concat(styles);
        let inputStyles = Object.assign({}, ...attrs);


        return (
            <View style={inputStyles}>
                {children}
            </View>
        )
    }
}

export class ModalHeader extends React.Component {
    render() {
        const { children, styles } = this.props;
        let attrs = [stylesModal.header];
        attrs = attrs.concat(styles);
        let inputStyles = Object.assign({}, ...attrs);
        return (
            <View style={inputStyles}>
                {children}
            </View>
        )
    }
}
export class ModalContent extends React.Component {
    render() {
        const { children, styles } = this.props;
        let attrs = [stylesModal.content];
        attrs = attrs.concat(styles);
        let inputStyles = Object.assign({}, ...attrs);
        return (
            <View style={inputStyles}>
                {children}
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },
    containerBackDrop: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    containerContent: {
        alignSelf: 'center',
        backgroundColor: 'white',
        width: Dimensions.get('window').width - 20,
        flex: 1,
        marginTop: 30,
        marginBottom: 50,
        borderRadius: 5
    },
    footer: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 0,
    },
    header: {
        flex: 1,
        justifyContent: 'flex-start',
        marginTop: 0,
    },
    content: {
        flex: 1,
        padding: 5
    }
};

const stylesModal = StyleSheet.create(styles);