import React from 'react';
import { View, Image } from 'react-native';
import PropTypes from 'prop-types';

export class CircularImage extends React.Component {

    render() {

        let { source, width = 0, height = 0, style, imageStyle, borderRadius } = this.props;
        borderRadius = borderRadius || (width / 2)

        return (
            <View style={[{
                borderRadius: borderRadius,
                justifyContent: 'center',
                alignItems: 'center',
                overflow: 'hidden',
                width,
                height
            }, style]}>
                <Image source={source}
                    style={[{
                        width,
                        height,
                        borderRadius: borderRadius
                    }, imageStyle]} />
            </View>
        )
    }

    static propTypes = {
        source: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.number
        ]).isRequired,
        width: PropTypes.number,
        height: PropTypes.number,
        style: PropTypes.object,
        imageStyle: PropTypes.object,
        borderRadius: PropTypes.number
    }

}