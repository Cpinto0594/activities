import React, { ReactNode } from 'react';
import { View, Image, Dimensions } from 'react-native';
import { COLORS } from '../../config/colors/colors';
import PropTypes from 'prop-types';
import TextView from '../TextView/TextView';
import { TouchableOpacity } from 'react-native-gesture-handler';


export class EventsComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            windowWidth: Dimensions.get('window').width
        }
    }
    private ASSETS_FOLDER = '../../../assets/';
    private defaultIcons = {
        'NODATA': 'no_data.png',
        'INFO': '',
        'ERROR': '',
        'NETWORK': ''
    }

    render() {
        const { style, type, legend, refresh, iconImage } = this.props;

        const image = React.isValidElement(iconImage) ?
            iconImage :
            this.getImage();

        return (
            <View style={[{
                flex: 1
            },
                style
            ]
            }>


                <View style={{
                    //padding: 10,
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: COLORS.white

                }}>
                    <TouchableOpacity
                        style={{
                            padding: 10
                        }}
                        disabled={!refresh}
                        >
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            //...boxShadowEventComponent.box_shadow
                        }}>
                            {
                                image
                            }

                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                {
                                    React.isValidElement(legend) ?
                                        legend :
                                        <TextView textValue={legend}
                                            styles={{
                                                color: COLORS.secondaryColor,
                                                fontSize: 15,
                                                fontWeight: 'bold'
                                            }} />
                                }
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }


    getImage = () => {
        const { type } = this.props;
        let imageResource = this.defaultIcons[type];
        let image = require('../../../assets/no_data.png');

        return <Image
            source={image}
            style={{
                width: 200,
                height: 200
            }} />

    }

    static defaultProps = {
        asButton: false
    }

    static propTypes = {
        style: PropTypes.object,
        type: PropTypes.oneOf(['ERROR', 'INFO', 'NODATA']).isRequired,
        legend: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.node
        ]).isRequired,
        refresh: PropTypes.bool,
        iconImage: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.object,
            PropTypes.node
        ])
    }

}