import React from 'react';
import { View, Animated, Easing } from 'react-native';
import { COLORS } from '../../config/colors/colors';
import { IonIcons, FaIcons } from '../../utils/Icons';
import { IonIconEnum, FaIconsEnum } from '../../utils/IconsEnum';
import InputText from '../InputText/InputText';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native-gesture-handler';

export class SearchBar extends React.Component {
    private openedAnimation = null;
    private opacityAnimation = null;
    private openedIconsWidthAnimation = null;

    constructor(props) {
        super(props);
        this.state = {
            opened: !props.slideSearch,
            searchingText: props.searchingText
        }

        this.openedAnimation = new Animated.Value(
            props.slideSearch ?
                0 :
                1
        );
        this.opacityAnimation = new Animated.Value(1);
        this.openedIconsWidthAnimation = new Animated.Value(1);

    }

    render() {

        const {
            placeHolder, closeBarIcon, searchIcon,
            inputCloseSearchIcon, placeHolderTextColor, inputStyles,
            onChange, style, slideSearch, leftIcon, rightIcon, onOpenSearchBar, onCloseSearchBar,
            hideIconsOnOpenBar, onSubmit, onCleanSearchText, inputContainerStyles
        } = this.props;


        return (
            <View style={[{
                alignItems: 'center',
                justifyContent: 'flex-end',
                backgroundColor: COLORS.white,
                flex: 1,
                flexDirection: 'row'

            }, style]}>
                {
                    (hideIconsOnOpenBar && !this.state.opened || !hideIconsOnOpenBar) &&
                    <View style={{
                        maxHeight: 100 + '%'
                    }}>
                        {
                            leftIcon
                        }
                    </View>
                }
                {
                    this.state.opened &&
                    <Animated.View style={[{
                        //backgroundColor: COLORS.backgroundColor,
                        borderRadius: 10,
                        padding: 0,
                        //borderColor: '#cdcdcd',
                        //borderWidth: 0.4,
                        //height: 100 + '%',
                        flexDirection: 'row',
                        alignItems: 'center',
                        //paddingHorizontal: this.state.opened ? 10 : 0,
                        //width:100+'%'
                        flex: this.openedAnimation.interpolate({
                            inputRange: [0, 0.5, 1],
                            outputRange: [0, 0.5, 1]
                        })
                    }, inputContainerStyles]}>


                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingHorizontal: 10
                        }}>
                            {
                                searchIcon
                            }
                        </View>

                        <InputText
                            ref='inputSearch'
                            value={this.state.searchingText}
                            styles={[{
                                backgroundColor: 'transparent',
                                color: COLORS.secondaryColor,
                                flex: 1,
                                borderRadius: 0,
                                paddingLeft: 0
                            }, inputStyles]}
                            onChange={(value) => {
                                this.setState({
                                    searchingText: value
                                }, () => {
                                    onChange && onChange(value)
                                })
                            }}
                            onSubmit={onSubmit}
                            placeHolderTextColor={placeHolderTextColor}
                            placeholder={placeHolder}
                        />
                        {
                            this.state.searchingText ?
                                <TouchableOpacity style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    paddingHorizontal: 10,
                                }}
                                    onPress={() => {
                                        this.setState({
                                            searchingText: null
                                        }, () => {
                                            onCleanSearchText && onCleanSearchText()
                                        })
                                    }}>
                                    {
                                        inputCloseSearchIcon
                                    }
                                </TouchableOpacity> :
                                null

                        }
                    </Animated.View>
                }
                {
                    slideSearch &&
                    <Animated.View style={{
                    }}>
                        <TouchableOpacity style={{
                            height: '100%',
                            padding: 10,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                            onPress={() => {
                                this.setState({
                                    opened: !this.state.opened
                                }, () => {

                                    Animated.timing(this.openedAnimation, {
                                        toValue: this.state.opened ? 1 : 0,
                                        duration: 200,
                                        //useNativeDriver: true
                                    }).start(() => {
                                        if (this.state.opened) {
                                            this.refs.inputSearch.focus();
                                            onOpenSearchBar && onOpenSearchBar()
                                        } else {
                                            onCloseSearchBar && onCloseSearchBar()
                                        }
                                    });

                                })
                            }}>
                            {
                                !this.state.opened ?
                                    searchIcon :
                                    closeBarIcon

                            }
                        </TouchableOpacity>
                    </Animated.View>
                }
                {
                    (hideIconsOnOpenBar && !this.state.opened || !hideIconsOnOpenBar) &&
                    <View style={{
                        maxHeight: 100 + '%'
                    }}>
                        {
                            rightIcon
                        }
                    </View>
                }
            </View >
        )
    }


    static propTypes = {
        slideSearch: PropTypes.bool,
        placeHolder: PropTypes.string,
        closeBarIcon: PropTypes.node,
        searchIcon: PropTypes.node,
        inputCloseSearchIcon: PropTypes.node,
        placeHolderTextColor: PropTypes.string,
        inputStyles: PropTypes.object,
        onChange: PropTypes.func,
        style: PropTypes.object,
        leftIcon: PropTypes.node,
        rightIcon: PropTypes.node,
        onOpenSearchBar: PropTypes.func,
        onCloseSearchBar: PropTypes.func,
        hideIconsOnOpenBar: PropTypes.bool,
        searchingText: PropTypes.string,
        onSubmit: PropTypes.func,
        onCleanSearchText: PropTypes.func,
        inputContainerStyles: PropTypes.object
    }

    static defaultProps = {
        inputCloseSearchIcon: IonIcons.getIcon(IonIconEnum.CLOSE_X, 20, COLORS.secondaryColor),
        searchIcon: IonIcons.getIcon(IonIconEnum.SEARCH_ICON, 20, COLORS.secondaryColor),
        closeBarIcon: IonIcons.getIcon(IonIconEnum.CLOSE_X, 20, COLORS.secondaryColor),
        slideSearch: false
    }


}