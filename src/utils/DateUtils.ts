import moment from 'moment';

export class DateUtils {

    static DATEFORMAT = 'DD-MM-YYYY';
    static PRETTY_DATEFORMAT = 'DD de MMMM del YYYY';
    static SHORT_PRETTY_DATEFORMAT = 'DD [de] MMM [de] YYYY';
    static SHORT_PRETTY_DATEFORMAT_NOYEAR = 'DD [de] MMM';
    static TIMEFORMAT = 'hh:mm a';


    static now = () => moment();

    static toDate = (date) => moment(date);

    static dateToString = (date, format) => {
        return moment(date).format(format);
    }

    static diffDuration = (a, b) => moment.duration(a.diff(b));


}