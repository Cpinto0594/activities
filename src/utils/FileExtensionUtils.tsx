const jsonExtension = require('./FileExtension.json');

export class FileExtensionUtils {


    static getFileExtension(fileFormat): any {
        return jsonExtension.find(element => element.extension === `.${fileFormat.replace('.', '')}`) || {};
    }


}