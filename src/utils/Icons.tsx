import React from 'react';
import FAIcons from 'react-native-vector-icons/FontAwesome';
import IONIcons from 'react-native-vector-icons/Ionicons';
import OCTIcons from 'react-native-vector-icons/Octicons'
import FeatIcons from 'react-native-vector-icons/Feather'
import SimplineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';



export class IonIcons {
    static getIcon(icon: string, size: number, color: string) {
        return <IONIcons name={icon} size={size} color={color} />
    }

}

export class FaIcons {
    static getIcon(icon: string, size: number, color: string) {
        return <FAIcons name={icon} size={size} color={color} />
    }
}


export class OctIcons {
    static getIcon(icon: string, size: number, color: string) {
        return <OCTIcons name={icon} size={size} color={color} />
    }
}


export class FeatherIcons {
    static getIcon(icon: string, size: number, color: string) {
        return <FeatIcons name={icon} size={size} color={color} />
    }
}

export class SimpleLineIcons {
    static getIcon(icon: string, size: number, color: string) {
        return <SimplineIcons name={icon} size={size} color={color}  />
    }
}

export class MaterialIcons {
    static getIcon(icon: string, size: number, color: string) {
        return <MaterialIcon name={icon} size={size} color={color} />
    }
}

export class AntDesignIcons {
    static getIcon(icon: string, size: number, color: string) {
        return <AntDesignIcon name={icon} size={size} color={color} />
    }
}