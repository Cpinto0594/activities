import { StyleSheet, Platform } from "react-native";
import { COLORS } from "../config/colors/colors";

export const textStyles = StyleSheet.create({
    underlined: { textDecorationLine: 'underline' },
    bold: { fontWeight: 'bold' },
    italic: { fontStyle: 'italic' }
});

export const containerStyles = {
    marginHorizontal: Platform.OS === 'android' ? 16 : 18
}


export const boxShadow = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 4,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 4 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 4
        }
    },
    ios: {
        box_shadow: {
            elevation: 4,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 4 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 4
        }
    }
}
));


export const boxShadowChatItem = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 3,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 3 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 3
        }
    },
    ios: {
        box_shadow: {
            elevation: 3,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 3 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 3
        }
    }
}
));

export const boxShadowSocialPostHeader = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 5,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 5 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 5
        }
    },
    ios: {
        box_shadow: {
            elevation: 5,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 5 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 5
        }
    }
}
));


export const boxShadowBottomTabs = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 1
        }
    },
    ios: {
        box_shadow: {
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.6,
            shadowRadius: 1,
        }
    }
}
));


export const boxShadowChatInput = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 3,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 3 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 3
        }
    },
    ios: {
        box_shadow: {
            elevation: 3,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 3 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 3
        }
    }
}
));


export const boxShadowButtonPanel = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 2
        }
    },
    ios: {
        box_shadow: {
            shadowColor: COLORS.midGrey,
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.6,
            shadowRadius: 1,
        }
    }
}
));

export const boxShadowUserProfile = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 1
        }
    },
    ios: {
        box_shadow: {
            shadowColor: COLORS.midGrey,
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.6,
            shadowRadius: 1,
        }
    }
}
));

export const boxShadowSocialUserProfile = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 2
        }
    },
    ios: {
        box_shadow: {
            shadowColor: COLORS.midGrey,
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.6,
            shadowRadius: 1,
        }
    }
}
));

export const boxShadowHeader = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 1
        }
    },
    ios: {
        box_shadow: {
            shadowColor: COLORS.midGrey,
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.5,
            shadowRadius: 1,
        }
    }
}
));


export const boxShadowImageProfile = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 5
        }
    },
    ios: {
        box_shadow: {
            shadowOpacity: 0.75,
            shadowRadius: 5,
            shadowColor: COLORS.midGrey,
            shadowOffset: { height: 0, width: 0 },
        }
    }
}
));


export const boxShadowEventComponent = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 0.6
        }
    },
    ios: {
        box_shadow: {
            shadowOpacity: 0.75,
            shadowRadius: 5,
            shadowColor: COLORS.midGrey,
            shadowOffset: { height: 0, width: 0 },
        }
    }
}
));

export const boxShadowConfigComponent = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 5,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 5 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 5
        }
    },
    ios: {
        box_shadow: {
            elevation: 5,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 5 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 3
        }
    }
}
));


export const boxShadowActionButton = StyleSheet.create(Platform.select({
    android: {
        box_shadow: {
            elevation: 5,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 5 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 5
        }
    },
    ios: {
        box_shadow: {
            elevation: 5,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 0.5 * 5 },
            shadowOpacity: 0.3,
            shadowRadius: 0.8 * 5
        }
    }
}
));


export const boxShadows = (elevation, color = 'black', shadowOpacity = 0.3) => StyleSheet.create({
    box_shadow: {
        elevation,
        shadowColor: color,
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity,
        shadowRadius: 0.8 * elevation
    }
});