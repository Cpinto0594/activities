export enum IonIconEnum {
    ARROW_BACK = 'md-arrow-round-back',
    ARROW_FORWARD = 'md-arrow-round-forward',
    FILTER = 'md-funnel',
    COMPANY = 'ios-business',
    PROJECT = 'md-filing',
    USERS = 'md-contacts',
    USER = 'md-contact',
    REFRESH = 'md-refresh',
    REFRESH_CIRCLE = 'md-refresh-circle',
    HEART_EMPTY = 'md-heart-empty',
    HEART_FULL = 'md-heart',
    ATTACHMENT_DOC = 'md-attach',
    ATTACHMENT_IMAGE = 'md-images',
    TRASH_FUL = 'md-trash',
    DRAFT = 'md-bookmark',
    CLOSE_X = 'ios-close',
    DOTS_MORE = 'md-more',
    IOS_CHAT_BOXES = 'ios-chatboxes',
    SEARCH_ICON = 'ios-search',
    TARGET = 'md-locate',
    RADIO_OFF = 'ios-radio-button-off',
    RADIO_ON = 'ios-radio-button-on',
    ANALITYCS = 'md-stats',
    KEY = 'ios-key'
}

export enum FaIconsEnum {
    ARROW_BACK = 'arrow-left',
    ARROW_FORWARD = 'arrow-right',
    FILTER = 'filter',
    REFRESH = 'refresh',
    COMMENT_EMPTY = 'comment-o',
    COMMENT_FULL = 'comments-o',
    TRASH_EMPTY = 'trash-o',
    PDF = 'file-pdf-o',
    WORD = 'file-word-o',
    ZIP = 'file-zip-o',
    EXCEL = 'file-excel-o',
    POWERPOINT = 'file-powerpoint-o',
    TXT = 'file-text-o',
    FILE = 'file-o',
    IMAGE = 'file-image-o',
    SOUND = 'file-sound-o',
    VIDEO = 'file-movie-o',
    CLOSE_X = 'close',
    EDIT_EMPTY = 'edit',
    CHECK = 'check',
    SEND_EMPTY = 'send-o',
    USER_CIRCLE_EMPTY = 'user-circle-o',
    USERS_FULL = 'users',
    NAVICON = 'navicon',
    FEEDS = 'feed',
    CHATS = 'wechat',
    PLAY = 'play',
    PAUSE = 'pause',
    STOP = 'stop',
    LIST_EMPTY = 'list-alt',
    GEAR_FULL = 'gear',
    PHOTO = 'photo',
    CAMERA_FULL = 'camera',
    SAVE = 'save',
    PLUS = 'plus',
    SEARCH = 'search',
    WORLD = 'globe',
    LOCK = 'lock',
    PENCIL = 'pencil',
    CLONE = 'clone'
}

export enum OCTIconsEnum {
    HOME = 'home',
    MUTE = 'mute',
    UNMUTE = 'unmute'
}

export enum FeatherIconsEnum {
    MESSAGE = 'message-circle',
    LOGOUT = 'log-out',
    EDIT = 'edit',
    EYE_OFF = 'eye-off',
    TRASH = 'trash-2'
}

export enum FontAwesome5Enum {
}

export enum SimpleLineIconsEnum {
    USER = 'user',
    PEOPLE = 'people',
    LOGOUT = 'logout',
    LAYERS = 'layers',
    MENU = 'menu',
    OPTS_VERTICAL = 'options-vertical',
    OPTS_HORIZONTAL = 'options',
    FEED = 'feed',
    ORGANIZATION = 'organization',
    NOTE = 'note',
    EDIT = 'note',
    LIKE = 'like',
    DISLIKE = 'dislike',
    BUBBLE = 'bubble',
    BUBBLES = 'bubbles',
    CAMERA = 'camera',
    DOWNLOAD = 'cloud-download',
    UPLOAD = 'cloud-upload',
    ATTACHMENT = 'paper-clip',
    SETTINGS = 'settings',
    CHART = 'chart',
    PROJECTS = 'briefcase',
    REDO = 'action-redo',
    ARROW_RIGHT = 'arrow-right',
    GLOBE = 'globe',
    BELL = 'bell'
}

export enum MaterialIconsEnum {
    CHAT = 'chat-bubble',
    EXPAND_MORE = 'expand-more',
    CLEAR_CIRCLE = 'remove-circle-outline',
    CHECKBOX_FULL = 'check-box',
    CHECKBOX_EMPTY = 'check-box-outline-blank'
}

export enum AntDesignIconsEnum {
    LIKEFUL = 'like1',
    LIKEEMPTY = 'like2'
}