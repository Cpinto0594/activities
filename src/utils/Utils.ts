import { StackActions, NavigationActions } from "react-navigation";


export default class Utils {

    static navigateWithStackReset(navigation, routeName, params: any = {}) {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: routeName, params: params })
            ]
        })
        navigation.dispatch(resetAction)
    }

    /**
     * Reset full app stack and do some navigations.
     * index: the action to get focused
     * 
     */
    static navigationFullStackResetWithActions(navigation, index: number, actions: Array<any>) {
        const resetAction = StackActions.reset({
            index: index,
            actions: actions
        })
        navigation.dispatch(resetAction)
    }

    static defaultNavigation(navigation, routeName, params: any = {}) {
        navigation.navigate(routeName, params)

    }

    static defaultNavigationReplace(navigation, routeName, params: any = {}, action?: any) {
        navigation.replace(NavigationActions.navigate({ routeName: routeName, params: params, action: action }))
    }

    /**
     * Reset the current stack and do some navigation actions.
     * index: The last position of the action array: array.length
     */
    static navigationResetWithActions(navigation, index: number, actions: Array<any>) {
        navigation.reset(
            actions, index
        );

    }

    static navigateToSubRoute = (navigation, parentRoutename, childRouteName, childParams = {}, parentParams = {}) => {
        navigation.navigate(parentRoutename, parentParams,
            NavigationActions.navigate({ routeName: childRouteName, params: childParams }))

    }

    static arrayDeleteItem(array, condition) {

        for (let i = 0; i < array.length; i++) {
            if (condition(array[i])) {
                array.splice(i, 1);
                break;
            }
        }
        return array;
    }

    static randomIntFromInterval = (min, max) => { // min and max included 
        return Math.floor(Math.random() * (max - min + 1) + min);
    }


}