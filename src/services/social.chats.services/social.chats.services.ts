import AsyncStorageProvider from "../../FunctionaityProviders/AsyncStorageprovider";
import { ChatModel } from "../../models/ChatModel";
import { AuthService } from "../AuthService";
import { ChatMessageModel } from "../../models/ChatMessageModel";
import FireBaseExpo from "../../FunctionaityProviders/Firebase/FirebaseExpo";
import { ChatContainerChatsMessages } from "../../models/ChatContainerChatsMessage";


export class SocialLocalChatServices {

    public static CHATS_STORAGE_KEY = 'UserChatsStorage_';
    public static CHATS_MESSAGES_STORAGE_KEY = 'UserMessageStorage_';

    public static INSTANCE = new SocialLocalChatServices;
    private firebaseInstance: FireBaseExpo = FireBaseExpo.instance;



    findLocalUserChats = async () => {

        let key = this.getUserChatsKey();

        let userChatsString = await AsyncStorageProvider.getItem(key);
        let arrChats: Array<ChatModel> = [];
        if (userChatsString) {
            arrChats = JSON.parse(userChatsString);
        }
        return arrChats;
    }

    findLocalChatByid = async (chatId) => {

        let key = this.getUserChatsKey();

        let userChatsString = await AsyncStorageProvider.getItem(key);
        let arrChats: Array<ChatModel> = [];
        if (userChatsString) {
            arrChats = JSON.parse(userChatsString);
        }

        return arrChats.find(chats => chats.chat_id === chatId);
    }

    findLocalUserMessages = async (): Promise<Array<ChatContainerChatsMessages>> => {

        let key = this.getUserMessagessKey();
        let messagesString = await AsyncStorageProvider.getItem(key);
        let arrMessages: Array<ChatContainerChatsMessages> = [];
        if (messagesString) {
            arrMessages = JSON.parse(messagesString);
        }
        return arrMessages;
    }

    findLocalUserMessagesByChatId = async (chat_id): Promise<ChatContainerChatsMessages> => {

        let userMessages = await SocialLocalChatServices.INSTANCE.findLocalUserMessages()
        let messagesByChat: ChatContainerChatsMessages = new ChatContainerChatsMessages
        if (userMessages && userMessages.length) {
            messagesByChat = userMessages.find(mss => mss.chat_id === chat_id)
        }
        return messagesByChat;
    }


    storeChats = async (arrChats: Array<ChatModel>) => {
        let userLocalChats = await SocialLocalChatServices.INSTANCE.findLocalUserChats();
        if (userLocalChats.length) {
            arrChats.forEach((chat, index) => {
                let exist = userLocalChats.findIndex(ch => ch.chat_id === chat.chat_id);
                if (exist !== -1) {
                    userLocalChats[exist] = chat;
                } else {
                    userLocalChats.unshift(chat)
                }
            });
        } else {
            userLocalChats = arrChats;
        }

        let key = this.getUserChatsKey()
        await AsyncStorageProvider.setItem(key, JSON.stringify(userLocalChats));

    }

    storeMessages = async (chat_id, arrMessages: Array<ChatMessageModel>) => {
        
        let userLocalMessages = await SocialLocalChatServices.INSTANCE.findLocalUserMessagesByChatId(chat_id);
        let storedMessages: ChatMessageModel[]
        if (userLocalMessages && userLocalMessages.messages && userLocalMessages.messages.length) {
            storedMessages = userLocalMessages.messages
            arrMessages.forEach((chat, index) => {
                let exist = storedMessages.findIndex(ch => ch.message_id === chat.message_id);
                if (exist !== -1) {
                    storedMessages[exist] = chat;
                } else {
                    storedMessages.unshift(chat)
                }
            });
            userLocalMessages.messages = storedMessages;
        } else {
            //Creamos contenedor de mensajes
            storedMessages = arrMessages;
            userLocalMessages = new ChatContainerChatsMessages;
            userLocalMessages.chat_id = chat_id
            userLocalMessages.messages = storedMessages
        }
        //Reemplazamos los mensajes en el storage
        let arrAllMessages = await SocialLocalChatServices.INSTANCE.findLocalUserMessages()
        if (arrAllMessages && arrAllMessages.length) {
            let index = arrAllMessages.findIndex(mss => mss.chat_id === chat_id)
            arrAllMessages[index] = userLocalMessages
        }else{
            arrAllMessages = [userLocalMessages] 
        }
        let key = this.getUserMessagessKey();
        await AsyncStorageProvider.setItem(key, JSON.stringify(arrAllMessages));

    }

    _getUserId = () => AuthService.userLoggedData.userIdentifier;

    findChatWithUser = async (userId: string): Promise<ChatModel> => {
        let userChats = await SocialLocalChatServices.INSTANCE.findLocalUserChats();

        return userChats.find(chat => chat.chat_members.includes(userId) &&
            chat.chat_destination_type === 'PEOPLE');
    }

    newChatResource = (source, destination) => {
        let userIdentifier = source.uuid;
        let destinationIdentifier = destination.uuid;

        return {
            resource: {
                chat_members: [
                    userIdentifier, destinationIdentifier
                ],
                chat_owner: {
                    name: source.nombre_completo,
                    pic: source.pic,
                    uuid: userIdentifier
                },
                chat_members_detail: [
                    {
                        name: source.nombre_completo,
                        pic: source.pic,
                        uuid: userIdentifier
                    },
                    {
                        name: destination.nombre_completo,
                        pic: destination.pic,
                        uuid: destination.uuid
                    }
                ],
                chat_description: destination.nombre_completo,
                chat_title: destination.nombre_completo,
                chat_pic: destination.pic
            }
        }
    }

    saveRemoteChat = async (chat: ChatModel) => {
        if (!chat) return null;

        return await this.firebaseInstance
            .db()
            .collection(FireBaseExpo.CHAT_LIST_COLLECTION)
            .add(chat);

    }

    saveRemoteMessage = async (message: ChatMessageModel) => {
        return this.firebaseInstance
            .db()
            .collection(FireBaseExpo.CHAT_MESSAGES_COLLECTION)
            .add(message)
    }

    updateRemoteChat = async (chat_id: string, chat_object: any) => {
        return this.firebaseInstance
            .db()
            .collection(FireBaseExpo.CHAT_LIST_COLLECTION)
            .doc(chat_id)
            .set(chat_object, { merge: true })
    }

    removeLocalChat = async (chat_id) => {

        let chatData = await SocialLocalChatServices.INSTANCE.findLocalUserChats()
        let chatMessagesContainer = await SocialLocalChatServices.INSTANCE.findLocalUserMessages()
        if (chatData && chatData.length) {
            let newChats = chatData.filter(chat => chat.chat_id !== chat_id)
            let newMessages = chatMessagesContainer.filter(mss => mss.chat_id !== chat_id)
            await this.removeLocalMessages()
            await this.removeLocalChats()
            await SocialLocalChatServices.INSTANCE.storeChats(newChats)
            let key = this.getUserMessagessKey();
            await AsyncStorageProvider.setItem(key, JSON.stringify(newMessages));
        }
    }

    getUserChatsKey = () => SocialLocalChatServices.CHATS_STORAGE_KEY + this._getUserId()
    getUserMessagessKey = () => SocialLocalChatServices.CHATS_MESSAGES_STORAGE_KEY + this._getUserId()


    removeLocalChats = async () => {
        let key = this.getUserChatsKey()
        await AsyncStorageProvider.removeItem(key)
    }

    removeLocalMessages = async () => {
        let key = this.getUserMessagessKey()
        await AsyncStorageProvider.removeItem(key)
    }



}