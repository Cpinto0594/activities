import { AuthService } from "../AuthService";
import LoggedUserModel from "../../models/loggedUserModel";
import { HttpServices, Request } from "../../FunctionaityProviders/http.services/http.service";
import axios from "axios";

export default class SocialPostsServices {
    public authService: AuthService;
    private userData: LoggedUserModel;
    private httpservice: HttpServices;

    static apiInstance = axios.create({
        baseURL: AuthService.API_URL.SERVER + AuthService.API_URL.API + AuthService.API_URL.POSTS.PATH,
        timeout: 60000,
        responseType: 'json'
    })

    constructor() {
        this.authService = new AuthService();
        this.userData = AuthService.userLoggedData;
        this.httpservice = new HttpServices(SocialPostsServices.apiInstance);
    }



    public findPosts(params): Promise<any> {
        return this.httpservice.get(AuthService.API_URL.POSTS.FIND,
            {
                params: params
            })
            .then(res => res.data)
    }

    public postContent(postUUid, params): Promise<any> {
        return this.httpservice.get(AuthService.API_URL.POSTS.FIND_POST_DETAIL.replace(':postUuid', postUUid),
            {
                params: params
            })
            .then(res => res.data)
    }


    public findComments(post, params): Request{
        return this.httpservice.get(AuthService.API_URL.POSTS.COMMENTS.replace(':postUuid', post),
            {
                params: params
            })
    }

    public saveComment(post, data): Promise<any> {
        return this.httpservice.post(AuthService.API_URL.POSTS.SAVE_COMMENT.replace(':postUuid', post),
            data,
            {
                headers: { 'Content-Type': 'multipart/form-data' }
            }
        ).then(res => res.data)
    }

    public deleteComment(post, comment): Promise<any> {
        return this.httpservice.delete(AuthService.API_URL.POSTS.DELETE_COMMENT.replace(':postUuid', post).replace(':commentUuid', comment))
            .then(res => res.data)
    }

    public savePost(data): Promise<any> {
        return this.httpservice.post(AuthService.API_URL.POSTS.SAVE_POST,
            data,
            {
                headers: { 'Content-Type': 'multipart/form-data' }
            })
            .then(res => res.data)
    }

    public deletePost(post): Promise<any> {
        return this.httpservice.delete(AuthService.API_URL.POSTS.DELETE_POST.replace(':postUuid', post))
            .then(res => res.data)
    }

    public likePost(post, user): Promise<any> {
        return this.httpservice.post(AuthService.API_URL.POSTS.LIKE_POST
            .replace(':postUuid', post),
            {},
            {
                params: {
                    user
                }
            }
        )
            .then(res => res.data)
    }

    public unlikePost(post, user): Promise<any> {
        return this.httpservice.delete(AuthService.API_URL.POSTS.UNLIKE_POST
            .replace(':postUuid', post),
            {
                params: {
                    user
                }
            }
        )
            .then(res => res.data)
    }

    public likePostComment(post, comment, user): Promise<any> {
        return this.httpservice.post(AuthService.API_URL.POSTS.LIKE_COMMENT
            .replace(':postUuid', post)
            .replace(':commentUuid', comment),
            {},
            {
                params: {
                    user
                }
            })
            .then(res => res.data)
    }

    public unLikePostComment(post, comment, user): Promise<any> {
        return this.httpservice.delete(AuthService.API_URL.POSTS.UNLIKE_COMMENT
            .replace(':postUuid', post)
            .replace(':commentUuid', comment),
            {
                params: {
                    user
                }
            })
            .then(res => res.data)
    }

    userPosts(userUUid, params): Promise<any> {
        return this.httpservice.get(AuthService.API_URL.POSTS.USER_POSTS
            .replace(':userUUid', userUUid),
            {
                params
            })
            .then(res => res.data);
    }



}