import { AuthService } from "../AuthService";
import LoggedUserModel from "../../models/loggedUserModel";
import { HttpServices } from "../../FunctionaityProviders/http.services/http.service";
import axios from 'axios';

export default class EmpresasServices {
    public authService: AuthService;
    private userData: LoggedUserModel;
    private httpservice: HttpServices;

    static apiInstance = axios.create({
        baseURL: AuthService.API_URL.SERVER + AuthService.API_URL.API + AuthService.API_URL.EMPRESAS.PATH,
        timeout:60000,
        responseType: 'json'
    })


    constructor() {
        this.authService = new AuthService();
        this.userData = AuthService.userLoggedData;
        this.httpservice = new HttpServices(EmpresasServices.apiInstance);
    }



    public save(empresa): Promise<any> {
        let url = AuthService.API_URL.getUrl(AuthService.API_URL.EMPRESAS.SAVE);
        let method = 'post';
        if (empresa.id) {
            url += empresa.id;
            method = 'put';
        }

        return this.httpservice[method](url,
            empresa)
            .then(res => res.data)
    }

    public findAllActives(): Promise<any> {
        return this.httpservice.get(AuthService.API_URL.getUrl(AuthService.API_URL.EMPRESAS.FIND_ACTIVE))
            .then(res => res.data)
    }

    public search(data): Promise<any> {
        return this.httpservice.post(AuthService.API_URL.getUrl(AuthService.API_URL.EMPRESAS.FIND_SEARCH),
            data)
            .then(res => res.data)
    }

    public delete(id): Promise<any> {
        return this.httpservice.delete(AuthService.API_URL.getUrl(AuthService.API_URL.EMPRESAS.DELETE.replace(':id', id)))
            .then(res => res.data)
    }


}