import { Alert, AlertButton } from "react-native";


export default class NotificationServices {
    static SUCCESS_TITLE = 'Correcto !';
    static INFO_TITLE = 'Atención !';
    static ERROR_TITLE = 'Error !';
    static CONFIRM_TITLE = 'Confirmar !';
    static CONFIRM_BUTTON = 'Ok';
    static CANCEL_BUTTON = 'Cancelar';

    success(message: string, callOk?: Function, title?: string) {
        Alert.alert(title || NotificationServices.SUCCESS_TITLE, message,
            [
                { text: NotificationServices.CONFIRM_BUTTON, onPress: () => { callOk ? callOk() : null } }
            ]);
    }


    fail(message: string, callOk?: Function, title?: string) {

        Alert.alert(title || NotificationServices.ERROR_TITLE, message,
            [
                { text: NotificationServices.CONFIRM_BUTTON, onPress: () => { callOk ? callOk() : null } }
            ]);

    }

    info(message: string, callOk?: Function, title?: string) {
        Alert.alert(title || NotificationServices.INFO_TITLE, message,
            [
                { text: NotificationServices.CONFIRM_BUTTON, onPress: () => { callOk ? callOk() : null } }
            ]);
    }

    confirm(message, callOk, callCancel?, title?, buttons?: AlertButton[]) {
        Alert.alert(title || NotificationServices.CONFIRM_TITLE, message,
            buttons ||
            [
                {
                    text: NotificationServices.CANCEL_BUTTON, onPress: callCancel ? callCancel : null
                },
                {
                    text: NotificationServices.CONFIRM_BUTTON, onPress: callOk ? callOk : null
                }
            ],
            { cancelable: false });
    }

}