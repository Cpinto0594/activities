import { AuthService } from "../AuthService";
import LoggedUserModel from "../../models/loggedUserModel";
import { HttpServices, Request } from "../../FunctionaityProviders/http.services/http.service";
import axios from 'axios';

export default class ActividadesServices {
    public authService: AuthService;
    private userData: LoggedUserModel;
    private httpservice: HttpServices;

    static apiInstance = axios.create({
        baseURL: AuthService.API_URL.SERVER + AuthService.API_URL.API + AuthService.API_URL.ACTIVIDADES.PATH,
        timeout: 60000,
        responseType: 'json'
    })


    constructor() {
        this.authService = new AuthService();
        this.userData = AuthService.userLoggedData;
        this.httpservice = new HttpServices(ActividadesServices.apiInstance);
    }

    public export() {

    }

    public saveActividad(actividad): Promise<any> {
        let url = AuthService.API_URL.getUrl(AuthService.API_URL.ACTIVIDADES.SAVE);
        let method = 'post';
        if (actividad.id) {
            url += actividad.id;
            method = 'put';
        }

        return this.httpservice[method](url,
            actividad
        ).then(res => res.data)

    }

    public createAllActividades(actividades): Promise<any> {
        let url = AuthService.API_URL.getUrl(AuthService.API_URL.ACTIVIDADES.SAVEALL);
        let method = 'post';

        return this.httpservice[method](url,
            actividades
        ).then(res => res.data)

    }

    public delete(id): Promise<any> {
        return this.httpservice.delete(AuthService.API_URL.getUrl(AuthService.API_URL.ACTIVIDADES.DELETE.replace(':id', id))
        ).then(res => res.data)
    }

    public search(data): Request {
        return this.httpservice.post(AuthService.API_URL.getUrl(AuthService.API_URL.ACTIVIDADES.SEARCH),
            data
        );
    }

    public findDashboard(data): Request {
        return this.httpservice.post(AuthService.API_URL.getUrl(AuthService.API_URL.ACTIVIDADES.DASHBOARD),
            data
        );
    }


    public autoComplete(query, usuario): Promise<any> {
        return this.httpservice.get(AuthService.API_URL.getUrl(AuthService.API_URL.ACTIVIDADES.AUTOCOMPLETE).replace(':usuario', usuario),
            {
                params: {
                    filtro: query
                }
            }
        ).then(res => res.data)
    }


}