import LoggedUserModel from "../models/loggedUserModel";
import { HttpServices } from "../FunctionaityProviders/http.services/http.service";
import STRINGS from "../config/strings/system.strings";
import AsyncStorageProvider from "../FunctionaityProviders/AsyncStorageprovider";
import { AppModel } from "../models/AppModel";
import { ViewsEnum, MenuStylesEnum } from "../models/MenuStyleEnum";

export class AuthService {

    public static userLoggedData: LoggedUserModel;
    public static appData: AppModel;
    public static firebaseToken: string;

    public static API_URL = {
        //SERVER: 'http://ec2-3-16-157-87.us-east-2.compute.amazonaws.com:3000',
        SERVER: 'http://192.168.1.184:3000',
        //SERVER: 'http://10.1.23.25:3000',
        API: '/api/v1',
        ACCESS: {
            PATH: '/access',
            LOGIN: '/login',
            CHECK_TOKEN: '/check-app-token',
            CHECK_APP_VERSION: '/check-app-version',
            CHECK_APP_INFO: '/app-info'
        },
        USERS: {
            PATH: '/usuarios',
            FINDBYID: '/find/',
            CREAR_ASOCIACIONES: '/createAndAsocciate',
            FIND_ALL: '/',
            FIND_ALL_ACTIVE: '/getAllActive',
            FIND_SEARCH: '/search',
            DELETE: '/:id',
            SAVE_DEVICE_INFO: '/saveDeviceInfo/:usuario',
            UPLOAD_PHOTO: '/uploadProfileImage/:usuario'
        },
        ACTIVIDADES: {
            PATH: '/actividades',
            SAVE: '/',
            SAVEALL: '/createAll',
            DELETE: '/:id',
            SEARCH: '/export',
            DASHBOARD: '/dashboard',
            AUTOCOMPLETE: '/autocomplete/:usuario'
        },
        EMPRESAS: {
            PATH: '/empresas',
            SAVE: '/',
            FIND_ACTIVE: '/getAllActive',
            FIND_ALL: '/',
            FIND_SEARCH: '/search',
            DELETE: '/:id'
        },
        PROYECTOS: {
            PATH: '/proyectos',
            SAVE: '/',
            FIND_ACTIVE: '/getAllActive',
            FIND_ALL: '/',
            FIND_SEARCH: '/search',
            DELETE: '/:id'
        },
        POSITIONS: {
            PATH: '/positions',
            SAVE: '/',
            FIND_ACTIVE: '/getAllActive',
            FIND_ALL: '/',
            FIND_SEARCH: '/search',
            DELETE: '/:id'
        },
        POSTS: {
            PATH: '/social/posts',
            FIND: '/',
            FIND_POST_DETAIL: '/:postUuid',
            COMMENTS: '/:postUuid/comments',
            SAVE_COMMENT: '/:postUuid/comment',
            DELETE_COMMENT: '/:postUuid/:commentUuid',
            SAVE_POST: '/',
            DELETE_POST: '/:postUuid',
            LIKE_POST: '/:postUuid/like',
            UNLIKE_POST: '/:postUuid/like',
            LIKE_COMMENT: '/:postUuid/:commentUuid/like',
            UNLIKE_COMMENT: '/:postUuid/:commentUuid/like',
            USER_POSTS:'/user/:userUUid'
        },
        CONNECTIONS: {
            PATH: '/social/connections',
            FIND_ALL_GROUPS: '/:userUuid/groups',
            FIND_ALL_PEOPLE: '/:userUuid/people',
            FIND_GROUP_MEMBERS: '/:groupUuid/members',
            SAVE_GROUP: '/groups',
            EDIT_GROUP: '/groups/:groupUuid',
            DELETE_GROUP: '/groups/:groupUuid',
        },
        getUrl: (route) => {
            return route;
        }
    };


    async loginUser(loginResponse: LoggedUserModel = null): Promise<any> {

        if (!loginResponse) return Promise.resolve({});

        let proyectos = (loginResponse.userProyectos || []).map(pro => ({ id: pro.proyecto_id, descripcion: pro.descripcion }))
        let empresas = (loginResponse.userEmpresas || []).map(emp => ({ id: emp.empresa_id, descripcion: emp.descripcion }));
        loginResponse.userProyectos = proyectos;
        loginResponse.userEmpresas = empresas;

        let rolesCode = (loginResponse.userRoles || []).map(rol => rol.codigo);
        loginResponse.userRolesCode = rolesCode;


        let model = new LoggedUserModel;
        model = loginResponse;

        if (!model.userMenuStyle) {
            model.userMenuStyle = 'DrawerNavigator';
        }
        await AuthService.saveUserToStorage(model);
        AuthService.userLoggedData = model;
        HttpServices.setHeader({
            'Authorization': 'Bearer ' + model.userToken,
            'X-RFTK': model.RfTkn
        })


        return Promise.resolve(model);
    }

    static async  saveUserToStorage(userData: LoggedUserModel) {
        let stringUsr = JSON.stringify(userData);
        return AsyncStorageProvider.setItem(STRINGS.KeyUserData, stringUsr);
    }



    async userLoggedData(): Promise<LoggedUserModel> {
        let userData = AuthService.userLoggedData;
        if (!userData) {
            let userDataString = await AsyncStorageProvider.getItem(STRINGS.KeyUserData);
            if (!userDataString) return null;
            userData = AuthService.userLoggedData = JSON.parse(userDataString);

        }
        return userData;
    }

    static async userLoggedDatas(): Promise<LoggedUserModel> {
        let userData = AuthService.userLoggedData;
        if (!userData) {
            let userDataString = await AsyncStorageProvider.getItem(STRINGS.KeyUserData);
            if (!userDataString) return null;
            userData = AuthService.userLoggedData = JSON.parse(userDataString);

        }
        return userData;
    }

    async isLoggedUser(): Promise<boolean> {
        let userData = await this.userLoggedData();
        return userData != null && userData.userToken != null;

    }

    async isAdmin(): Promise<boolean> {
        let isLogged = await this.isLoggedUser();
        let userData = await this.userLoggedData();
        return isLogged && userData.roles.indexOf('ADM') !== -1
    }


    async checkLogged(navigator): Promise<LoggedUserModel> {
        let isLogged = await this.isLoggedUser();

        return new Promise((succ, fail) => {
            try {

                if (!isLogged) {
                    navigator.replace('Home');
                    fail(isLogged);
                }

                succ(this.userLoggedData());

            } catch (e) {
                fail(e)
            }
        });

    }

    static shouldHideView(view: ViewsEnum) {
        if (!AuthService.userLoggedData) return false;
        if (!AuthService.userLoggedData.userExcludedViews) return false;

        return AuthService.userLoggedData.userExcludedViews
            .some(viewExcluded => viewExcluded === view);
    }

    static userLackOfViews() {

        if (!AuthService.userLoggedData) return false;
        if (!(AuthService.userLoggedData.userExcludedViews || []).length) return false;

        let viewsEnum = [
            ViewsEnum.ListActivity.toString(),
            ViewsEnum.Profile.toString(),
            ViewsEnum.DashBoard.toString(),
        ];

        const { userLoggedData: { userMenuStyle } } = AuthService;

        if (userMenuStyle === MenuStylesEnum.BottomTab) {
            viewsEnum.push(
                ViewsEnum.Social.toString(),
                ViewsEnum.Parameters.toString()
            )
        } else {
            viewsEnum.push(
                ViewsEnum.ListCompany.toString(),
                ViewsEnum.ListUser.toString(),
                ViewsEnum.ListProject.toString(),
            )
        }

        return viewsEnum.
            every(view => AuthService.userLoggedData.userExcludedViews.some(ve => ve === view) != null)

    }


    static async logOut() {
        AuthService.userLoggedData = null;
        HttpServices.removeHeader('Authorization');
        HttpServices.removeHeader('X-RFTK');
        return await AsyncStorageProvider.removeItem("usrDta");
    }





    ////////////// APP


    static async saveAppInfo(data) {

        let app_id = data.app_id;
        let app_name = data.app_name;
        let app_descripcion = data.app_descripcion;
        let app_version = data.app_version;
        let main_color = data.app_main_color;

        let appModel: AppModel = new AppModel;
        appModel.appKey = app_id;
        appModel.appName = app_name;
        appModel.appDescripcion = app_descripcion;
        appModel.appVersion = app_version;
        appModel.appMainColor = main_color;

        AuthService.appData = appModel;

        AuthService.saveAppToStorage(appModel);

        return Promise.resolve(appModel);
    }

    static async  saveAppToStorage(appData: AppModel) {
        let appStr = JSON.stringify(appData);
        return AsyncStorageProvider.setItem(STRINGS.KeyAppData, appStr);
    }

    static async getAppData(): Promise<AppModel> {
        let _appData = AuthService.appData;
        if (!_appData) {
            let apprDataString = await AsyncStorageProvider.getItem(STRINGS.KeyAppData);
            if (!apprDataString) return null;
            _appData = AuthService.appData = JSON.parse(apprDataString);
        }
        return _appData;
    }

    static getsecondaryColors() {
        let data = AuthService.appData || {
            appMainColor: '#003B5E'
        };

        return data;
    }


    ///// FIREBASE


    static getDeviceFirebaseMessagingToken = async () => {
        return AsyncStorageProvider.getItem(STRINGS.cloudMessaging.TokenStorage);
    }

    static setDeviceFirebaseMessagingToken = (token) => {
        AsyncStorageProvider.setItem(STRINGS.cloudMessaging.TokenStorage, token);
        AuthService.firebaseToken = token;
    }



}

