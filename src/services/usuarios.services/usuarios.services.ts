import { AuthService } from "../AuthService";
import LoggedUserModel from "../../models/loggedUserModel";
import { HttpServices } from "../../FunctionaityProviders/http.services/http.service";
import axios from "axios";

export default class UsuariosServices {

    public authService: AuthService;
    private userData: LoggedUserModel;
    private httpservice: HttpServices;

    static apiInstance = axios.create({
        baseURL: AuthService.API_URL.SERVER + AuthService.API_URL.API + AuthService.API_URL.USERS.PATH,
        timeout:60000,
        responseType: 'json'
    })

    constructor() {
        this.authService = new AuthService();
        this.userData = AuthService.userLoggedData;
        this.httpservice = new HttpServices(UsuariosServices.apiInstance);
    }

    public findUserData(usuarioId: Number): Promise<any> {

        return this.httpservice.get(AuthService.API_URL.getUrl(AuthService.API_URL.USERS.FINDBYID) + usuarioId)
            .then((resp) => resp.data);
    }

    public crear(data): Promise<any> {
        return this.httpservice.post(AuthService.API_URL.getUrl(AuthService.API_URL.USERS.CREAR_ASOCIACIONES),
            data
        ).then((resp) => resp.data);
    }

    public search(data): Promise<any> {
        return this.httpservice.post(AuthService.API_URL.getUrl(AuthService.API_URL.USERS.FIND_SEARCH),
            data
        ).then(res => res.data)
    }


    public delete(id): Promise<any> {
        return this.httpservice.delete(AuthService.API_URL.getUrl(AuthService.API_URL.USERS.DELETE.replace(':id', id)))
            .then(res => res.data)
    }

    public saveDeviceInfo(user, data): Promise<any> {
        return this.httpservice.post(AuthService.API_URL.getUrl(AuthService.API_URL.USERS.SAVE_DEVICE_INFO.replace(':usuario', user)),
            data)
            .then(res => res.data)
    }



    public uploadPhoto(user, image): Promise<any> {

        return this.httpservice.post(AuthService.API_URL.getUrl(AuthService.API_URL.USERS.UPLOAD_PHOTO.replace(':usuario', user)),
            image,
            {
                headers: { 'Content-Type': 'multipart/form-data' }
            })
            .then(resp => resp.data);
    }

    public findAllActive(): Promise<any> {
        return this.httpservice.get(AuthService.API_URL.getUrl(AuthService.API_URL.USERS.FIND_ALL_ACTIVE)).then(res => res.data)
    }


    //ACCESS

    public login(data): Promise<any> {
        return axios.post(
            AuthService.API_URL.SERVER + AuthService.API_URL.API + AuthService.API_URL.ACCESS.PATH +
            AuthService.API_URL.ACCESS.LOGIN,
            data,
            {
                headers: HttpServices.getHeaders(),
                responseType: 'json',
            }
        ).then(resp => resp.data);
    }


    public checkAppInfo(): Promise<any> {
        return axios.get(
            AuthService.API_URL.SERVER + AuthService.API_URL.API + AuthService.API_URL.ACCESS.PATH +
            AuthService.API_URL.ACCESS.CHECK_APP_INFO,
            {
                responseType: 'json',
                headers: HttpServices.getHeaders()
            }
        )
            .then(resp => resp.data);
    }

    public checkToken(token): Promise<any> {
        return axios.post(
            AuthService.API_URL.SERVER + AuthService.API_URL.API + AuthService.API_URL.ACCESS.PATH +
            AuthService.API_URL.ACCESS.CHECK_TOKEN,
            {},
            {
                headers: HttpServices.getHeaders(),
                responseType: 'json'
            })
            .then(resp => resp.data);
    }


}