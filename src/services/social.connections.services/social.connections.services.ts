import { AuthService } from "../AuthService";
import LoggedUserModel from "../../models/loggedUserModel";
import { HttpServices } from "../../FunctionaityProviders/http.services/http.service";
import axios from "axios";

export default class SocialConnectionsServices {
    public authService: AuthService;
    private httpservice: HttpServices;

    static apiInstance = axios.create({
        baseURL: AuthService.API_URL.SERVER + AuthService.API_URL.API + AuthService.API_URL.CONNECTIONS.PATH,
        timeout: 60000,
        responseType: 'json'
    })

    constructor() {
        this.authService = new AuthService();
        this.httpservice = new HttpServices(SocialConnectionsServices.apiInstance);
    }

    findUserPeople(userUuid, data) {
        return this.httpservice.get(AuthService.API_URL.CONNECTIONS.FIND_ALL_PEOPLE.replace(':userUuid', userUuid),
            {
                params: data
            })
            .then(res => res.data);
    }

    findUserGroups(userUuid, data) {
        return this.httpservice.get(AuthService.API_URL.CONNECTIONS.FIND_ALL_GROUPS.replace(':userUuid', userUuid),
            {
                params: data
            })
            .then(res => res.data);
    }


    findGroupMembers(groupUuid) {
        return this.httpservice.get(AuthService.API_URL.CONNECTIONS.FIND_GROUP_MEMBERS
            .replace(':groupUuid', groupUuid))
            .then(res => res.data);
    }

    saveGroup(data) {
        return this.httpservice.post(AuthService.API_URL.CONNECTIONS.SAVE_GROUP,
            data,
            {
                headers: { 'Content-Type': 'multipart/form-data' }
            })
            .then(res => res.data);
    }

    updateGroup(groupUuid, data) {
        return this.httpservice.put(AuthService.API_URL.CONNECTIONS.EDIT_GROUP
            .replace(':groupUuid', groupUuid),
            data,
            {
                headers: { 'Content-Type': 'multipart/form-data' }
            })
            .then(res => res.data);
    }

    deleteGroup(groupUuid) {
        return this.httpservice.delete(AuthService.API_URL.CONNECTIONS.DELETE_GROUP
            .replace(':groupUuid', groupUuid))
            .then(res => res.data);
    }



}