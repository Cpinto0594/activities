import { createDrawerNavigator } from 'react-navigation-drawer';
import DrawerCustomContent from "./drawer.custom.view";
import { SocialButtomTabsStack, ActivityTabsNavigator, AdminTabsNavigator, PlannerTabsNavigator } from "./RoutesBottomTab";


const DrawerNavigatorContainerFn = () => {


    const DrawerNavigator = createDrawerNavigator({
        SocialStack: SocialButtomTabsStack,
        ActivityStack: ActivityTabsNavigator,
        AdminStack: AdminTabsNavigator,
        PlannerStack: PlannerTabsNavigator
    }, {
        contentComponent: DrawerCustomContent
    });



    return DrawerNavigator;

};

export default DrawerNavigatorContainerFn();