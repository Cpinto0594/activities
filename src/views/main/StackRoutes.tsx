import { createStackNavigator } from "react-navigation-stack";
import { SocialFeedStack, ConnectionStack, ChatStack, SocialProfileStack } from "./RoutesDefinitions";

export const SocialNetworkStack = createStackNavigator({
    Feed: {
        screen: SocialFeedStack,
        navigationOptions: {
            title: 'Feed'
        }
    },
    Connections: {
        screen: ConnectionStack,
        navigationOptions: {
            title: 'Connections'
        }
    },
    Chat: {
        screen: ChatStack,
        navigationOptions: {
            title: 'Chat'
        }
    },
    Profile: {
        screen: SocialProfileStack,
        navigationOptions: {
            title: 'Profile'
        }
    },
}, {
    initialRouteName: 'Feed',
    // defaultNavigationOptions: ({ navigation }) => {
    //     return {
    //         header: null
    //     }
    // }
});