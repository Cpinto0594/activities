import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import Login from "../security/Login";
import App from "../security/App";
import { DefaultScreen } from "../screens/default.screen/default.screen";
import { MainBottomNavigator } from "./navigation/BottomMainNavigation";
import { SocialCommentsScreen } from "../screens/social.screens/social.comments";
import { SocialChatViewScreen } from "../screens/social.screens/social.chat.view.screen";



const MainNavigator = createStackNavigator(
    {
        Default: {
            screen: DefaultScreen,
            navigationOptions: {
                header: null
            }
        },
        Start: App,
        Login: Login,

        ModulesDrawer: {
            screen: MainBottomNavigator,
            navigationOptions: {
                header: null
            }
        },
        Comments: {
            screen: SocialCommentsScreen,
            navigationOptions: {
                title: 'Comentarios'
            }
        },
        ChatView: {
            screen: SocialChatViewScreen,
            navigationOptions: {
                title: 'Mensajes',
                tabBarVisible: false,
            }
        }
    }, 
    {
        initialRouteName: "Start",
        mode:'modal'
    });


const Routes = createAppContainer(MainNavigator);

export default Routes;