import React from 'react';
import { TouchableOpacity } from "react-native";
import FAICon from 'react-native-vector-icons/FontAwesome';
import { COLORS } from "../../config/colors/colors";
import { DrawerActions, NavigationActions } from 'react-navigation';
import { MenuStylesEnum, ViewsEnum } from '../../models/MenuStyleEnum';
import { AuthService } from '../../services/AuthService';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { containerStyles } from '../../utils/Styles';


export const HeaderButtons = (props) => {
    return (
        <TouchableHighlight
            underlayColor={'rgba(200,200,200,0.5)'}
            activeOpacity={0.8}
            style={[{
                width: 50,
                alignItems: 'center',
                //paddingHorizontal: containerStyles.marginHorizontal,
                paddingHorizontal: 5,
                //paddingVertical: 10,
                height: 100 + '%',
                justifyContent: 'center',
                ...{
                    ...props.disabled ? {
                        opacity: 0.6
                    } : null
                }
            },
            props.style]}
            disabled={props.disabled}
            onPress={props.action}>
            {
                props.children
            }
        </TouchableHighlight>
    )
}

const OpenDrawerButton = (navigation) => {
    return (
        <HeaderButtons
            action={() => { navigation.dispatch(DrawerActions.toggleDrawer()) }}>
            <FAICon name="navicon" size={20} color={COLORS.secondaryColor}></FAICon>
        </HeaderButtons>
    )
}
const NewButton = (newView, navigation) => {
    return (
        <HeaderButtons
            action={() => { navigation.navigate(newView) }}>
            <FAICon name="plus" size={20} color={COLORS.secondaryColor}></FAICon>
        </HeaderButtons>
    )
}
const BackButton = (navigation, color = COLORS.secondaryColor) => {
    return (
        <HeaderButtons
            action={() => {
                navigation.dispatch(NavigationActions.back())
            }}>
            <FAICon name="arrow-left" size={20} color={color}></FAICon>
        </HeaderButtons>
    )
}

const headerOptions = {
    headerForceInset: { 'top': 'never' },
    headerStyle: {
        height: 50,
        backgroundColor: COLORS.white,
        shadowOpacity: 0,
        shadowOffset: {
            height: 0,
        },
        shadowRadius: 0,
        elevation: 0
    },
    headerTitleStyle: {
        fontSize: 18
    },
    headerTintColor: COLORS.secondaryColor,
}

const actividadesNavigationButtons = (menustyle, navigation) => {

    let rightButton;
    let leftButton;

    //Validamos las vistas a excluir
    //let shouldShowCreateButton = AuthService.shouldHideView(ViewsEnum.CreateActivity);

    leftButton = BackButton(navigation);
    //rightButton = !shouldShowCreateButton ? NewButton('NuevaActividad', navigation) : null;

    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const dashBoardNavigationButtons = (menustyle, navigation) => {
    let leftButton;
    let rightButton;

    leftButton = BackButton(navigation);
    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const proyectosNavigationButtons = (menustyle, navigation) => {
    let leftButton;
    let rightButton;

    //Validamos las vistas a excluir
    //let shouldShowCreateButton = AuthService.shouldHideView(ViewsEnum.CreateProject);

    leftButton = BackButton(navigation);
    //rightButton = !shouldShowCreateButton ? NewButton('NuevoProyecto', navigation) : null;

    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const empresasNavigationButtons = (menustyle, navigation) => {
    let leftButton;
    let rightButton;

    //Validamos las vistas a excluir
    //let shouldShowCreateButton = AuthService.shouldHideView(ViewsEnum.CreateCompany);

    leftButton = BackButton(navigation);
    //rightButton = !shouldShowCreateButton ? NewButton('NuevaEmpresa', navigation) : null;

    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const usuariosNavigationButtons = (menustyle, navigation) => {
    let leftButton;
    let rightButton;

    // let shouldShowCreateButton = AuthService.shouldHideView(ViewsEnum.CreateUser);

    leftButton = BackButton(navigation);
    //rightButton = !shouldShowCreateButton ? NewButton('NuevoUsuario', navigation) : null;

    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const positionsNavigationButtons = (menustyle, navigation) => {
    let leftButton;
    let rightButton;

    //Validamos las vistas a excluir
    //let shouldShowCreateButton = AuthService.shouldHideView(ViewsEnum.CreateProject);

    leftButton = BackButton(navigation);
    //rightButton = !shouldShowCreateButton ? NewButton('NuevoProyecto', navigation) : null;

    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const perflNavigationButtons = (menustyle, navigation) => {
    let leftButton;
    let rightButton;

    switch (menustyle) {
        case MenuStylesEnum.Drawer:
            leftButton = OpenDrawerButton(navigation)
            break;
    }

    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}


const commentsNavigationButtons = (menustyle, navigation) => {
    let leftButton;
    let rightButton;

    leftButton = BackButton(navigation)
    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const newPostNavigationButtons = (menustyle, navigation) => {
    let leftButton;
    let rightButton;


    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const gelleryEditNavigationButtons = (menustyle, navigation) => {
    let leftButton;
    let rightButton;

    leftButton = BackButton(navigation, COLORS.secondaryColor)

    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const connetionsNavigationOptions = (menustyle, navigation) => {
    let leftButton;
    let rightButton;
    leftButton = BackButton(navigation, COLORS.secondaryColor)

    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const chatNavigationOptions = (menustyle, navigation) => {
    let leftButton;
    let rightButton;
    //leftButton = BackButton(navigation, COLORS.secondaryColor)
    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const chatViewNavigationOptions = (menustyle, navigation) => {
    let leftButton;
    let rightButton;
    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}

const userProfileNavigationOptions = (menustyle, navigation) => {
    let leftButton;
    let rightButton;

    leftButton = BackButton(navigation, COLORS.secondaryColor)
    return {
        ...headerOptions,
        headerLeft: leftButton,
        headerRight: rightButton,
    }
}


const ViewsNavigation = {
    ActividadesNavigationButtons: actividadesNavigationButtons,
    DashBoardNavigationButtons: dashBoardNavigationButtons,
    ProyectosNavigationButtons: proyectosNavigationButtons,
    EmpresasNavigationButtons: empresasNavigationButtons,
    PositionsNavigationButtons: positionsNavigationButtons,
    UsuariosNavigationButtons: usuariosNavigationButtons,
    PerflNavigationButtons: perflNavigationButtons,
    CommentsNavigationButtons: commentsNavigationButtons,
    NewPostNavigationButtons: newPostNavigationButtons,
    GelleryEditNavigationButtons: gelleryEditNavigationButtons,
    ConnetionsNavigationOptions: connetionsNavigationOptions,
    ChatNavigationOptions: chatNavigationOptions,
    ChatViewNavigationOptions: chatViewNavigationOptions,
    UserProfileNavigationOptions: userProfileNavigationOptions

}
export default ViewsNavigation;