import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { COLORS } from '../../config/colors/colors';

export default class Separator extends React.Component {



    constructor(props) {
        super(props);
    }



    render() {
        const { styles, color, strength } = this.props;
        return (

            <View style={[
                defaultStyles.separator,
                {
                    borderBottomColor: color,
                    borderBottomWidth: strength
                },
                styles
            ]}>
            </View>

        )

    }

    static propTypes = {
        styles: PropTypes.object,
        color: PropTypes.string,
        strength: PropTypes.number
    }

    static defaultProps = {
        color: COLORS.midGrey,
        strength: 0.3
    }

}
const defaultStyles = StyleSheet.create({

    separator: {
        marginBottom: 5,
        marginTop: 5
    }

})