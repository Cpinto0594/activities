import React from "react";
import { createBottomTabNavigator } from "react-navigation-tabs";

import { COLORS } from "../../config/colors/colors";
import { View, Animated, TouchableOpacity } from "react-native";
import TextView from "../../components/TextView/TextView";
import { AuthService } from "../../services/AuthService";
import Utils from "../../utils/Utils";
import { SimpleLineIconsEnum, IonIconEnum, FaIconsEnum } from "../../utils/IconsEnum";
import { boxShadowBottomTabs } from "../../utils/Styles";
import { SimpleLineIcons, IonIcons, FaIcons } from "../../utils/Icons";
//import { FeedsTopBarNavigation, ActivityTopBarNavigation, AdminTopBarNavigation, PlannerTopBarNavigation } from "./TopMaterialTabs";
import { SocialFeedStack, ConnectionStack, ChatStack, SocialProfileStack, ActivityStack, UsersStack, ProjectsStack, CompanyStack, PositionsStack } from "./RoutesDefinitions";
import STRINGS_PERFIL from "../../config/strings/perfil.string";
import { USUARIOS } from "../../config/strings/usuarios.strings";
import { PROYECTOS } from "../../config/strings/proyectos.strings";
import { EMPRESAS } from "../../config/strings/empresas.strings";
import { CARGOS } from "../../config/strings/cargos.strings";

export const SocialBottomTabsStack = createBottomTabNavigator({
    Feed: {
        screen: SocialFeedStack,
        navigationOptions: {
            header: null,
            title: 'Feed',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.FEED, 20, tintColor)
            )
        },
        tabBarVisible: false
    },
    Connections: {
        screen: ConnectionStack,
        navigationOptions: {
            title: 'Conexiones',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.PEOPLE, 20, tintColor)
            )
        }
    },
    Chat: {
        screen: ChatStack,
        navigationOptions: {
            title: 'Chat',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.BUBBLES, 20, tintColor)
            )
        },
    },
    Profile: {
        screen: SocialProfileStack,
        navigationOptions: {
            title: 'Profile',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.USER, 20, tintColor)
            )
        }
    }
}, {
    swipeEnabled: true,
    animationEnabled: true,
    defaultNavigationOptions: ({ navigation }) => ({
        headerForceInset: { 'top': 'never' },
        header: null
    }),
    tabBarComponent: (props) => {
        const {
            navigation,
            navigation: { state: { index, routes } },
            activeTintColor,
            inactiveTintColor,
            renderIcon } = props;
        return (<View style={{
            flexDirection: 'row',
            width: '100%',
            borderColor: '#cdcdcd',
            borderWidth: 1,
            height: 50,
            justifyContent: 'center',
            alignItems: 'center',
            ...boxShadowBottomTabs.box_shadow,
            backgroundColor: COLORS.white
        }}>

            {
                routes
                    //.filter(view => !shouldHideButton(view.params.permissionValidator))
                    .map((route, idx) => {
                      //  route.animationScale = route.animationScale || new Value(0);
                        return (
                            <View
                                key={route.key}
                                style={{
                                    flex: 1,
                                    // transform: [
                                    //     {
                                    //         scale: route.animationScale.interpolate({
                                    //             inputRange: [0, 1],
                                    //             outputRange: [1, 0.8]
                                    //         })
                                    //     }
                                    // ]
                                }}
                            >
                                <TouchableOpacity
                                    // onPressIn={() => {
                                    //     timing(
                                    //         route.animationScale,
                                    //         {
                                    //             toValue: 1,
                                    //             duration: 10,
                                    //             useNativeDriver: true
                                    //         }
                                    //     ).start()
                                    // }}
                                    // onPressOut={() => {
                                    //     timing(
                                    //         route.animationScale,
                                    //         {
                                    //             toValue: 0,
                                    //             duration: 10,
                                    //             useNativeDriver: true
                                    //         }
                                    //     ).start()
                                    // }}
                                    onPress={() => {
                                        Utils.defaultNavigation(navigation, route.key)
                                        //jumpTo(route.key)
                                    }}
                                    style={{
                                        height: '100%',
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}
                                >

                                    {renderIcon({
                                        route,
                                        focused: index === idx,
                                        tintColor: index === idx ? activeTintColor : inactiveTintColor
                                    })}
                                    {
                                        index === idx &&
                                        <TextView textValue={route.routeName} styles={{
                                            fontSize: 12
                                        }} />
                                    }

                                </TouchableOpacity>
                            </View>
                        )
                    })
            }

        </View>)
    },
    tabBarOptions: {
        activeTintColor: COLORS.secondaryColor,
        inactiveTintColor: COLORS.secondaryColor,
        showLabel: false,
    },

});






export const ActivityControlBottomStack = createBottomTabNavigator({
    DashBoard: {
        screen: DeskTopStack,
        navigationOptions: {
            header: null,
            title: 'Dashboard',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.CHART, 20, tintColor)
            )
        },
        tabBarVisible: false
    },
    ActivityControl: {
        screen: ActivityStack,
        navigationOptions: {
            title: 'Actividades',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.LAYERS, 20, tintColor)
            )
        }
    }
}, {
    initialRouteName: 'DashBoard',
    swipeEnabled: true,
    animationEnabled: true,
    defaultNavigationOptions: ({ navigation }) => ({
        headerForceInset: { 'top': 'never' },
        header: null
    }),
    tabBarComponent: (props) => {
        const {
            navigation,
            navigation: { state: { index, routes } },
            activeTintColor,
            inactiveTintColor,
            renderIcon } = props;
        return (<View style={{
            flexDirection: 'row',
            width: '100%',
            borderColor: '#cdcdcd',
            borderWidth: 1,
            height: 50,
            justifyContent: 'center',
            alignItems: 'center',
            ...boxShadowBottomTabs.box_shadow,
            backgroundColor: COLORS.white
        }}>

            {
                routes
                    //.filter(view => !shouldHideButton(view.params.permissionValidator))
                    .map((route, idx) => {
                        //route.animationScale = route.animationScale || new Value(0);
                        return (
                            <View
                                key={route.key}
                                style={{
                                    flex: 1,
                                    
                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => {
                                        Utils.defaultNavigation(navigation, route.key)
                                        //jumpTo(route.key)
                                    }}
                                    style={{
                                        height: '100%',
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}
                                >

                                    {renderIcon({
                                        route,
                                        focused: index === idx,
                                        tintColor: index === idx ? activeTintColor : inactiveTintColor
                                    })}
                                    {
                                        index === idx &&
                                        <TextView textValue={route.routeName} styles={{
                                            fontSize: 12
                                        }} />
                                    }

                                </TouchableOpacity>
                            </View>
                        )
                    })
            }

        </View>)
    },
    tabBarOptions: {
        activeTintColor: COLORS.secondaryColor,
        inactiveTintColor: COLORS.secondaryColor,
        showLabel: false,
    },

});


export const AdminBottomStack = createBottomTabNavigator({
    Profile: {
        screen: ProfileStack,
        navigationOptions: {
            title: STRINGS_PERFIL.Title,
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.USER, 20, tintColor)
            )
        },
        params: {
            permissionValidator: 'Profile',
            icon: (iconSize, tintColor) => FaIcons.getIcon(FaIconsEnum.USER_CIRCLE_EMPTY, iconSize, tintColor),
            title: STRINGS_PERFIL.Title,
        }
    },
    Users: {
        screen: UsersStack,
        navigationOptions: {
            title: USUARIOS.usuarioTitle,
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.PEOPLE, 20, tintColor)
            )
        },
        params: {
            permissionValidator: 'Users',
            icon: (iconSize, tintColor) => SimpleLineIcons.getIcon(SimpleLineIconsEnum.PEOPLE, iconSize, tintColor),
            title: USUARIOS.usuarioTitle
        }
    },
    Projects: {
        screen: ProjectsStack,
        navigationOptions: {
            title: PROYECTOS.ProyectoTitle,
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.PROJECTS, 20, tintColor)
            )
        },
        params: {
            permissionValidator: 'Projects',
            icon: (iconSize, tintColor) => SimpleLineIcons.getIcon(SimpleLineIconsEnum.PROJECTS, iconSize, tintColor),
            title: PROYECTOS.ProyectoTitle,
        }
    },
    Company: {
        screen: CompanyStack,
        navigationOptions: {
            title: EMPRESAS.EmpresaTitle,
            tabBarIcon: ({ tintColor }) => (
                IonIcons.getIcon(IonIconEnum.COMPANY, 20, tintColor)
            )
        },
        params: {
            permissionValidator: 'Company',
            icon: (iconSize, tintColor) => IonIcons.getIcon(IonIconEnum.COMPANY, iconSize, tintColor),
            title: EMPRESAS.EmpresaTitle,
        }
    },
    Charges: {
        screen: PositionsStack,
        navigationOptions: {
            title: CARGOS.CargoTitle,
            tabBarIcon: ({ tintColor }) => (
                IonIcons.getIcon(IonIconEnum.TARGET, 20, tintColor)
            )
        },
        params: {
            permissionValidator: 'Charge',
            icon: (iconSize, tintColor) => IonIcons.getIcon(IonIconEnum.TARGET, iconSize, tintColor),
            title: CARGOS.CargoTitle,
        }
    },
}, {
    initialRouteName: 'Profile',
    swipeEnabled: true,
    animationEnabled: true,
    defaultNavigationOptions: ({ navigation }) => ({
        headerForceInset: { 'top': 'never' },
        header: null
    }),
    tabBarComponent: (props) => {
        const {
            navigation,
            navigation: { state: { index, routes } },
            activeTintColor,
            inactiveTintColor,
            renderIcon } = props;
        return (<View style={{
            flexDirection: 'row',
            width: '100%',
            borderColor: '#cdcdcd',
            borderWidth: 1,
            height: 50,
            justifyContent: 'center',
            alignItems: 'center',
            ...boxShadowBottomTabs.box_shadow,
            backgroundColor: COLORS.white
        }}>

            {
                routes
                    //.filter(view => !shouldHideButton(view.params.permissionValidator))
                    .map((route, idx) => {
                        //route.animationScale = route.animationScale || new Value(0);
                        return (
                            <View
                                key={route.key}
                                style={{
                                    flex: 1,
                                    
                                }}
                            >
                                <TouchableOpacity
                                    
                                    onPress={() => {
                                        Utils.defaultNavigation(navigation, route.key)
                                        //jumpTo(route.key)
                                    }}
                                    style={{
                                        height: '100%',
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}
                                >

                                    {renderIcon({
                                        route,
                                        focused: index === idx,
                                        tintColor: index === idx ? activeTintColor : inactiveTintColor
                                    })}
                                    {
                                        index === idx &&
                                        <TextView textValue={route.routeName} styles={{
                                            fontSize: 12
                                        }} />
                                    }

                                </TouchableOpacity>
                            </View>
                        )
                    })
            }

        </View>)
    },
    tabBarOptions: {
        activeTintColor: COLORS.secondaryColor,
        inactiveTintColor: COLORS.secondaryColor,
        showLabel: false,
    },

});


const shouldHideButton = (name) => {
    return name === 'Default' || AuthService.shouldHideView(name);
}