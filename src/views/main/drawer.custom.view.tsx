import React from 'react';
import { View, StyleSheet, ImageBackground, Image } from 'react-native';
import { ScrollView } from 'react-navigation';
import { COLORS } from '../../config/colors/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import IconFa from 'react-native-vector-icons/FontAwesome';
import TextView from '../../components/TextView/TextView';
import { TouchableOpacity } from 'react-native-gesture-handler';
import STRINGS_LOGIN from '../../config/strings/login.strings';
import { AuthService } from '../../services/AuthService';
import Separator from './separator.view';
import Utils from '../../utils/Utils';
import { boxShadow } from '../../utils/Styles';
import { FaIconsEnum, SimpleLineIconsEnum } from '../../utils/IconsEnum';
import { FaIcons, SimpleLineIcons } from '../../utils/Icons';
import NotificationServices from '../../services/NotificationsServices';


export default class DrawerCustomContent extends React.Component {

    private notificationServices: NotificationServices;

    constructor(props) {
        super(props);
        this.notificationServices = new NotificationServices;

    }

    private itemMenu = [
        {
            text: 'Feed',
            icon: SimpleLineIcons.getIcon(SimpleLineIconsEnum.FEED, 24, COLORS.secondaryColor),
            action: 'SocialStack', permissionValidator: 'DashBoard'
        },
        {
            text: 'Registro Actividades',
            icon: SimpleLineIcons.getIcon(SimpleLineIconsEnum.LAYERS, 24, COLORS.secondaryColor),
            action: 'ActivityStack', permissionValidator: 'Profile'
        },
        {
            text: 'Planificador',
            icon: SimpleLineIcons.getIcon(SimpleLineIconsEnum.ORGANIZATION, 24, COLORS.secondaryColor),
            action: 'AdminStack', permissionValidator: 'ListActivity'
        },
        {
            text: 'Administración',
            icon: SimpleLineIcons.getIcon(SimpleLineIconsEnum.SETTINGS, 24, COLORS.secondaryColor),
            action: 'AdminStack', permissionValidator: 'ListActivity'
        }
    ]


    state = {
        itemMenus: []
    }


    componentDidMount() {
        setTimeout(() => {
            this.setState({
                itemMenus: this.itemMenu
            })
        }, 200)
    }

    render() {
        return (

            <View style={styles.main_container}>
                <ScrollView style={styles.scroll_container}>
                    <View style={styles.header}>

                        <ImageBackground source={require('../../../assets/social_profile_background_image.jpeg')} style={{
                            flex: 1, width: 100 + '%'
                        }} >
                            <View style={{
                                backgroundColor: 'rgba(0,0,0,0.4)',
                                flex: 1,
                                justifyContent: 'center',
                            }}>
                                <View style={styles.header_text_circle}>
                                    {AuthService.userLoggedData.userPic ?
                                        <Image source={{ uri: AuthService.userLoggedData.userPic }}
                                            style={{ width: 60, height: 60, backgroundColor: COLORS.white }} /> :
                                        <TextView styles={{
                                            color: COLORS.secondaryColor
                                        }} textValue={this._nameInitials()} />
                                    }
                                </View>
                                <TextView styles={styles.header_text} textValue={this._nameCapitalize()} />
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={{
                        padding: 10,
                        justifyContent: 'center',
                        borderBottomColor: COLORS.softGrey,
                        borderBottomWidth: 0.7,
                        backgroundColor: COLORS.secondaryColor
                    }}>
                        <TextView textValue={'Módulos Con Acceso'}
                            styles={{
                                fontSize: 15,
                                fontWeight: 'bold',
                                color: COLORS.white
                            }} />
                    </View>

                    <View >
                        {
                            this.state.itemMenus
                                .filter(menu => {
                                    return !AuthService.shouldHideView(menu.permissionValidator)
                                })
                                .map((menu, index) => {
                                    return (this._itemMenu(menu, index));
                                })
                        }
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity onPress={this._logOut.bind(this)} style={styles.footer_touchable}>
                        {
                            SimpleLineIcons.getIcon(SimpleLineIconsEnum.LOGOUT, 20, COLORS.labelDanger)
                        }
                        <TextView textValue={STRINGS_LOGIN.CerrarSesion}
                            styles={{ marginLeft: 10, marginBottom: 1,  fontSize: 16, color: COLORS.labelDanger }} />
                    </TouchableOpacity>
                </View>
            </View>

        )

    }

    _itemMenu = (item, index) => {

        return (
            item.separator ?
                <Separator key={index} /> :
                <TouchableOpacity key={index} onPress={() => { this._navigate(item) }}>
                    <View style={styles.drawer_icons_container}>
                        {
                            item.icon
                        }
                        <TextView textValue={item.text} styles={{ marginLeft: 20, marginBottom: 1, height: 24 }} />
                    </View>
                </TouchableOpacity>
        )

    }

    _nameInitials() {
        let parts: string[] = AuthService.userLoggedData.userFullName.split(" ");
        return parts[0].substring(0, 1) + parts[1].substring(0, 1);
    }

    _nameCapitalize() {
        let parts: string[] = AuthService.userLoggedData.userFullName.split(" ");
        return parts.map(part => part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase()).join(" ")
    }

    _navigate = (item) => {
        Utils.defaultNavigationReplace(this.props.navigation, item.action)
    }

    _logOut() {
        this.notificationServices.confirm('¿Desea cerrar sesion?', () => {
            AuthService.logOut()
                .then(() => {
                    Utils.navigateWithStackReset(this.props.navigation, 'Login')
                });
        })
    }


}

const styles = StyleSheet.create({

    main_container: {
        flex: 1,
        backgroundColor: COLORS.backgroundColor
    },
    header: {
        height: 150,
    },
    header_text: {
        fontSize: 15,
        position: 'absolute',
        bottom: 10,
        color: COLORS.white,
        marginLeft: 10
    },
    header_text_circle: {
        borderRadius: 50,
        ...boxShadow.box_shadow,
        height: 60,
        width: 60,
        marginLeft: 10,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: COLORS.white,
        borderWidth: 2
    },
    drawer_icons_container: {
        padding: 10,
        flexDirection: 'row'
    },
    scroll_container: {
        flex: 1,
        flexDirection: 'column'
    },
    footer: {
        alignContent: 'center',
        justifyContent: 'center',
        borderTopColor: COLORS.secondaryColor,
        borderTopWidth: 1,
        width: 100 + '%'
    },
    footer_touchable: {

        flexDirection: 'row',
        height: 50,
        paddingLeft: 10,
        alignItems: 'center'
    }

})