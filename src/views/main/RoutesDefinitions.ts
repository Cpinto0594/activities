import { createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import ActividadesScreen from "../screens/actividades.screens/actividades.screen";
import CrearActividadesScreen from "../screens/actividades.screens/actividades.create.screen";
import EmpresasScreen from "../screens/empresas.screens/empresas.screen";
import CrearEmpresasScreen from "../screens/empresas.screens/empresas.crear.screen";
import ProyectosScreen from "../screens/proyectos.screens/proyectos.screen";
import CrearProyectosScreen from "../screens/proyectos.screens/proyectos.crear.screen";
import UsuariosScreen from "../screens/usuarios.screens/usuarios.screen";
import CrearUsuariosScreen from "../screens/usuarios.screens/usuarios.crear.screen";
import { ACTIVIDADES } from '../../config/strings/actividades.strings';
import { EMPRESAS } from '../../config/strings/empresas.strings';
import { PROYECTOS } from '../../config/strings/proyectos.strings';
import { USUARIOS } from '../../config/strings/usuarios.strings';
import ProyectosServices from '../../services/proyectos.services/proyectos.services';
import EmpresasServices from '../../services/empresas.services/empresas.services';
import UsuariosServices from '../../services/usuarios.services/usuarios.services';
import { ConfigurationScreen } from '../screens/config.screen/config.screen';
import DashBoard from '../screens/dashboard/dashboard';
import PerfilScreen from '../screens/perfil.screen/perfil.screen';
import STRINGS_PERFIL from '../../config/strings/perfil.string';
import { SocialPostsScreen } from '../screens/social.screens/social.posts.screen';
import { SocialCommentsScreen } from '../screens/social.screens/social.comments';
import { PostPublisherComponent } from '../screens/social.screens/components/post.publisher';
import { PostImageGalleryDetailComponent } from '../screens/social.screens/components/post.imagegallery.detail.component';
import { fromBottom, fromLeft, fromRight } from 'react-navigation-transitions';
import { ConnectionsScreen } from "../screens/social.screens/social.connections.screen";
import { ChatListScreen } from "../screens/social.screens/social.chat.screen";
import { SocialChatViewScreen } from "../screens/social.screens/social.chat.view.screen";
import { SocialUserProfile } from "../screens/social.screens/social.user.profile";
import { CARGOS } from "../../config/strings/cargos.strings";
import { SocialGroupProfile } from "../screens/social.screens/social.group.profile";
import { SocialGroupsCreate } from "../screens/social.screens/social.groups.create";
import { SocialConnectionsSelect } from "../screens/social.screens/social.connections.select";
import PositionsServices from "../../services/positions.services/positions.services";
import PositionsScreen from "../screens/positions.screens/positions.screen";
import PositionCreateScreen from "../screens/positions.screens/positions.create.screen";
import ActivitiesExportScreen from "../screens/actividades.screens/actividades.export.screen";
import { SocialChatSelectDestination } from "../screens/social.screens/social.chat.select.dest";
import { SocialChatGroupCreation } from "../screens/social.screens/social.chat.group_create";
import { NotificationListScreen } from "../screens/notifications.screens/notification.list.screen";
import { PerfilSelectEmpresas, PerfilSelectProyectos } from "../screens/perfil.screen/perfil.select_items";





export const CompanyStack = createStackNavigator({
    ListarEmpresas: {
        screen: EmpresasScreen,
        params: {
            service: EmpresasServices,
            editRoute: 'NuevaEmpresa',
            listTitle: EMPRESAS.EmpresaTitle,
            dataNotFound: 'No se pudo obtener información de las Empresas.',
            deleteFail: 'No se pudo eliminar Empresa'
        },
        navigationOptions: {
            title: EMPRESAS.ListarTitle
        }
    },
    NuevaEmpresa: {
        screen: CrearEmpresasScreen,
        navigationOptions: {
            title: EMPRESAS.EmpresaTitle
        }
    }
}, {
    initialRouteName: 'ListarEmpresas',
});



export const ProjectsStack = createStackNavigator({
    ListarProyectos: {
        screen: ProyectosScreen,
        params: {
            service: ProyectosServices,
            listTitle: PROYECTOS.ProyectoTitle,
            editRoute: 'NuevoProyecto',
            dataNotFound: 'No se pudo obtener información de los proyectos.',
            deleteFail: 'No se pudo eliminar proyecto'
        },
        navigationOptions: {
            title: PROYECTOS.ListarTitle
        }
    },
    NuevoProyecto: {
        screen: CrearProyectosScreen,
        navigationOptions: {
            title: PROYECTOS.ProyectoTitle
        }
    }
}, {
    initialRouteName: 'ListarProyectos',
});


export const PositionsStack = createStackNavigator({
    ListarCargos: {
        screen: PositionsScreen,
        params: {
            service: PositionsServices,
            listTitle: CARGOS.ListarTitle,
            editRoute: 'NuevoCargo',
            dataNotFound: 'No se pudo obtener información de los cargos.',
            deleteFail: 'No se pudo eliminar cargo'
        },
        navigationOptions: {
            title: CARGOS.ListarTitle
        }
    },
    NuevoCargo: {
        screen: PositionCreateScreen,
        navigationOptions: {
            title: CARGOS.CargoTitle
        }
    }
}, {
    initialRouteName: 'ListarCargos',
});

export const UsersStack = createStackNavigator({
    ListarUsuarios: {
        screen: UsuariosScreen,
        navigationOptions: {
            title: USUARIOS.usuarioTitle
        },
        params: {
            service: UsuariosServices,
            listTitle: USUARIOS.usuarioTitle,
            editRoute: 'NuevoUsuario',
            dataNotFound: 'No se pudo obtener información de los usuarios.',
            deleteFail: 'No se pudo eliminar usuario'
        }
    },
    NuevoUsuario: {
        screen: CrearUsuariosScreen,
        navigationOptions: {
            title: USUARIOS.usuarioTitle
        }
    },
    PerfilListEmpresas: {
        screen: PerfilSelectEmpresas,
        navigationOptions: {
        }
    },
    PerfilListProyectos: {
        screen: PerfilSelectProyectos,
        navigationOptions: {
        }
    }
}, {
    initialRouteName: 'ListarUsuarios',
});



export const ActivityControlStack = createStackNavigator({
    ListaActividad: {
        screen: ActividadesScreen,
        navigationOptions: {
            title: ACTIVIDADES.ActividadesTitle
        }
    },
    NuevaActividad: {
        screen: CrearActividadesScreen,
        navigationOptions: {
            title: ACTIVIDADES.CrearTitle
        }
    }
}, {
    initialRouteName: 'ListaActividad',
});

export const DashBoardStack = createStackNavigator({
    Dashboard: {
        screen: DashBoard,
        navigationOptions: {
            title: 'Dashboard',
        }
    }
}, {
    initialRouteName: 'Dashboard',
});


export const ActivityExportStack = createStackNavigator({
    Export: {
        screen: ActivitiesExportScreen,
        navigationOptions: {
            title: 'Export Activities',
        }
    }
}, {
    initialRouteName: 'Export',
});




export const SocialFeedStack = createStackNavigator({
    Social: {
        screen: SocialPostsScreen,
        navigationOptions: {
            title: 'Social',
        }
    },
    NewPost: {
        screen: PostPublisherComponent,
        navigationOptions: {
            title: 'Nueva Publicación'
        }
    },
    EditGallery: {
        screen: PostImageGalleryDetailComponent,
        navigationOptions: {
            title: 'Editar'
        }
    }
}, {
    initialRouteName: 'Social',
    transitionConfig: () => fromBottom(),
    navigationOptions: ({ navigation }) => {
        let tabBarVisible = true;
        if (navigation.state.index > 0 && navigation.state.routes[1].routeName === "Comments") {
            tabBarVisible = false;
        }

        return {
            tabBarVisible,
        };
    },
});

export const ConnectionStack = createStackNavigator({
    Connections: {
        screen: ConnectionsScreen,
        navigationOptions: {
            title: 'Conexiones',
        },
    },
    UserProfile: {
        screen: SocialUserProfile,
        navigationOptions: {
            title: 'Perfil Usuario',
            header: null
        }
    },
    GroupProfile: {
        screen: SocialGroupProfile,
        navigationOptions: {
            title: 'Perfil Grupo',
            header: null
        }
    },
    GroupCreation: {
        screen: SocialGroupsCreate,
        navigationOptions: {
            title: 'Crear Grupo',
        },
    },
    SocialMemberPicker: {
        screen: SocialConnectionsSelect,
        navigationOptions: {
            title: 'Seleccionar Miembros',
        },

    }
}, {
    initialRouteName: 'Connections',
    transitionConfig: () => fromLeft(),
});

export const ChatStack = createStackNavigator({
    Chats: {
        screen: ChatListScreen,
        navigationOptions: {
            title: 'Chats',
        }
    },
    CreateChatDestinationSelect: {
        screen: SocialChatSelectDestination,
        navigationOptions: {
            title: 'Seleccionar destinatario',
        }
    },
    CreateChatConfirmation: {
        screen: SocialChatGroupCreation,
        navigationOptions: {
            title: 'Confirmación',
        }
    },
    
}, {
    initialRouteName: 'Chats',
    transitionConfig: () => fromRight(),
    navigationOptions: ({ navigation }) => {
        let tabBarVisible = true;
        if (navigation.state.index > 0 && navigation.state.routes[1].routeName === "ChatView") {
            tabBarVisible = false;
        }

        return {
            tabBarVisible,
        };
    },
    defaultNavigationOptions: () => {
        return {
            header: null,
            tabBarVisible: false
        }
    }
});

export const SocialProfileStack = createStackNavigator({
    Profile: {
        screen: SocialUserProfile,
        navigationOptions: {
            title: 'Profile',
        }
    }
}, {
    initialRouteName: 'Profile',
    defaultNavigationOptions: () => {
        return {
            header: null
        }
    },
    transitionConfig: () => fromRight(),
});

export const NotificationsStack = createStackNavigator({
    Notifications: {
        screen: NotificationListScreen,
        navigationOptions: {
            title: 'Notificaciones'
        }
    }
}, {
    initialRouteName: 'Notifications',
});

export const OtherStacks = createStackNavigator({
    Main: {
        screen: ConfigurationScreen,
        navigationOptions: {
        }
    },
    Connections: {
        screen: ConnectionStack,
        navigationOptions: {
            title: 'Conexiones',
        }

    },
    UserProfile: {
        screen: SocialProfileStack,
        navigationOptions: {
            title: 'Perfil',
        }
    },
    Companies: {
        screen: CompanyStack,
        navigationOptions: {
            title: 'Empresas',
        }
    },
    Projects: {
        screen: ProjectsStack,
        navigationOptions: {
            title: 'Proyectos',
        }
    },
    Positions: {
        screen: PositionsStack,
        navigationOptions: {
            title: 'Cargos',
        }
    },
    Users: {
        screen: UsersStack,
        navigationOptions: {
            title: 'Usuario',
        }
    },
    Dashboard: {
        screen: DashBoardStack,
        navigationOptions: {
            title: 'Dashboard',
        }
    },
    ActivitiesExport: {
        screen: ActivityExportStack,
    },


}, {
    initialRouteName: 'Main',
    defaultNavigationOptions: ({ navigation }) => {
        return {
            header: null
        }
    }
});
