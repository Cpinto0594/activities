import React from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { SocialFeedStack, ConnectionStack, ChatStack, ActivityControlStack, OtherStacks, NotificationsStack } from '../RoutesDefinitions';
import { SimpleLineIcons } from '../../../utils/Icons';
import { SimpleLineIconsEnum } from '../../../utils/IconsEnum';
import { View } from 'react-native';
import { boxShadowBottomTabs } from '../../../utils/Styles';
import { COLORS } from '../../../config/colors/colors';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Utils from '../../../utils/Utils';
import TextView from '../../../components/TextView/TextView';

export const MainBottomNavigator = createMaterialTopTabNavigator({
    Feed: {
        screen: SocialFeedStack,
        navigationOptions: {
            tabBarLabel: 'Feed',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.FEED, 20, tintColor)
            )
        },
        params: {
            label: 'Feed'
        }
    },
    Chat: {
        screen: ChatStack,
        navigationOptions: {
            tabBarLabel: 'Chat',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.BUBBLES, 20, tintColor)
            )
        },
        params: {
            label: 'Chat'
        }
    },
    ActivityControl: {
        screen: ActivityControlStack,
        navigationOptions: {
            tabBarLabel: 'Dashboard',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.LAYERS, 20, tintColor)
            )
        },
        params: {
            label: 'Dashboard'
        }
    },
    Notifications: {
        screen: NotificationsStack,
        navigationOptions: {
            tabBarLabel: 'Notificaciones',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.BELL, 20, tintColor)
            )
        },
        params: {
            label: 'Notificaciones'
        }
    },
    Others: {
        screen: OtherStacks,
        navigationOptions: {
            tabBarLabel: 'More',
            tabBarIcon: ({ tintColor }) => (
                SimpleLineIcons.getIcon(SimpleLineIconsEnum.OPTS_HORIZONTAL, 20, tintColor)
            )
        },
        params: {
            label: 'More'
        }
    },
}, {
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    animationEnabled: true,
    initialRouteName: 'Feed',
    backBehavior: 'initialRoute',
    lazy: true,
    tabBarComponent: (props) => {
        const {
            navigation,
            navigation: { state: { index, routes } },
            activeTintColor,
            inactiveTintColor,
            jumpTo,
            renderIcon } = props;
        return (
            <View style={{
                flexDirection: 'row',
                width: '100%',
                borderColor: '#cdcdcd',
                borderWidth: 0.5,
                height: 50,
                justifyContent: 'center',
                alignItems: 'center',
                //...boxShadowBottomTabs.box_shadow,
                backgroundColor: COLORS.white
            }}>

                {
                    routes
                        //.filter(view => !shouldHideButton(view.params.permissionValidator))
                        .map((route, idx) => {
                            //  route.animationScale = route.animationScale || new Value(0);
                            return (
                                <View
                                    key={route.key}
                                    style={{
                                        flex: 1,
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => {
                                            jumpTo(route.key)
                                        }}
                                        activeOpacity={1}
                                        style={{
                                            height: '100%',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                    >

                                        <View style={{
                                            padding: 10,
                                            alignItems: 'center',
                                            justifyContent: 'center',

                                            ...(
                                                index === idx ?
                                                    {
                                                        backgroundColor: 'rgba(0,0,0,0.2)',
                                                        borderRadius: 50
                                                    } :
                                                    null
                                            )
                                        }}>
                                            {renderIcon({
                                                route,
                                                focused: index === idx,
                                                tintColor: index === idx ? activeTintColor : inactiveTintColor
                                            })}
                                            {/* {
                                                index === idx &&
                                                <TextView textValue={route.params.label} styles={{
                                                    fontSize: 12
                                                }} />
                                            } */}
                                        </View>

                                    </TouchableOpacity>
                                </View>
                            )
                        })
                }

            </View>)
    },
    tabBarOptions: {
        activeTintColor: COLORS.secondaryColor,
        inactiveTintColor: COLORS.secondaryColor,
        showLabel: false,
    },

});