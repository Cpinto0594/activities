import React from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { COLORS } from '../../config/colors/colors';
import { SimpleLineIconsEnum } from '../../utils/IconsEnum';
import { SimpleLineIcons } from '../../utils/Icons';
import { AnimatedSlideHeader } from '../../components/Headers/AnimatedSlideHeader';
import { View } from 'react-native';
import { SocialBottomTabsStack, ActivityControlBottomStack, AdminBottomStack } from './RoutesBottomTab';


class SocialBottomTabsStackComponent extends React.Component {
    static router = SocialBottomTabsStack.router
    constructor(props) {
        super(props);
    }

    render() {
        const { params } = this.props.navigation.state;
        return (
            <View style={{
                flex: 1
            }}>
                <SocialBottomTabsStack navigation={this.props.navigation} screenProps={{
                    stateParams: params
                }} />
            </View>
        )
    }
}

class ActivityControlBottomTabsStackComponent extends React.Component {
    static router = ActivityControlBottomStack.router
    constructor(props) {
        super(props);
    }

    render() {
        const { params } = this.props.navigation.state;
        return (
            <View style={{
                flex: 1
            }}>
                <ActivityControlBottomStack navigation={this.props.navigation} screenProps={{
                    stateParams: params
                }} />
            </View>
        )
    }
}

class AdminBottomTabsStackComponent extends React.Component {
    static router = AdminBottomStack.router
    constructor(props) {
        super(props);
    }

    render() {
        const { params } = this.props.navigation.state;
        return (
            <View style={{
                flex: 1
            }}>
                <AdminBottomStack navigation={this.props.navigation} screenProps={{
                    stateParams: params
                }} />
            </View>
        )
    }
}

export const TopBarNavigation = createMaterialTopTabNavigator({
    Social: {
        screen: SocialBottomTabsStackComponent,
        navigationOptions: {
            title: 'Feed',
        },
        params: {
            icon: (iconSize, tintColor) => SimpleLineIcons.getIcon(SimpleLineIconsEnum.FEED, iconSize, tintColor),
            title: 'Feed',
        }
    },
    ActivityControl: {
        screen: ActivityControlBottomTabsStackComponent,
        navigationOptions: {
            title: 'ActivityControl',
        },
        params: {
            icon: (iconSize, tintColor) => SimpleLineIcons.getIcon(SimpleLineIconsEnum.LAYERS, iconSize, tintColor),
            title: 'ActivityControl',
        }
    },
    AdminControl: {
        screen: AdminBottomTabsStackComponent,
        navigationOptions: {
            title: 'Admin',
        },
        params: {
            icon: (iconSize, tintColor) => SimpleLineIcons.getIcon(SimpleLineIconsEnum.SETTINGS, iconSize, tintColor),
            title: 'Admin',
        }
    },
}, {
    initialRouteName: 'Social',
    // swipeEnabled: true,
    lazy: true,
    backBehavior: 'initialRoute',
    tabBarOptions: {
        activeTintColor: COLORS.secondaryColor,
        inactiveTintColor: COLORS.secondaryColor,
        labelColor: COLORS.white,
        showLabel: false,
        showIcon: true,
    },
    defaultNavigationOptions: () => ({
        headerForceInset: { 'top': 'never' },
    }),
    navigationOptions: () => ({
          tabBarVisible:false
    }),
    tabBarComponent: (props) => {
        const {
            navigation,
            navigation: { state: { index, routes, routeName } },
            jumpTo
        } = props;
        return (
            <AnimatedSlideHeader
                ref={(ref) => {
                    if (ref !== null)
                        routes.map(route => {
                            route.params.headerTabRef = ref
                        })
                }}
                tabButtonOptions={{
                    activeTintColor: COLORS.secondaryColor,
                    inactiveTintColor: COLORS.secondaryColor,
                }}
                initialTitle={routeName}
                icons={
                    routes
                        //.filter(view => !shouldHideButton(view.params.permissionValidator))
                        .map((route, idx) => {
                            //console.log(route.params.title, route.params.headerTabRef !== null )
                            route.params.rootNavigation = navigation;
                            route.params.headerTabRef = route.params.headerTabRef;
                            return {
                                text: route.params.title,
                                icon: route.params.icon,
                                action: route.key,
                                activated: index === idx,
                                onClick: () => {
                                    jumpTo(route.key)
                                }
                            }
                        })
                }
                navigation={navigation} />


        )
    }
});