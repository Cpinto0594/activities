import React from 'react';
import { View } from 'react-native';
import TextView from '../../../components/TextView/TextView';

export class NotificationListScreen extends React.Component {

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <TextView textValue={'Notificaciones'} />
            </View>
        )
    }

}