import React from 'react';
import { View } from 'react-native';
import { COLORS } from '../../../config/colors/colors';
import ListarScreen from '../listar.screen/listar.screen';
import TextView from '../../../components/TextView/TextView';
import ViewsNavigation, { HeaderButtons } from '../../main/navigation.config';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';


export default class PositionsScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Cargos'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                navigation.goBack(null)
                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
        }
    }

    render() {

        return (
            <ListarScreen navigation={this.props.navigation}
                screenProps={this.props.screenProps}
                renderedChildren={(item) => {

                    return (
                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.white,
                            justifyContent: 'center',
                        }}>

                            <View style={{
                                flex: 1,
                                justifyContent: 'center'
                            }}>
                                <TextView textValue={'[ ' + item.code + ' ]  ' + item.description}
                                    styles={{ fontSize: 15, fontWeight: 'bold', color: COLORS.secondaryColor }}></TextView>
                            </View>
                        </View>
                    )
                }} />
        )
    }


}