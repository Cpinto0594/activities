import React from 'react';
import { View, StyleSheet } from 'react-native';
import NotificationServices from '../../../services/NotificationsServices';
import { COLORS } from '../../../config/colors/colors';
import { LoadingIndicator } from '../../../components/loading/loding';
import ButtonPanel from '../../../components/Button/ButtonPanel';
import { TextField } from 'react-native-material-textfield';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { HeaderButtons } from '../../main/navigation.config';
import { NavigationActions } from 'react-navigation';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { FaIcons } from '../../../utils/Icons';
import { CARGOS } from '../../../config/strings/cargos.strings';
import PositionsServices from '../../../services/positions.services/positions.services';
import { containerStyles } from '../../../utils/Styles';


export default class PositionCreateScreen extends React.Component {

    private notificationServices: NotificationServices;
    private cargosServices: PositionsServices;

    state = {
        cargo: this.props.navigation.getParam('item') || {}
    }

    constructor(props) {
        super(props);
        this.notificationServices = new NotificationServices;
        this.cargosServices = new PositionsServices;
    }

    static navigationOptions = (props) => {

        return {
            tabBarVisible: false,
            header:
                <DefaultCustomHeader
                    headerTitle={'Registro de Cargos'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                props.navigation.dispatch(NavigationActions.back())

                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
        }
    }

    componentWillUnmount() {
        this.state.cargo = {}
    }


    render() {

        return (
            <View style={styles.main_container}>

                <View style={styles.form_container}>
                    <View style={styles.form_group}>
                        {/* <TextView textValue={CARGOS.Codigo + ' *'} styles={styles.label_color}></TextView>
                        <InputText value={this.state.cargo.codigo} placeholder={CARGOS.Codigo}
                            onChange={(value) => { this.setState({ cargo: { ...this.state.cargo, codigo: value } }); }}
                            styles={styles.border_input}
                            toUpper></InputText> */}
                        <TextField
                            ref='codigo'
                            label={CARGOS.Codigo}
                            value={this.state.cargo.codigo}
                            tintColor={COLORS.secondaryColor}
                            baseColor={COLORS.secondaryColor}
                            animationDuration={200}
                            onChangeText={(value) => this.setState({ cargo: { ...this.state.cargo, codigo: value } })}
                            onBlur={() => {
                                this.refs.descripcion.focus()
                            }}
                            labelHeight={15}
                        />
                    </View>
                    <View style={styles.form_group}>
                        {/* <TextView textValue={CARGOS.Nombre + ' *'} styles={styles.label_color}></TextView>
                        <InputText value={this.state.cargo.descripcion} placeholder={CARGOS.Nombre}
                            onChange={(value) => { this.setState({ cargo: { ...this.state.cargo, descripcion: value } }); }}
                            styles={styles.border_input}
                            toUpper></InputText> */}
                        <TextField
                            ref='descripcion'
                            label={CARGOS.Nombre}
                            value={this.state.cargo.descripcion}
                            tintColor={COLORS.secondaryColor}
                            baseColor={COLORS.secondaryColor}
                            animationDuration={200}
                            onChangeText={(value) => this.setState({ cargo: { ...this.state.cargo, descripcion: value } })}
                            onBlur={() => {
                                this.refs.codigo.focus()
                            }}
                            labelHeight={15}
                        />
                    </View>

                </View>
                <ButtonPanel
                    buttons={[
                        {
                            text: CARGOS.GuardarCargo,
                            icon: FaIcons.getIcon(FaIconsEnum.SAVE, 20, COLORS.secondaryColor),
                            action: this._crearEmpresa.bind(this)
                        }
                    ]}
                    buttonTextStyle={{
                        color: COLORS.secondaryColor
                    }}
                    buttonStyle={{
                        backgroundColor: COLORS.primaryColor
                    }} />
                <LoadingIndicator ref='loading' />
            </View>
        )

    }

    _showLoading(bool) {
        this.refs.loading[bool ? 'show' : 'hide']();
    }


    _crearEmpresa() {

        let data = Object.assign({}, this.state.cargo);
        if (!data ||
            !data['codigo'] ||
            !data['descripcion']) {
            this.notificationServices.info('Ingrese los Datos Requeridos');
            return;
        }
        this._showLoading(true);
        this.cargosServices.save(data)
            .then(response => {

                if (response.success) {
                    this._cancelar();
                    this._clearEmpresasForm();
                } else {
                    throw Error(response.message)
                }
                this._showLoading(false);
            })
            .catch((e) => {
                console.log(e)
                this._showLoading(false);
                this.notificationServices.fail('No se pudo realizar la operación');
            })

    }

    _cancelar() {
        this.props.navigation.goBack(null);
    }

    _clearEmpresasForm() {
        this.setState({ cargo: {} });

    }

}
const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.backgroundColor,
        height: '100%',
        flex: 1
    },
    form_container: {
        marginTop: 0,
        paddingTop: 5,
        paddingBottom: 5,
        marginHorizontal: containerStyles.marginHorizontal,
        flex: 1,
        flexDirection: 'column',
    },
    form_group: {
        marginBottom: 10,
        marginTop: 10
    },
    label_color: {
        color: COLORS.secondaryColor
    },
    border_input: {
        borderColor: COLORS.secondaryColor,
        borderWidth: 1
    }
})