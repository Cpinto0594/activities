import React from 'react';
import { View, Image, StyleSheet, ActivityIndicator, Dimensions, RefreshControl, TouchableOpacity, Animated, Easing } from 'react-native';
import TextView from '../../../components/TextView/TextView';
import STRINGS from '../../../config/strings/system.strings';
import { COLORS } from '../../../config/colors/colors';
import NotificationServices from '../../../services/NotificationsServices';
import SocialPostsServices from '../../../services/socialposts.services/social.posts.services';
import { PostComponent } from './components/post.component';
import { PostTemplateLoading } from '../../../components/loadingTemplates/loading.templates';
import { PostHeaderComponent } from './components/post.header.component';
import Utils from '../../../utils/Utils';
import { PostPublishingComponent } from './components/post.publishing.component';
import { AuthService } from '../../../services/AuthService';
import { CustomListComponent } from '../../../components/List/CustomList';
import { containerStyles, boxShadows } from '../../../utils/Styles';
import RBSheet from 'react-native-raw-bottom-sheet';
import Separator from '../../main/separator.view';
import { FaIcons, SimpleLineIcons } from '../../../utils/Icons';
import { FaIconsEnum, SimpleLineIconsEnum } from '../../../utils/IconsEnum';
import Button from '../../../components/Button/Button';
import { SocialPostComponentStyles } from './social.styles';
import { NavigationActions, StackActions } from 'react-navigation';



export class SocialPostsScreen extends React.Component {
    private notificationService: NotificationServices;
    private postsService: SocialPostsServices;
    private SocialPostStyles;
    private ScrolledValue: number = 50;

    private traslateAnimation = new Animated.Value(0);

    defaultState = {
        isLoading: true, //Esta cargando
        isLoadingMore: false, //Busqueda por scroll
        noData: false,//Si no se encontraton datos
        listEnd: false, //Cuando la peticion no trae mas datos,
        isRefreshing: false,
        page: 1,
        arrPosts: [],
        windowWidth: Dimensions.get('window').width,
        hasScrolled: false,
        currentPost: null

    }


    state = Object.assign({}, this.defaultState);

    static navigationOptions = () => {
        return {
            header: null
        }
    }


    constructor(props) {
        super(props);
        this.notificationService = new NotificationServices;
        this.postsService = new SocialPostsServices;
        this.SocialPostStyles = SocialPostComponentStyles();

    }

    componentDidMount() {
        this._findData();

    }



    render() {

        const traslateAnimation = this.traslateAnimation.interpolate({
            inputRange: [0, 50],
            outputRange: [0, -50],
            extrapolate: 'clamp'
        })

        const transparencyAnimation = this.traslateAnimation.interpolate({
            inputRange: [0, 10, 20, 30, 40, 50],
            outputRange: [1, 0.2, 0.2, 0.2, 0.2, 0],
            extrapolate: 'clamp'
        })

        return (
            <View style={styles.main_container}>
                <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    zIndex: 2,
                    borderBottomColor: COLORS.headerBorderBottomColor,
                    borderBottomWidth: 1,
                    ...boxShadows(5).box_shadow,
                    transform: [
                        {
                            translateY: traslateAnimation

                        }
                    ]
                }}>

                    <Animated.View style={{
                        height: 50,
                        backgroundColor: COLORS.white,
                        width: '100%',
                        paddingHorizontal: containerStyles.marginHorizontal,

                    }}>
                        <Animated.View style={{
                            opacity: transparencyAnimation,
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            <TextView textValue={'Feed'} styles={{
                                fontWeight: 'bold',
                                fontSize: 25,
                                flex: 1
                            }} />
                            <TouchableOpacity
                                style={{
                                    padding: 5,
                                    borderRadius: 50,
                                    backgroundColor: COLORS.softGrey,
                                    marginRight: 10
                                }}
                                onPress={this._goToSocial}>
                                {
                                    SimpleLineIcons.getIcon(SimpleLineIconsEnum.GLOBE, 20, COLORS.secondaryColor)
                                }
                            </TouchableOpacity>
                        </Animated.View>

                    </Animated.View>

                    <PostHeaderComponent inputTextDisabled={true}
                        inputTextClickEvent={() => {
                            Utils.defaultNavigation(this.props.navigation, 'NewPost', {
                                onPostCreated: (postEvent) => {
                                    this._afterCreatedPost(postEvent);
                                }
                            })
                        }} />

                </Animated.View>
                <Animated.View style={{
                    flex: 1,
                    marginTop: 50,
                }}>
                    <CustomListComponent
                        ref='listPost'
                        data={this.state.arrPosts}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onEndReached={this._handlePage}
                        onEndReachedThreshold={0.2}
                        initialNumToRender={1}
                        maxToRenderPerBatch={2}
                        onMomentumScrollBegin={() => {
                            this.state.hasScrolled = true;
                        }}
                        refreshControl={
                            <RefreshControl
                                colors={[COLORS.secondaryColor]}
                                onRefresh={() => {
                                    this.setState({
                                        isRefreshing: true,
                                        page: 1,
                                    }, () => {
                                        this._findData();
                                    })
                                }}
                                refreshing={this.state.isRefreshing}
                            />
                        }

                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={() => <View style={{ height: 10 }}></View>}
                        ListHeaderComponent={() =>
                            <View style={{ height: 70 }}></View>
                        }
                        ListEmptyComponent={this._renderItemNoData}
                        ListFooterComponent={
                            () => {
                                //if (!this.state.isLoadingMore) return null;
                                return (
                                    <View style={styles.list_footer}>
                                        <ActivityIndicator size={'small'} color={COLORS.secondaryColor} />
                                    </View>
                                )

                            }
                        }
                        scrollEventThrottle={20}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: this.traslateAnimation } } }]
                        )}
                        showLoadingTemplate={this.state.isLoading}
                        loadingTemplate={() =>
                            <View style={{
                                backgroundColor: COLORS.white,
                                paddingVertical: 5
                            }}>
                                <PostTemplateLoading width={this.state.windowWidth} />
                            </View>
                        }
                    />
                </Animated.View>

                <RBSheet
                    ref='buttonSheet'
                    duration={250}
                    height={150}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {
                            justifyContent: "flex-start",
                            alignItems: "flex-start",
                            borderTopLeftRadius: 15,
                            borderTopRightRadius: 15
                        },
                        wrapper: {
                        }
                    }}
                >

                    <View style={this.SocialPostStyles.bottomsheet_container}>
                        <View style={{
                            marginBottom: 10,
                            height: 40,
                        }}>
                            <TextView textValue={'¿Que deseas hacer?'} />
                            <Separator styles={{
                                borderColor: '#bdbdbd',
                                borderSize: 0.6
                            }} />
                        </View>
                        <View style={{
                            justifyContent: 'flex-start',
                        }}>
                            <Button text={'Eliminar Publicación'} type={'light'}
                                icon={FaIcons.getIcon(FaIconsEnum.TRASH_EMPTY, 20, COLORS.secondaryColor)}
                                buttonColorStyles={{
                                    backgroundColor: 'transparent',

                                }}
                                buttonStyles={{
                                    justifyContent: 'flex-start',
                                    borderWidth: 0,
                                    paddingLeft: 0
                                }}
                                textStyles={{
                                    fontSize: 16
                                }}
                                onPress={this._onDeletePOst} />
                        </View>

                    </View>

                </RBSheet>
            </View >
        )

    }

    _listScrollTop = () => {
        this.refs.listPost._getRef().scrollToOffset({ offset: 0, animated: true });
    }

    _keyExtractor = (item) => item.uuid + '';

    _renderItem = ({ item }) => (
        item.post_type === 'post' ?
            <PostComponent post={item}
                navigation={this.props.navigation}
                onMoreOptions={this._onMoreOptions}
            /> :
            <PostPublishingComponent />
    );
    _renderItemNoData = () => (
        <TouchableOpacity
            disabled={this.state.isLoading}
            onPress={() => {
                this.setState({
                    isLoading: true,
                    page: 1
                }, this._findData)
            }}>
            <View style={styles.no_data_container}>
                <Image source={require('../../../../assets/no_data.png')} style={{
                    width: 80,
                    height: 80
                }} />
                <TextView textValue={STRINGS.No_Data_Found}
                    styles={{
                        color: COLORS.secondaryColor
                    }}></TextView>
                <TextView textValue={'Toca para refrescar'}
                    styles={{
                        color: COLORS.midSoftGrey,
                        fontSize: 10
                    }}></TextView>

            </View>
        </TouchableOpacity>
    );

    _onMoreOptions = (post) => {
        this.refs.buttonSheet.open();
        this.state.currentPost = post;
    }
    _onDeletePOst = () => {
        let { currentPost } = this.state;
        this.notificationService.confirm('¿Desea eliminar el post?', () => {
            this.postsService.deletePost(currentPost.uuid)
                .then(resp => {
                    if (resp.success) {

                        this.refs.buttonSheet.close();

                        let data = this.state.arrPosts;
                        Utils.arrayDeleteItem(data, (item) => item.uuid === currentPost.uuid)
                        this.setState({
                            arrPosts: [...data],
                            currentPost: null
                        })

                    } else {
                        throw Error(resp.message)
                    }
                })
                .catch(err => {
                    console.log(err)
                    this.notificationService.fail('No se pudo eliminar post')
                })
        })
    }

    _handlePage = () => {
        if (this.state.isLoadingMore || this.state.listEnd) return null;
        console.log('Paging')

        this.setState({
            page: this.state.page + 1,
            isLoadingMore: true,
            noData: false,
        }, () => {
            this._findData();
        })
    }



    _findData = () => {

        let request: any = {};
        request.page = this.state.page;
        request.max_rows = 10;
        request.include = 'images,attachments';
        request.user_name = AuthService.userLoggedData.userName

        this.postsService.findPosts(request)
            .then(response => {
                if (response.success) {
                    this.setState({
                        arrPosts: this.state.isRefreshing ?
                            [...response.data] :
                            this.state.arrPosts.concat(response.data),
                        isLoading: false,
                        isLoadingMore: false,
                        noData: false,
                        isRefreshing: false
                    })
                } else {
                    this.setState({
                        listEnd: true,
                        isLoading: false,
                        isLoadingMore: false,
                        isRefreshing: false,
                        noData: this.state.arrPosts.concat(response.data).length <= 0
                    })
                }

            })
            .catch(err => {
                console.log(err);
                this.setState({
                    listEnd: true,
                    isLoading: false,
                    isLoadingMore: false,
                    isRefreshing: false,
                    arrPosts: []
                })
                this.notificationService.fail('No se pudo obtener posts');
            });

    }

    _afterCreatedPost = (postEvent) => {

        this._listScrollTop();
        let data = [...this.state.arrPosts];
        data.unshift({
            post_type: 'saving'
        });
        this.setState({
            arrPosts: data
        }, () => {
            this.postsService.savePost(postEvent)
                .then(res => {

                    if (res.success) {
                        data = this.state.arrPosts.filter(post => post.post_type !== 'saving');
                        data.unshift(res.data);

                        this.setState({
                            arrPosts: data
                        })

                    } else {
                        throw Error(res.message)
                    }
                })
                .catch(err => {
                    console.log(err);
                    this.notificationService.fail('No se pudo generar publicación');
                    let data = this.state.arrPosts;
                    Utils.arrayDeleteItem(data, (item) => item.post_type === 'saving');
                    this.setState({
                        arrPosts: data
                    })
                })
        })
    }

    _goToSocial = () => {
        Utils.defaultNavigation(this.props.navigation, 'Connections')
    }

}
const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: '#DFE3E8'
    },
    list_footer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50
    },
    no_data_container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 500,
        borderColor: 'red',
        borderWidth: 1
    },



});