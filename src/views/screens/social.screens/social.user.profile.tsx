import React from 'react';
import { HeaderButtons } from '../../main/navigation.config';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { COLORS } from '../../../config/colors/colors';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { View, FlatList, ImageBackground, Image, StatusBar, ActivityIndicator } from 'react-native';
import TextView from '../../../components/TextView/TextView';
import { boxShadowSocialUserProfile, containerStyles, boxShadows } from '../../../utils/Styles';
import { NavigationEvents, ScrollView } from 'react-navigation';
import ProgressiveImage from '../../../components/Image/ProgressiveImage';
import { Platform } from '@unimodules/core';
import { AuthService } from '../../../services/AuthService';
import { CircularImage } from '../../../components/CircularImage/circular_image';
import SocialPostsServices from '../../../services/socialposts.services/social.posts.services';
import { PostComponent } from './components/post.component';
import Separator from '../../main/separator.view';


export class SocialUserProfile extends React.Component {

    private socialPostsService: SocialPostsServices

    constructor(props) {
        super(props);

        this.state = {
            arrPosts: [],
            user: props.navigation.getParam('user') || {
                pic: AuthService.userLoggedData.userPic,
                nombre_completo: AuthService.userLoggedData.userFullName,
                uuid: AuthService.userLoggedData.userIdentifier,
                position: AuthService.userLoggedData.userPosition
            }
        }
        this.socialPostsService = new SocialPostsServices
    }


    componentDidMount() {

        this._findUserPosts();

    }

    render() {

        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.white
            }}>
                <NavigationEvents
                    onWillFocus={() => {
                        if (Platform.OS === 'android') {
                            StatusBar.setBackgroundColor('rgba(0,0,0,1)', true)
                            StatusBar.setBarStyle('light-content')
                            StatusBar.setTranslucent(true)
                        }
                    }}
                    onWillBlur={() => {
                        if (Platform.OS === 'android') {
                            StatusBar.setBackgroundColor(COLORS.primaryColor, true)
                            StatusBar.setBarStyle('dark-content')
                            StatusBar.setTranslucent(true)
                        }
                    }}
                />
                <DefaultCustomHeader
                    style={{
                        backgroundColor: COLORS.white,
                        borderBottomColor: COLORS.softGrey,
                        borderBottomWidth: 1
                    }}
                    textColor={COLORS.white}
                    headerTitle={null}
                    headerLeft={
                        <HeaderButtons

                            action={() => this.props.navigation.goBack(null)}
                        >
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        flexGrow: 1
                    }}
                >

                    <View style={{
                        backgroundColor: COLORS.white,
                        marginHorizontal: containerStyles.marginHorizontal,
                        marginVertical: 10,
                        borderRadius: 10
                    }}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingVertical: 20
                        }}>

                            <CircularImage
                                width={120}
                                height={120}
                                style={{
                                    borderColor: COLORS.secondaryColor,
                                    borderWidth: 2,
                                    ...boxShadows(6, COLORS.red).box_shadow
                                }}
                                source={{ uri: this.state.user.pic || null }}
                            />

                        </View>
                        <View style={{
                            width: 100 + '%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginBottom: 10
                        }}>
                            <View>
                                <TextView textValue={this.state.user.nombre_completo}
                                    styles={{
                                        color: COLORS.secondaryColor,
                                        fontWeight: 'bold',
                                        fontSize: 20
                                    }} />
                            </View> 
                            {
                                this.state.user.position !== null &&
                                <View style={{ 
                                    marginTop: 5
                                }}>
                                    <TextView textValue={this.state.user.position} styles={{ color: COLORS.secondaryColor }} />
                                </View>
                            }


                        </View>
                    </View>
                    <Separator color={COLORS.midSoftGrey} strength={0.6} styles={{
                        marginHorizontal: containerStyles.marginHorizontal
                    }} />
                    <View style={{
                        flex: 1,
                        paddingHorizontal: containerStyles.marginHorizontal,
                        paddingVertical: 10,
                    }}>
                        <View style={{
                            paddingTop: 5,
                            marginTop: 5,
                            width: '100%',
                            justifyContent: 'center',
                            alignItems: 'flex-start',
                        }}>
                            <TextView textValue={'- Aptitudes'} styles={{ color: COLORS.secondaryColor }} />
                            <TextView textValue={'- Aptitudes'} styles={{ color: COLORS.secondaryColor }} />
                            <TextView textValue={'- Aptitudes'} styles={{ color: COLORS.secondaryColor }} />

                        </View>
                    </View>

                    <View style={{
                        flex: 1,
                        backgroundColor: '#DFE3E8'
                    }}>
                        <View style={{
                            flex: 1,
                            height: 40,
                            backgroundColor: COLORS.white,
                            marginVertical: 10,
                            justifyContent: 'center',
                            alignItems: 'flex-start',
                            paddingHorizontal: containerStyles.marginHorizontal
                        }}>
                            <TextView textValue={'Posts'} styles={{
                                fontSize: 15,
                                fontWeight: 'bold'
                            }} />
                        </View>

                        <FlatList
                            data={this.state.arrPosts}
                            keyExtractor={(item) => item.uuid}
                            renderItem={this._renderPost}
                            maxToRenderPerBatch={5}
                            initialNumToRender={5}
                            showsVerticalScrollIndicator={false}
                            ItemSeparatorComponent={() => <View style={{ height: 10 }}></View>}
                            ListEmptyComponent={
                                <View style={{
                                    marginHorizontal: containerStyles.marginHorizontal,
                                    marginVertical: 20,
                                    justifyContent: 'center',
                                    alignContent: 'center',
                                    flex: 1
                                }}>
                                    <ActivityIndicator size={'small'} color={COLORS.secondaryColor} />
                                </View>
                            }
                        />

                    </View>

                </ScrollView>


            </View>
        )

    }

    _renderPost = ({ item }) => {
        return <PostComponent post={item}
            navigation={this.props.navigation} />
    }

    _findUserPosts = () => {
        let user = this.state.user.uuid;
        let request: any = {};
        request.page = this.state.page;
        request.max_rows = 10;
        request.include = 'images,attachments';
        request.user_name = AuthService.userLoggedData.userName

        this.socialPostsService.userPosts(user, request)
            .then(res => {
                if (res.success) {
                    this.setState({
                        arrPosts: res.data
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })


    }

}