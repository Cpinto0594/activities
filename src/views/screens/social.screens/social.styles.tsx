import { StyleSheet } from "react-native";
import { COLORS } from "../../../config/colors/colors";
import { boxShadow, boxShadowChatInput, boxShadowSocialPostHeader, containerStyles, boxShadows } from "../../../utils/Styles";

export const SocialCommentStyles = () => {
    const fontIndicatorsColor = COLORS.midGrey;
    const fontIndicatorSize = 12;
    const socialPostCommentImagePicSize = { w: 40, h: 40 };
    const socialPostCommentUserNameTextSize = 14;
    const socialPostCommentContentTextSize = 14;

    return StyleSheet.create({
        main_container: {
            flex: 1
        },
        comment_header_image_pic: {
            width: socialPostCommentImagePicSize.w,
            height: socialPostCommentImagePicSize.h,
        },
        comment_header_image_pic_answer: {
            width: socialPostCommentImagePicSize.w - 10,
            height: socialPostCommentImagePicSize.h - 10,
        },
        comment_container: {
            flexDirection: 'row',
            flex: 1
        },
        comment_username_text_styles: {
            fontSize: socialPostCommentUserNameTextSize,
            fontWeight: 'bold',
            flexWrap: 'wrap'
        },
        comment_content_text_styles: {
            fontSize: socialPostCommentContentTextSize,
            color: COLORS.secondaryColor,
            flexWrap: 'wrap'
        },
        comment_content_container: {
            paddingHorizontal: 10,
            //  flex: 0,
        },
        comment_indicators_text_style: {
            fontSize: fontIndicatorSize,
            color: fontIndicatorsColor,
            fontWeight: 'bold',
        },
        comment_subcomments_container: {
            justifyContent: 'flex-start',
            padding: 0,
            flex: 1,
            width: '100%'
        },

        //Comments Publisher
        comment_publisher_main_container: {
            flex: 1,
            backgroundColor: COLORS.listBackgroundColor
        },
        list_container: {
            backgroundColor: COLORS.white,
            flex: 1
        },
        list_footer: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            height: 50
        },
        no_data_container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },
        comment_publisher_answering_container: {
            borderTopColor: COLORS.softGrey,
            borderTopWidth: 0.5,
            alignItems: 'center',
            flex: 0,
            ...boxShadowChatInput.box_shadow,
            backgroundColor: COLORS.white
        },
        comment_publisher_answering: {
            height: 30,
            width: '100%',
            backgroundColor: COLORS.softGrey,
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 10
        }


    })
}

export const SocialPostComponentStyles = () => {
    const socialPostContainerBackgroundColor = COLORS.white;
    const socialPostContainerBorderColors = COLORS.white;
    const socialPostImagePicSize = { w: 40, h: 40 };
    const socialPostUserNameTextColor = COLORS.fontColor;
    const socialPostUserNameTextSize = 14;
    const socialPostContentTextSize = 15;
    const socialPostDurationTextColor = COLORS.midGrey;
    const socialPostFooterBorderTopColor = COLORS.softGrey;

    return StyleSheet.create({
        //POST STYLES
        post_container: {
            backgroundColor: socialPostContainerBackgroundColor,
            borderTopColor: socialPostContainerBorderColors,
            borderBottomColor: socialPostContainerBorderColors,
            borderTopWidth: 1,
            borderBottomWidth: 1,
        },
        post_header_container: {
            paddingBottom: 5,
            marginBottom: 10,
            marginLeft: containerStyles.marginHorizontal,
            flexDirection: 'row',
            alignItems: 'center'
        },
        post_header_username: {
            flex: 1,
            justifyContent: 'center',
            marginLeft: 10

        },
        post_header_image_pic: {
            width: socialPostImagePicSize.w,
            height: socialPostImagePicSize.h,
            borderRadius: socialPostImagePicSize.w / 2

        },
        post_header_userName_text_style: {
            fontSize: socialPostUserNameTextSize,
            fontWeight: 'bold',
            color: socialPostUserNameTextColor
        },
        post_header_post_created_text_style: {
            fontSize: 11,
            color: socialPostDurationTextColor
        },
        post_content_container: {
            paddingTop: 5,
            paddingBottom: 5,
            marginBottom: 10,
            marginHorizontal: containerStyles.marginHorizontal
        },
        post_content_text_styles: {
            fontSize: socialPostContentTextSize,
            color: COLORS.fontColor
        },

        post_footer_container: {
            flex: 1,
            //marginHorizontal: containerStyles.marginHorizontal,
            paddingHorizontal: 10,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            borderTopColor: socialPostFooterBorderTopColor,
            borderTopWidth: 0.4
        },
        post_button_reaction: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 5,
            paddingTop: 5,
            paddingBottom: 10,
            marginRight: 10
        },
        post_footer_button_reaction_text_styles: {
            fontSize: 15,
            marginLeft: 5
        },
        touchable_post_options: {
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 50,
            padding: containerStyles.marginHorizontal,
            width: 40,
            height: 40

        },
        bottomsheet_container: {
            backgroundColor: COLORS.white,
            paddingVertical: 20,
            paddingHorizontal: containerStyles.marginHorizontal,
            width: '100%'
        }
    })
}

export const SocialPostPublisher = () => {

    const postPublisherImageSize = { w: 40, h: 40 }

    return StyleSheet.create({
        main_container: {
            flex: 1,
            backgroundColor: COLORS.white
        },
        publisher_header_container: {
            width: 100 + '%',
            marginHorizontal: containerStyles.marginHorizontal,
            paddingVertical: 10,
            flexDirection: 'row',
            backgroundColor: COLORS.white,
            borderBottomColor: COLORS.softGrey,
            borderBottomWidth: 0.5,
            alignItems: 'center',
        },
        publisher_header_image_pic: {
            width: postPublisherImageSize.w,
            height: postPublisherImageSize.h
        },
        publisher_content_text_style: {
            // width: 100 + '%',
            //alignItems: 'stretch',
            marginHorizontal: containerStyles.marginHorizontal,
            minHeight: 40,
            flex: 0
        },
        publisher_attachments_button_container: {
            flexDirection: 'row',
            justifyContent: 'flex-start',
            paddingVertical: 5,
            marginHorizontal: containerStyles.marginHorizontal,
            borderTopColor: COLORS.softGrey,
            borderTopWidth: 0.5,
            backgroundColor: COLORS.white

        },
        publisher_attachment_button: {
            padding: 5,
            paddingLeft: 10,
            paddingRight: 10,
            flexDirection: 'row',
            borderColor: COLORS.midGrey,
            borderWidth: 0.5,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10
        }
    })
}

export const SocialPostAtttachmentsStyles = () => {

    const attachment_name_text_size = 13;
    const attachment_icon_size = 13;
    const attachment_container_leftborder_color = COLORS.secondaryColor;
    const attachment_container_bottomtborder_color = COLORS.softGrey;


    return StyleSheet.create({
        main_container: {
            flex: 0,
        },
        attachment_touchable: {
            flexDirection: 'row',
            justifyContent: 'center',
            borderLeftColor: attachment_container_leftborder_color,
            borderLeftWidth: 3,
            borderBottomColor: attachment_container_bottomtborder_color,
            borderBottomWidth: 1,
            marginHorizontal: 5
        },
        attachment_name_text_style: {
            fontSize: attachment_name_text_size,
            flexWrap: 'wrap'
        },
        attachment_icon_size: {
            height: attachment_icon_size,
            color: COLORS.secondaryColor
        },
        attachment_icon_delete_size: {
            height: attachment_icon_size,
            color: COLORS.labelDanger
        },
        attachment_name_container: {
            padding: 5,
            justifyContent: 'flex-start',
            flex: 1,
            flexDirection: 'row'
        }

    })
}

export const SocialPostHeaderStyles = () => {
    const postUserImageSize = { w: 40, h: 40 }


    return StyleSheet.create({
        main_container: {
            flex: 1,
            backgroundColor: COLORS.white,
            //...boxShadowSocialPostHeader.box_shadow,
        },
        header_container: {
            flexDirection: 'row',
            alignItems: 'flex-start',
            paddingVertical: 10,
            marginHorizontal: containerStyles.marginHorizontal,
        },
        header_image_container: {
            borderRadius: 20,
            //borderColor: COLORS.secondaryColor,
            //borderWidth: 1,
            overflow: 'hidden',
            width: postUserImageSize.w,
            height: postUserImageSize.h
        },
        header_input_styles: {
            flex: 1,
            borderBottomColor: COLORS.secondaryColor,
            borderBottomWidth: 0.4,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            color: COLORS.white,
            marginLeft: 10,
            minHeight: 40
        },
        header_input_placeholder_styles: {
            color: COLORS.secondaryColor
        }

    })
}