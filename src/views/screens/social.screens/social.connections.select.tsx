import React from 'react';
import { View, FlatList, ActivityIndicator, TouchableOpacity, Switch, Dimensions } from 'react-native';
import { COLORS } from '../../../config/colors/colors';
import TextView from '../../../components/TextView/TextView';
import ProgressiveImage from '../../../components/Image/ProgressiveImage';
import Separator from '../../main/separator.view';
import PropTypes from 'prop-types';
import UsuariosServices from '../../../services/usuarios.services/usuarios.services';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { HeaderButtons } from '../../main/navigation.config';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import SocialConnectionsServices from '../../../services/social.connections.services/social.connections.services';
import { AuthService } from '../../../services/AuthService';
import { SocialPeopleTemplateLoading } from '../../../components/loadingTemplates/loading.templates';
import { ItemUserSelectable } from './components/item.user.selectable';
import { containerStyles } from '../../../utils/Styles';
import { CustomListComponent } from '../../../components/List/CustomList';

export class SocialConnectionsSelect extends React.Component {

    private socialConnectionServices: SocialConnectionsServices;

    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Seleccionar Miembros'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                navigation.goBack(null)
                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                    headerRight={
                        <HeaderButtons
                            style={{
                                width: 60
                            }}
                            action={() => {
                                navigation.getParam('onClickBackButton')()
                            }}>
                            <TextView textValue={'Listo'} styles={{
                                fontWeight: 'bold',
                                fontSize: 12
                            }} />
                        </HeaderButtons>
                    }
                />
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            arrUsuarios: [],
            selectedMembers: props.navigation.getParam('members') || [],
            windowWidth: Dimensions.get('window').width,
            loadingData: true,
            groupUuid: props.navigation.getParam('group')
        }
        this.socialConnectionServices = new SocialConnectionsServices;
    }

    componentDidMount() {
        this._findData();
        this.props.navigation.setParams({
            onClickBackButton: () => {
                let backAction = this.props.navigation.getParam('onBack');
                let selectedItems = this.state.arrUsuarios.filter(item => item.selected);
                backAction(selectedItems);
                this.props.navigation.goBack(null)
            }
        })
    }

    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.listBackgroundColor
            }}>

                <CustomListComponent
                    data={this.state.arrUsuarios}
                    keyExtractor={(item) => String(item.uuid)}
                    renderItem={this._renderItem}
                    loadingTemplate={() =>
                        <View style={{
                            marginHorizontal: containerStyles.marginHorizontal,
                            paddingVertical: 5,
                        }}>
                            <SocialPeopleTemplateLoading width={this.state.windowWidth - (containerStyles.marginHorizontal + 10)} />
                        </View>
                    }
                    showLoadingTemplate={this.state.loadingData}
                    ItemSeparatorComponent={() => {
                        return <Separator styles={{
                            marginTop: 0,
                            marginBottom: 0,
                            borderBottomColor: COLORS.softGrey
                        }} />
                    }}
                />

            </View>
        )
    }

    _renderItem = ({ item }) =>
        <View style={{
            flex: 1,
            backgroundColor: COLORS.white
        }}>
            <ItemUserSelectable
                user={item}
                selected={item.selected}
                onPress={(selected) => {
                    item.selected = selected;
                    this.setState({
                        arrUsuarios: [...this.state.arrUsuarios]
                    });
                }} />
        </View>

    _findData() {
        let params = {
            page: 1,
            max_rows: 10
        }

        let user = AuthService.userLoggedData.userIdentifier;

        this.socialConnectionServices
            .findUserPeople(user, params)
            .then(async res => {
                if (res.success) {

                    let userPeople = (res.data || [])

                    let hasPeople = userPeople.length > 0;

                    //Si estamos editando los miembros de un grupo guardado
                    if (hasPeople && this.state.selectedMembers) {

                        userPeople.forEach(member => {
                            let exist = this.state.selectedMembers
                                .find(sel => sel.uuid === member.uuid);

                            if (exist) member.selected = true
                        });
                    }

                    let members = userPeople
                        .filter(member => member.uuid !== user);

                    this.setState({
                        arrUsuarios: members,
                        loadingData: false,
                        refreshing: false
                    })



                } else {
                    throw Error(res.message);
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loadingData: false,
                    refreshing: false
                })
            })
    }

}

