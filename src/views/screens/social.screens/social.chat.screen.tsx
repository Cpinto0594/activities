import React from 'react';
import { View, TouchableHighlight, FlatList, Image, YellowBox, Dimensions, RefreshControl } from 'react-native';
import TextView from '../../../components/TextView/TextView';
import ViewsNavigation, { HeaderButtons } from '../../main/navigation.config';
import { COLORS } from '../../../config/colors/colors';
import Utils from '../../../utils/Utils';
import { DateUtils } from '../../../utils/DateUtils';
import moment from 'moment';
import { SearchBar } from '../../../components/SearchBar/SearchBar';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { FaIcons, MaterialIcons } from '../../../utils/Icons';
import { FaIconsEnum, MaterialIconsEnum } from '../../../utils/IconsEnum';
import FireBaseExpo from '../../../FunctionaityProviders/Firebase/FirebaseExpo';
import ActionButton from 'react-native-action-button';
import { AuthService } from '../../../services/AuthService';
import { CustomListComponent } from '../../../components/List/CustomList';
import { ListLoadingTemplateLoading, SocialChatsTemplateLoading } from '../../../components/loadingTemplates/loading.templates';
import { containerStyles, boxShadowActionButton } from '../../../utils/Styles';
import { CircularImage } from '../../../components/CircularImage/circular_image';
import AsyncStorageProvider from '../../../FunctionaityProviders/AsyncStorageprovider';
import { ChatModel } from '../../../models/ChatModel';
import { SocialLocalChatServices } from '../../../services/social.chats.services/social.chats.services';

export class ChatListScreen extends React.Component {

    private firebaseInstance: FireBaseExpo = FireBaseExpo.instance;
    private firebaseDBListener;


    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Chats'}

                />
        }
    }

    private today = moment()


    constructor(props) {
        super(props);

        this.state = {
            searchingText: null,
            arrChats: [],
            loading: true,
            windowWidth: Dimensions.get('window').width,
            refreshing: false,
        }
    }

    async componentWillMount() {
        this._findLocalChats();
    }

    componentWillUnmount() {
        if (this.firebaseDBListener) {
            this.firebaseDBListener();
        }
    }



    render() {
        return (
            <View style={{
                backgroundColor: COLORS.listBackgroundColor,
                flex: 1
            }}  >

                {
                    this._searchBar()
                }

                <View style={{
                    flex: 1,
                }}>

                    <CustomListComponent
                        data={this.state.arrChats}
                        keyExtractor={(item) => item.chat_id}
                        renderItem={this._renderChatItem}
                        showsVerticalScrollIndicator={false}
                        showLoadingTemplate={this.state.loading}
                        loadingTemplate={() =>
                            <View style={{
                                paddingVertical: 5,
                                marginHorizontal: containerStyles.marginHorizontal
                            }}>
                                <SocialChatsTemplateLoading width={this.state.windowWidth - containerStyles.marginHorizontal - 20} />
                            </View>
                        }
                        refreshing={this.state.refreshing}
                        onRefresh={() => {

                            this.setState({
                                refreshing: true
                            }, this._findChats)

                        }}
                    />

                </View>
                <ActionButton
                    buttonColor={COLORS.contrastColor}
                    offsetX={containerStyles.marginHorizontal}
                    offsetY={10}
                    onPress={() => {
                        Utils.defaultNavigation(this.props.navigation, 'CreateChatDestinationSelect')
                    }}
                    renderIcon={() => MaterialIcons.getIcon(MaterialIconsEnum.CHAT, 22, COLORS.white)}
                    shadowStyle={{
                        ...boxShadowActionButton.box_shadow
                    }}
                    fixNativeFeedbackRadius={true}
                />
            </View >
        )
    }

    _searchBar = () =>
        <View style={{
            height: 50,
            paddingHorizontal: containerStyles.marginHorizontal,
            backgroundColor: COLORS.primaryColor,
            borderBottomColor: '#f2f2f4',
            borderBottomWidth: 1
        }}>
            <SearchBar
                placeHolder={'Buscar Chat'}
                placeHolderTextColor={COLORS.midSoftGrey}
                style={{
                    backgroundColor: COLORS.primaryColor,
                }}
                inputContainerStyles={{
                    backgroundColor: COLORS.white,
                    color: COLORS.secondaryColor,
                    borderColor: 'transparent'
                }}
            />

        </View>


    parseChatData = (item) => {
        let userIdentifier = AuthService.userLoggedData.userIdentifier;

        let data = item.data();
        let type = data.chat_destination_type;
        let chat_title = null;
        let chat_pic = null;
        let chat_last_message = null;

        let members_data = type === 'PEOPLE' ?
            data.chat_members_detail
                .filter(itm => itm.uuid !== userIdentifier)[0] :
            { name: data.chat_description, pic: data.chat_pic };

        chat_title = members_data.name;
        chat_pic = members_data.pic;

        return {
            chat_id: item.id,
            chat_pic: chat_pic,
            chat_title: chat_title,
            chat_last_message: chat_last_message,
            ...data,
            chat_created_at: (data.chat_created_at && data.chat_created_at.toDate) ?
                data.chat_created_at.toDate()
                : data.chat_created_at,
            chat_last_message_date: (data.chat_last_message_date && data.chat_last_message_date.toDate) ?
                data.chat_last_message_date.toDate()
                : data.chat_last_message_date
        }
    }
    _orderChats = (chats: Array<ChatModel>) => {

        chats.sort((a, b) => (new Date(b.chat_last_message_date).getTime() - new Date(a.chat_last_message_date).getTime()))

    }

    editChatData = (chats: Array<ChatModel>, item) => {

        let index = chats.findIndex(itm => itm.chat_id === item.chat_id);
        if (index !== -1) {
            chats[index] = item
        }

    }

    _findLocalChats = async () => {
        let chats = await SocialLocalChatServices.INSTANCE.findLocalUserChats()
        let messages = await SocialLocalChatServices.INSTANCE.findLocalUserMessages()

        let arrChats: Array<ChatModel> =
            await SocialLocalChatServices.INSTANCE.findLocalUserChats();

        this._orderChats(arrChats);

        this.setState({
            arrChats,
            loading: arrChats.length < 0
        }, () => {
            this._findChats();
        })
    }

    _findChats = () => {
        let user = AuthService.userLoggedData.userIdentifier;

        this.firebaseDBListener = this.firebaseInstance.db()
            .collection('social_chats')
            .where('state', '==', 'A')
            .where('chat_members', 'array-contains', user)
            .orderBy('chat_created_at', 'desc')
            .limit(20)
            .onSnapshot(conversations => {
                let chats: Array<ChatModel> = this.state.arrChats || [];
                let empty = chats.length === 0;

                conversations.docChanges().forEach(change => {
                    console.log(change.type)
                    if (change.type === 'added') {
                        if (!empty) {
                            let exist = chats.find(chat => chat.chat_id === change.doc.id);
                            if (!exist) {
                                chats.unshift(this.parseChatData(change.doc))
                            }
                        } else {
                            chats.push(this.parseChatData(change.doc))
                        }
                    } else if (change.type === 'removed') {
                        Utils.arrayDeleteItem(chats, (chat: ChatModel) => chat.chat_id === change.doc.id)
                        SocialLocalChatServices.INSTANCE.removeLocalChat(change.doc.id)
                    } else if (change.type === 'modified') {
                        this.editChatData(chats,
                            this.parseChatData(change.doc));
                    }
                })

                this._orderChats(chats);

                this.setState({
                    arrChats: chats,
                    loading: false,
                    refreshing: false
                }, () => {
                    SocialLocalChatServices.INSTANCE.storeChats(chats)
                })
            });
    }

    _renderChatItem = ({ item }) =>
        <View style={{
            flex: 1,
            backgroundColor: COLORS.white,
        }}>

            <TouchableHighlight
                style={{

                }}
                activeOpacity={0.7}
                underlayColor={'#cdcdcd'}
                onPress={() => { this._goToChat(item) }}
            >
                <View style={{
                    flexDirection: 'row',
                    marginVertical: 5,
                    paddingHorizontal: containerStyles.marginHorizontal,
                    alignItems: 'center'
                }}>
                    <CircularImage
                        width={40}
                        height={40}
                        source={item.chat_pic ?
                            { uri: item.chat_pic } :
                            require('../../../../assets/user_image.png')
                        }
                    />


                    <View style={{
                        flex: 1,
                        marginLeft: 15,
                        borderBottomColor: '#f2f2f4',
                        borderBottomWidth: 1,
                        flexDirection: 'row',

                    }}>

                        <View style={{
                            flex: 1,
                            height: 50,
                            justifyContent: 'space-evenly'
                        }}>
                            <TextView textValue={item.chat_title}
                                numberOfLines={1}
                                styles={{
                                    fontSize: 15,
                                    fontWeight: 'bold',
                                    flexWrap: 'wrap',
                                }} />
                            {
                                item.chat_last_message &&
                                <TextView textValue={item.chat_last_message}
                                    numberOfLines={1}
                                    styles={{
                                        fontSize: 14,
                                        color: COLORS.midGrey,
                                    }} />
                            }
                        </View>

                        <View style={{
                            justifyContent: 'space-around',
                            padding: 10,
                            alignItems: 'center'
                        }}>
                            {
                                !!item.chat_unread_messages &&
                                <View style={{
                                    width: 20,
                                    height: 20,
                                    borderRadius: 50,
                                    backgroundColor: COLORS.secondaryColor,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    alignSelf: 'flex-start',
                                }}>

                                    <TextView textValue={'3'} styles={{
                                        color: COLORS.white,
                                        fontSize: 10
                                    }} />

                                </View>
                            }
                            <View style={{
                                alignItems: 'flex-end',
                            }}>
                                <TextView textValue={this._messageTime(item)}
                                    styles={{
                                        fontSize: 10

                                    }} />
                            </View>

                        </View>

                    </View>

                </View>
            </TouchableHighlight>

        </View>

    _goToChat = (item) => {
        Utils.defaultNavigation(this.props.navigation, 'ChatView', {
            resource: item
        })
    }

    _messageTime = (item) => {

        if (!item.chat_last_message_date) return;
        let time = item.chat_last_message_date;
        let hour = moment(time);
        let difference = DateUtils.diffDuration(hour, this.today);
        let days = Math.round(difference.asDays());

        if (days > 0) {
            return hour.format('DD/MM/YYYY');
        }
        return hour.format('HH:mm');
    }
}