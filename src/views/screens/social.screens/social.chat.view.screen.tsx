import React from 'react';
import { TouchableHighlight, FlatList } from 'react-native-gesture-handler';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { COLORS } from '../../../config/colors/colors';
import { NavigationActions } from 'react-navigation';
import { HeaderButtons } from '../../main/navigation.config';
import { View, Image } from 'react-native';
import InputText from '../../../components/InputText/InputText';
import { AuthService } from '../../../services/AuthService';
import TextView from '../../../components/TextView/TextView';
import { boxShadowChatInput, containerStyles, boxShadow, boxShadows } from '../../../utils/Styles';
import DefaultCustomHeader, { headerTitleStyle } from '../../../components/Headers/custom_header';
import FireBaseExpo from '../../../FunctionaityProviders/Firebase/FirebaseExpo';
import { SocialChatsMessageItem } from './components/social.chat.item';
import Utils from '../../../utils/Utils';
import AsyncStorageProvider from '../../../FunctionaityProviders/AsyncStorageprovider';
import { ChatMessageModel } from '../../../models/ChatMessageModel';
import { SocialLocalChatServices } from '../../../services/social.chats.services/social.chats.services';
import { ChatModel } from '../../../models/ChatModel';
import { ChatContainerChatsMessages } from '../../../models/ChatContainerChatsMessage';

export class SocialChatViewScreen extends React.Component {
    private firebaseInstance: FireBaseExpo = FireBaseExpo.instance;
    private firebaseDBInstance;
    private chat_id: string;

    static navigationOptions = ({ navigation }) => {
        let resource = navigation.getParam('resource') || {};
        let backCaller = resource.caller;
        let title = resource.chat_title;
        let image = resource.chat_pic;

        return {
            header:
                <DefaultCustomHeader
                    style={{
                        ...boxShadows(3).box_shadow
                    }}
                    headerLeft={
                        <HeaderButtons
                            action={() => {

                                navigation.dispatch(NavigationActions.back())
                                if (backCaller) {
                                    navigation.navigate(backCaller);
                                }

                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                    headerTitle={
                        <View style={{
                            flex: 1,
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            flexDirection: 'row'
                        }}>
                            <View style={{
                                borderRadius: 30 / 2,
                                overflow: 'hidden',
                                width: 30,
                                height: 30,
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginRight: 10
                            }}>
                                {
                                    <Image source={
                                        image ?
                                            { uri: image } :
                                            require('../../../../assets/user_image.png')
                                    }
                                        style={{
                                            width: 30,
                                            height: 30,
                                            backgroundColor: COLORS.white
                                        }} />
                                }
                            </View>
                            <TextView textValue={title} styles={headerTitleStyle} />
                        </View>
                    }
                />
        }
    }



    constructor(props) {
        super(props);

        this.state = {
            arrMessages: [],
            messageText: null,
            resource: props.navigation.getParam('resource') || {},
            sendingMessage: false
        }
    }



    componentDidMount() {
        this.chat_id = this.state.resource.chat_id
        if (this.chat_id) {
            this._findLocalMessages();
        }
    }

    componentWillUnmount() {
        if (this.firebaseDBInstance) {
            this.firebaseDBInstance();
        }
    }

    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.white
            }}>

                <View style={{
                    flex: 1,
                    transform: [{ scaleY: -1 }]
                }}>
                    <FlatList
                        contentContainerStyle={{
                            marginHorizontal: containerStyles.marginHorizontal,
                            paddingBottom: 10
                        }}
                        ref='messagesList'
                        data={this.state.arrMessages}
                        keyExtractor={message => message.message_id}
                        renderItem={this._renderMessage}
                        initialNumToRender={10}
                        maxToRenderPerBatch={10}
                        showsVerticalScrollIndicator={false}
                    />
                </View>

                <View style={{
                    paddingVertical: 5,
                    justifyContent: 'center',
                    paddingHorizontal: 10,
                    backgroundColor: COLORS.primaryColor
                }}>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <InputText
                            ref='inputComment'
                            type={'textarea'}
                            multilineMaxHeight={100}
                            placeholder={'Comentar'}
                            styles={{
                                flex: 1,
                                marginRight: 5,
                                marginLeft: 5,
                                minHeight: 40,
                                borderRadius: 20,
                                borderColor: '#cdcdcd',
                                borderWidth: 0.3,
                                //...boxShadowChatInput.box_shadow,
                                paddingLeft: 15,
                                paddingTop: 12,
                                paddingBottom: 12
                            }}
                            value={this.state.messageText}
                            onChange={(value) => {
                                this.setState({
                                    messageText: value
                                })
                            }}
                            enabled={!this.state.savingComment}
                        />

                        <View style={{
                            //opacity: this.state.messageText ? 1 : 0.5,
                        }}>
                            <TouchableHighlight style={{
                                paddingHorizontal: 10,
                                paddingVertical: 10,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 50,
                                ...boxShadowChatInput.box_shadow,
                                backgroundColor: COLORS.secondaryColor
                            }}
                                underlayColor='#cdcdcd'
                                onPress={() => {
                                    this._sendComment()
                                }}
                                disabled={!this.state.messageText || this.state.sendingMessage}>
                                {FaIcons.getIcon(FaIconsEnum.SEND_EMPTY, 20, COLORS.white)}
                            </TouchableHighlight>
                        </View>
                    </View>

                </View>


            </View>
        )
    }

    _renderMessage = ({ item }) =>
        <SocialChatsMessageItem
            message={item}
            chat_type={this.state.resource.chat_destination_type}
        />

    _findLocalMessages = async () => {


        let arrMessages: ChatContainerChatsMessages =
            await SocialLocalChatServices.INSTANCE.findLocalUserMessagesByChatId(this.chat_id);

        this.setState({
            arrMessages: arrMessages.messages
        }, () => {
            this._findMessages();
        })


    }
    _findMessages = () => {


        this.firebaseDBInstance = this.firebaseInstance
            .db()
            .collection(FireBaseExpo.CHAT_MESSAGES_COLLECTION)
            .where('chat_id', '==', this.chat_id)
            .where('state', '==', 'A')
            .orderBy('message_created_at', 'desc')
            .limit(20)
            .onSnapshot(coll => {
                //console.log('Snapshot')
                let messages: Array<ChatMessageModel> = this.state.arrMessages || [];
                let empty = messages.length === 0;


                coll.docChanges().forEach(change => {
                    if (change.type === 'added') {
                        //console.log('added', this.state.arrMessages.length);
                        if (!empty) {
                            let exist = messages.find(mess => mess.message_id === change.doc.id);
                            if (!exist) {
                                messages.unshift(this.parseMessageData(change.doc))
                            }
                        } else {
                            messages.push(this.parseMessageData(change.doc))
                        }
                    } else if (change.type === 'modified') {
                        //console.log('modified');
                        this.editMessageData(messages,
                            this.parseMessageData(change.doc));
                    } else if (change.type === 'removed') {
                        // console.log('removed');
                        Utils.arrayDeleteItem(messages, (message) => message.message_id === change.doc.id)
                    }
                });
                this.setState({
                    arrMessages: messages
                }, () => {
                    SocialLocalChatServices.INSTANCE.storeMessages(this.chat_id, messages);
                })
            });
    }



    editMessageData = (messages: Array<any>, item) => {

        let index = messages.findIndex(itm => itm.message_id === item.message_id);
        if (index !== -1) {
            messages[index] = item
        }

    }

    parseMessageData = (item) => {
        let data = item.data();
        return {
            message_id: item.id,
            ...data,
            message_created_at: (data.message_created_at && data.message_created_at.toDate) ?
                data.message_created_at.toDate() :
                data.message_created_at

        }
    }



    _sendComment = () => {


        let { messageText, resource: {
            chat_id,
            chat_description,
            chat_members,
            chat_owner,
            chat_members_detail
        }
        } = this.state;


        if (!messageText) return;

        this.setState({
            sendingMessage: true,
            messageText: null
        }, async () => {


            if (!chat_id) {
                let chat: ChatModel = {
                    chat_description: chat_description,
                    chat_members: chat_members,
                    chat_members_detail: chat_members_detail,
                    chat_owner: chat_owner,
                    chat_created_at: this.firebaseInstance.serverTimestamp(),
                    state: 'A',
                    chat_destination_type: 'PEOPLE',
                    chat_last_message: messageText,
                    chat_seen: false
                }

                let chat_doc =
                    await SocialLocalChatServices.INSTANCE.saveRemoteChat(chat);
                if (chat_doc.id) {
                    chat_id = chat_doc.id;
                    this.chat_id = chat_doc.id;
                    this._findMessages();
                }
            }

            let message: ChatMessageModel = {
                chat_id: chat_id,
                message_text: messageText.trim(),
                message_created_at: this.firebaseInstance.serverTimestamp(),
                message_owner: {
                    name: AuthService.userLoggedData.userFullName,
                    pic: AuthService.userLoggedData.userPic,
                    uuid: AuthService.userLoggedData.userIdentifier
                },
                state: 'A'
            }

            this.setState({
                sendingMessage: false,
            }, () => {
                SocialLocalChatServices
                    .INSTANCE.saveRemoteMessage(message)
                    .then(res => {
                        SocialLocalChatServices
                            .INSTANCE
                            .updateRemoteChat(chat_id,
                                {
                                    chat_last_message: messageText,
                                    chat_last_message_date: this.firebaseInstance.serverTimestamp(),
                                    chat_seen: false
                                })
                    })
            })

        })



    }

}