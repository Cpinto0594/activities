import React from 'react';
import { View, TouchableHighlight, FlatList, Image, TouchableOpacity, Dimensions, Animated } from 'react-native';
import TextView from '../../../components/TextView/TextView';
import { HeaderButtons } from '../../main/navigation.config';
import { COLORS } from '../../../config/colors/colors';
import { IonIcons, FeatherIcons, FaIcons, MaterialIcons } from '../../../utils/Icons';
import { IonIconEnum, FeatherIconsEnum, FaIconsEnum, MaterialIconsEnum } from '../../../utils/IconsEnum';
import UsuariosServices from '../../../services/usuarios.services/usuarios.services';
import SocialConnectionsServices from '../../../services/social.connections.services/social.connections.services';
import { AuthService } from '../../../services/AuthService';
import PropTypes from 'prop-types';
import Utils from '../../../utils/Utils';
import { SocialPeopleTemplateLoading } from '../../../components/loadingTemplates/loading.templates';
import RBSheet from "react-native-raw-bottom-sheet";
import Separator from '../../main/separator.view';
import Button from '../../../components/Button/Button';
import { SearchBar } from '../../../components/SearchBar/SearchBar';
import { EventsComponent } from '../../../components/EventsComponents/EventsComponent';
import NotificationServices from '../../../services/NotificationsServices';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import ActionButton from 'react-native-action-button';
import { containerStyles, boxShadowActionButton } from '../../../utils/Styles';
import { CustomListComponent } from '../../../components/List/CustomList';
import { NavigationActions } from 'react-navigation';
import { SocialLocalChatServices } from '../../../services/social.chats.services/social.chats.services';
import { ChatUserModel, ChatModel } from '../../../models/ChatModel';



export class ConnectionsScreen extends React.Component {

    private connectionsServices: SocialConnectionsServices;
    private notificationServices: NotificationServices;

    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerLeft={
                        <HeaderButtons
                            action={() => navigation.goBack(null)}>
                            {FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)}
                        </HeaderButtons>
                    }
                    headerTitle={'Conexiones'}

                />
        }
    }

    private arrTabsNames = {
        people: 'Personas',
        groups: 'Grupos'
    }

    constructor(props) {
        super(props);

        this.state = {
            currentTab: 'people',
            arrPeople: [],
            arrGroups: [],
            searchingText: null,
            loadingData: true,
            refreshing: false,
            windowWidth: Dimensions.get('window').width
        }

        this.connectionsServices = new SocialConnectionsServices;
        this.notificationServices = new NotificationServices;
    }

    componentDidMount() {
        this._findPeople();
    }





    render() {

        return (
            <View style={{
                backgroundColor: COLORS.listBackgroundColor,
                flex: 1
            }}  >
                <View style={{
                    flexDirection: 'row',
                    backgroundColor: COLORS.primaryColor
                }}>

                    <TouchableHighlight style={[{
                        padding: 15,
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        //borderBottomColor: '#cdcdcd',
                        //borderBottomWidth: 0.3
                    },
                    this.state.currentTab === 'people' ?
                        {
                            borderBottomColor: COLORS.secondaryColor,
                            borderBottomWidth: 3,
                        } : null
                    ]}
                        underlayColor={'#cdcdcd'}
                        activeOpacity={0.7}
                        onPress={() => { this._changeTab('people') }}>
                        <TextView textValue={'Personas'} />
                    </TouchableHighlight>

                    <TouchableHighlight style={[{
                        padding: 15,
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        //borderBottomColor: '#cdcdcd',
                        //borderBottomWidth: 0.3
                    },
                    this.state.currentTab === 'groups' ?
                        {
                            borderBottomColor: COLORS.secondaryColor,
                            borderBottomWidth: 3,
                        } : null
                    ]}
                        underlayColor={'#cdcdcd'}
                        activeOpacity={0.7}
                        onPress={() => { this._changeTab('groups') }}>
                        <TextView textValue={'Grupos'} />
                    </TouchableHighlight>

                </View>
                {
                    this._headerSearchbarGroups()
                }

                <View style={{
                    flex: 1
                }}>
                    {
                        this.state.currentTab === 'people' &&
                        <CustomListComponent
                            data={this.state.arrPeople}
                            keyExtractor={(item) => String(item.uuid)}
                            renderItem={this._userItem}
                            initialNumToRender={10}
                            maxToRenderPerBatch={5}
                            showsVerticalScrollIndicator={false}
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                            loadingTemplate={() =>
                                <View style={{
                                    paddingVertical: 5,
                                    alignItems: 'center'
                                }}>
                                    <SocialPeopleTemplateLoading width={this.state.windowWidth - (containerStyles.marginHorizontal * 2)} />
                                </View>
                            }
                            showLoadingTemplate={this.state.loadingData}
                        />
                    }
                    {
                        this.state.currentTab === 'groups' &&
                        <CustomListComponent
                            data={this.state.arrGroups}
                            keyExtractor={(item) => String(item.uuid)}
                            renderItem={this._groupsItem}
                            initialNumToRender={10}
                            maxToRenderPerBatch={5}
                            showsVerticalScrollIndicator={false}
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                            loadingTemplate={() =>
                                <View style={{
                                    paddingVertical: 5,
                                    alignItems: 'center'
                                }}>
                                    <SocialPeopleTemplateLoading width={this.state.windowWidth - (containerStyles.marginHorizontal * 2)} />
                                </View>
                            }
                            showLoadingTemplate={this.state.loadingData}

                        />
                    }
                </View>

                {
                    this.state.currentTab === 'groups' &&
                    <ActionButton
                        buttonColor={COLORS.contrastColor}
                        offsetX={containerStyles.marginHorizontal}
                        offsetY={10}
                        onPress={this._handleNavigateCreateGroup}
                        renderIcon={() => FaIcons.getIcon(FaIconsEnum.PENCIL, 22, COLORS.white)}
                        shadowStyle={{
                            ...boxShadowActionButton.box_shadow
                        }}
                        fixNativeFeedbackRadius={true}
                    />
                }

            </View >
        )
    }

    _onRefresh = () => {
        this.setState({
            refreshing: true,
            page: 1,
            loadingData: true,
        }, () => {
            if (this.state.currentTab === 'groups') {
                this._findGroups();
            } else {
                this._findPeople();
            }
        })
    }

    _emptyComponent = () =>
        <EventsComponent
            type='NODATA'
            legend={
                <View style={{
                    justifyContent: 'center'
                }}>

                    <TextView textValue={'No se encontraron datos.'}
                        styles={{
                            fontSize: 15,
                            fontWeight: 'bold',
                            color: COLORS.secondaryColor
                        }} />
                    <Button type={'light'}
                        onPress={() => {
                            this.setState({
                                loadingData: true,
                                page: 1
                            }, this._findGroups)
                        }}
                        text={'Toca para refrescar'}
                        icon={FaIcons.getIcon(FaIconsEnum.REFRESH, 10, COLORS.midGrey)}
                    />

                </View>
            }
        />


    _headerSearchbarGroups = () =>
        <View style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            backgroundColor: COLORS.primaryColor,
            paddingHorizontal: containerStyles.marginHorizontal
        }}>

            <SearchBar
                placeHolder={`Buscar ${this.arrTabsNames[this.state.currentTab]}`}
                placeHolderTextColor={COLORS.midSoftGrey}
                style={{
                    backgroundColor: COLORS.primaryColor,
                }}
                inputContainerStyles={{
                    backgroundColor: COLORS.white,
                    color: COLORS.secondaryColor,
                    borderColor: 'transparent'
                }}
                searchIcon={FaIcons.getIcon(FaIconsEnum.SEARCH, 20, COLORS.secondaryColor)}
                closeBarIcon={FaIcons.getIcon(FaIconsEnum.CLOSE_X, 20, COLORS.secondaryColor)}
            />

        </View>



    _userItem = ({ item }) =>
        <ItemSocialConnectionUser user={item} navigation={this.props.navigation} />

    _groupsItem = ({ item }) =>
        <ItemSocialConnectionsGroup group={item} navigation={this.props.navigation}
            onDelete={this._handleGroupDelete} />

    _contentLoadingTemplate = () =>
        <SocialPeopleTemplateLoading width={this.state.windowWidth} />

    _changeTab = (tab) => {

        this.setState({
            currentTab: tab,
            loadingData: tab === 'groups' && !this.state.arrGroups.length

        }, () => {
            const { currentTab } = this.state;
            if (currentTab === 'groups' && !this.state.arrGroups.length) {

                this._findGroups();
            }
        })
    }

    _findPeople = () => {

        let params = {
            page: 1,
            max_rows: 10
        }


        this.connectionsServices
            .findUserPeople(AuthService.userLoggedData.userIdentifier, params)
            .then(res => {
                if (res.success) {
                    this.setState({
                        arrPeople: res.data,
                        loadingData: false,
                        refreshing: false
                    })
                } else {
                    throw Error(res.message);
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loadingData: false,
                    refreshing: false
                })
            })

    }

    _findGroups = () => {

        let params = {
            page: 1,
            max_rows: 10
        }

        this.connectionsServices.findUserGroups(AuthService.userLoggedData.userIdentifier, params)
            .then(res => {
                if (res.success) {
                    this.setState({
                        arrGroups: res.data,
                        loadingData: false,
                        refreshing: false
                    })
                } else {
                    throw Error(res.message);
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loadingData: false,
                    refreshing: false,
                    arrGroups: []
                })
            })

    }

    _handleNavigateCreateGroup = () => {
        Utils.defaultNavigation(this.props.navigation, 'GroupCreation', {
            onBack: (group) => {
                this.setState({
                    arrGroups: [group, ...this.state.arrGroups]
                })
            }
        })
    }

    _handleGroupDelete = (group) => {
        this.notificationServices.confirm('Esta acción no podrá ser reversada', () => {

            //let user = AuthService.userLoggedData.userIdentifier;
            let _group = group.uuid;

            this.connectionsServices.deleteGroup(_group)
                .then(res => {
                    if (res.success) {

                        let arrGroups = this.state.arrGroups;
                        Utils.arrayDeleteItem(arrGroups, (item) => item.uuid === _group);
                        this.setState({
                            arrGroups
                        })

                    } else {
                        throw Error(res.message)
                    }
                })
                .catch(err => {
                    console.log(err)
                    this.notificationServices.fail('No se pudo eliminar grupo');
                })

        }, undefined, '¿Desea eliminar grupo?')
    }

}

export class ItemSocialConnectionUser extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            user: props.user
        }
    }

    shouldComponentUpdate() {
        return false;
    }

    render() {

        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.white,
            }}>
                <View style={{
                    paddingVertical: 10,
                    alignItems: 'center',
                    paddingHorizontal: containerStyles.marginHorizontal,
                    flexDirection: 'row',
                }}>
                    <View style={{
                        flexDirection: 'row',
                        flex: 1
                    }}>
                        <View style={{
                            borderRadius: 55 / 2,
                            overflow: 'hidden',
                            width: 55,
                            height: 55,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            {
                                this.state.user.pic ?
                                    <Image source={{ uri: this.state.user.pic }} style={{
                                        width: 55,
                                        height: 55
                                    }} /> :
                                    <Image source={require('../../../../assets/user_image.png')} style={{
                                        width: 55,
                                        height: 55
                                    }} />
                            }
                        </View>

                        <View style={{
                            flex: 1,
                            borderBottomColor: COLORS.headerBorderBottomColor,
                            borderBottomWidth: 1,

                            flexDirection: 'row',
                            height: 55,
                        }}>
                            <TouchableOpacity
                                onPress={() => {
                                    Utils.defaultNavigation(this.props.navigation, 'UserProfile', {
                                        user: this.state.user
                                    })
                                }}
                                style={{
                                    flex: 1
                                }}
                            >
                                <View style={{
                                    justifyContent: 'space-evenly',
                                    marginLeft: 10,
                                    flex: 1,
                                }}>

                                    <TextView textValue={this.state.user.nombre_completo}
                                        styles={{
                                            fontSize: 15,
                                            fontWeight: 'bold'
                                        }} /> 
                                    <TextView textValue={this.state.user.email}
                                        styles={{
                                            fontSize: 12
                                        }} />

                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {
                        this.state.user.uuid !== AuthService.userLoggedData.userIdentifier &&
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>

                            <TouchableHighlight
                                style={{
                                    padding: 10,
                                    width: 40,
                                    height: 40,
                                    borderRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}
                                activeOpacity={0.7}
                                underlayColor={'#cdcdcd'}
                                onPress={() => {
                                    //this.refs.buttonSheet.open()
                                    this.navigateToChat()
                                }}
                            >
                                {
                                    MaterialIcons.getIcon(MaterialIconsEnum.CHAT, 20, COLORS.secondaryColor)
                                }
                            </TouchableHighlight>

                        </View>
                    }
                </View>

            </View>
        )
    }

    navigateToChat = async () => {

        let destinationIdentifier = this.state.user.uuid;

        let chat: ChatModel =
            await SocialLocalChatServices.INSTANCE.findChatWithUser(destinationIdentifier);

        //Si no existe chat con este usuario, creamos uno nuevo
        if (!chat) {
            let newChatObject =
                SocialLocalChatServices.INSTANCE.newChatResource({
                    pic: AuthService.userLoggedData.userPic,
                    nombre_completo: AuthService.userLoggedData.userFullName,
                    uuid: AuthService.userLoggedData.userIdentifier
                }, this.state.user);

            //Navegamos al chat
            Utils.defaultNavigationReplace(this.props.navigation, 'ChatView',
                newChatObject);

        } else {
            //Si existe vamos al chat
            let destinationData = chat.chat_members_detail
                .find(member => member.uuid === destinationIdentifier);

            //Navegamos al chat
            Utils.defaultNavigationReplace(this.props.navigation, 'ChatView',
                {
                    resource: {
                        chat_id: chat.chat_id,
                        chat_title: destinationData.name,
                        chat_pic: destinationData.pic
                    }
                });
        }

    }

    static propTypes = {
        user: PropTypes.object,
        navigation: PropTypes.object
    }

}

export class ItemSocialConnectionsGroup extends React.Component {
    private opacityAnimation = null;
    private borderAnimation = null;

    constructor(props) {
        super(props);

        this.state = {
            group: props.group
        }

        this.opacityAnimation = new Animated.Value(0);
        this.borderAnimation = new Animated.Value(0);
    }


    shouldComponentUpdate(nextProps, nextState) {
        if (
            (nextState.group.group_members !== this.state.group.group_members) ||
            (nextState.group.group_description !== this.state.group.group_description) ||
            (nextState.group.group_photo !== this.state.group.group_photo)
        ) {
            return true;
        }
        return false;
    }

    componentDidMount() {
        const { group: { isNew }, group } = this.props;

        if (isNew) {
            Animated.timing(this.opacityAnimation, {
                toValue: 1,
                duration: 300,
                delay: 100,
                useNativeDriver: true
            }).start()
            Animated.spring(this.borderAnimation, {
                toValue: 1,
                speed: 5,
                velocity: 10,
                bounciness: 10,
                useNativeDriver: true
            }).start()
            group.isNew = false;
        } else {
            this.opacityAnimation.setValue(1)
            this.borderAnimation.setValue(1)
        }
    }


    render() {

        return (
            <Animated.View style={{
                flex: 1,
                backgroundColor: COLORS.white,
                paddingHorizontal: containerStyles.marginHorizontal,
                transform: [
                    {
                        scale: this.borderAnimation.interpolate({
                            inputRange: [0, 0.5, 1],
                            outputRange: [0, 0.5, 1]
                        })
                    }
                ],
                opacity: this.opacityAnimation.interpolate({
                    inputRange: [0, 0.5, 1],
                    outputRange: [0, 0.5, 1]
                })
            }}>

                <TouchableOpacity
                    style={{
                        paddingVertical: 5
                    }}
                    onPress={() => {
                        Utils.defaultNavigation(this.props.navigation, 'GroupProfile', {
                            group: this.state.group
                        })
                    }}>
                    <View style={{
                        flexDirection: 'row',
                        paddingVertical: 5,
                        alignItems: 'center'
                    }}>
                        <View style={{
                            borderRadius: 55 / 2,
                            overflow: 'hidden',
                            width: 55,
                            height: 55,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            {
                                this.state.group.group_photo ?
                                    <Image source={{ uri: this.state.group.group_photo }} style={{
                                        width: 55,
                                        height: 55
                                    }} /> :
                                    <Image source={require('../../../../assets/group_image.png')} style={{
                                        width: 55,
                                        height: 55
                                    }} />
                            }
                        </View>

                        <View style={{
                            flex: 1,
                            justifyContent: 'flex-start',
                            paddingLeft: 10,
                            paddingVertical: 5,
                            borderBottomColor: COLORS.headerBorderBottomColor,
                            borderBottomWidth: 1,
                            flexDirection: 'row'
                        }}>

                            <View style={{
                                justifyContent: 'center',
                                flex: 1
                            }}>
                                <TextView textValue={this.state.group.group_description}
                                    styles={{
                                        fontSize: 15,
                                        fontWeight: 'bold'
                                    }} />
                                <TextView textValue={`Miembros: `}
                                    styles={{
                                        fontSize: 12,
                                        fontWeight: 'bold'
                                    }} >
                                    <TextView textValue={String(this.state.group.group_members)}>

                                    </TextView>
                                </TextView>

                                {
                                    !!this.state.group.is_group_owner &&
                                    <TextView textValue={`Eres Dueño`}
                                        styles={{
                                            fontSize: 12,
                                            fontWeight: 'bold',
                                            color:COLORS.labelSuccess
                                        }} >
                                    </TextView>
                                }
                            </View>

                            <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>

                                <TouchableHighlight
                                    style={{
                                        padding: 10,
                                        height: 30,
                                        width: 30,
                                        borderRadius: 15,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                    activeOpacity={0.7}
                                    underlayColor={'#cdcdcd'}
                                    onPress={() => {
                                        this.refs.buttonSheet.open()
                                    }}
                                >
                                    {
                                        IonIcons.getIcon(IonIconEnum.DOTS_MORE, 20, COLORS.secondaryColor)
                                    }
                                </TouchableHighlight>

                            </View>
                        </View>

                    </View>
                </TouchableOpacity>
                <RBSheet
                    ref='buttonSheet'
                    duration={200}
                    height={200}
                    animationType={'fade'}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {
                            justifyContent: "flex-start",
                            alignItems: "flex-start",
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10
                        },
                        wrapper: {
                            // backgroundColor: 'rgba(212,217,219,0.6)'

                        }
                    }}
                >
                    <View style={{
                        backgroundColor: COLORS.white,
                        paddingVertical: 20,
                        paddingHorizontal: containerStyles.marginHorizontal,
                        width: '100%'
                    }}>
                        <View style={{
                            height: 40,
                        }}>
                            <TextView textValue={'¿Que deseas hacer?'} />
                            <Separator styles={{
                                borderColor: COLORS.softGrey,
                                borderWidth: 0.5
                            }} />
                        </View>
                        <View style={{
                            justifyContent: 'flex-start',
                        }}>
                            <Button text={'Enviar Mensaje'} type={'light'}
                                icon={FeatherIcons.getIcon(FeatherIconsEnum.MESSAGE, 20, COLORS.secondaryColor)}
                                buttonColorStyles={{
                                    backgroundColor: 'transparent',
                                }}
                                buttonStyles={{
                                    justifyContent: 'flex-start',
                                    borderWidth: 0,
                                    paddingHorizontal: 0
                                }}
                                textStyles={{
                                    fontSize: 16,
                                    color: COLORS.secondaryColor
                                }}
                                onPress={() => {
                                    this.refs.buttonSheet.close()
                                    this.navigateToChat();
                                }} />
                            <Button text={'Modificar'} type={'light'}
                                icon={FeatherIcons.getIcon(FeatherIconsEnum.EDIT, 20, COLORS.secondaryColor)}
                                buttonColorStyles={{
                                    backgroundColor: 'transparent',

                                }}
                                buttonStyles={{
                                    justifyContent: 'flex-start',
                                    borderWidth: 0,
                                    paddingHorizontal: 0
                                }}
                                textStyles={{
                                    fontSize: 16,
                                    color: COLORS.secondaryColor
                                }}
                                onPress={() => {
                                    this.refs.buttonSheet.close()
                                    this.navigateEditGroup();
                                }} />
                            {
                                !!this.state.group.is_group_owner &&
                                <Button text={'Eliminar Grupo'} type={'light'}
                                    icon={FeatherIcons.getIcon(FeatherIconsEnum.TRASH, 20, COLORS.secondaryColor)}
                                    buttonColorStyles={{
                                        backgroundColor: 'transparent',

                                    }}
                                    buttonStyles={{
                                        justifyContent: 'flex-start',
                                        borderWidth: 0,
                                        paddingHorizontal: 0
                                    }}
                                    textStyles={{
                                        fontSize: 16,
                                        color: COLORS.secondaryColor
                                    }}
                                    onPress={() => {
                                        this.refs.buttonSheet.close()
                                        this.deleteGroup()
                                    }} />
                            }
                            {
                                !(!!this.state.group.is_group_owner) &&
                                <Button text={'Dejar Grupo'} type={'light'}
                                    icon={FeatherIcons.getIcon(FeatherIconsEnum.EYE_OFF, 20, COLORS.secondaryColor)}
                                    buttonColorStyles={{
                                        backgroundColor: 'transparent',

                                    }}
                                    buttonStyles={{
                                        justifyContent: 'flex-start',
                                        borderWidth: 0,
                                        paddingHorizontal: 0
                                    }}
                                    textStyles={{
                                        fontSize: 16,
                                        color: COLORS.secondaryColor
                                    }}
                                    onPress={() => {
                                        this.refs.buttonSheet.close()

                                    }} />
                            }
                        </View>

                    </View>

                </RBSheet>

            </Animated.View>
        )
    }

    navigateToChat = () => {
        Utils.navigateToSubRoute(this.props.navigation, 'Chat', 'ChatView', {
            resource: {
                title: this.state.group.descripcion,
                resource_id: this.state.group.uuid,
                type: 'group',
                caller: this.props.navigation.state.routeName
            },
        })
    }

    navigateEditGroup = () => {
        Utils.defaultNavigation(this.props.navigation, 'GroupCreation', {
            group: this.state.group,
            onBack: (group) => {
                this.setState({
                    group
                })
            }
        })
    }

    deleteGroup = () => {
        let ondelete = this.props.onDelete;
        ondelete && ondelete(this.state.group);
    }

    static propTypes = {
        group: PropTypes.object,
        navigation: PropTypes.object,
        onDelete: PropTypes.func
    }

}