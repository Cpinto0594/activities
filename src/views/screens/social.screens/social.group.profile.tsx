import React from 'react';
import { View, ImageBackground, ActivityIndicator, TouchableOpacity } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import TextView from '../../../components/TextView/TextView';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { COLORS } from '../../../config/colors/colors';
import { HeaderButtons } from '../../main/navigation.config';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { boxShadowSocialUserProfile } from '../../../utils/Styles';
import ProgressiveImage from '../../../components/Image/ProgressiveImage';

export class SocialGroupProfile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            group: props.navigation.getParam('group') || {}
        }
    }

    render() {
        return (
            <View style={{
                flex: 1
            }}>
                <FlatList
                    data={[]}
                    keyExtractor={(item) => item.id}
                    renderItem={this._renderPosts}
                    ListHeaderComponent={
                        <View style={{
                        }}>

                            <ImageBackground source={require('../../../../assets/social_profile_background_image.jpeg')}
                                style={{
                                    width: '100%',

                                }}
                                blurRadius={0.3}>

                                <View style={{
                                    backgroundColor: 'rgba(0,0,0, 0.5)'
                                }}>

                                    <DefaultCustomHeader
                                        style={{
                                            backgroundColor: 'transparent',
                                            height: 40
                                        }}
                                        textColor={COLORS.white}
                                        headerTitle={null}
                                        headerLeft={
                                            <HeaderButtons

                                                action={() => this.props.navigation.goBack(null)}
                                            >
                                                {
                                                    FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 23, COLORS.white)
                                                }
                                            </HeaderButtons>
                                        }
                                    />
                                    <View style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        paddingVertical: 10
                                    }}>
                                        <View style={{
                                            borderRadius: 50,
                                            width: 100,
                                            height: 100,
                                            borderColor: COLORS.white,
                                            borderWidth: 2,
                                            overflow: 'hidden',
                                            ...boxShadowSocialUserProfile.box_shadow,
                                        }}>

                                            <ProgressiveImage
                                                style={{
                                                    width: 100,
                                                    height: 100
                                                }}
                                                source={{ uri: this.state.group.group_photo || null }}
                                               loading
                                            />

                                        </View>

                                    </View>
                                    <View style={{
                                        width: 100 + '%',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                        <View>
                                            <TextView textValue={this.state.group.group_description}
                                                styles={{
                                                    color: COLORS.white,
                                                    fontWeight: 'bold'
                                                }} />
                                        </View>
                                    </View>

                                    <View style={{
                                        marginTop: 5,
                                        width: '100%',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        paddingHorizontal: 10,
                                        backgroundColor: COLORS.white,
                                        flexDirection: 'row',
                                        borderBottomColor: COLORS.midGrey,
                                        borderBottomWidth: 0.5,
                                        paddingVertical: 5
                                    }}>
                                        <View style={{
                                            flex: 1
                                        }}>
                                            <TouchableOpacity style={{
                                                paddingHorizontal: 10,
                                                paddingVertical: 5,
                                                alignItems: 'center',
                                                borderRightColor: COLORS.midGrey,
                                                borderRightWidth: 0.5
                                            }}>
                                                <TextView textValue={String(this.state.group.group_members)}
                                                    styles={{
                                                        color: COLORS.secondaryColor,
                                                        fontWeight: 'bold',
                                                        fontSize: 13
                                                    }} />
                                                <TextView textValue={'Miembros'}
                                                    styles={{
                                                        color: COLORS.midSoftGrey,
                                                        fontSize: 11
                                                    }} />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={{
                                            flex: 1,
                                            paddingVertical: 5,
                                            paddingHorizontal: 10,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                            <TextView textValue={String(this.state.group.group_members)}
                                                styles={{
                                                    color: COLORS.secondaryColor,
                                                    fontWeight: 'bold',
                                                    fontSize: 13
                                                }} />
                                            <TextView textValue={'Publicaciones'}
                                                styles={{
                                                    color: COLORS.midSoftGrey,
                                                    fontSize: 11
                                                }} />
                                        </View>


                                    </View>
                                </View>
                            </ImageBackground>

                        </View>
                    }
                />
            </View>
        )
    }

    _renderPosts = ({ item }) =>
        <View style={{
            flex: 1,
            padding: 10,
            borderColor: COLORS.midGrey,
            borderWidth: 1
        }}>

        </View>


}