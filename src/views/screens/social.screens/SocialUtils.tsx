import { DateUtils } from "../../../utils/DateUtils";

export class SocialPostsUtils {


    static durationOfPost(date) {
        
        let dateObject = DateUtils.toDate(date);
        let now = DateUtils.now();
        let duration = DateUtils.diffDuration(now, dateObject);
        let hours = duration.asHours();
        let minutes = duration.asMinutes();
        let days = duration.asDays();
        let months = duration.asMonths();
        let years = duration.asYears();
        let dateYear = dateObject.year();
        let currentYear = now.year();
        let text = 'Hace unos momentos';


        //console.log(hours, days, years, currentYear, dateYear)
        if (years > 1 || (currentYear > dateYear)) {
            //Mostramos fecha normal
            text = DateUtils.dateToString(date, DateUtils.SHORT_PRETTY_DATEFORMAT + ' ' + DateUtils.TIMEFORMAT)
        } else if (months > 1) {
            //Mostramos dia y mes del año con hora
            text = DateUtils.dateToString(date, DateUtils.SHORT_PRETTY_DATEFORMAT_NOYEAR + ' ' + DateUtils.TIMEFORMAT)
        } else if (days > 1) {
            //Mostramos hace cuantos dias
            text = Math.floor(days) + 'd';
        } else if (hours > 1) {
            //Mostramos hace cuantas
            text = Math.floor(hours) + 'h';
        } else if (minutes > 10) {
            //mostramos hace cuantos minutos
            text = Math.floor(minutes) + 'm';
        }
        return text;
    }


}