import React from 'react';
import { View } from 'react-native';
import { COLORS } from '../../../config/colors/colors';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { CustomListComponent } from '../../../components/List/CustomList';
import { ItemUserSelectable } from './components/item.user.selectable';
import ProgressiveImage from '../../../components/Image/ProgressiveImage';
import TextView from '../../../components/TextView/TextView';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { HeaderButtons } from '../../main/navigation.config';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { boxShadow, containerStyles } from '../../../utils/Styles';
import InputText from '../../../components/InputText/InputText';
import FireBaseExpo from '../../../FunctionaityProviders/Firebase/FirebaseExpo';
import { AuthService } from '../../../services/AuthService';
import Utils from '../../../utils/Utils';
import { StackActions, NavigationActions } from 'react-navigation';
import { CircularImage } from '../../../components/CircularImage/circular_image';

export class SocialChatGroupCreation extends React.Component {
    private firebaseInstance: FireBaseExpo = FireBaseExpo.instance;

    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Confirmación'}
                    headerLeft={
                        <HeaderButtons
                            action={() => { navigation.goBack(null) }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                    headerRight={
                        <HeaderButtons
                            style={{
                                width: 50,
                                alignItems: 'flex-start'
                            }}
                            disabled={navigation.getParam('disableSave', true)}
                            action={() => {
                                let action = navigation.getParam('saveAction');
                                action && action();
                            }}
                        >
                            <TextView textValue={'Hecho'} styles={{
                                fontSize: 12,
                                fontWeight: 'bold',
                                color: COLORS.secondaryColor
                            }} />
                        </ HeaderButtons>
                    }

                />

        }
    }

    constructor(props) {
        super(props);
        this.state = {
            group: {},
            arrPeople: props.navigation.getParam('arrPeople', []),
        }
        this.state.selected = this.state.arrPeople.filter(itm => itm.selected).length
    }

    componentDidMount() {
        this.props.navigation.setParams({
            saveAction: () => {
                this._saveChatAndGo();
            },
            disableSave: this.state.selected <= 0 || !this.state.group.description
        })
    }

    render() {

        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.listBackgroundColor,

            }}>

                <View style={{
                    paddingHorizontal: 16,
                    paddingVertical: containerStyles.marginHorizontal,
                    backgroundColor: COLORS.primaryColor,
                    ...boxShadow.box_shadow
                }}>

                    <View style={{
                        flexDirection: 'row'
                    }}>

                        <TouchableOpacity
                            onPress={() => {

                            }}>
                            <CircularImage
                                width={60}
                                height={60}
                                style={{
                                    backgroundColor: COLORS.white
                                }}
                                source={
                                    this.state.group.pic ?
                                        { uri: this.state.group.pic } :
                                        require('../../../../assets/group_image.png')
                                }
                            />
                        </TouchableOpacity>


                        <View style={{
                            flex: 1,
                            paddingLeft: 10,
                            justifyContent: 'center'
                        }}>

                            <InputText
                                value={this.state.group.descripcion}
                                placeholder={'Ingresa nombre para el grupo'}
                                styles={{
                                    borderBottomColor: COLORS.secondaryColor,
                                    borderBottomWidth: 2,
                                    paddingLeft: 5,
                                    fontWeight: 'bold'
                                }}
                                onChange={(value) => {
                                    this.setState({
                                        group: {
                                            ...this.state.group,
                                            description: value
                                        }
                                    }, () => {
                                        this.props.navigation.setParams({
                                            disableSave: this.state.selected <= 0 || !value
                                        })
                                    })
                                }}
                            />
                            <TextView
                                textValue={'Debes proveer el nombre del grupo y una imagen (opcional).'}
                                styles={{
                                    marginTop: 5,
                                    color: COLORS.midSoftGrey,
                                    fontSize: 10
                                }} />

                        </View>


                    </View>

                    <View style={{
                        paddingVertical: 5,
                        backgroundColor: COLORS.primaryColor,
                    }}>
                        <View style={{
                            marginVertical: 10,
                        }}>
                            <TextView textValue={`Miembro(s): ${this.state.selected}`}
                                styles={{
                                    color: COLORS.secondaryColor,
                                    fontSize: 12
                                }} />
                        </View>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{
                                flexGrow: 1
                            }}>
                            {
                                this.state.arrPeople
                                    .filter(itm => itm.selected)
                                    .map(usr =>
                                        <View style={{
                                            marginRight: 10,
                                            paddingVertical: 5,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            width: 60

                                        }}
                                            key={usr.uuid}>
                                            <View style={{
                                                height: 40,
                                                width: 40,
                                                borderRadius: 20,
                                                overflow: 'hidden',
                                                justifyContent: 'center',
                                                alignItems: 'center'
                                            }}>
                                                <ProgressiveImage
                                                    source={
                                                        usr.pic ?
                                                            { uri: usr.pic } :
                                                            require('../../../../assets/user_image.png')
                                                    }
                                                    style={{
                                                        width: 40,
                                                        height: 40
                                                    }}
                                                />
                                            </View>
                                            <TextView textValue={usr.nombre_completo}
                                                styles={{
                                                    color: COLORS.secondaryColor,
                                                    fontSize: 9,
                                                    flexWrap: 'wrap'
                                                }}
                                                numberOfLines={1} />

                                        </View>
                                    )
                            }
                        </ScrollView>

                    </View>
                </View>
                <CustomListComponent
                    data={this.state.arrPeople}
                    renderItem={this.renderItem}
                    keyExtractor={(i) => i.uuid}
                />

            </View>
        )

    }

    renderItem = ({ item }) =>
        <ItemUserSelectable user={item}
            userImageContainerStyles={{
                width: 40,
                height: 40,
                borderRadius: 20
            }}
            userImageStyles={{
                width: 40,
                height: 40
            }}
            selected={item.selected}
            onPress={(selected) => {
                item.selected = selected;
                this.setState({
                    arrPeople: [...this.state.arrPeople],
                    selected: this.state.arrPeople.filter(itm => itm.selected).length
                }, () => {

                    this.props.navigation.setParams({
                        disableSave: this.state.selected <= 0 || !this.state.group.description
                    })
                })
            }}
            style={{
                backgroundColor: COLORS.white
            }} />


    _saveChatAndGo = () => {

        let owner = {
            name: AuthService.userLoggedData.userFullName,
            uuid: AuthService.userLoggedData.userIdentifier,
            pic: AuthService.userLoggedData.userPic
        };

        let members = this.state.arrPeople
            .filter(it => it.selected)
            .map(it => ({
                name: it.nombre_completo,
                pic: it.pic,
                uuid: it.uuid
            }));
        members.push(owner);

        let member_ids = members.map(it => it.uuid);

        let group = {
            chat_description: this.state.group.description,
            chat_members_detail: members,
            chat_members: member_ids,
            chat_created_at: this.firebaseInstance.fireStore().FieldValue.serverTimestamp(),
            state: 'A',
            chat_owner: owner,
            pic: '',
            chat_destination_type: 'GROUP'
        }

        this.firebaseInstance
            .db()
            .collection('social_chats')
            .add(group)
            .then(doc => {
                console.log(`Group Chat generated ID : ${doc.id}`)
                //Navegamos al chat
                Utils.navigationFullStackResetWithActions(this.props.navigation , 1 , [
                    NavigationActions.navigate({
                        routeName: 'ModulesDrawer', 
                        action:
                            NavigationActions.navigate({ routeName: 'Chat' })
                    }),
                    NavigationActions.navigate({ routeName: 'ChatView', params: {
                        resource: {
                            chat_uuid: doc.id,
                            chat_title: group.chat_description
                        }
                    } })
                ])
            })
            .catch(err => {
                console.log(err);
            })

    }
}