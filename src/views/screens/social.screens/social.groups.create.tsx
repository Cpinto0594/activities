import React from 'react';
import { View, TouchableOpacity, ScrollView, Platform } from 'react-native';
import { COLORS } from '../../../config/colors/colors';
import { TextField } from 'react-native-material-textfield';
import TextView from '../../../components/TextView/TextView';
import ProgressiveImage from '../../../components/Image/ProgressiveImage';
import { HitTestResultTypes } from 'expo/build/AR';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { FaIcons, IonIcons } from '../../../utils/Icons';
import { FaIconsEnum, IonIconEnum } from '../../../utils/IconsEnum';
import Separator from '../../main/separator.view';
import { HeaderButtons } from '../../main/navigation.config';
import Utils from '../../../utils/Utils';
import ButtonPanel from '../../../components/Button/ButtonPanel';
import ImageLoader from '../../../FunctionaityProviders/image_camerapicker/image_camera_picker';
import SocialConnectionsServices from '../../../services/social.connections.services/social.connections.services';
import { AuthService } from '../../../services/AuthService';
import NotificationServices from '../../../services/NotificationsServices';
import { containerStyles } from '../../../utils/Styles';


export class SocialGroupsCreate extends React.Component {
    private imageLoader: ImageLoader;
    private connectionServices: SocialConnectionsServices;
    private notificationServices: NotificationServices;


    static navigationOptions = ({ navigation }) => {
        return {
            header: <DefaultCustomHeader
                headerLeft={
                    <HeaderButtons
                        action={() => {
                            navigation.goBack(null);
                        }}>
                        {
                            FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                        }
                    </HeaderButtons>
                }
                headerRight={null}
                headerTitle={'Crear Grupo'}
            />
        }
    }


    constructor(props) {
        super(props);
        this.state = {
            group: props.navigation.getParam('group', {
                members: [],
                group_privacy: 1
            })
        }
        this.state.group.members_length = this.state.group.group_members - 1;

        this.imageLoader = new ImageLoader
        this.connectionServices = new SocialConnectionsServices;
        this.notificationServices = new NotificationServices;
    }

    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.white,

            }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{
                        flex: 1,
                        paddingVertical: 10,
                        paddingHorizontal: containerStyles.marginHorizontal
                    }}>

                        <View style={{
                            paddingVertical: 10,
                            width: 100 + '%'
                        }}>
                            <TextField
                                ref='codigo'
                                label={'Nombre del Grupo'}
                                value={this.state.group.group_description}
                                tintColor={COLORS.secondaryColor}
                                baseColor={COLORS.secondaryColor}
                                animationDuration={200}
                                onChangeText={(value) =>
                                    this.setState({ group: { ...this.state.group, group_description: value } })}
                                onBlur={() => {

                                }}
                                labelHeight={15}
                            />
                        </View>
                        <View style={{
                            paddingVertical: 10,
                            justifyContent: 'flex-start',
                            width: 100 + '%',
                        }}>
                            <View style={{
                                marginBottom: 10,
                            }}>
                                <TextView textValue={'Seleccionar Imagen'} styles={{
                                    fontWeight: 'bold',
                                    fontSize: 15
                                }} />
                            </View>

                            <View style={{
                                width: 100 + '%'
                            }}>

                                <TouchableOpacity style={{
                                    paddingVertical: 5,
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    flexDirection: 'row'
                                }}
                                    onPress={this._openImagePicker}>
                                    <View style={{
                                        width: 30,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                        {
                                            this.state.group.group_photo ?
                                                <View style={{
                                                    width: 30,
                                                    height: 30,
                                                    borderRadius: 10,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    overflow: 'hidden'
                                                }}>
                                                    <ProgressiveImage
                                                        source={{ uri: this.state.group.group_photo }}
                                                        style={{
                                                            width: 30,
                                                            height: 30
                                                        }} />
                                                </View>
                                                :
                                                FaIcons.getIcon(FaIconsEnum.IMAGE, 26, COLORS.secondaryColor)
                                        }
                                    </View>
                                    <TextView textValue={'Escoge la foto del grupo'} styles={{
                                        color: COLORS.secondaryColor,
                                        fontWeight: 'bold',
                                        marginLeft: 10
                                    }} />
                                </TouchableOpacity>
                            </View>

                        </View>
                        <Separator />
                        <View style={{
                            paddingVertical: 10,
                            justifyContent: 'flex-start',
                            width: 100 + '%',
                        }}>
                            <View style={{
                                marginBottom: 10,
                            }}>
                                <TextView textValue={'Invitar Miembros'} styles={{
                                    fontWeight: 'bold',
                                    fontSize: 15
                                }} />
                            </View>
                            <View>
                                <TouchableOpacity style={{
                                    paddingVertical: 5,
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    flexDirection: 'row'
                                }}
                                    onPress={this._openMemberPicker}
                                >
                                    <View style={{
                                        width: 30,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                        {
                                            FaIcons.getIcon(FaIconsEnum.USERS_FULL, 25, COLORS.secondaryColor)
                                        }
                                    </View>
                                    <View style={{
                                        flex: 1,
                                        marginLeft: 10
                                    }}>
                                        <TextView textValue={'Escoge amigos'} styles={{
                                            color: COLORS.secondaryColor,
                                            fontWeight: 'bold',
                                        }} />
                                        {
                                            this.state.group.members_length ?
                                                <TextView textValue={`Tú y ${this.state.group.members_length} miembro(s) más.`} styles={{
                                                    color: COLORS.midSoftGrey,
                                                }} /> : null
                                        }
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <Separator />
                        <View style={{
                            paddingVertical: 10,
                            justifyContent: 'flex-start',
                            width: 100 + '%',
                        }}>
                            <View style={{
                                marginBottom: 10,
                            }}>
                                <TextView textValue={'Privacidad'} styles={{
                                    fontWeight: 'bold',
                                    fontSize: 15
                                }} />
                            </View>

                            <TouchableOpacity style={{
                                paddingVertical: 5,
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                flexDirection: 'row',
                                width: 100 + '%',

                            }}
                                onPress={() => {
                                    this._selectPrivacity(1)
                                }}
                            >
                                <View style={{
                                    width: 30,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {
                                        FaIcons.getIcon(FaIconsEnum.WORLD, 25, COLORS.secondaryColor)
                                    }
                                </View>
                                <View style={{
                                    flex: 1,
                                    paddingLeft: 10,
                                }}>
                                    <TextView textValue={'Público'} styles={{
                                        color: COLORS.secondaryColor,
                                        fontWeight: 'bold',

                                    }} />
                                    <TextView
                                        textValue={'Todos pueden encontrar el grupo, ver sus integrantes y las publicaciones que en el se han hecho.'}
                                        styles={{
                                            color: COLORS.midSoftGrey,
                                            fontSize: 10
                                        }} />
                                </View>
                                <View style={{
                                    width: 30,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {
                                        IonIcons.getIcon(+this.state.group.group_privacy === 1 ?
                                            IonIconEnum.RADIO_ON :
                                            IonIconEnum.RADIO_OFF, 20, COLORS.secondaryColor)
                                    }
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{
                                paddingVertical: 5,
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                flexDirection: 'row',
                                width: 100 + '%',

                            }}
                                onPress={() => {
                                    this._selectPrivacity(0)
                                }}
                            >
                                <View style={{
                                    width: 30,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {
                                        FaIcons.getIcon(FaIconsEnum.LOCK, 25, COLORS.secondaryColor)
                                    }
                                </View>
                                <View style={{
                                    flex: 1,
                                    paddingLeft: 10,
                                }}>
                                    <TextView textValue={'Privado'} styles={{
                                        color: COLORS.secondaryColor,
                                        fontWeight: 'bold',

                                    }} />
                                    <TextView
                                        textValue={'Solo los miembros pueden ver lo que en el se ha publicado.'}
                                        styles={{
                                            color: COLORS.midSoftGrey,
                                            fontSize: 10
                                        }} />
                                </View>
                                <View style={{
                                    width: 30,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {
                                        IonIcons.getIcon(+this.state.group.group_privacy === 0 ?
                                            IonIconEnum.RADIO_ON :
                                            IonIconEnum.RADIO_OFF, 20, COLORS.secondaryColor)
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <ButtonPanel
                    buttons={[
                        {
                            text: 'Guardar',
                            icon: FaIcons.getIcon(FaIconsEnum.SAVE, 20, COLORS.secondaryColor),
                            action: this._saveGroup,
                            disabled: !this.state.group.group_description
                        }
                    ]}
                    buttonTextStyle={{
                        color: COLORS.secondaryColor
                    }} />
            </View>
        )

    }

    _selectPrivacity = (privacy) => {
        this.setState({
            group: {
                ...this.state.group,
                group_privacy: privacy
            }
        })
    }

    _openImagePicker = () => {

        this.imageLoader.launchGallery({
            quality: 0.7,
        })
            .then(result => {
                //console.log(result)
                //EXPO
                var cancelled = result.cancelled;
                var uri = result.uri;

                //EJECTED   
                // var uri = pickerResult.uri || pickerResult.origURL;
                // var cancelled = pickerResult.didCancel;

                if (cancelled) return;

                let dataImage = {
                    image_name: uri.substring(uri.lastIndexOf('/') + 1, uri.length),
                    image_format: uri.substring(uri.lastIndexOf('.') + 1, uri.length),
                    image_mime: 'image/' + uri.substring(uri.lastIndexOf('.') + 1, uri.length),
                    image_url:
                        Platform.OS === "android" ? uri : uri.replace("file://", ""),
                    image_width: result.width,
                    image_height: result.height,
                    id: new Date().getTime(),

                };

                this.setState({
                    group: {
                        ...this.state.group,
                        group_photo: uri,
                        group_data_photo: dataImage
                    }
                }, () => {
                    //this._togglePublishButton();
                })


            })
    }
    _openMemberPicker = () => {
        Utils.defaultNavigation(this.props.navigation, 'SocialMemberPicker', {
            onBack: (members) => {
                this.setState({
                    group: {
                        ...this.state.group,
                        members_length: members.length,
                        members: members
                    }
                })
            },
            members: this.state.group.members,
            group: this.state.group.uuid
        })
    }


    _saveGroup = () => {
        let group = Object.assign({}, this.state.group);
        let group_members = [...group.members];
        let image = group.group_data_photo;
        let userOwner = AuthService.userLoggedData.userIdentifier;

        delete group['group_data_photo'];

        //Si estamos creando el grupo, asignamos el Identificador del Dueño
        if (!group.id) {
            group.owner_uuid = userOwner;
        }

        if (group_members) {
            group_members = group_members
                .filter(gm => gm.uuid !== userOwner)
                .map(gm => ({
                    group_member_uuid: gm.uuid,
                    group_member_role: 'MEMBER'
                }))
            group.members = group_members;
        }

        let objectRequest = new FormData();

        Object.keys(group).forEach(key => {
            objectRequest.append(key,
                (group[key] instanceof Array)
                    ? JSON.stringify(group[key]) : group[key])
        });

        if (image) {

            objectRequest.append('photo', {
                name: String(image.id),
                type: image.image_mime,
                uri: image.image_url
            });
        }



        let promise = this.state.group.uuid ?
            this.connectionServices.updateGroup(this.state.group.uuid, objectRequest) :
            this.connectionServices.saveGroup(objectRequest);

        promise
            .then(res => {

                if (res.success) {
                    let onBack = this.props.navigation.getParam('onBack');
                    //Retornamos el grupo creado a la pantalla de grupos
                    let group = res.data;
                    group.isNew = true;

                    //Eliminamos el usuario actual del array de miembros
                    let members = group_members;
                    group.members = members;

                    onBack && onBack(group);

                    this.props.navigation.goBack(null);

                } else {
                    throw Error(res.message)
                }

            })
            .catch(err => {
                console.log(err);
                this.notificationServices.fail('No se pudo registrar Grupo')
            })


    }

}