import React from 'react';
import { View } from 'react-native';
import { COLORS } from '../../../../config/colors/colors';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import TextView from '../../../../components/TextView/TextView';
import { FaIcons } from '../../../../utils/Icons';
import { FaIconsEnum } from '../../../../utils/IconsEnum';
import PropTypes from 'prop-types';
import { SocialPostAtttachmentsStyles } from '../social.styles';

export class PostAttachmentsComponent extends React.Component {

    private SocialPostAtttachmentsStyles;




    constructor(props) {
        super(props);
        this.SocialPostAtttachmentsStyles = SocialPostAtttachmentsStyles();
        this.state = {
            items: []
        }
    }


    render() {

        const { items } = this.props;
        this.state.items = items;
        return (

            <View style={{
                flex: 1,
                backgroundColor: COLORS.white,
                marginLeft: 10,
                marginTop:10
            }}>

                <FlatList
                    data={this.state.items}
                    keyExtractor={(item) => String(item.id)}
                    renderItem={this._renderItem}
                    ItemSeparatorComponent={() => <View style={{ height: 5 }}></View>}
                />

            </View>

        )

    }

    _renderItem = ({ item }) =>
        <View style={this.SocialPostAtttachmentsStyles.main_container}
            key={item.id}>
            <TouchableOpacity style={this.SocialPostAtttachmentsStyles.attachment_touchable}>

                <View style={{
                    padding: 5,
                    justifyContent: 'center'
                }}>
                    {this._getIcon(item)}
                </View>

                <View style={this.SocialPostAtttachmentsStyles.attachment_name_container}>
                    <TextView textValue={item.attachment_name.substring(0, item.attachment_name.lastIndexOf('.'))}
                        styles={this.SocialPostAtttachmentsStyles.attachment_name_text_style}
                    />
                    <TextView textValue={'.' + item.attachment_format}
                        styles={this.SocialPostAtttachmentsStyles.attachment_name_text_style}
                    />
                </View>

                {
                    this.props.canDelete &&
                    <View style={{
                        justifyContent: 'center'
                    }}>
                        <TouchableOpacity style={{
                            padding: 5,
                        }}
                            onPress={() => {
                                this.props.onRemove && this.props.onRemove(item)
                            }}>
                            {FaIcons.getIcon(FaIconsEnum.TRASH_EMPTY,
                                this.SocialPostAtttachmentsStyles.attachment_icon_delete_size.height,
                                this.SocialPostAtttachmentsStyles.attachment_icon_delete_size.color)}
                        </TouchableOpacity>
                    </View>
                }
            </TouchableOpacity>

        </View>


    _getIcon(item) {
        let icon = null;
        let icons =
        {
            'doc': FaIconsEnum.WORD,
            'docx': FaIconsEnum.WORD,
            'xls': FaIconsEnum.EXCEL,
            'xlsx': FaIconsEnum.EXCEL,
            'pdf': FaIconsEnum.PDF,
            'rar': FaIconsEnum.ZIP,
            'zip': FaIconsEnum.ZIP,
            'tar.gz': FaIconsEnum.ZIP,
            'txt': FaIconsEnum.FILE,
            'png': FaIconsEnum.IMAGE,
            'jpg': FaIconsEnum.IMAGE,
            'jpeg': FaIconsEnum.IMAGE,
            'mp3': FaIconsEnum.SOUND,
            'mp4': FaIconsEnum.VIDEO,
            'avi': FaIconsEnum.VIDEO,
            'mpeg': FaIconsEnum.VIDEO
        }

        icon = icons[item.attachment_format] || FaIconsEnum.FILE;
        return FaIcons.getIcon(icon, this.SocialPostAtttachmentsStyles.attachment_icon_size.height,
            this.SocialPostAtttachmentsStyles.attachment_icon_size.color);
    }

    static propTypes = {
        items: PropTypes.array,
        onRemove: PropTypes.func,
        canDelete: PropTypes.bool
    }

}