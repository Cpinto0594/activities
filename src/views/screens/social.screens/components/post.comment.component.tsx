import React from 'react';
import SocialPostsServices from '../../../../services/socialposts.services/social.posts.services';
import { View, Image, StyleSheet } from 'react-native';
import { TouchableWithoutFeedback, TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import { FaIcons, IonIcons } from '../../../../utils/Icons';
import { FaIconsEnum, IonIconEnum } from '../../../../utils/IconsEnum';
import { COLORS } from '../../../../config/colors/colors';
import TextView from '../../../../components/TextView/TextView';
import { SocialPostsUtils } from '../SocialUtils';
import PropTypes from 'prop-types';
import { SocialCommentStyles } from '../social.styles';
import { AuthService } from '../../../../services/AuthService';
import { boxShadows } from '../../../../utils/Styles';
import { CircularImage } from '../../../../components/CircularImage/circular_image';


export class PostCommentsComponent extends React.Component {

    private socialPostServices: SocialPostsServices;
    private ComponentStyles;

    constructor(props) {
        super(props);

        this.state = {
            comment: props.comment,
            showingMoreComments: false,
            page: 0,
            loadingComments: false,
            visibleComments: 0
        }

        this.socialPostServices = new SocialPostsServices;
        this.ComponentStyles = SocialCommentStyles();


    }

    componentWillReceiveProps(props) {
        this.setState({
            comment: props.comment,
            showingMoreComments: false,
            visibleComments: 0,
            page: 0,
            // arrAnswerComments: []
        })
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        //Verificamos si algo del item ha cambiado y lo actualizamos
        if ((this.props.comment.post_comments !== nextProps.comment.post_comments) ||
            (this.props.comment.showingAnswers !== nextProps.comment.showingAnswers) ||
            (this.state.showingMoreComments !== nextState.showingMoreComments) ||
            (this.state.loadingComments !== nextState.loadingComments) ||
            // (this.state.arrAnswerComments !== nextState.arrAnswerComments) ||
            (this.state.numberOfComments !== nextState.numberOfComments) ||
            (this.state.comment.post_likes !== nextState.comment.post_likes) ||
            (this.state.comment.liking !== nextState.comment.liking)
        ) {
            return true;
        }
        return false;
    }

    render() {
        const { onClickAnswerComment, onLongPress, isAnswer } = this.props;

        return (
            <View style={[
                this.ComponentStyles.main_container,
                {
                    paddingVertical: 5,
                    //paddingBottom: isAnswer ? 5 : 10
                }
            ]}
            >
                <TouchableWithoutFeedback
                    onLongPress={onLongPress}
                    style={{
                        flex: 1
                    }}>
                    <View style={this.ComponentStyles.comment_container}>

                        <View style={{
                            paddingTop: isAnswer ? 0 : 10
                        }}>

                            <CircularImage
                                source={this.state.comment.author.pic ?
                                    { uri: this.state.comment.author.pic } :
                                    require('../../../../../assets/user_image.png')}
                                style={isAnswer ?
                                    this.ComponentStyles.comment_header_image_pic_answer :
                                    this.ComponentStyles.comment_header_image_pic}
                                imageStyle={isAnswer ?
                                    this.ComponentStyles.comment_header_image_pic_answer :
                                    this.ComponentStyles.comment_header_image_pic}
                                borderRadius={(isAnswer ?
                                    this.ComponentStyles.comment_header_image_pic_answer :
                                    this.ComponentStyles.comment_header_image_pic).width / 2}
                            />
                        </View>
                        <View style={{
                            paddingLeft: 10,
                            flex: 1,
                            alignItems: 'flex-start',
                            justifyContent: 'center'
                        }}>
                            <View style={
                                [this.ComponentStyles.comment_content_container,
                                {
                                    backgroundColor: COLORS.primaryColor,
                                    borderBottomRightRadius: 10,
                                    borderTopRightRadius: 10,
                                    borderBottomLeftRadius: 10,
                                    paddingVertical: 10,
                                }
                                ]}>
                                <TouchableOpacity style={{
                                    padding: 0,
                                    paddingBottom: 3,
                                }}
                                    onPress={() => { }}>
                                    <TextView textValue={this.state.comment.author.nombre_completo}
                                        styles={this.ComponentStyles.comment_username_text_styles} />
                                </TouchableOpacity>
                                <View style={{
                                }}>
                                    <TextView textValue={this.state.comment.post_content}
                                        styles={this.ComponentStyles.comment_content_text_styles} />
                                </View>

                            </View>
                            <View style={{
                                alignItems: 'center',
                                flexDirection: 'row',
                            }}>
                                <View style={{
                                    justifyContent: 'center',
                                    flexDirection: 'row',
                                    paddingVertical: 5
                                }}>
                                    <TextView textValue={SocialPostsUtils.durationOfPost(this.state.comment.post_created_at)}
                                        styles={this.ComponentStyles.comment_indicators_text_style} />
                                    <TextView textValue={' - '}
                                        styles={this.ComponentStyles.comment_indicators_text_style} />
                                </View>

                                <TouchableOpacity style={{
                                    paddingVertical: 10
                                }}
                                    onPress={() => {
                                        onClickAnswerComment &&
                                            onClickAnswerComment(this.state.comment,
                                                this.props.isAnswer ? this.props.parent : null);
                                        this.setState({
                                            comment: {
                                                ...this.state.comment,
                                                showingAnswers: false,
                                                arrAnswerComments: []
                                            },
                                            visibleComments: 0
                                        })
                                    }}>
                                    <TextView textValue={'Responder'}
                                        styles={this.ComponentStyles.comment_indicators_text_style}
                                    />
                                </TouchableOpacity>
                                <TextView textValue={String(' - ')}
                                    styles={this.ComponentStyles.comment_indicators_text_style} />
                                <TouchableOpacity
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        padding: 5,
                                        paddingTop: 10,
                                        paddingBottom: 10,
                                        marginRight: 10
                                    }}
                                    disabled={this.state.comment.liking}
                                    onPress={this._clickLikePost}
                                >

                                    {IonIcons.getIcon(this.state.comment.user_liked ?
                                        IonIconEnum.HEART_FULL :
                                        IonIconEnum.HEART_EMPTY, 12,
                                        this.state.comment.user_liked ? COLORS.secondaryColor : COLORS.secondaryColor)
                                    }
                                    <TextView textValue={String(this.state.comment.post_likes)}
                                        styles={[this.ComponentStyles.comment_indicators_text_style, { marginLeft: 5 }]} />
                                </TouchableOpacity>
                            </View>
                            {
                                this.state.comment.post_comments ?
                                    <View style={{
                                        justifyContent: 'center',
                                    }}>
                                        {
                                            (!this.state.loadingComments &&
                                                this.state.comment.post_comments > 0 &&
                                                this._numberOfHiddenComments() > 0
                                            ) &&
                                            <TouchableOpacity style={{
                                                paddingBottom: 5
                                            }}
                                                onPress={this._showMoreComments.bind(this)}>
                                                {/* // SI HAY COMENTARIOS  */}

                                                < TextView textValue={String(this._numberOfHiddenComments())}
                                                    styles={[
                                                        this.ComponentStyles.comment_indicators_text_style,
                                                        { fontWeight: 'bold' }]}  >
                                                    <TextView textValue={' Respuestas ' + (this.state.showingMoreComments ? 'más' : '')}
                                                        styles={this.ComponentStyles.comment_indicators_text_style} />
                                                </TextView>
                                            </TouchableOpacity>
                                        }

                                        {
                                            // SI YA NO QUEDAN COMENTARIOS
                                            (!this.state.loadingComments &&
                                                this.state.comment.post_comments > 0 &&
                                                this._numberOfHiddenComments() <= 0
                                            ) &&
                                            <TouchableOpacity
                                                onPress={this._resetCommentsView.bind(this)}
                                                style={{
                                                    paddingBottom: 5
                                                }}>
                                                < TextView textValue={'Ocultar Comentarios'}
                                                    styles={[
                                                        this.ComponentStyles.comment_indicators_text_style,
                                                        { fontWeight: 'bold' }]} />
                                            </TouchableOpacity>
                                        }
                                        {
                                            // SI ESTÄ CARGANDO COMENTRIOS
                                            this.state.loadingComments &&
                                            <TextView textValue={'Cargando comentarios'} styles={[
                                                this.ComponentStyles.comment_indicators_text_style,
                                                { fontWeight: 'bold', paddingBottom: 5 }]}  ></TextView>
                                        }

                                    </View> : null
                            }
                            {
                                (this.state.comment.showingAnswers &&
                                    !this.props.isAnswer &&
                                    this.state.comment.arrAnswerComments
                                ) ?
                                    <View style={this.ComponentStyles.comment_subcomments_container}>
                                        {
                                            this.state.comment.arrAnswerComments.map(comm =>
                                                this._renderChildComment(comm, this.state.comment)
                                                //El parent para estos subcomentarios 
                                                //es el comentario actual
                                            )
                                        }
                                    </View> : null
                            }
                        </View>
                    </View>

                </TouchableWithoutFeedback>
                {/* {
                    !isAnswer &&
                    <View style={{ marginTop: 5, opacity: 0.7, width: '90%', borderBottomColor: '#cdcdcd', borderBottomWidth: 0.3, alignSelf: 'center' }}></View>
                } */}
            </View >
        )
    }

    _numberOfHiddenComments = () => {
        return this.state.comment.post_comments - this.state.visibleComments
    }
    _resetCommentsView = () => {
        this.setState({
            showingMoreComments: false,
            visibleComments: 0,
            page: 0,
            comment: {
                ...this.state.comment,
                showingAnswers: false,
                arrAnswerComments: [],
            }
        })
    }

    _renderChildComment = (item, parent) => {
        return <PostCommentsComponent
            comment={item}
            parent={parent}
            key={`comm_${item.uuid}`}
            isAnswer={true}
            onClickAnswerComment={this.props.onClickAnswerComment}
            onLongPress={() => {
                this.setState({
                    showModalEdit: true,
                    commentToEdit: item
                })
            }} />
    }


    _showMoreComments = () => {
        this.setState({
            showingMoreComments: true,
            page: this.state.page + 1,
            loadingComments: true,
            comment: {
                ...this.state.comment,
                showingAnswers: true
            }
        }, () => {

            this._findComments();
        })
    }

    _findComments = () => {

        let post = this.state.comment.uuid;
        let params = {
            page: this.state.page,
            max_rows: 2
        }
        this.socialPostServices.findComments(post, params)
            .then((res) => {

                if (res.success) {

                    //ordenamos los comentarios en orden ascencente
                    //EJ; [5,6,7]
                    let comments = res.data;
                    comments = comments.sort((a, b) => (new Date(a.post_created_at).getTime() - new Date(b.post_created_at).getTime()))

                    //Aqui hacemos un preprend de los comentarios al array anterior
                    //quedando así:
                    //[2,3,4,5,6,7]
                    let arrComments = res.data.concat(this.state.comment.arrAnswerComments || []);

                    this.setState({
                        loadingComments: false,
                        visibleComments: arrComments.length,
                        comment: {
                            ...this.state.comment,
                            arrAnswerComments: arrComments,
                        }
                    })
                } else {
                    throw Error(res.message);
                }

            })
            .catch((err) => {
                console.log(err);
                this.setState({
                    page: this.state.page - 1,
                    loadingComments: false
                })
            })

    }

    _clickLikePost = () => {

        if (this.state.comment.liking) return;
        //Convertimios el 1 a boolean en caso de que el dato sea 1.
        let userLiked = !!(this.state.comment.user_liked);

        //Si el usuario ha dado like, restamos uno, de lo contrario aumentamos el contador
        let numberOfLikes = this.state.comment.post_likes;
        numberOfLikes = userLiked ? numberOfLikes - 1 : numberOfLikes + 1;

        this.setState({
            comment: {
                ...this.state.comment,
                post_likes: numberOfLikes,
                user_liked: !userLiked,
                liking: true
            }

        }, () => {
            this._likePost();
        })
    }

    _likePost = () => {
        let method = this.state.comment.user_liked ?
            'likePostComment' : //Si el usuario le dio like, mandamos a crear
            'unLikePostComment'; //Mandamos a quitar el like del post


        this.socialPostServices[method](this.state.comment.post_parent_uuid, this.state.comment.uuid,
            AuthService.userLoggedData.userIdentifier
        )
            .then(res => {
                if (res.success) {
                    this.setState({
                        comment: {
                            ...this.state.comment,
                            post_likes: res.data.likes_count,
                            liking: false
                        }
                    })
                } else {
                    throw Error(res.message)
                }
            })
            .catch(err => {
                //Retornamos los likes 
                let liked = this.state.comment.user_liked;
                let post_likes = this.state.comment.post_likes;
                this.setState({
                    comment: {
                        ...this.state.comment,
                        post_likes: liked ? post_likes - 1 : post_likes + 1,
                        user_liked: !(this.state.comment.post_liked),
                        liking: false
                    }
                })
            })
    }

    static propTypes = {
        comment: PropTypes.object,
        parent: PropTypes.object,
        onClickAnswerComment: PropTypes.func,
        onLongPress: PropTypes.func,
        isAnswer: PropTypes.bool
    }

}
