import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { COLORS } from '../../../../config/colors/colors';
import { AuthService } from '../../../../services/AuthService';
import InputText from '../../../../components/InputText/InputText';
import Utils from '../../../../utils/Utils';
import PropTypes from 'prop-types';
import { FaIcons } from '../../../../utils/Icons';
import { SocialPostHeaderStyles } from '../social.styles';
import TextView from '../../../../components/TextView/TextView';
import { CircularImage } from '../../../../components/CircularImage/circular_image';



export class PostHeaderComponent extends React.Component {

    private SocialPostHeaderStyles;

    constructor(props) {
        super(props);
        this.SocialPostHeaderStyles = SocialPostHeaderStyles();
    }


    shouldComponentUpdate = () => false

    render() {

        const { onValueChange, inputTextDisabled, inputTextClickEvent } = this.props;

        return (
            <View style={this.SocialPostHeaderStyles.main_container}>
                <View style={this.SocialPostHeaderStyles.header_container}>

                    <CircularImage
                        width={this.SocialPostHeaderStyles.header_image_container.width}
                        height={this.SocialPostHeaderStyles.header_image_container.height}
                        source={AuthService.userLoggedData.userPic ?
                            { uri: AuthService.userLoggedData.userPic } :
                            require('../../../../../assets/user_image.png')
                        }
                    />

                    <TouchableOpacity
                        style={this.SocialPostHeaderStyles.header_input_styles}
                        onPress={
                            () => {
                                inputTextDisabled &&
                                    inputTextClickEvent()
                            }
                        }>

                        <TextView textValue={'¿Que quieres publicar?'}
                            styles={{
                                color: COLORS.secondaryColor,
                                fontSize: 15
                            }} />

                    </TouchableOpacity>
                </View>
            </View>
        )


    }

    static propTypes = {
        navigation: PropTypes.object,
        onChangeValue: PropTypes.func,
        inputTextDisabled: PropTypes.bool,
        inputTextClickEvent: PropTypes.func
    }
}
