import React from "react";
import PropTypes from 'prop-types';
import { View } from "react-native";
import ProgressiveImage from "../../../../components/Image/ProgressiveImage";
import TextView from "../../../../components/TextView/TextView";
import { COLORS } from "../../../../config/colors/colors";
import { Switch, TouchableWithoutFeedback } from "react-native-gesture-handler";
import { containerStyles } from "../../../../utils/Styles";

export class ItemUserSelectable extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            user: props.user,
            selected: props.selected
        }
    }

    componentWillReceiveProps(nextProps) {

        if (
            (nextProps.selected !== this.props.selected)
        ) {
            this.setState({
                selected: nextProps.selected
            })
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (
            (nextProps.selected !== this.props.selected)
        ) {
            return true;
        }
        return false;
    }

    render() {

        const { onPress, onLongPress, style,
            checkToSelect, selected, userImageStyles,
            userImageContainerStyles } = this.props;


        return (

            <View style={[{
                flex: 1,

            }, style]}>

                <TouchableWithoutFeedback
                    style={{
                        paddingVertical: 5,
                        backgroundColor: selected ? 'rgba(200,200,200,0.5)' : 'transparent',
                    }}
                    onPress={() => {
                        //console.log(checkToSelect, !selected)
                        if (!checkToSelect) {
                            onPress &&
                                onPress(!selected)
                        }

                    }}
                    onLongPress={() => {
                        if (!checkToSelect)
                            onLongPress
                                && onLongPress(!selected)
                    }}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginHorizontal: containerStyles.marginHorizontal,
                    }}>
                        <View style={[{
                            overflow: 'hidden',
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: 55,
                            height: 55,
                            borderRadius: 55 / 2,
                            marginRight: 10
                        }, userImageContainerStyles]}>
                            {

                                <ProgressiveImage
                                    source={
                                        this.state.user.pic ?
                                            { uri: this.state.user.pic } :
                                            require('../../../../../assets/user_image.png')
                                    }
                                    style={[{
                                        width: 55,
                                        height: 55
                                    }, userImageStyles]}
                                    loading
                                />
                            }
                        </View>
                        <View style={{
                            flex: 1,
                            paddingHorizontal: 5,
                            paddingVertical: 5,
                        }}>
                            <TextView textValue={this.state.user.nombre_completo}
                                styles={{
                                    fontWeight: 'bold',
                                    fontSize: 13,
                                    color: COLORS.secondaryColor,
                                    flexWrap: 'wrap'
                                }} />
                        </View>
                        {
                            checkToSelect &&
                            <View style={{
                                padding: 10,
                            }}>

                                <Switch
                                    value={selected}
                                    onValueChange={(selected) => {
                                        onPress && onPress(selected)
                                    }}
                                />

                            </View>
                        }
                    </View>
                </TouchableWithoutFeedback>

            </View >

        )
    }

    static propTypes = {
        user: PropTypes.object,
        onPress: PropTypes.func,
        onLongPress: PropTypes.func,
        style: PropTypes.object,
        checkToSelect: PropTypes.bool,
        selected: PropTypes.bool,
        userImageStyles: PropTypes.object,
        userImageContainerStyles: PropTypes.object
    }
}