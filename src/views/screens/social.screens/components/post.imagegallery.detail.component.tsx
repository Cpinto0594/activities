import React from 'react';
import ViewsNavigation from '../../../main/navigation.config';
import { View, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { COLORS } from '../../../../config/colors/colors';
import PropTypes from 'prop-types';
import { FaIcons } from '../../../../utils/Icons';
import { FaIconsEnum } from '../../../../utils/IconsEnum';
import Image from 'react-native-scalable-image';

export class PostImageGalleryDetailComponent extends React.Component {


    static navigationOptions = ({ navigation }) => {
        return {
            ...ViewsNavigation.GelleryEditNavigationButtons('', navigation),
            headerLeft:
                <TouchableOpacity style={{ width: 60, alignContent: 'center', alignItems: 'center' }}
                    onPress={navigation.getParam('goBack')}>
                    {
                        FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.white)
                    }
                </TouchableOpacity>
        };
    }


    constructor(props) {
        super(props);
        
        this.state = {
            data: this.props.navigation.getParam('images') || [],
            windowWidth: Dimensions.get('window').width
        }
    }


    componentDidMount() {
        this.props.navigation.setParams({ goBack: this.onBackButtonPressAndroid })
    }




    render() {

        return (

            <View style={{
                flex: 1,
                backgroundColor: COLORS.backgroundColor,

            }}>
                <FlatList data={this.state.data}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                        <View style={{
                            flex: 1,
                            backgroundColor: COLORS.white,
                            marginVertical: 5
                        }}>
                            {
                                this._getImage(item)
                            }
                        </View>
                    }
                    keyExtractor={(item, index) => `image_${index}_${item.image_name}`}
                    ItemSeparatorComponent={() => <View style={{ padding: 5 }} ></View>}
                />

            </View>

        )

    }

    _getImage = (image) =>
        <View style={{
        }}>
            {
                this.props.canDelete &&
                <TouchableOpacity style={{
                    position: 'absolute',
                    right: 0,
                    top: 0,
                    zIndex: 99999,
                    padding: 5
                }}
                    onPress={() => {
                        const onremove = this.props.onRemove
                        onremove && onremove(image)
                    }}>
                    {FaIcons.getIcon(FaIconsEnum.CLOSE_X, 25, COLORS.white)}
                </TouchableOpacity>
            }
            <Image
                source={{ uri: image.image_url }}
                width={this._imageWidth()}
            />

        </View>


    _imageWidth() {
        return (this.state.windowWidth)
    }

    _deleteImage = (item) => {
        let arrImages = this.state.data;
        let i = 0;
        for (let element in arrImages) {
            element = arrImages[element];
            if (element.id === item.id) {
                arrImages.splice(i, 1)
                this.setState({
                    data: arrImages
                });
                return;
            }
            i++;
        };
    }

    onBackButtonPressAndroid = () => {
        let callback = this.props.navigation.getParam('callback');
        callback && callback(this.state.data);
        this.props.navigation.goBack(null)
    };


    static propTypes = {
        images: PropTypes.array,
        navigation: PropTypes.object,
        canDelete: PropTypes.bool
    }

}