import React from 'react';
import { View, Image, Platform, BackHandler } from 'react-native';
import { COLORS } from '../../../../config/colors/colors';
import { ScrollView, TouchableOpacity, TouchableWithoutFeedback, TouchableHighlight } from 'react-native-gesture-handler';
import TextView from '../../../../components/TextView/TextView';
import { IonIcons, FaIcons } from '../../../../utils/Icons';
import { IonIconEnum, FaIconsEnum } from '../../../../utils/IconsEnum';
import ViewsNavigation, { HeaderButtons } from '../../../main/navigation.config';
import InputText from '../../../../components/InputText/InputText';
import { AuthService } from '../../../../services/AuthService';
import { PostImagesGallery } from './post.images.gallery';
import ImageLoader from '../../../../FunctionaityProviders/image_camerapicker/image_camera_picker';
import { PostAttachmentsComponent } from './post.attachments.component';
import { DocumentsPicker } from '../../../../FunctionaityProviders/documents_picker/documents_picket';
import NotificationServices from '../../../../services/NotificationsServices';
import Utils from '../../../../utils/Utils';
import Button from '../../../../components/Button/Button';
import Modal from 'react-native-modal';
import AsyncStorageProvider from '../../../../FunctionaityProviders/AsyncStorageprovider';
import SocialPostsServices from '../../../../services/socialposts.services/social.posts.services';
import { FileExtensionUtils } from '../../../../utils/FileExtensionUtils';
import { SocialPostPublisher } from '../social.styles';
import DefaultCustomHeader from '../../../../components/Headers/custom_header';
import { containerStyles } from '../../../../utils/Styles';
import RBSheet from 'react-native-raw-bottom-sheet';

export class PostPublisherComponent extends React.Component {

    static SOCIAL_POST_STORAGE_KEY = 'social_pot_stor_key'

    private imageLoader: ImageLoader;
    private documentPicker: DocumentsPicker;
    private notificationServices: NotificationServices;
    private socialPostServices: SocialPostsServices;

    private _didFocusSubscription;
    private _willBlurSubscription;
    private savingPost: boolean = false;
    private SocialPostPublisherStyles;

    static navigationOptions = ({ navigation }) => {
        let options = ViewsNavigation.NewPostNavigationButtons('', navigation);
        return {
            header:
                <DefaultCustomHeader
                    headerLeft={
                        <HeaderButtons
                            action={navigation.getParam('beforeLeave')}>
                            {FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)}
                        </HeaderButtons>
                    }
                    headerTitle={'Nueva Publicación'}
                    headerRight={
                        <HeaderButtons
                            disabled={navigation.getParam('PublishDisabled', true)}
                            action={navigation.getParam('savePost')}

                        >
                            <TextView textValue='Hecho' styles={{
                                fontWeight: 'bold',
                                color: COLORS.secondaryColor,
                                fontSize: 12
                            }} />
                        </HeaderButtons>
                    }
                />


        }
    }

    defaultState = {
        post: {
            post_content: null,
            post_user_owner_id: AuthService.userLoggedData.userId,
            post_user_owner: AuthService.userLoggedData.userName,
            post_type: 'post',
            images: [],
            attachments: [],
        },
        showModalDraft: false,
        isAbouttoPublish: false,
        showCommentsSection: false,
        afterCreatedEvent: this.props.navigation.getParam('onPostCreated')
    }

    state = Object.assign({}, this.defaultState);

    constructor(props) {
        super(props);
        this.imageLoader = new ImageLoader;
        this.documentPicker = new DocumentsPicker;
        this.notificationServices = new NotificationServices;
        this.socialPostServices = new SocialPostsServices;
        this.SocialPostPublisherStyles = SocialPostPublisher();


        this._didFocusSubscription = props.navigation.addListener('didFocus', () =>
            BackHandler.addEventListener('hardwareBackPress', this._checkDraft)
        );
    }



    componentDidMount() {

        (async () => {
            let hasPostInDraft = await AsyncStorageProvider.getItem(PostPublisherComponent.SOCIAL_POST_STORAGE_KEY)
            if (hasPostInDraft)
                try {

                    this.state.post = JSON.parse(hasPostInDraft);
                } catch (error) {
                    console.log('No se pudo deserializar objeto ', error)
                }
        })();

        this.props.navigation.setParams({
            savePost: this._savePost,
            beforeLeave: () => {
                let canleave = !this._checkDraft();
                canleave && this.props.navigation.goBack(null)
            }
        });

        //Agregamos el backButtonPress listener
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', () =>
            BackHandler.removeEventListener('hardwareBackPress', this._checkDraft)
        );
    }
    componentWillUnmount() {
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }

    render() {

        return (

            <View style={this.SocialPostPublisherStyles.main_container}>
                <View style={this.SocialPostPublisherStyles.publisher_header_container}>
                    <View style={{
                        borderRadius: 50,
                        overflow: "hidden"
                    }}>
                        {
                            AuthService.userLoggedData.userPic ?

                                <Image source={{ uri: AuthService.userLoggedData.userPic }}
                                    style={this.SocialPostPublisherStyles.publisher_header_image_pic} />
                                :
                                <Image source={require('../../../../../assets/user_image.png')}
                                    style={this.SocialPostPublisherStyles.publisher_header_image_pic} />
                        }

                    </View>
                    <View style={{
                        paddingLeft: 10,
                    }}>
                        <TextView textValue={AuthService.userLoggedData.userFullName} />
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}
                    style={{
                        flex: 1
                    }}
                    contentContainerStyle={{
                        flexGrow: 1,
                        //justifyContent: 'flex-start',
                    }}>
                    <View style={this.SocialPostPublisherStyles.publisher_content_text_style}>

                        <InputText
                            ref='inputComment'
                            type='textarea'
                            placeholder={'¿Que quieres publicar?'}
                            styles={{
                                minHeight: 40,
                                paddingLeft: 0,
                            }}
                            value={this.state.post.post_content}
                            multilineMaxHeight={1000}
                            onChange={(value) => {
                                this.setState({
                                    post: {
                                        ...this.state.post,
                                        post_content: value
                                    }
                                }, () => {
                                    this._togglePublishButton();
                                })
                            }}
                        />

                    </View>
                    {/* GALLERY AND ATTACHMENTS */}
                    <View style={{
                        flex: 0,
                    }}>
                        {
                            this.state.post.images.length > 0 &&
                            <PostImagesGallery
                                canDelete={true}
                                data={this.state.post.images}
                                navigation={this.props.navigation}
                                onRemove={(item) => {
                                    let arrImages = this.state.post.images || [];
                                    Utils.arrayDeleteItem(arrImages, (image) => image.id === item.id)
                                    this.setState({
                                        post: {
                                            ...this.state.post,
                                            images: arrImages
                                        }
                                    }, () => {
                                        this._togglePublishButton();
                                    })
                                }}
                            />
                        }
                        {/* ATTACHMENTS */}
                        {
                            this.state.post.attachments.length > 0 &&
                            <View style={{
                                marginTop: 10
                            }}>
                                <PostAttachmentsComponent
                                    canDelete={true}
                                    items={this.state.post.attachments}
                                    onRemove={(item) => {
                                        let atts = this.state.post.attachments;
                                        Utils.arrayDeleteItem(atts, (att) => att.id === item.id)
                                        this.setState({
                                            post: {
                                                ...this.state.post,
                                                attachments: atts
                                            }
                                        }, () => {
                                            this._togglePublishButton();
                                        })

                                    }} />
                            </View>
                        }
                    </View>
                    <View style={{
                        flex: 0
                    }}>
                        <TouchableWithoutFeedback
                            style={{
                                height: 100 + '%'
                            }}
                            onPress={() => {
                                this.refs.inputComment.focus()
                            }}>

                        </TouchableWithoutFeedback>
                    </View>
                </ScrollView>
                <View style={this.SocialPostPublisherStyles.publisher_attachments_button_container}>
                    {/* ADD ATTACHMENT */}
                    <TouchableOpacity style={this.SocialPostPublisherStyles.publisher_attachment_button}
                        onPress={this._openGallery}>
                        {IonIcons.getIcon(IonIconEnum.ATTACHMENT_IMAGE, 20, COLORS.secondaryColor)}
                        <TextView textValue={'Imagenes'} styles={{
                            marginLeft: 10
                        }} />
                    </TouchableOpacity>

                    <TouchableOpacity style={this.SocialPostPublisherStyles.publisher_attachment_button}
                        onPress={this._openFilePicker}>

                        {IonIcons.getIcon(IonIconEnum.ATTACHMENT_DOC, 20, COLORS.secondaryColor)}
                        <TextView textValue={'Archivos'} styles={{
                            marginLeft: 10
                        }} />
                    </TouchableOpacity>

                </View>
                <RBSheet
                    ref='buttonSheet'
                    duration={200}
                    height={200}
                    closeOnDragDown={true}
                    animationType={'fade'}
                    customStyles={{
                        container: {
                            justifyContent: "flex-start",
                            alignItems: "flex-start",
                            borderTopLeftRadius: 15,
                            borderTopRightRadius: 15
                        }
                    }}
                >

                    <View style={{
                        backgroundColor: COLORS.white,
                        paddingHorizontal: containerStyles.marginHorizontal
                    }}>
                        <View style={{
                            padding: 10,
                            marginBottom: 10
                        }}>
                            <TextView textValue={'¿Deseas guardar la publicación como borrador?'} styles={{
                                fontWeight: 'bold',
                                fontSize: 14
                            }} />
                            <TextView textValue={'o puedes seguir editandola'} styles={{
                                fontSize: 12,
                                color: '#cdcdcd'
                            }} />

                        </View>
                        <Button text={'Guardar publicación'} type='light'
                            onPress={this._savePostLocally.bind(this)}
                            icon={
                                IonIcons.getIcon(IonIconEnum.DRAFT, 20, COLORS.secondaryColor)
                            }
                            textStyles={{
                                fontSize: 15,
                            }}
                            buttonColorStyles={{
                                borderWidth: 0,
                                backgroundColor: 'transparent',
                            }}
                            buttonStyles={{
                                justifyContent: 'flex-start'
                            }}
                        />
                        <Button text={'Descartar'} type='light'
                            onPress={this._discardChanges.bind(this)}
                            icon={
                                FaIcons.getIcon(FaIconsEnum.TRASH_EMPTY, 20, COLORS.secondaryColor)
                            }
                            textStyles={{
                                fontSize: 15
                            }}
                            buttonColorStyles={{
                                borderWidth: 0,
                                backgroundColor: 'transparent'
                            }}
                            buttonStyles={{
                                justifyContent: 'flex-start'
                            }}
                        />
                        <Button text={'Seguir editando'} type='light'
                            onPress={() => { this.setState({ showModalDraft: false }) }}
                            icon={
                                FaIcons.getIcon(FaIconsEnum.CHECK, 20, COLORS.secondaryColor)
                            }
                            textStyles={{
                                fontSize: 15
                            }}
                            buttonColorStyles={{
                                borderWidth: 0,
                                backgroundColor: COLORS.white
                            }}
                            buttonStyles={{
                                justifyContent: 'flex-start'
                            }}
                        />

                    </View>

                </RBSheet>
            </View>
        )
    }

    _postHasContent = () => {
        let post = this.state.post;
        let retun = !!(post &&
            (
                post.post_content ||
                post.attachments.length ||
                post.images.length
            )
        );

        return retun;
    }

    _checkDraft = () => {
        let hasContent = this._postHasContent();

        if (hasContent) {
            this.refs.buttonSheet.open()

            return true;
        }
        return false;
    }

    _togglePublishButton = () => {
        let hasContent = this._postHasContent();
        this.props.navigation.setParams({ 'PublishDisabled': !hasContent })
    }




    _openGallery = () => {

        this.imageLoader.launchGallery({
            quality: 0.8,
        })
            .then(result => {
                //console.log(result)
                //EXPO
                var cancelled = result.cancelled;
                var uri = result.uri;

                //EJECTED   
                // var uri = pickerResult.uri || pickerResult.origURL;
                // var cancelled = pickerResult.didCancel;

                if (cancelled) return;

                let dataImage = {
                    image_name: uri.substring(uri.lastIndexOf('/') + 1, uri.length),
                    image_format: uri.substring(uri.lastIndexOf('.') + 1, uri.length),
                    image_mime: 'image/' + uri.substring(uri.lastIndexOf('.') + 1, uri.length),
                    image_url:
                        Platform.OS === "android" ? uri : uri.replace("file://", ""),
                    image_width: result.width,
                    image_height: result.height,
                    image_order: this.state.post.images.length + 1,
                    id: new Date().getTime(),

                };

                this.setState({
                    post: {
                        ...this.state.post,
                        images: this.state.post.images.concat(dataImage)
                    }
                }, () => {
                    this._togglePublishButton();
                })


            })


    }

    _openFilePicker = () => {

        this.documentPicker.getDocument()
            .then(result => {
                //console.log(result)

                let cancel = result.type === 'cancel';

                if (cancel) return;

                let document = {
                    attachment_name: result.name,
                    attachment_order: 1,
                    attachment_size: result.size,
                    attachment_url: result.uri,
                    attachment_format: this._docFormat(result.name),
                    attachment_mime: this._docExtension(this._docFormat(result.name)).mimetype,
                    id: new Date().getTime()
                }
                this.setState({
                    post: {
                        ...this.state.post,
                        attachments: this.state.post.attachments.concat(document)
                    }
                }, () => {
                    this._togglePublishButton();
                })
            })


    }
    _savePostLocally = async () => {
        await AsyncStorageProvider.setItem(PostPublisherComponent.SOCIAL_POST_STORAGE_KEY,
            JSON.stringify(this.state.post))
        this.props.navigation.goBack(null)
    }

    _discardChanges = async () => {
        await AsyncStorageProvider.removeItem(PostPublisherComponent.SOCIAL_POST_STORAGE_KEY)
        this.refs.buttonSheet.close()
        this.props.navigation.goBack(null)
    }

    _docFormat(name) {
        let contained = ['tar.gz'].find(format => name.toLowerCase().indexOf(format) !== -1);
        return (contained) ?
            contained :
            name.substring(name.lastIndexOf('.') + 1, name.length)
    }

    _docExtension(format) {
        let extension = FileExtensionUtils.getFileExtension(format)
        return extension;
    }

    _checkSavePost = () => {

        return this.state.post &&
            (this.state.post.post_content ||
                this.state.post.attachments.length ||
                this.state.post.images.length
            );

    }

    _savePost = () => {

        if (!this._checkSavePost() || this.savingPost) {
            return;
        }


        let post = Object.assign({}, this.state.post);
        post.post_content = (this.state.post.post_content || '');
        let images = post['images'];
        let attachments = post['attachments'];

        let objectRequest = new FormData();

        Object.keys(post).forEach(key => {
            objectRequest.append(key,
                (post[key] instanceof Array)
                    ? JSON.stringify(post[key]) : post[key])
        });

        images.forEach(image => {
            objectRequest.append('images_data', {
                name: String(image.id),
                type: image.image_mime,
                uri: image.image_url
            })
        });

        attachments.forEach(att => {
            objectRequest.append('attachs_data', {
                name: String(att.id),
                type: att.attachment_mime,
                uri: att.attachment_url
            })
        });


        this._discardChanges();
        this.state.afterCreatedEvent &&
            this.state.afterCreatedEvent(objectRequest, post)

        // this.savingPost = true;
        // this.socialPostServices.savePost(objectRequest)
        //     .then(res => {

        //         if (res.success) {

        //             this.props.navigation.goBack(null);
        //             this.state.afterCreatedEvent &&
        //                 this.state.afterCreatedEvent(post)

        //         } else {
        //             throw Error(res.message)
        //         }
        //         this.savingPost = false;

        //     })
        //     .catch(err => {
        //         console.log(err);
        //         this.savingPost = false;
        //         this.notificationServices.fail('No se pudo generar publicación')
        //     })
    }
}