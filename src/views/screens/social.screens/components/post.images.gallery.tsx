import React from 'react';
import { View, Image, TouchableOpacity, Dimensions, Text, ActivityIndicator, Modal, TouchableWithoutFeedback } from 'react-native';
import { COLORS } from '../../../../config/colors/colors';
import PropTypes from 'prop-types';
import TextView from '../../../../components/TextView/TextView';
import Utils from '../../../../utils/Utils';
import { FaIcons, OctIcons } from '../../../../utils/Icons';
import { FaIconsEnum, OCTIconsEnum } from '../../../../utils/IconsEnum';
import ScalableImage from 'react-native-scalable-image';
import ImageView from 'react-native-image-view';
import { Video } from 'expo-av'

export class PostImagesGallery extends React.Component {

    defaultState = {
        data: [],
        dataSize: 0,
        howManyMore: 0,
        firstImage: {},
        imageMore: {},
        windowWidth: Dimensions.get('window').width,
        showImageDetail: false,
        imageIndex: 0,
        imagesViewer: [],
        isVertical: false,
        videoPlayer: {
            shouldPlay: false,
            shouldSound: true
        }
    }
    private imageMoreSize: any;

    componentWillReceiveProps(props) {

        const { data } = props;
        let dataSize = (data || []).length;
        this.setState({
            dataSize: dataSize,
            data: data,
            firstImage: data[0],
            imageMore: dataSize > 3 ? data[3] : null,
            howManyMore: (dataSize - 4),
            imagesViewer: this.state.data.map(img => ({
                source: {
                    uri: img.image_url
                },
                width: this.state.windowWidth,
                height: img.image_height * (this.state.windowWidth / img.image_width),
                title: img.image_name
            }))
        });


    }

    constructor(props) {
        super(props)

        this.state = Object.assign({}, this.defaultState)

        const { data } = props;
        this.state.dataSize = (data || []).length;
        this.state.data = data;
        this.state.firstImage = data[0]
        this.state.imageMore = this.state.dataSize > 3 ? data[3] : null;
        this.state.howManyMore = (this.state.data.length - 4);
        this.state.imagesViewer = data.map(img => ({
            source: {
                uri: img.image_url
            },
            width: this.state.windowWidth,
            height: img.image_height * (this.state.windowWidth / img.image_width),
            title: img.image_name
        }));
        this.state.isHorizontalAlignment = this.state.firstImage.image_width < this.state.firstImage.image_height;
    }

    render() {



        return (
            <View style={{
                flex: 1
            }}>
                {
                    this.state.showImageDetail &&
                    <View style={{
                        flex: 1,
                        position: 'absolute',
                        top: 0,
                        left: 0, 
                        zIndex: 999999 
                    }}>
                        <ImageView
                            glideAlways
                            images={this.state.imagesViewer}
                            imageIndex={this.state.imageIndex}
                            animationType="fade"
                            isVisible={this.state.showImageDetail}
                            onClose={() => this.setState({ showImageDetail: false })}
                            onImageChange={index => {
                                console.log(index);
                            }}
                            renderFooter={this.renderFooter}
                        />

                    </View>
                }

                {
                    this.state.dataSize <= 2 &&
                    <View style={{
                        flexDirection: this.state.isHorizontalAlignment ? 'row' : 'column',
                        justifyContent: 'space-between'
                    }}>

                        {
                            this.state.data.map((image, index) =>

                                <TouchableWithoutFeedback
                                    style={{
                                        flex: 1,
                                        marginVertical: 2
                                    }}
                                    onPress={() => {
                                        this.setState({
                                            showImageDetail: true,
                                            imageIndex: index
                                        })
                                    }}
                                    key={`img_${image.id}`}>
                                    <View style={{
                                        width: '100%',
                                        flex: 1,
                                        marginVertical: 2
                                    }}
                                    >
                                        {
                                            <ScalableImage source={{ uri: image.image_url }}
                                                width={this.state.windowWidth / (this.state.isHorizontalAlignment ? this.state.dataSize : 1)}
                                            />
                                        }
                                    </View>
                                </TouchableWithoutFeedback>
                            )
                        }
                    </View>

                }
                {
                    this.state.dataSize > 2
                    &&
                    < View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'flex-start',
                        flexWrap: 'wrap'
                    }}>
                        <View style={{
                            flex: 0,
                            backgroundColor: COLORS.backgroundColor,
                            height: 200,
                            width: 100 + '%',
                        }}
                            key={this.state.firstImage.id}>
                            {
                                this._getImage(this.state.firstImage)
                            }

                        </View>
                        {
                            this.state.data
                                .filter((img, index) => (index > 0 && index <= 2))
                                .map((img, index) =>
                                    <View style={{
                                        flex: 0,
                                        backgroundColor: COLORS.backgroundColor,
                                        margin: 2
                                    }}
                                        key={img.image_name}>
                                        {
                                            this._getScalableImage(img)
                                        }

                                    </View>
                                )
                        }
                        {
                            this.state.imageMore != null &&
                            <View style={{
                                flex: 0,
                                backgroundColor: COLORS.backgroundColor,
                                margin: 2
                            }}
                                key={this.state.imageMore.image_name}>
                                <TouchableOpacity onPress={() => {
                                    Utils.defaultNavigation(this.props.navigation, 'EditGallery', {
                                        images: this.state.data,
                                        callback: (data) => {
                                            this.setState({
                                                data: data
                                            })
                                        }
                                    })
                                }}
                                    style={{
                                        flex: 0
                                    }}>
                                    {
                                        this._getScalableImage(this.state.imageMore)
                                    }
                                    {
                                        this.state.howManyMore ?
                                            <View style={{
                                                backgroundColor: 'rgba(205,205,205,0.55)',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                height: (this.imageMoreSize || { height: 100 }).height,
                                                width: 100 + '%',
                                                position: 'absolute',
                                                left: 0,
                                                top: 0,
                                                zIndex: 99999999
                                            }}>
                                                <TextView textValue={
                                                    this.state.howManyMore + ' más '
                                                } styles={{ color: COLORS.white, fontWeight: 'bold', fontSize: 14 }} />
                                            </View> : null
                                    }

                                </TouchableOpacity>

                            </View>
                        }
                    </View>
                }

            </View >
        )
    }


    renderFooter = ({ title }) => {
        //const {likes} = this.state;

        return (
            <View style={{
                width: this.state.windowWidth,
                height: 50,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.4)',
                paddingHorizontal: 20,
                paddingVertical: 5,
            }}>
                <Text style={{
                    fontSize: 14,
                    color: '#FFF',
                    textAlign: 'center',
                }}>
                    {title}
                </Text>
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                    }}
                    onPress={() => {
                        console.log('Pressed')
                    }}
                >
                    <Text style={{
                        fontSize: 14,
                        color: '#FFF',
                        textAlign: 'center',
                    }}>
                        ♥
                    </Text>
                    <Text style={[{
                        fontSize: 14,
                        color: '#FFF',
                        textAlign: 'center',
                    }, { marginLeft: 7 }]}>
                        1
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }

    _getImage = (image) =>
        <View style={{
            flex: 1

        }}>
            {
                this.props.canDelete &&
                <TouchableOpacity style={{
                    position: 'absolute',
                    right: 0,
                    top: 0,
                    zIndex: 99999,
                    padding: 5
                }}
                    onPress={() => {
                        const onremove = this.props.onRemove
                        onremove && onremove(image)
                    }}>
                    {FaIcons.getIcon(FaIconsEnum.CLOSE_X, 25, COLORS.white)}
                </TouchableOpacity>
            }
            <Image
                source={{ uri: image.image_url }} style={{
                    height: undefined,
                    width: undefined,
                    flex: 1,
                }}
                resizeMode='cover'
            />

        </View>

    _getScalableImage = (image) =>

        < ScalableImage source={{ uri: image.image_url }}
            width={(this.state.windowWidth - (Math.min(this.state.dataSize - 1, 3) * 4)) / (Math.min(this.state.dataSize - 1, 3))}
            onSize={(size) => { this.imageMoreSize = size; }}
        />




    static propTypes = {
        data: PropTypes.array,
        navigation: PropTypes.object,
        onRemove: PropTypes.func,
        canDelete: PropTypes.bool
    }

}