import React from 'react';
import { View, TouchableOpacity, Dimensions, TouchableHighlight } from 'react-native';
import TextView from '../../../../components/TextView/TextView';
import { IonIcons, SimpleLineIcons, AntDesignIcons } from '../../../../utils/Icons';
import { IonIconEnum, SimpleLineIconsEnum, AntDesignIconsEnum } from '../../../../utils/IconsEnum';
import { COLORS } from '../../../../config/colors/colors';
import PropTypes from 'prop-types';
import Utils from '../../../../utils/Utils';
import { SocialPostsUtils } from '../SocialUtils';
import { PostImagesGallery } from './post.images.gallery';
import { PostAttachmentsComponent } from './post.attachments.component';
import { AuthService } from '../../../../services/AuthService';
import SocialPostsServices from '../../../../services/socialposts.services/social.posts.services';
import { SocialPostComponentStyles } from '../social.styles';
import { CircularImage } from '../../../../components/CircularImage/circular_image';



export class PostComponent extends React.Component {


    private postsService: SocialPostsServices;
    private SocialPostStyles;

    constructor(props) {
        super(props)
        this.postsService = new SocialPostsServices;
        this.state = {
            post: props.post,
            windowWidth: Dimensions.get('window').width,
        }
        this.SocialPostStyles = SocialPostComponentStyles();

    }

    componentWillUnmount() {
    }

    // componentWillReceiveProps(nextProps) {
    //     if (
    //         (nextProps.post.post_likes !== this.props.post.post_likes) ||
    //         (nextProps.post.user_liked !== this.props.post.user_liked) ||
    //         (nextProps.post.post_comments !== this.props.post.post_comments)
    //     ) {
    //         this.setState({
    //             post: nextProps.post
    //         })
    //     }
    // }


    shouldComponentUpdate(nextProps, nextState) {
        if (
            (nextState.post.post_comments !== this.state.post.post_comments) ||
            (nextState.post.post_content !== this.state.post.post_content) ||
            (nextState.post.post_likes !== this.state.post.post_likes) ||
            (nextState.post.liking !== this.state.post.liking) ||
            (nextProps.post.post_likes !== this.props.post.post_likes) ||
            (nextProps.post.user_liked !== this.props.post.user_liked)
        ) {
            return true;
        }
        return false;
    }

    render() {

        const { onMoreOptions } = this.props;

        return (
            <View style={this.SocialPostStyles.post_container}>


                {/* HEADER */}
                <View style={this.SocialPostStyles.post_header_container}>
                    <CircularImage
                        width={this.SocialPostStyles.post_header_image_pic.width}
                        height={this.SocialPostStyles.post_header_image_pic.height}
                        source={this.state.post.author.pic ?
                            { uri: this.state.post.author.pic } :
                            require('../../../../../assets/user_image.png')}
                        style={{
                            marginTop: 10
                        }}
                    />

                    <View style={this.SocialPostStyles.post_header_username}>
                        <TextView textValue={this.state.post.author.nombre_completo}
                            styles={this.SocialPostStyles.post_header_userName_text_style} />
                        <TextView textValue={SocialPostsUtils.durationOfPost(this.state.post.post_created_at)}
                            styles={this.SocialPostStyles.post_header_post_created_text_style} />
                    </View>
                    {
                        this.state.post.author.usuario === AuthService.userLoggedData.userName &&

                        <TouchableHighlight style={this.SocialPostStyles.touchable_post_options}
                            underlayColor='#cdcdcd'
                            onPress={() => {
                                onMoreOptions && onMoreOptions(this.state.post)
                            }}>
                            {IonIcons.getIcon(IonIconEnum.DOTS_MORE, 20, COLORS.secondaryColor)}
                        </TouchableHighlight>
                    }
                </View>
                {/* CONTENT */}
                {
                    (this.state.post.post_content && this.state.post.post_content.trim().length > 0) ?
                        <View style={this.SocialPostStyles.post_content_container}>
                            <TextView textValue={this.state.post.post_content}
                                styles={this.SocialPostStyles.post_content_text_styles} />
                        </View> : null
                }
                {/* IMAGE */}

                {
                    (this.state.post.images && this.state.post.images.length) ?
                        <View style={{
                            marginBottom: 10
                        }}>
                            <PostImagesGallery
                                data={this.state.post.images} canDelete={false}
                                navigation={this.props.navigation} />
                        </View> : null
                }

                {/* ATTACHMENTS */}
                {
                    (this.state.post.attachments && this.state.post.attachments.length) ?
                        <View style={{
                            marginBottom: 10
                        }}>
                            <PostAttachmentsComponent items={this.state.post.attachments} canDelete={false} />
                        </View> : null
                }

                {/* FOOTER */}
                <View style={this.SocialPostStyles.post_footer_container}>
                    <TouchableOpacity
                        disabled={this.state.post.liking}
                        style={this.SocialPostStyles.post_button_reaction} onPress={() => { this._postButtonReaction('like') }}>
                        {AntDesignIcons.getIcon(this.state.post.user_liked ?
                            AntDesignIconsEnum.LIKEFUL :
                            AntDesignIconsEnum.LIKEEMPTY, 25,
                            this.state.post.user_liked ? COLORS.contrastColor : COLORS.midSoftGrey)}
                        <TextView textValue={String(this.state.post.post_likes)}
                            styles={this.SocialPostStyles.post_footer_button_reaction_text_styles} />
                    </TouchableOpacity>

                    <TouchableOpacity style={this.SocialPostStyles.post_button_reaction} onPress={() => { this._postButtonReaction('comment') }}>
                        {SimpleLineIcons.getIcon(SimpleLineIconsEnum.BUBBLE, 24, COLORS.midSoftGrey)}
                        <TextView textValue={String(this.state.post.post_comments)}
                            styles={this.SocialPostStyles.post_footer_button_reaction_text_styles} />
                    </TouchableOpacity>
                </View>


            </View>

        )
    }

    _findPostUpdates = () => {
        this.postsService.postContent(this.state.post.uuid, {
            include: 'images,attachments',
            page: 1,
            max_rows: 1
        })
            .then(res => {
                if (res.success) {
                    let post_response = res.data[0];

                    if (!post_response) return;

                    this.setState({
                        post: {
                            ...this.state.post,
                            post_comments: post_response.post_comments,
                            post_content: post_response.post_content,
                            attachments: post_response.attachments,
                            images: post_response.images
                        }
                    })
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    _clickLikePost = () => {

        if (this.state.post.liking) return;
        //Convertimios el 1 a boolean en caso de que el dato sea 1.
        let userLiked = this.state.post.user_liked;


        //Si el usuario ha dado like, restamos uno, de lo contrario aumentamos el contador
        let numberOfLikes = this.state.post.post_likes;
        numberOfLikes = userLiked ? numberOfLikes - 1 : numberOfLikes + 1;

        this.setState({
            post: {
                ...this.state.post,
                post_likes: numberOfLikes,
                user_liked: !userLiked,
                liking: true
            }

        }, () => {
            this._likePost();
        })
    }

    _likePost = () => {
        let method = this.state.post.user_liked ?
            'likePost' : //Si el usuario le dio like, mandamos a crear
            'unlikePost'; //Mandamos a quitar el like del post


        this.postsService[method](this.state.post.uuid, AuthService.userLoggedData.userIdentifier)
            .then(res => {
                if (res.success) {
                    this.setState({
                        post: {
                            ...this.state.post,
                            post_likes: res.data.likes_count,
                            liking: false
                        }
                    }, () => {
                        this.props.post.post_likes = this.state.post.post_likes;
                        this.props.post.user_liked = this.state.post.user_liked;
                    })
                } else {
                    throw Error(res.message)
                }
            })
            .catch(() => {
                //Retornamos los likes 
                let liked = this.state.post.user_liked;
                let post_likes = this.state.post.post_likes;
                this.setState({
                    post: {
                        ...this.state.post,
                        post_likes: liked ? post_likes - 1 : post_likes + 1,
                        user_liked: !(this.state.post.user_liked),
                        liking: false

                    }
                })
            })
    }

    _postButtonReaction = (button) => {
        switch (button) {
            case 'comment':
                try {

                    this.props.onClickCommentButton && this.props.onClickCommentButton();
                } catch (error) {
                    console.log('Error on comment button press ', error)
                }
                Utils.defaultNavigation(this.props.navigation, 'Comments', {
                    post: this.state.post,
                    onReturnToPost: () => {
                        this._findPostUpdates();
                    }
                })
                break;
            case 'like':
                try {

                    this.props.onClickAppreciateButton && this.props.onClickAppreciateButton();
                } catch (error) {
                    console.log('Error on appreciate button press ', error)
                }

                this._clickLikePost();

                break;
        }
    }




    static propTypes = {
        post: PropTypes.object,
        navigation: PropTypes.object,
        onMoreOptions: PropTypes.func,
        onClickCommentButton: PropTypes.func,
        onClickAppreciateButton: PropTypes.func
    }


}
