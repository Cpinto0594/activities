import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { boxShadowChatItem } from '../../../../utils/Styles';
import TextView from '../../../../components/TextView/TextView';
import { AuthService } from '../../../../services/AuthService';
import { COLORS } from '../../../../config/colors/colors';
import moment from 'moment';
import { DateUtils } from '../../../../utils/DateUtils';
import Image from 'react-native-scalable-image';



export class SocialChatsMessageItem extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            message: props.message,
            chat_type: props.chat_type
        }
    }

    componentWillReceiveProps(nextProps) {
        if ((nextProps.message.message_created_at
            !== this.props.message.message_created_at)) {
            this.setState({
                message: nextProps.message
            })
        }

    }

    shouldComponentUpdate(nextProps, nextState) {

        if (
            (nextState.message.message_created_at !== this.state.message.message_created_at)
        ) {
            return true;
        }

        return false;

    }

    private today = moment();


    render() {
        const { style } = this.props;
        const { message, chat_type } = this.state;

        let fromMe = message.message_owner.uuid === AuthService.userLoggedData.userIdentifier;
        const horizontalAlign = fromMe ? 'flex-end' : 'flex-start';
        const messageFontColor = fromMe ? COLORS.white : COLORS.secondaryColor;
        const messageTimeFontColor = fromMe ? COLORS.softGrey : COLORS.midSoftGrey;
        const messageBackgroundColor = fromMe ? COLORS.contrastColor : COLORS.primaryColor;
        const radius = fromMe ? { tl: 10, bl: 10, tr: 10, br: 0 } : { tl: 10, bl: 0, tr: 10, br: 10 };
        const padding = fromMe ? { pr: 10, pl: 10 } : { pr: 10, pl: 10 };
        const shouldShowOwner = !fromMe && chat_type === 'GROUP';

        return (
            <View style={[{
                flex: 1,
                justifyContent: 'center',
                alignItems: horizontalAlign,
                transform: [{ scaleY: -1 }],
                paddingVertical: 2,
                paddingHorizontal: 2
            }, style]}>

                <View style={{
                    maxWidth: '80%',
                    minWidth: 100,
                    flex: 0,
                    backgroundColor: messageBackgroundColor,
                    borderTopLeftRadius: radius.tl,
                    borderTopRightRadius: radius.tr,
                    borderBottomLeftRadius: radius.bl,
                    borderBottomRightRadius: radius.br,
                    //...boxShadowChatItem.box_shadow,
                }}>
                    {
                        shouldShowOwner &&
                        <View style={{
                            paddingLeft: padding.pl,
                            paddingRight: padding.pr,
                            paddingTop: 5,
                        }}>
                            <TextView textValue={message.message_owner.name}
                                styles={{
                                    color: messageTimeFontColor,
                                    fontSize: 10
                                }} />
                        </View>

                    }
                    <View style={{
                        flexDirection: 'row'
                    }}>
                        <View style={{
                            paddingTop: 5,
                            paddingBottom: 15,
                            paddingLeft: padding.pl,
                            paddingRight: padding.pr,
                        }}>
                            <TextView textValue={message.message_text}
                                styles={{
                                    fontSize: 15,
                                    color: messageFontColor
                                }} />

                            {
                                message.images &&
                                <View style={{
                                    marginTop: 5,
                                    padding: 3
                                }}>

                                    {

                                        message.images.map((img, indx) =>
                                            <Image
                                                source={{ uri: img.image_url }}
                                                width={200} key={`Image_${indx}`} />)
                                    }

                                </View>
                            }

                        </View>
                        <View style={{
                            justifyContent: 'flex-end',
                            alignSelf: 'flex-end',
                            paddingLeft: padding.pl,
                            paddingRight: padding.pr,
                            position: 'absolute',
                            bottom: 1,
                            right: 1
                        }}>

                            <TextView textValue={this._messageTime(message)}
                                styles={{
                                    fontSize: 10,
                                    color: messageTimeFontColor,
                                    alignSelf: 'flex-end'
                                }} />
                        </View>
                    </View>

                </View>

            </View>
        )
    }

    _messageTime = (message) => {
        if (!message.message_created_at) return;

        let time = message.message_created_at.toDate ?
            message.message_created_at.toDate() :
            message.message_created_at;
        let hour = moment(time);
        let difference = DateUtils.diffDuration(hour, this.today);
        let days = Math.round(difference.asDays());
        if (days > 0) {
            return hour.format('DD/MM/YYYY');
        }
        return hour.format('HH:mm');
    }

    static propTypes = {
        chat_type: PropTypes.string,
        message: PropTypes.object,
        style: PropTypes.object
    }
}