import React from 'react';
import { View, ActivityIndicator, Image, StyleSheet } from 'react-native';
import { COLORS } from '../../../../config/colors/colors';
import { AuthService } from '../../../../services/AuthService';
import TextView from '../../../../components/TextView/TextView';

export class PostPublishingComponent extends React.Component {

    render() {
        return (
            <View style={styles.main_container}>

                <View style={styles.image_container}>
                    <ActivityIndicator size={45} color={COLORS.secondaryColor}></ActivityIndicator>
                    <View style={styles.image_wrapper}>
                        {
                            AuthService.userLoggedData.userPic ?
                                <Image source={{ uri: AuthService.userLoggedData.userPic }} style={{
                                    width: 30,
                                    height: 30,
                                    borderRadius: 50
                                }} /> :
                                <Image source={require('../../../../../assets/user_image.png')} style={{
                                    width: 30,
                                    height: 30,
                                    borderRadius: 50
                                }} />
                        }
                    </View>
                </View>
                <View style={styles.text_container}>
                    <TextView textValue={'Creando Publicación'} />
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({

    main_container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        backgroundColor: COLORS.white,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderColor: COLORS.labelWarning,
        borderWidth: 2
    },
    image_container: {
        flex: 0,
        borderRadius: 50,
        borderColor: COLORS.secondaryColor
    },
    image_wrapper: {
        position: 'absolute',
        zIndex: 9999,
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'

    },
    text_container: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        flex: 1,
        marginLeft: 10
    }

})