import React from 'react';
import { View, ActivityIndicator, Dimensions, FlatList, StyleSheet, KeyboardAvoidingView, Image } from 'react-native';
import { COLORS } from '../../../config/colors/colors';
import TextView from '../../../components/TextView/TextView';
import ViewsNavigation, { HeaderButtons } from '../../main/navigation.config';
import SocialPostsServices from '../../../services/socialposts.services/social.posts.services';
import NotificationServices from '../../../services/NotificationsServices';
import InputText from '../../../components/InputText/InputText';
import { TouchableOpacity, TouchableHighlight, ScrollView } from 'react-native-gesture-handler';
import { FaIcons, IonIcons } from '../../../utils/Icons';
import { FaIconsEnum, IonIconEnum } from '../../../utils/IconsEnum';
import { AuthService } from '../../../services/AuthService';
import Toast from 'react-native-easy-toast';
import Modal from 'react-native-modal';
import Button from '../../../components/Button/Button';
import Utils from '../../../utils/Utils';
import { PostCommentsComponent } from './components/post.comment.component';
import { SocialCommentStyles } from './social.styles';
import { PostHeaderComponent } from './components/post.header.component';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { containerStyles } from '../../../utils/Styles';
import { Request } from '../../../FunctionaityProviders/http.services/http.service';
import Axios from 'axios';

export class SocialCommentsScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerLeft={
                        <HeaderButtons
                            action={
                                () => {
                                    let backFn = navigation.getParam('onReturnToPost');
                                    backFn && backFn();
                                    navigation.goBack(null);
                                }
                            }>
                            {FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)}
                        </HeaderButtons>
                    }
                    headerTitle={'Comentarios'}
                />

        }
    }

    private socialPostServices: SocialPostsServices;
    private notificationServices: NotificationServices;
    private SocialCommentPublisherStyles;
    private requestComments: Request;



    defaultState = {
        isLoading: true,
        post: this.props.navigation.getParam('post') || {},
        page: 1,
        isLoadingMore: false,
        listEnd: false,
        isRefreshing: false,
        windowWidth: Dimensions.get('window').width,
        arrComments: [],
        currentComment: {
            post_content: null,
            post_user_owner_id: AuthService.userLoggedData.userId,
            post_user_owner: AuthService.userLoggedData.userName,
            post_type: 'comment',
            images: [],
            attachments: []
        },
        savingComment: false,
        showModalEdit: false,
        commentToEdit: null,
        commentToAnswer: null,
        parentComment: null,
        scrollBegun: false

    }

    state = Object.assign({}, this.defaultState)

    constructor(props) {
        super(props);

        this.SocialCommentPublisherStyles = SocialCommentStyles()

        this.socialPostServices = new SocialPostsServices;
        this.notificationServices = new NotificationServices;
    }

    componentWillMount() {
        this._findData();
    }

    componentWillUnmount() {
        if (this.requestComments) {
            this.requestComments.cancel('Cancelado componentWillUnmount')
        }
    }

    render() {

        return (

            <View style={this.SocialCommentPublisherStyles.list_container}>
                {
                    this.state.isLoading &&
                    <View style={this.SocialCommentPublisherStyles.no_data_container
                    }>
                        <ActivityIndicator animating size={'small'} color={COLORS.secondaryColor} />
                        <TextView textValue={'Cargando comentarios ....'} />
                    </View>
                }
                {
                    !this.state.isLoading &&
                    <View style={{
                        flex: 1
                    }}>

                        <FlatList
                            contentContainerStyle={{
                                marginHorizontal: containerStyles.marginHorizontal
                            }}
                            data={this.state.arrComments}
                            keyExtractor={(item) => item.uuid}
                            renderItem={this._renderComment}
                            onEndReached={this._handlePage}
                            onEndReachedThreshold={0.2}
                            showsVerticalScrollIndicator={false}
                            onMomentumScrollBegin={() => {
                                this.state.scrollBegun = true;
                            }}
                            initialNumToRender={10}
                            maxToRenderPerBatch={2}
                            onRefresh={() => {
                                this.setState({
                                    isRefreshing: true,
                                    page: 1
                                }, () => {
                                    this._findData();
                                })
                            }}
                            refreshing={this.state.isRefreshing}
                            ListHeaderComponent={this._listHeader}
                            ListFooterComponent={
                                () => {
                                    if (!this.state.isLoadingMore) return null;
                                    return (
                                        <View style={this.SocialCommentPublisherStyles.list_footer}>
                                            <ActivityIndicator size={20} color={COLORS.secondaryColor} />
                                        </View>
                                    )
                                }
                            }
                        />
                        <View style={this.SocialCommentPublisherStyles.comment_publisher_answering_container}>
                            {
                                this.state.commentToAnswer !== null &&
                                <View style={this.SocialCommentPublisherStyles.comment_publisher_answering}>
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'row'
                                    }}>
                                        <TextView textValue={`Respondiendo a `} styles={{
                                            fontSize: 12
                                        }} />
                                        <TextView textValue={this.state.commentToAnswer.author.usuario} styles={{
                                            fontWeight: 'bold',
                                            fontSize: 12
                                        }} />
                                    </View>
                                    <View style={{
                                        width: 30,
                                        borderRadius: 50
                                    }}>
                                        <TouchableOpacity style={{
                                            padding: 5,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}
                                            onPress={this._releaseComment}>
                                            {
                                                IonIcons.getIcon(IonIconEnum.CLOSE_X, 15, COLORS.white)
                                            }
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            }

                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                padding: 2
                            }}>
                                <InputText
                                    ref='inputComment'
                                    type={'textarea'}
                                    multilineMaxHeight={100}
                                    placeholder={'Comentar'}
                                    styles={{
                                        flex: 1,
                                        minHeight: 40
                                    }}
                                    value={this.state.currentComment.post_content}
                                    onChange={(value) => {
                                        this.setState({
                                            currentComment: {
                                                ...this.state.currentComment,
                                                post_content: value
                                            }
                                        })
                                    }}
                                    enabled={!this.state.savingComment}
                                />

                                <View style={{
                                    opacity: this.state.currentComment.post_content ? 1 : 0.5,
                                }}>
                                    <TouchableOpacity style={{
                                        paddingHorizontal: 10,
                                        paddingVertical: 10,
                                        justifyContent: 'center',
                                        alignItems: 'center',

                                    }}
                                        onPress={() => {
                                            this.state.currentComment.post_content != null && this._sendComment()
                                        }}
                                        // activeOpacity={!this.state.currentComment.post_content ? 1 : 0.5}
                                        disabled={!this.state.currentComment.post_content || this.state.savingComment}>
                                        {FaIcons.getIcon(FaIconsEnum.SEND_EMPTY, 20, COLORS.secondaryColor)}
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                }

                <Modal
                    onBackdropPress={() => {
                        this.setState({
                            showModalEdit: false
                        })
                    }
                    }
                    isVisible={this.state.showModalEdit}
                    style={{
                        justifyContent: 'flex-end',
                        margin: 0,
                    }} >

                    <View style={{
                        backgroundColor: COLORS.white,
                        paddingLeft: 20,
                        paddingBottom: 10
                    }}>
                        <View style={{
                            padding: 10,
                            marginBottom: 5
                        }}>
                            <TextView textValue={'¿Que deseas hacer?'} styles={{
                                fontWeight: 'bold',
                                fontSize: 14
                            }} />
                        </View>
                        <Button text={'Editar Comentario'} type='light'
                            onPress={this._editComment.bind(this)}
                            icon={
                                FaIcons.getIcon(FaIconsEnum.EDIT_EMPTY, 20, COLORS.secondaryColor)
                            }
                            textStyles={{
                                fontSize: 15,
                            }}
                            buttonColorStyles={{
                                borderWidth: 0,
                                backgroundColor: 'transparent',
                            }}
                            buttonStyles={{
                                justifyContent: 'flex-start'
                            }}
                        />
                        <Button text={'Eliminar Comentario'} type='light'
                            onPress={this._deleteComment.bind(this)}
                            icon={
                                FaIcons.getIcon(FaIconsEnum.TRASH_EMPTY, 20, COLORS.secondaryColor)
                            }
                            textStyles={{
                                fontSize: 15
                            }}
                            buttonColorStyles={{
                                borderWidth: 0,
                                backgroundColor: 'transparent'
                            }}
                            buttonStyles={{
                                justifyContent: 'flex-start'
                            }}
                        />

                    </View>

                </Modal>
                <Toast
                    ref='toast'
                    position='top'
                />
            </View>
        )
    }

    _listHeader = () =>
        <View style={{
            flex: 1,
            paddingVertical: 10,
            flexDirection: 'row',
            backgroundColor: COLORS.white,
            borderBottomColor: COLORS.softGrey,
            borderBottomWidth: 0.5,
        }}>
            <View style={{
                borderRadius: 20,
                overflow: "hidden",
                marginRight: 10,
                height: 40,
                width: 40
            }}>
                {
                    this.state.post.author.pic ?

                        <Image source={{ uri: this.state.post.author.pic }}
                            style={{
                                height: 40,
                                width: 40
                            }} />
                        :
                        <Image source={require('../../../../assets/user_image.png')}
                            style={{
                                height: 40,
                                width: 40
                            }} />
                }

            </View>
            <View style={{
                flex: 1,
                justifyContent: 'center'
            }}>
                <TextView textValue={this.state.post.author.nombre_completo}
                    styles={{
                        fontWeight: 'bold',
                        fontSize: 14
                    }} />
                {
                    !!this.state.post.post_content &&
                    <TextView textValue={this.state.post.post_content}
                        styles={{
                            fontSize: 13
                        }} />
                }
            </View>
        </View>

    _renderComment = ({ item }) =>
        <PostCommentsComponent comment={item}
            parent={this.state.post} //Todas las respuestas que se generen dentro de este comentario, serán asociadas a el mismo
            onClickAnswerComment={this._answerPost}
            onLongPress={() => {
                this.setState({
                    showModalEdit: true,
                    commentToEdit: item
                })
            }} />

    _editComment = () => {

    }

    _deleteComment = () => {

        let commentToEdit = this.state.commentToEdit;
        if (commentToEdit) {

            this.socialPostServices.deleteComment(this.state.post.uuid, commentToEdit.uuid)
                .then(res => {

                    if (res.success) {

                        let comments = this.state.arrComments;
                        Utils.arrayDeleteItem(comments, (comm) => comm.uuid === commentToEdit.uuid);
                        this.setState({
                            arrComments: comments,
                            showModalEdit: false
                        })
                        this.refs.toast.show('Eliminado', 800)

                    } else {
                        throw Error(res.message);
                    }

                })
                .catch(err => {
                    console.log(err);
                    this.notificationServices.fail('No se pudo eliminar comentario');
                })

        }

    }

    _sendComment = () => {

        let comment = Object.assign({}, this.state.currentComment);
        let images = comment['images'];
        let attachments = comment['attachments'];

        let parentPostUuid = this.state.commentToAnswer ?
            this.state.parentComment ? // Si es un comentario hijo de otro comentario quien se responde
                this.state.parentComment.uuid : //Cojemos el uuid del comentario padre
                this.state.commentToAnswer.uuid : //Cogemos el uuid del comentario que se responde(Hino de un post)
            this.state.post.uuid  //Se está respondiendo un post      

        let objectRequest = new FormData();

        Object.keys(comment).forEach(key => {
            objectRequest.append(key,
                (comment[key] instanceof Array)
                    ? JSON.stringify(comment[key]) : comment[key])
        });

        images.forEach(image => {
            objectRequest.append('images_data', {
                name: String(image.id),
                type: image.image_mime,
                uri: image.image_url
            })
        });

        attachments.forEach(att => {
            objectRequest.append('attachs_data', {
                name: String(att.id),
                type: att.attachment_mime,
                uri: att.attachment_url
            })
        });

        this.socialPostServices.saveComment(parentPostUuid, objectRequest)
            .then(res => {
                if (res.success) {

                    this.refs.toast.show('Publicado', 1000);

                    let data = [];

                    //Actualizamos el comentario actual para que refresque el
                    //# de comentarios agregando otro más
                    if (this.state.commentToAnswer) {

                        //Si no es un comentario Hijo de otro comentario, 
                        //actualizamos el comentario pricipal(Este es el hijo del post)
                        if (!this.state.parentComment) {

                            data = this.state.arrComments.map(item => {
                                return item.uuid === this.state.commentToAnswer.uuid ?
                                    Object.assign({}, item) :
                                    item
                            });

                            data.forEach(comm => {
                                if (comm.uuid === this.state.commentToAnswer.uuid) {
                                    comm.post_comments += 1;
                                    comm.post_likes = 0;
                                    comm.user_liked = false;
                                }
                            })
                        } else {
                            //Si es comentario Hijo de otro comentario, actualizamos el comentario padre
                            //Este comentario padre es un comentario de un Post
                            data = this.state.arrComments.map(item => {
                                return item.uuid === this.state.parentComment.uuid ?
                                    Object.assign({}, item) :
                                    item
                            });

                            data.forEach(comm => {
                                if (comm.uuid === this.state.parentComment.uuid) {
                                    comm.post_comments += 1;
                                    comm.post_likes = 0;
                                    comm.user_liked = false;
                                }
                            })

                        }
                    } else {
                        //Comenta un POST
                        this.state.arrComments.unshift(res.data);
                        data = this.state.arrComments;
                    }

                    this.setState({
                        currentComment: this.defaultState.currentComment,
                        arrComments: data,
                        savingComment: false,
                        commentToAnswer: null
                    }, () => {
                        this.refs.inputComment.blur()
                    });

                } else {
                    throw Error(res.message)
                }
            })
            .catch(err => {
                console.log(err);
                this.notificationServices.fail('No se pudo guardar Comentario')
            })

    }


    _answerPost = (comment, parentComment) => {
        this.refs.inputComment.focus();
        this.setState({
            commentToAnswer: comment,
            parentComment: parentComment
        })
    }

    _releaseComment = () => {
        this.setState({
            commentToAnswer: null,
            currentComment: {
                ...this.state.currentComment,
                post_content: null
            }
        })
    }

    _handlePage = () => {
        if (this.state.isLoadingMore || this.state.listEnd || !this.state.scrollBegun) return null;
        console.log('Paging comments')

        this.setState({
            page: this.state.page + 1,
            isLoadingMore: true,
            noData: false,
        }, () => {
            this._findData();
        })
    }


    _findData() {
        let post = this.state.post.uuid;
        let params = {
            page: this.state.page,
            max_rows: 10,
            user_name: AuthService.userLoggedData.userName
        }

        this.requestComments = this.socialPostServices.findComments(post, params)

        this.requestComments
            .then(res => res.data)
            .then((res: any) => {
                if (res.success) {

                    this.setState({
                        isLoading: false,
                        arrComments: this.state.isRefreshing ? res.data :
                            this.state.arrComments.concat(res.data),
                        isLoadingMore: false,
                        listEnd: !res.data.length,
                        isRefreshing: false
                    })
                    this.requestComments = null

                } else {
                    throw Error(res.message);
                }

            })
            .catch((err) => {
                console.log(err);
                if (!Axios.isCancel(err)) {
                    this.notificationServices.fail('No se pudo cargar los comentarios');
                    this.setState({
                        isRefreshing: false,
                        isLoading: false,
                        isLoadingMore: false,
                    })
                }
            })

    }

}
