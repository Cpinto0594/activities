import React from 'react';
import { CustomListComponent } from '../../../components/List/CustomList';
import { ItemUserSelectable } from './components/item.user.selectable';
import SocialConnectionsServices from '../../../services/social.connections.services/social.connections.services';
import { AuthService } from '../../../services/AuthService';
import { View, Dimensions } from 'react-native';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { HeaderButtons } from '../../main/navigation.config';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { COLORS } from '../../../config/colors/colors';
import { SocialPeopleTemplateLoading } from '../../../components/loadingTemplates/loading.templates';
import TextView from '../../../components/TextView/TextView';
import Utils from '../../../utils/Utils';
import FireBaseExpo from '../../../FunctionaityProviders/Firebase/FirebaseExpo';
import { NavigationActions, StackActions } from 'react-navigation';
import { containerStyles } from '../../../utils/Styles';

export class SocialChatSelectDestination extends React.Component {
    private socialConnectionServices: SocialConnectionsServices;
    private firebaseInstance = FireBaseExpo;



    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={navigation.getParam('title', 'Destinatario')}
                    headerLeft={
                        <HeaderButtons action={() => { navigation.goBack(null) }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                    headerRight={
                        <HeaderButtons
                            disabled={navigation.getParam('disableContinue', true)}
                            action={() => {
                                let action = navigation.getParam('backAction');
                                action && action();
                            }}
                        >
                            <TextView textValue={'Hecho'} styles={{
                                fontSize: 12,
                                fontWeight: 'bold',
                                color: COLORS.secondaryColor
                            }} />
                        </ HeaderButtons>
                    }
                />

        }
    }


    constructor(props) {
        super(props);
        this.state = {
            arrPeople: [],
            selectedUsers: [],
            loadingData: true,
            windowWidth: Dimensions.get('window').width
        }
        this.socialConnectionServices = new SocialConnectionsServices;

    }

    componentDidMount() {
        this._findData();
        this.props.navigation.setParams({
            backAction: () => {
                this._navigateConfirmGroup();
            },
        })
    }


    render() {
        return (
            <View style={{
                flex: 1
            }}>

                <CustomListComponent
                    data={this.state.arrPeople}
                    keyExtractor={(item, index) => item.uuid}
                    renderItem={this._renderItem}
                    showLoadingTemplate={this.state.loadingData}
                    loadingTemplate={() =>
                        <View style={{
                            marginHorizontal: containerStyles.marginHorizontal,
                            paddingVertical: 5
                        }}>
                            <SocialPeopleTemplateLoading width={this.state.windowWidth - (containerStyles.marginHorizontal + 20)} />
                        </View>
                    }
                    ItemSeparatorComponent={() => <View style={{ height: 2 }}></View>}
                />

            </View>
        )
    }

    _renderItem = ({ item }) =>
        <ItemUserSelectable user={item}
            selected={item.selected}
            onPress={(selected) => {

                item.selected = selected;

                this.setState({
                    arrPeople: [...this.state.arrPeople]
                }, () => {
                    let hasSelected = this.state.arrPeople.filter(itm => itm.selected).length;
                    this.props.navigation.setParams({
                        disableContinue: hasSelected <= 0
                    });
                    this.props.navigation.setParams({
                        title: hasSelected > 1 ? 'Crear grupo' : 'Destinatario'
                    })
                })

            }} />


    _findData() {
        let params = {
            page: 1,
            max_rows: 10
        }

        let user = AuthService.userLoggedData.userIdentifier;

        this.socialConnectionServices
            .findUserPeople(user, params)
            .then(async res => {
                if (res.success) {

                    let userPeople = (res.data || [])

                    let members = userPeople
                        .filter(member => member.uuid !== user);

                    this.setState({
                        arrPeople: members,
                        loadingData: false
                    })



                } else {
                    throw Error(res.message);
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loadingData: false,
                })
            })
    }

    _navigateConfirmGroup = () => {
        let memberSelected = this.state.arrPeople.filter(itm => itm.selected) || [];
        let createGroup = memberSelected.length > 1;
        //Si seleccionamos mas de un integrante, vamos a la pantalla de crear grupo, de lo contrario 
        //creamos la conversación privada
        if (createGroup) {
            Utils.defaultNavigation(this.props.navigation, 'CreateChatConfirmation', {
                arrPeople: this.state.arrPeople
            })
        } else {
            let dest = memberSelected[0];

            let chat_owner = {
                uuid: AuthService.userLoggedData.userIdentifier,
                name: AuthService.userLoggedData.userFullName,
                pic: AuthService.userLoggedData.userPic
            }

            let chat_destination = {
                uuid: dest.uuid,
                name: dest.nombre_completo,
                pic: dest.pic
            }

            let navigationParams = {
                resource: {
                    chat_description: dest.nombre_completo,
                    chat_title: dest.nombre_completo,
                    chat_owner: {
                        uuid: AuthService.userLoggedData.userIdentifier,
                        name: AuthService.userLoggedData.userFullName,
                        pic: AuthService.userLoggedData.userPic
                    },
                    chat_members: [
                        AuthService.userLoggedData.userIdentifier,
                        dest.uuid
                    ],
                    chat_members_detail: [
                        chat_destination,
                        chat_owner
                    ]
                }
            };

            Utils.navigationFullStackResetWithActions(this.props.navigation , 1 , [
                    NavigationActions.navigate({
                        routeName: 'ModulesDrawer', 
                        action:
                            NavigationActions.navigate({ routeName: 'Chat' })
                    }),
                    NavigationActions.navigate({ routeName: 'ChatView', params: navigationParams })
                ])

            // Utils.navigationResetWithActions(this.props.navigation, 0, [
            //     NavigationActions.navigate({ routeName: 'Chats' }),
            //     StackActions-
            // ])
        }
    }

}