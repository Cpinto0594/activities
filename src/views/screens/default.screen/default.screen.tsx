import React from 'react';
import { View, Image } from 'react-native';
import { COLORS } from '../../../config/colors/colors';
import TextView from '../../../components/TextView/TextView';
import STRINGS from '../../../config/strings/system.strings';
import Button from '../../../components/Button/Button';
import STRINGS_LOGIN from '../../../config/strings/login.strings';
import { AuthService } from '../../../services/AuthService';
import Utils from '../../../utils/Utils';

export class DefaultScreen extends React.Component {

    render() {

        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.backgroundColor,
                justifyContent: 'center',
                alignItems: 'center'
            }}>

                <Image source={require('../../../../assets/no_access_cellphone.png')} style={{
                    width: 80,
                    height: 80
                }} />
                <TextView textValue={STRINGS.NoMenu}
                    styles={{
                        fontSize: 12,
                        marginTop: 20
                    }} />
                <Button type='danger'
                    text={STRINGS_LOGIN.CerrarSesion}
                    buttonStyles={{
                        borderRadius: 0,
                        position: 'absolute',
                        bottom: 0,
                        width: 100 + '%'
                    }}
                    onPress={() => {
                        AuthService.logOut();
                        Utils.navigateWithStackReset(this.props.navigation, 'Login')
                    }}
                />


            </View>
        )

    }

}