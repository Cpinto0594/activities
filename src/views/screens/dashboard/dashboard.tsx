import React from 'react';
import { View, StyleSheet, Animated, TouchableOpacity, ActivityIndicator } from 'react-native';
import { AuthService } from '../../../services/AuthService';
import { ScrollView } from 'react-navigation';
import UsuariosServices from '../../../services/usuarios.services/usuarios.services';
import { COLORS } from '../../../config/colors/colors';
import TextView from '../../../components/TextView/TextView';
import { LoadingIndicator } from '../../../components/loading/loding';
import moment from 'moment';

import {
    LineChart,
} from 'react-native-chart-kit';
import { Dimensions } from 'react-native'
import NotificationServices from '../../../services/NotificationsServices';
import { TouchableHighlight } from 'react-native-gesture-handler';
import ActividadesServices from '../../../services/actividades.services/actividades.services';
import { boxShadow, containerStyles } from '../../../utils/Styles';
import Toast from 'react-native-easy-toast'

//Comment for ejected App
//import Orientation from 'react-native-orientation';

//Comment for Expo App
import 'moment/locale/es';
import { ScreenOrientation } from 'expo';
import { HeaderButtons } from '../../main/navigation.config';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import Axios from 'axios';
import { Request } from '../../../FunctionaityProviders/http.services/http.service';
import AsyncStorageProvider from '../../../FunctionaityProviders/AsyncStorageprovider';
import DefaultCustomHeader from '../../../components/Headers/custom_header';


export default class DashBoard extends React.Component {
    private notificationServices: NotificationServices;
    private actividadesServices: ActividadesServices;
    private isLoadingAnimationProyectos = false;
    private isLoadingAnimationEmpresa = false;
    private requestDashBoard: Request;


    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Dahboard'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                navigation.goBack(null)
                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
        }
    };

    defaultState = {
        userInfo: {
            empresas: [],
            proyectos: []
        },
        periodicidad: 'WEEK',
        usuarioSeleccionado: AuthService.userLoggedData.userName,
        dashBoardLoaded: false,
        proyectoSeleccionado: null,
        empresaSeleccionada: null,
        screenWidth: Dimensions.get('window').width,
        animFadeProyecto: new Animated.Value(0),
        animHeightProyectos: new Animated.Value(0),
        animFadeEmpresa: new Animated.Value(0),
        animHeightEmpresa: new Animated.Value(0),
        animOpacityContainer: new Animated.Value(0),
    }

    state = Object.assign({}, this.defaultState);



    constructor(props) {
        super(props);
        this.notificationServices = new NotificationServices;
        this.actividadesServices = new ActividadesServices;


        this._buidUserInfo();

    }

    _orientationListener = () => {
        this.setState({
            screenWidth: Dimensions.get('window').width
        })
    }

    //FOR EXPO

    componentDidMount() {
        this._findData();
        ScreenOrientation.addOrientationChangeListener(this._orientationListener);
    }

    componentWillUnmount() {
        if (this.requestDashBoard) this.requestDashBoard.cancel('Se cancela petición del Dashboard');
        ScreenOrientation.removeOrientationChangeListeners();
    }


    //EJECTED
    // componentDidMount() {
    //     Orientation.addOrientationListener(this._orientationListener);
    // }

    // componentWillUnmount() {
    //     Orientation.removeOrientationListener(this._orientationListener);
    // }

    _buidUserInfo = () => {
        let userData = AuthService.userLoggedData;
        let usuario = {
            id: userData.userId,
            nombre_completo: userData.userFullName,
            usuario: userData.userName,
            email: userData.userEmail,
            empresas: userData.userEmpresas,
            proyectos: userData.userProyectos,
            userPic: userData.userPic
        };
        this.state.userInfo = usuario;
    }

    _clearDashboard() {
        this.state = Object.assign({}, this.defaultState);
        this.setState({ ...this.state });

    }




    render() {
        return (

            <View style={{
                flex: 1
            }}
            >
                {
                    this.state.userInfo.usuario ?
                        <Animated.View style={[styles.main_container, {
                            //opacity: this.state.animOpacityContainer
                        }]}>
                            <ScrollView ref='scrollView'
                                showsVerticalScrollIndicator={false}>
                                {/* Header Info Usuario */}
                                {/* {this._headerSection()} */}
                                {/* Header Info Usuario */}



                                {/* Fecha */}
                                {this._fechaInfo()}
                                {/* Fecha */}


                                {/* Selector de periodicidad */}
                                {this._periodicidadSelector()}
                                {/* Selector de periodicidad */}


                                {/* Bubbles Horas por empresa y proyecto */
                                    this.state.dashBoardLoaded ?
                                        <View style={styles.bubbles_main_container}>
                                            <View style={{
                                                ...style_box_shadow.box_shadow,
                                                marginBottom: 10,
                                                borderRadius: 10,
                                                backgroundColor: COLORS.white,
                                                marginRight: 5,
                                                marginLeft: 5
                                            }}>
                                                {this._horasProyectosBubbles()}
                                                {this._graficasHorasProyectos()}

                                            </View>
                                            <View style={{
                                                ...style_box_shadow.box_shadow,
                                                marginBottom: 10,
                                                borderRadius: 10,
                                                backgroundColor: COLORS.white,
                                                marginRight: 5,
                                                marginLeft: 5
                                            }}>
                                                {this._horasEmpresasBubbles()}
                                                {this._graficasHorasEmpresas()}
                                            </View>
                                        </View>
                                        :
                                        <View style={[{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            height: 300
                                        }]}>
                                            <ActivityIndicator
                                                size={'small'}
                                                color={COLORS.secondaryColor}
                                            ></ActivityIndicator>
                                            <TextView textValue='Cargando Dashboard ...' styles={{
                                                fontSize: 15,
                                                marginTop: 10
                                            }} />
                                        </View>
                                }
                            </ScrollView>
                        </Animated.View>
                        :
                        // Loading DashBoard
                        <View style={[{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex: 1
                        }]}>
                            <ActivityIndicator
                                size={'small'}
                                color={COLORS.secondaryColor}
                            ></ActivityIndicator>
                            <TextView textValue='Cargando Dashboard ...' styles={{
                                fontSize: 15,
                                marginTop: 10
                            }} />
                        </View>
                }
                {/* Loading */}
                <Toast
                    position='center'
                    ref="toast"
                />
                <LoadingIndicator ref='loading' />
                {/* Loading */}
            </View >
        )
    }

    // HEADER

    _headerSection = () =>
        <View style={styles.header_container}>
            <TextView textValue={'Dashboard'} styles={{
                color: COLORS.secondaryColor,
                fontSize: 25,
                fontWeight: 'bold'
            }} />
            {/* <View style={{
                flex: 0.3
            }}>
                <View style={styles.header_text_circle}>
                    {this.state.userInfo.userPic ?
                        <Image source={{ uri: this.state.userInfo.userPic }}
                            style={{ width: 70, height: 70, backgroundColor: COLORS.white }} /> :
                        <TextView styles={{
                            color: COLORS.white
                        }} textValue={this._nameInitials()} />
                    }

                </View>
            </View>
            <View style={{
                flex: 0.7,
                //paddingLeft: 30
            }}>
                <TextView styles={styles.header_text_name} textValue={this._nameCapitalize()} />
                <TextView styles={styles.header_text_others} textValue={this.state.userInfo.email} />
                <TextView styles={styles.header_text_others} textValue={this.state.userInfo.usuario} />
                <TextView styles={styles.header_text_others} textValue={'Ult. Login: ' + AuthService.userLoggedData.lastLoggedDate} />
            </View> */}

        </View>

    _nameInitials() {
        if (!this.state.userInfo.nombre_completo) return '';
        let parts: string[] = this.state.userInfo.nombre_completo.split(" ");
        return parts[0].substring(0, 1) + parts[1].substring(0, 1);
    }

    _nameCapitalize() {
        if (!this.state.userInfo.nombre_completo) return '';
        let parts: string[] = this.state.userInfo.nombre_completo.split(" ");
        return parts.map(part => part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase()).join(" ")
    }

    // HEADER


    // BUBBLE SECTION

    _bubbles = () =>
        <View style={styles.bubbles_main_container}>
            {this._horasProyectosBubbles()}
            {this._horasEmpresasBubbles()}
        </View>


    _horasProyectosBubbles = () =>
        <View style={styles.bubbles_container}>
            <View style={{
                flexDirection: 'row',
                alignContent: 'center',
                marginHorizontal: containerStyles.marginHorizontal
            }}>
                {
                    FaIcons.getIcon('clock-o', 20, COLORS.secondaryColor)
                }
                <TextView textValue='Proyectos' styles={{
                    fontSize: 15,
                    paddingLeft: 10,
                    marginBottom: 10
                }} />
            </View>
            <ScrollView horizontal
                showsHorizontalScrollIndicator={false}

                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    marginHorizontal: containerStyles.marginHorizontal
                }}>
                {

                    this.state.userInfo.proyectos.map(proyecto => {
                        let color = proyecto.backgroundColor ? proyecto.backgroundColor : bubbleColor();
                        proyecto.backgroundColor = color;
                        proyecto.animScaleBubble = proyecto.animScaleBubble ? proyecto.animScaleBubble : new Animated.Value(0);
                        return (
                            <Animated.View

                                key={proyecto.id}
                                style={[
                                    styles.bubbles,
                                    {
                                        transform: [
                                            {
                                                scale: proyecto.animScaleBubble.interpolate({
                                                    inputRange: [0, 1],
                                                    outputRange: [1, 0.8]
                                                })
                                            }
                                        ]
                                    }
                                ]}>
                                <TouchableHighlight underlayColor={'white'}
                                    activeOpacity={0.8}
                                    onPressIn={() => { this._onPressInBubble(proyecto); }}
                                    onPressOut={() => { this._onPressOutBubble(proyecto) }}
                                    onPress={() => { this._drawChartProyecto(proyecto) }}
                                >

                                    <View style={
                                        [styles.bubble_proyectos_horas_container,
                                        {
                                            backgroundColor: color, borderColor: color, borderWidth: 1
                                        }

                                        ]}>
                                        <TextView textValue={proyecto.horas + ''} styles={styles.bubble_text_horas} />
                                        <TextView textValue={proyecto.nombre} styles={styles.bubble_text_label} />
                                    </View>

                                </TouchableHighlight>
                            </Animated.View>
                        )
                    })

                }

            </ScrollView>
        </View>



    _horasEmpresasBubbles = () =>
        <View style={styles.bubbles_container}>
            <View style={{
                flexDirection: 'row',
                alignContent: 'center',
                marginHorizontal: containerStyles.marginHorizontal
            }}>
                {
                    FaIcons.getIcon('clock-o', 20, COLORS.secondaryColor)
                }
                <TextView textValue='Empresas' styles={{
                    fontSize: 15,
                    paddingLeft: 10,
                    marginBottom: 10
                }} />
            </View>
            <ScrollView horizontal
                showsHorizontalScrollIndicator={false}

                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    marginHorizontal: containerStyles.marginHorizontal
                }}>

                {

                    this.state.userInfo.empresas.map(empresa => {
                        let color = empresa.backgroundColor ? empresa.backgroundColor : bubbleColor();
                        empresa.backgroundColor = color;
                        empresa.animScaleBubble = empresa.animScaleBubble ? empresa.animScaleBubble : new Animated.Value(0);
                        return (
                            <Animated.View
                                key={empresa.id}
                                style={[
                                    styles.bubbles,
                                    {
                                        transform: [
                                            {
                                                scale: empresa.animScaleBubble.interpolate({
                                                    inputRange: [0, 1],
                                                    outputRange: [1, 0.8]
                                                })
                                            }
                                        ]
                                    }
                                ]}>
                                <TouchableHighlight underlayColor={'white'}
                                    activeOpacity={0.8}
                                    onPressIn={() => { this._onPressInBubble(empresa); }}
                                    onPressOut={() => { this._onPressOutBubble(empresa) }}
                                    onPress={() => { this._drawChartEmpresa(empresa) }}
                                >

                                    <View style={
                                        [styles.bubble_proyectos_horas_container,
                                        {
                                            backgroundColor: color, borderColor: color, borderWidth: 1
                                        }

                                        ]}>
                                        <TextView textValue={empresa.horas + ''} styles={styles.bubble_text_horas} />
                                        <TextView textValue={empresa.nombre} styles={styles.bubble_text_label} />
                                    </View>

                                </TouchableHighlight>
                            </Animated.View>
                        )
                    })

                }

            </ScrollView>
        </View>

    _onPressInBubble = (item) => {
        Animated.spring(
            item.animScaleBubble,
            {
                toValue: 1,
                friction: 40,
                velocity: 5
            }
        ).start()
    }

    _onPressOutBubble = (item) => {
        Animated.spring(
            item.animScaleBubble,
            {
                toValue: 0,
                friction: 40,
                velocity: 5
            }
        ).start()
    }

    _drawChartProyecto = (proyecto) => {
        this.setState({
            ...this.state,
            proyectoSeleccionado: proyecto
        });
        this._showChartProyectos();
    }

    _drawChartEmpresa = (empresa) => {

        this.setState({
            ...this.state,
            empresaSeleccionada: empresa
        });
        this._showChartEmpresas();
    }

    _showChartProyectos = () => {
        if (this.isLoadingAnimationProyectos) {
            return;
        }
        this.isLoadingAnimationProyectos = true;

        if (this.state.empresaSeleccionada) {
            this.state.animFadeEmpresa.setValue(1);
            this.state.animHeightEmpresa.setValue(1);
        }
        Animated.parallel([
            Animated.timing(
                this.state.animFadeProyecto,
                {
                    toValue: 1,
                    duration: 300,
                    useNativeDriver: true
                }
            ),
            Animated.spring(
                this.state.animHeightProyectos,
                {
                    toValue: 1,
                    tension: 40,
                    friction: 5,
                    velocity: 5,
                    useNativeDriver: true
                }
            )
        ]).start(() => {
            this.state.animFadeProyecto = new Animated.Value(0)
            this.state.animHeightProyectos = new Animated.Value(0)
            this.isLoadingAnimationProyectos = false;

        });
    }


    _showChartEmpresas = () => {
        if (this.isLoadingAnimationEmpresa) {
            return;
        }
        this.isLoadingAnimationEmpresa = true;

        if (this.state.proyectoSeleccionado) {
            this.state.animFadeProyecto.setValue(1);
            this.state.animHeightProyectos.setValue(1);
        }

        Animated.parallel([
            Animated.timing(
                this.state.animFadeEmpresa,
                {
                    toValue: 1,
                    duration: 300,
                    useNativeDriver: true
                }
            ),
            Animated.spring(
                this.state.animHeightEmpresa,
                {
                    toValue: 1,
                    tension: 40,
                    friction: 5,
                    velocity: 5,
                    useNativeDriver: true
                }
            )
        ]).start(() => {
            this.state.animFadeEmpresa = new Animated.Value(0)
            this.state.animHeightEmpresa = new Animated.Value(0)
            this.isLoadingAnimationEmpresa = false;

            this.refs.scrollView.scrollToEnd();
        });
    }


    // BUBBLE SECTION


    // PERIODICIDAD SECTION

    _periodicidadSelector = () =>
        <View style={{
            flex: 1,
            backgroundColor: COLORS.primaryColor,
            ...style_box_shadow.box_shadow,
            paddingVertical: 10,
            marginBottom: 10,
            paddingHorizontal: containerStyles.marginHorizontal

        }}>
            <View style={{
                flexDirection: 'row',

            }}>
                <TextView textValue={this._labelPeriodicidad()}
                    styles={[styles.fecha_container_text,
                    { width: '90%' }]} />
            </View>
            <View style={styles.periodicidad_container}>

                <TouchableOpacity
                    style={
                        [
                            styles.periodicidad_text_container,
                            this.state.periodicidad === 'WEEK' ? styles.periodicidad_text_seleccionada : null
                        ]}
                    onPress={() => { this._changePeriodicidad('WEEK') }}>
                    <TextView textValue='Esta Semana' styles={[
                        styles.periodicidad_text_label,
                        this.state.periodicidad === 'WEEK' ? { color: COLORS.white } : null
                    ]} />
                </TouchableOpacity>
                <TouchableOpacity
                    style={
                        [
                            styles.periodicidad_text_container,
                            this.state.periodicidad === 'MONTH' ? styles.periodicidad_text_seleccionada : null
                        ]
                    }
                    onPress={() => { this._changePeriodicidad('MONTH') }}>
                    <TextView textValue='Este Mes' styles={[
                        styles.periodicidad_text_label,
                        this.state.periodicidad === 'MONTH' ? { color: COLORS.white } : null
                    ]} />
                </TouchableOpacity>
                <TouchableOpacity
                    style={
                        [
                            styles.periodicidad_text_container,
                            this.state.periodicidad === 'TRIM' ? styles.periodicidad_text_seleccionada : null
                        ]
                    }
                    onPress={() => { this._changePeriodicidad('TRIM') }}>
                    <TextView textValue='Ultimo Trimestre' styles={[
                        styles.periodicidad_text_label,
                        this.state.periodicidad === 'TRIM' ? { color: COLORS.white } : null
                    ]} />
                </TouchableOpacity>
            </View>
        </View>


    _changePeriodicidad = (periodicidad: string) => {
        this.setState({
            periodicidad: periodicidad,
            empresaSeleccionada: null,
            proyectoSeleccionado: null,
            dashBoardLoaded: false
        }, () => {
            this._storeData(null);
            // this._showLoading(true);
            this._findDashboard();
        });

    }

    _fechaInfo = () =>
        null

    _labelPeriodicidad() {
        var date = moment().locale('es');

        if (this.state.periodicidad === 'WEEK') {
            const inicio_semana = date.startOf('isoWeek').get('date');
            const fin_semana = date.endOf('isoWeek').get('date');
            const month = date.format('MMMM')
            return month.toUpperCase() + ' ' + inicio_semana + ' - ' + fin_semana;
        } else if (this.state.periodicidad === 'MONTH') {
            return date.format('MMMM').toUpperCase();
        } else {
            return date.clone().subtract(2, 'months').format('MMMM').toUpperCase() + ' - ' + date.format('MMMM').toUpperCase();
        }

    }



    // PERIODICIDAD SECTION


    // GRAFICAS SECTION
    _graficasHorasProyectos = () => {
        let colorChart = bubbleColor();
        let config = chart_styles();
        let data = this.state.proyectoSeleccionado;

        config.chart_config.color = () => colorChart;

        if (data) {

            let data_chart = data.data.length ? data.data : [0];
            let labels = data.labels.length ? data.labels : ['None'];

            config.data_config.datasets[0].data = data_chart;
            config.data_config.labels = labels;
            config.data_config.datasets[0].color = () => data.backgroundColor;
        }

        return (
            this.state.proyectoSeleccionado ?
                <Animated.View style={[styles.chart_container, {
                    opacity: this.state.animFadeProyecto,
                    transform: [
                        { scale: this.state.animHeightProyectos }
                    ]
                }]}
                >
                    <View style={{
                        padding: 5,
                        borderTopColor: '#cdcdcd',
                        borderTopWidth: 0.5
                    }}>
                        <TextView textValue={this.state.proyectoSeleccionado.nombre}
                            styles={{
                                fontSize: 15,
                                textAlign: 'center',
                                color: this.state.proyectoSeleccionado.backgroundColor,
                                marginBottom: 15
                            }} />
                        <LineChart
                            key={this.state.proyectoSeleccionado.id}
                            data={config.data_config}
                            width={this.state.screenWidth - 30}
                            height={220}
                            chartConfig={config.chart_config}
                            bezier
                            onDataPointClick={(a) => {
                                this.refs.toast.show(a.value)
                            }}

                        />
                    </View>
                </Animated.View>
                : null
        )
    }



    _graficasHorasEmpresas = () => {
        let colorChart = bubbleColor();
        let config = chart_styles();
        let data = this.state.empresaSeleccionada;

        config.chart_config.color = () => colorChart;

        if (data) {

            let data_chart = data.data.length ? data.data : [0];
            let labels = data.labels.length ? data.labels : ['None'];

            config.data_config.datasets[0].data = data_chart;
            config.data_config.labels = labels;
            config.data_config.datasets[0].color = () => data.backgroundColor;
        }

        return (
            this.state.empresaSeleccionada ?
                <Animated.View style={[styles.chart_container, {
                    opacity: this.state.animFadeEmpresa,
                    transform: [
                        { scale: this.state.animHeightEmpresa }
                    ]
                }]}
                >
                    <View style={{
                        padding: 5,
                        borderTopColor: '#cdcdcd',
                        borderTopWidth: 0.5
                    }}>
                        <TextView textValue={this.state.empresaSeleccionada.nombre}
                            styles={{
                                fontSize: 15,
                                textAlign: 'center',
                                color: this.state.empresaSeleccionada.backgroundColor,
                                marginBottom: 15
                            }} />
                        <LineChart
                            key={this.state.empresaSeleccionada.id}
                            data={config.data_config}
                            width={this.state.screenWidth - 30}
                            height={220}
                            chartConfig={config.chart_config}
                            bezier
                            onDataPointClick={(a) => {
                                this.refs.toast.show(a.value)
                            }}
                        />
                    </View>
                </Animated.View>
                : null
        )
    }

    // GRAFICAS SECTION

    _showLoading(bool) {
        this.refs.loading[bool ? 'show' : 'hide']();
    }


    ///////Fetch Data

    _findData() {
        // this._showLoading(true);
        Promise.resolve()
            .then(() => {

            })
            .then(async () => {

                // let storedData = await this._retrieveStoredData();
                // //Almacenamos el dashboard por tiempo de 1 hora
                // if (storedData) {
                //     let diff = DateUtils.diffDuration(moment(), moment(storedData.last_check));
                //     if (diff.asHours() < 1) {
                //         this._buildDashBoardData(storedData);
                //         return;
                //     }
                // }
                this._findDashboard();

            })
            .catch(
                (e) => {
                    this.notificationServices.fail('No se pudo obtener información del Usuario');
                    console.log(e)
                    this._showLoading(false);
                });
    }

    _findDashboard() {
        this.requestDashBoard = this.actividadesServices.findDashboard({
            usuario: AuthService.userLoggedData.userName,
            date: moment().format('YYYY-MM-DD'),
            periodicidad: this.state.periodicidad
        })

        this.requestDashBoard.then(res => res.data)
            .then(response => {
                this.requestDashBoard = null;
                if (response.success) {
                    let data = response.data;
                    data.last_check = new Date()


                    this._buildDashBoardData(data)


                }
            })
            .catch((e) => {
                if (!Axios.isCancel(e)) {
                    this.notificationServices.fail(e.message, () => {
                        // this._showLoading(false);
                    })
                }
            });
    }
    _buildDashBoardData(data: any) {

        //Guardamos el data en el storage para reducir el llamado a este
        //Servicio en caso de cambios de Stacks.
        //Sobreescribir este valor cuando se presionen los botones de periodicidad.
        this._storeData(data);

        this.state.userInfo.empresas.forEach(emp => {
            let hora_empresa = data.horasEmpresas
                .find(empresa => empresa.empresa_id === emp.id &&
                    empresa.usuario === this.state.userInfo.usuario);

            let char_data = data.chartEmpresas
                .filter(empresa => empresa.empresa_id === emp.id &&
                    empresa.dia !== 'NONE' && empresa.usuario === this.state.userInfo.usuario);

            Object.assign(emp, {
                nombre: emp.descripcion,
                horas: hora_empresa.cantidad_horas,
                labels: ['X'].concat(char_data.map(empresa => empresa.dia)),
                data: [0].concat(char_data.map(empresa => empresa.cantidad_horas))
            });
        });

        this.state.userInfo.proyectos.forEach(pro => {
            let hora_proyecto = data.horasProyectos
                .find(proyecto => proyecto.proyecto_id === pro.id &&
                    proyecto.usuario === this.state.userInfo.usuario);

            let char_data = data.chartProyectos
                .filter(proyecto => proyecto.proyecto_id === pro.id &&
                    proyecto.dia !== 'NONE' && proyecto.usuario === this.state.userInfo.usuario);

            Object.assign(pro, {
                nombre: pro.descripcion,
                horas: hora_proyecto.cantidad_horas,
                labels: ['X'].concat(char_data.map(proyecto => proyecto.dia)),
                data: [0].concat(char_data.map(proyecto => proyecto.cantidad_horas))
            })
        });

        //Order Descending
        this.state.userInfo.empresas.sort((a, b) => {
            return b.horas - a.horas;
        });

        this.state.userInfo.proyectos.sort((a, b) => {
            return b.horas - a.horas;
        });

        this.state.animFadeEmpresa.setValue(1);
        this.state.animFadeProyecto.setValue(1);
        this.state.animHeightEmpresa.setValue(1);
        this.state.animHeightProyectos.setValue(1);
        this.state.proyectoSeleccionado = this.state.userInfo.proyectos[0];
        this.state.empresaSeleccionada = this.state.userInfo.empresas[0];

        this.setState({
            userInfo: this.state.userInfo,
            dashBoardLoaded: true
        }, () => {

            //Si no ha sido animado el container, lo animamos
            // if (!this.isLoadingMainContainer)
            //     Animated.timing(this.state.animOpacityContainer, {
            //         toValue: 1,
            //         duration: 500
            //     }).start(() => {
            //         this.isLoadingMainContainer = true
            //     });

        });
        // this._showLoading(false);
    }

    _storeData = async (data) => {
        AsyncStorageProvider.setItem('dashboard', JSON.stringify(data))
    }

    _retrieveStoredData = async () => {
        let data = await AsyncStorageProvider.getItem('dashboard');
        return data ? JSON.parse(data) : null;
    }


}


const style_box_shadow = boxShadow;

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.listBackgroundColor,
        flex: 1,
        width: 100 + '%'
    },
    header_container: {
        width: '100%',
        flexDirection: 'row',
        paddingTop: 10,
        //paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        //height: 100,
        backgroundColor: COLORS.white,
        justifyContent: 'flex-start',
        alignItems: 'center',
        ...style_box_shadow.box_shadow
    },
    header_text_circle: {
        borderColor: COLORS.white,
        borderWidth: 2,
        borderRadius: 50,
        width: 70,
        height: 70,
        marginLeft: 10,
        backgroundColor: COLORS.secondaryColor,
        ...style_box_shadow.box_shadow,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    header_text_name: {
        fontSize: 18,
        marginBottom: 5,
        color: COLORS.white
    },
    header_text_others: {
        fontSize: 12,
        color: COLORS.white
    },
    fecha_container: {
        alignItems: 'flex-start',
        //marginTop: 15,
        paddingLeft: 10,
        height: 20
    },
    fecha_container_text: {
        fontSize: 15,
        color: COLORS.secondaryColor,
        fontWeight: 'bold'
    },
    periodicidad_container: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        marginTop: 10,

    },
    periodicidad_text_container: {
        padding: 5,
        width: 33.3 + '%',
        borderRadius: 5
    },
    periodicidad_text_label: {
        color: '#9e9e9e',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 12,

    },
    periodicidad_text_seleccionada: {
        backgroundColor: COLORS.secondaryColor,
        borderRadius: 10
    },
    bubbles_main_container: {
        flex: 1,
        marginTop: 2
    },
    bubbles_container: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 2,
        paddingRight: 2,
        height: 170,

    },
    bubbles: {
        borderRadius: 50,
        marginRight: 10,
        elevation: 3,
        overflow: 'hidden'

    },
    bubble_proyectos_horas_container: {
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    bubble_text_horas: {
        fontSize: 30,
        textAlign: 'center',
        color: COLORS.white
    },
    bubble_text_label: {
        fontSize: 10,
        flexWrap: 'wrap',
        textAlign: 'center',
        color: COLORS.white
    },
    chart_container: {
        flex: 1,
    }

});

const bubble_colors = [
    '#044689',
    '#097684',
    '#4D3E6B',
    '#D97E68',
    '#D3B66E',
    '#C62DA3',
    '#1B998B',
    '#44ABA0',
    '#21343A',
    '#7E90BA',
    '#A57EBA',
    '#BC0505',
]



const bubbleColor = () => {
    let min = 0;
    let max = bubble_colors.length - 1;
    var random = Math.floor(Math.random() * (+max - +min) + +min);
    return bubble_colors[random];

}
const chart_styles = () => ({
    chart_config: {
        backgroundColor: 'white',
        backgroundGradientFrom: 'white',
        backgroundGradientTo: 'white',
        decimalPlaces: 0, // optional, defaults to 2dp
        stroke: 3,
        color: () => `#8A3393`,
        style: {
            borderRadius: 16
        }
    },
    data_config: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June'],
        datasets: [
            {
                data: [20, 45, 28, 80, 99, 43],
                color: () => `#BA4878`,// optional
                strokeWidth: 2 // optional
            }
        ]
    }
})
