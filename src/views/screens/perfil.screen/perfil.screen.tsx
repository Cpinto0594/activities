import React from 'react';
import TextView from '../../../components/TextView/TextView';
import { View, StyleSheet, Animated, Dimensions, Platform } from 'react-native';
import { USUARIOS } from '../../../config/strings/usuarios.strings';
import InputText from '../../../components/InputText/InputText';
import Button from '../../../components/Button/Button';
import { COLORS } from '../../../config/colors/colors';
import NotificationServices from '../../../services/NotificationsServices';
import UsuariosServices from '../../../services/usuarios.services/usuarios.services';
import { sha256 } from 'js-sha256';
import EmpresasServices from '../../../services/empresas.services/empresas.services';
import ProyectosServices from '../../../services/proyectos.services/proyectos.services';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import { LoadingIndicator } from '../../../components/loading/loding';
import Modal from "react-native-modal";
import PropTypes from 'prop-types';
import ImageLoader from '../../../FunctionaityProviders/image_camerapicker/image_camera_picker';

import { boxShadow, containerStyles, boxShadows } from '../../../utils/Styles';
import Separator from '../../main/separator.view';
import { AuthService } from '../../../services/AuthService';
import ImageManipulator from '../../../FunctionaityProviders/image_manipulator/image_manipulator';
import Toast from 'react-native-easy-toast';
import { ViewsEnum } from '../../../models/MenuStyleEnum';

import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import Utils from '../../../utils/Utils';
import { CircularImage } from '../../../components/CircularImage/circular_image';


export default class PerfilScreen extends React.Component {

    private notificationServices: NotificationServices;
    private usuariosServices: UsuariosServices;
    private empresasServices: EmpresasServices;
    private proyectosServices: ProyectosServices;
    private imageLoader: ImageLoader;
    private imageManipulator: ImageManipulator;




    defaultState = {
        usuario: this.props.navigation.getParam('item') || {},
        empresas: [],
        proyectos: [],
        userAppConfig: {},
        arrEmpresas: [],
        arrProyectos: [],
        tabIndex: 0,
        animationSlide: new Animated.Value(1),
        action: '',
        showModalImagePicker: false,
        canSelectImage: true,
        shouldHideConfig: AuthService.shouldHideView(ViewsEnum.ProfileAppConfig),
        shouldHideAsocc: AuthService.shouldHideView(ViewsEnum.ProfileAssociate),
        userSelectedEmpresas: [],
        userSelectedProyectos: []
    }

    state = Object.assign({}, this.defaultState);


    constructor(props) {
        super(props);
        this.notificationServices = new NotificationServices;
        this.usuariosServices = new UsuariosServices;
        this.empresasServices = new EmpresasServices;
        this.proyectosServices = new ProyectosServices;
        this.imageLoader = new ImageLoader;
        this.imageManipulator = new ImageManipulator;

    }




    componentDidMount() {
        this.getPermissionAsync();
        this._findData();
    }

    _clearData() {
        this.setState({
            ...this.defaultState
        })
    }


    render() {

        const { action } = this.props;
        this.state.action = action;
        let disableUserName = 'profile' === action;


        return (
            <View style={styles.main_container}>
                <ScrollView showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        flexGrow: 1
                    }}>
                    <View style={styles.header_container}>

                        <View style={styles.header_image_container}>

                            <View style={[styles.photo, { ...boxShadows(6, COLORS.red).box_shadow }]}>
                                <View style={[styles.photo, { overflow: 'hidden' }]}>
                                    <TouchableHighlight onPress={this._showImagePicker.bind(this)}
                                        style={{
                                            borderColor: 'red',
                                            borderWidth: 1,
                                            height: 80,
                                            width: 80,
                                        }}>

                                        <CircularImage
                                            source={
                                                this.state.usuario.pic ?
                                                    { uri: this.state.usuario.pic } :
                                                    require('../../../../assets/user_image.png')
                                            }
                                            width={80}
                                            height={80}
                                            style={{ backgroundColor: COLORS.white }}
                                        />


                                    </TouchableHighlight>
                                </View>
                            </View>

                        </View>
                        <View style={styles.header_data_container}>

                            <View style={{
                                padding: 10
                            }}>
                                <TextView styles={{
                                    fontSize: 15,
                                    fontWeight: 'bold',
                                    color: COLORS.secondaryColor
                                }} textValue={this.state.usuario.nombre_completo} />
                                <TextView
                                    styles={{
                                        fontSize: 10,
                                        color: COLORS.secondaryColor
                                    }} textValue={this.state.usuario.email} />
                                <TextView
                                    styles={{
                                        fontSize: 10,
                                        color: COLORS.secondaryColor
                                    }} textValue={this.state.usuario.usuario} />

                                <Button type='light' onPress={this._crearUsuario.bind(this)}
                                    text={USUARIOS.Guardarusuario}
                                    buttonColorStyles={{
                                    }}
                                    buttonStyles={{
                                        borderRadius: 50,
                                        padding: 5,
                                        marginTop: 10,
                                        borderColor: COLORS.softGrey,
                                        borderWidth: 0.5,
                                        ...boxShadows(4).box_shadow
                                    }}></Button>
                            </View>

                        </View>
                    </View>
                    {/* BottomTab */}
                    <View style={styles.forms_main_container}>
                        <View style={{
                            flex: 1,
                            marginHorizontal: containerStyles.marginHorizontal
                        }}>
                            <View style={[styles.form_group]}>
                                <View style={{ flex: 0.4 }}>
                                    <TextView textValue={USUARIOS.Identificacion + ' *'} styles={styles.label_color}></TextView>
                                    <InputText value={this.state.usuario.identificacion} placeholder={USUARIOS.Identificacion}
                                        onChange={(value) => { this.setState({ usuario: { ...this.state.usuario, identificacion: value } }); }}
                                        styles={styles.border_input}></InputText>

                                    {/* <TextField
                                    ref='identificacion'
                                    label={USUARIOS.Identificacion}
                                    value={this.state.usuario.identificacion}
                                    tintColor={COLORS.secondaryColor}
                                    baseColor={COLORS.softGrey}
                                    animationDuration={200}
                                    onChangeText={(value) => this.setState({ usuario: { ...this.state.usuario, identificacion: value } })}
                                    returnKeyType='next'
                                    onSubmitEditing={() => {
                                        this.refs.usuario.focus()
                                    }}
                                    labelHeight={15}

                                /> */}

                                </View>

                            </View>
                            <View style={styles.form_group}>
                                <TextView textValue={USUARIOS.Codigo + ' *'} styles={styles.label_color}></TextView>
                                <InputText value={this.state.usuario.usuario} placeholder={USUARIOS.Codigo}
                                    onChange={(value) => { this.setState({ usuario: { ...this.state.usuario, usuario: value } }); }}
                                    styles={[styles.border_input]}
                                    enabled={!disableUserName}
                                    toUpper
                                ></InputText>

                                {/* <TextField
                                    ref='usuario'
                                    label={USUARIOS.Codigo}
                                    value={this.state.usuario.usuario}
                                    tintColor={COLORS.secondaryColor}
                                    baseColor={COLORS.secondaryColor}
                                    animationDuration={200}
                                    onChangeText={(value) => this.setState({ usuario: { ...this.state.usuario, usuario: value } })}
                                    returnKeyType='next'
                                    onSubmitEditing={() => {
                                        this.refs.nombres.focus()
                                    }}
                                    labelHeight={15}
                                /> */}
                            </View>
                            <View style={styles.form_group}>
                                <TextView textValue={USUARIOS.Nombre + ' *'} styles={styles.label_color}></TextView>
                                <InputText value={this.state.usuario.nombre_completo} placeholder={USUARIOS.Nombre}
                                    onChange={(value) => { this.setState({ usuario: { ...this.state.usuario, nombre_completo: value } }); }}
                                    styles={styles.border_input}
                                    toUpper></InputText>
                                {/* <TextField
                                ref='nombres'
                                label={USUARIOS.Nombre}
                                value={this.state.usuario.nombre_completo}
                                tintColor={COLORS.secondaryColor}
                                baseColor={COLORS.secondaryColor}
                                animationDuration={200}
                                onChangeText={(value) => this.setState({ usuario: { ...this.state.usuario, nombre_completo: value } })}
                                returnKeyType='next'
                                onSubmitEditing={() => {
                                    this.refs.email.focus()
                                }}
                                labelHeight={15}
                            /> */}
                            </View>

                            <View style={styles.form_group}>
                                <TextView textValue={USUARIOS.Email + ' *'} styles={styles.label_color}></TextView>
                                <InputText value={this.state.usuario.email} placeholder={USUARIOS.Email}
                                    onChange={(value) => { this.setState({ usuario: { ...this.state.usuario, email: value } }); }}
                                    styles={styles.border_input}></InputText>
                                {/* <TextField
                                ref='email'
                                label={USUARIOS.Email}
                                value={this.state.usuario.email}
                                tintColor={COLORS.secondaryColor}
                                baseColor={COLORS.secondaryColor}
                                animationDuration={200}
                                onChangeText={(value) => this.setState({ usuario: { ...this.state.usuario, email: value } })}
                                returnKeyType='next'
                                onSubmitEditing={() => {
                                    this.refs.clave.focus()
                                }}
                                labelHeight={15}
                            /> */}
                            </View>

                            <View style={styles.form_group}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 0.2 }}>
                                        <TextView textValue={USUARIOS.Clave + ' *'} styles={[styles.label_color]}></TextView>
                                    </View>
                                    {this.state.usuario.id ?
                                        (<View style={{ flex: 0.8, alignItems: 'flex-end' }}>
                                            <TextView textValue={'( Dejar en blanco si no modifica )'} styles={[styles.label_color]}></TextView>
                                        </View>) :
                                        null}
                                </View>
                                <InputText type='password' value={this.state.usuario.clave} placeholder={USUARIOS.Clave}
                                    onChange={(value) => { this.setState({ usuario: { ...this.state.usuario, clave: value } }); }}
                                    styles={styles.border_input}></InputText>
                                {/* <TextField
                                ref='clave'
                                label={USUARIOS.Clave}
                                value={this.state.usuario.clave}
                                tintColor={COLORS.secondaryColor}
                                baseColor={COLORS.secondaryColor}
                                animationDuration={200}
                                onChangeText={(value) => this.setState({ usuario: { ...this.state.usuario, clave: value } })}
                                labelHeight={15}
                            /> */}
                                {/* {this.state.usuario.id ?
                                (<View style={{ flex: 0.8, alignItems: 'flex-end' }}>
                                    <TextView textValue={'( Dejar en blanco si no modifica )'} styles={[styles.label_color]}></TextView>
                                </View>) :
                                null} */}
                            </View>

                            <View style={styles.form_group}>
                                <TouchableOpacity style={{
                                    flexDirection: 'row'
                                }}
                                    onPress={this._redirectSelectEmpresas}>
                                    <View style={{
                                        paddingVertical: 5,
                                        flex: 1,
                                        flexWrap: 'wrap'
                                    }}>
                                        <TextView textValue={'Empresas'}
                                            styles={{
                                                fontSize: 15,
                                                marginBottom: 10
                                            }} />
                                        <TextView textValue={
                                            this.state.userSelectedEmpresas.map(emp => emp.descripcion).join(', ')
                                        }
                                            numberOfLines={1}
                                            styles={{
                                                fontSize: 12,
                                            }}
                                        />
                                    </View>
                                    <View style={{
                                        width: 50,
                                        justifyContent: 'center',
                                        alignItems: 'flex-end'
                                    }}>
                                        {
                                            FaIcons.getIcon(FaIconsEnum.ARROW_FORWARD, 20, COLORS.midSoftGrey)
                                        }
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.form_group}>
                                <TouchableOpacity style={{
                                    flexDirection: 'row'
                                }}
                                    onPress={this._redirectSelectProyectos}>
                                    <View style={{
                                        paddingVertical: 5,
                                        flex: 1,
                                        flexWrap: 'wrap'
                                    }}>
                                        <TextView textValue={'Proyectos'}
                                            styles={{
                                                fontSize: 15,
                                                marginBottom: 10
                                            }} />
                                        <TextView textValue={
                                            this.state.userSelectedProyectos.map(pro => pro.descripcion).join(', ')
                                        }
                                            numberOfLines={1}
                                            styles={{
                                                fontSize: 12,
                                            }}
                                        />
                                    </View>
                                    <View style={{
                                        width: 50,
                                        justifyContent: 'center',
                                        alignItems: 'flex-end'
                                    }}>
                                        {
                                            FaIcons.getIcon(FaIconsEnum.ARROW_FORWARD, 20, COLORS.midSoftGrey)
                                        }
                                    </View>
                                </TouchableOpacity>
                            </View>

                            {/* <View style={{ marginTop: 20 }}>

                                <SectionedMultiSelect
                                    items={this.state.arrEmpresas}
                                    uniqueKey="id"
                                    displayKey="descripcion"
                                    selectText={USUARIOS.AgregarEmpresas}
                                    onSelectedItemsChange={this._onChangeEmpresas.bind(this)}
                                    selectedItems={this.state.empresas}
                                    styles={styles_select_multiple}

                                />
                            </View>
                            <View style={{ marginTop: 20 }}>
                                <SectionedMultiSelect
                                    items={this.state.arrProyectos}
                                    uniqueKey="id"
                                    displayKey="descripcion"
                                    selectText={USUARIOS.AgregarProyectos}
                                    onSelectedItemsChange={this._onChangeProyectos.bind(this)}
                                    selectedItems={this.state.proyectos}
                                    styles={styles_select_multiple}
                                />
                            </View>
                            <View style={{
                                justifyContent: 'center',
                                paddingVertical: 10,
                                paddingHorizontal: 2
                            }}>


                                <View style={styles.card}>
                                    <View style={styles.form_group}>
                                        <TextView textValue={STRINGS.Notification} styles={[
                                            styles.label_color,
                                            { marginBottom: 20, fontSize: 18, fontWeight: 'bold' }
                                        ]} />

                                        <View style={{
                                            marginTop: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                            <View style={{
                                                flex: 0.8
                                            }}>
                                                <TextView textValue={STRINGS.Notification_AppUpdate}
                                                    styles={[styles.label_color,
                                                    {
                                                        fontSize: 15
                                                    }]}></TextView>
                                            </View>
                                            <View style={{
                                                flex: 0.2
                                            }}>
                                                <Switch value={this.state.userAppConfig.appUpdateNotification}
                                                    onValueChange={(value) => {
                                                        this.setState({
                                                            userAppConfig: {
                                                                ...this.state.userAppConfig,
                                                                appUpdateNotification: value
                                                            }
                                                        })
                                                    }} />
                                            </View>
                                        </View>
                                        <View style={{
                                            marginTop: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                            <View style={{
                                                flex: 0.8
                                            }}>
                                                <TextView textValue={STRINGS.Notification_NoActivities}
                                                    styles={[styles.label_color,
                                                    {
                                                        fontSize: 15
                                                    }]}></TextView>
                                            </View>
                                            <View style={{
                                                flex: 0.2
                                            }}>
                                                <Switch value={this.state.userAppConfig.appNoActivitiesNotification}
                                                    onValueChange={(value) => {
                                                        this.setState({
                                                            userAppConfig: {
                                                                ...this.state.userAppConfig,
                                                                appNoActivitiesNotification: value
                                                            }
                                                        })
                                                    }} />
                                            </View>
                                        </View>

                                    </View>
                                </View>

                            </View> */}

                        </View>
                    </View>
                </ScrollView>
                <LoadingIndicator ref='loading' />
                <Modal
                    onBackdropPress={() => {
                        this.setState({
                            showModalImagePicker: false
                        })
                    }
                    }
                    isVisible={this.state.showModalImagePicker}
                    style={{
                        justifyContent: 'flex-end',
                        margin: 0,
                    }} >

                    <View style={{
                        backgroundColor: COLORS.white
                    }}>

                        <Button text='Galeria' type='light'
                            onPress={this._pickImageFromGallery.bind(this)}
                            icon={
                                FaIcons.getIcon(FaIconsEnum.PHOTO, 20, COLORS.secondaryColor)
                            }
                            textStyles={{
                                fontSize: 20
                            }}
                            buttonColorStyles={{
                                borderWidth: 0,
                                backgroundColor: 'transparent'
                            }}
                        />
                        <Button text='Camara' type='light'
                            onPress={this._pickImageFromCamera.bind(this)}
                            icon={
                                FaIcons.getIcon(FaIconsEnum.CAMERA_FULL, 20, COLORS.secondaryColor)
                            }
                            textStyles={{
                                fontSize: 20
                            }}
                            buttonColorStyles={{
                                borderWidth: 0,
                                backgroundColor: 'transparent'
                            }}
                        />
                        <Separator />
                        <Button text='Cancelar' type='light'
                            onPress={() => { this.setState({ showModalImagePicker: false }) }}
                            icon={
                                FaIcons.getIcon(FaIconsEnum.CLOSE_X, 20, COLORS.secondaryColor)
                            }
                            textStyles={{
                                fontSize: 20
                            }}
                            buttonColorStyles={{
                                borderWidth: 0,
                                backgroundColor: 'transparent'
                            }}
                        />

                    </View>

                </Modal>
                <Toast
                    position='center'
                    ref="toast"
                />
            </View >
        )
    }



    _redirectSelectEmpresas = () => {
     
        let empresas = this.state.arrEmpresas.map(emp => ({ id: emp.id, descripcion: emp.descripcion }));
        Utils.defaultNavigation(this.props.navigation, 'PerfilListEmpresas', {
            arrEmpresas: empresas,
            arrSelected: this.state.empresas,
            onBack: (items) => {
                let selected = items.filter(emp => emp.selected);
                selected.sort((a, b) => a.descripcion > b.descripcion ? 1 : -1)
                this.setState({
                    userSelectedEmpresas: selected,
                    empresas: selected.map(emp => emp.id)
                })
            }
        })
    }

    _redirectSelectProyectos = () => {

        let proyectos = this.state.arrProyectos.map(pro => ({ id: pro.id, descripcion: pro.descripcion }));

        Utils.defaultNavigation(this.props.navigation, 'PerfilListProyectos', {
            arrProyectos: proyectos,
            arrSelected: this.state.proyectos,
            onBack: (items) => {
                let selected = items.filter(pro => pro.selected);
                selected.sort((a, b) => a.descripcion > b.descripcion ? 1 : -1)

                this.setState({
                    userSelectedProyectos: selected,
                    proyectos: selected.map(pro => pro.id)

                })
            }
        })
    }


    getPermissionAsync = async () => {

        let hasPermission = this.imageLoader.requestPermission();
        if (!hasPermission) {
            this.state.canSelectImage = false;
            this.notificationServices.fail('No podrá escoger Imagen de Perfil');
        }
    }

    _showImagePicker() {
        if (!this.state.canSelectImage) return;

        this.setState({
            showModalImagePicker: !this.state.showModalImagePicker
        })
    }

    _pickImageFromGallery = async () => {
        //EXPO
        var config = {
            //base64: true,
            allowsEditing: true,
            aspect: [1, 1],
            quality: 0.8
        }

        //EJECTED
        // var config = {
        //     mediaType: 'photo',
        //     allowsEditing: true,
        //     quality: 0.8
        // }

        this.imageLoader.launchGallery(config)
            .then(async (pickerResult) => {

                //EXPO
                var base64 = pickerResult.base64;
                var cancelled = pickerResult.cancelled;
                var uri = pickerResult.uri;

                //EJECTED   
                // var base64 = pickerResult.data;
                // var uri = pickerResult.uri || pickerResult.origURL;
                // var cancelled = pickerResult.didCancel;

                if (cancelled) {
                    return;
                }
                let image: any = await this.imageManipulator.resizeImage(uri, 500, 500, 'PNG', 100);


                let dataImage = {
                    name: image.uri.substring(image.uri.lastIndexOf('/') + 1, image.uri.length),
                    type: 'image/' + image.uri.substring(image.uri.lastIndexOf('.') + 1, image.uri.length),
                    uri:
                        Platform.OS === "android" ? image.uri : image.uri.replace("file://", "")
                };
                // //EXPO
                base64 = image.base64;
                // //EJECTED
                // //base64 = await this.imageManipulator.imageUriToBase64(image.uri);
                // base64 = base64.replace(/\n/g, '');


                // AuthService.userLoggedData.userPic = base64;
                this.setState({
                    usuario: {
                        ...this.state.usuario,
                        pic: dataImage,
                        userPic: 'data:image/jpg;base64,' + base64
                    },
                    showModalImagePicker: false
                });
            })
            .catch(() => {
                this.notificationServices.fail('No se pudo obtener foto de la galeria.');
            });


    }

    _pickImageFromCamera = async () => {

        //EXPO
        var config = {
            //base64: true,
            allowsEditing: true,
            aspect: [1, 1],
            quality: 0.8
        }

        //EJECTED
        // var config = {
        //     mediaType: 'photo',
        //     allowsEditing: true,
        //     quality: 0.8
        // }
        this.imageLoader.launchCamera(config)
            .then(async (pickerResult) => {

                //EXPO
                var base64 = pickerResult.base64;
                var cancelled = pickerResult.cancelled;
                var uri = pickerResult.uri;

                //EJECTED   
                // var base64 = pickerResult.data;
                // var uri = pickerResult.uri || pickerResult.origURL;
                // var cancelled = pickerResult.didCancel;

                if (cancelled) {
                    return;
                }

                let image: any = await this.imageManipulator.resizeImage(uri, 500, 500, 'PNG', 100);
                let dataImage = {
                    name: image.uri.substring(image.uri.lastIndexOf('/') + 1, image.uri.length),
                    type: 'image/' + image.uri.substring(image.uri.lastIndexOf('.') + 1, image.uri.length),
                    uri:
                        Platform.OS === "android" ? image.uri : image.uri.replace("file://", "")
                };
                // //EXPO
                base64 = image.base64;
                // //EJECTED
                // //base64 = await this.imageManipulator.imageUriToBase64(image.uri);
                // base64 = base64.replace(/\n/g, '');


                // AuthService.userLoggedData.userPic = base64;
                this.setState({
                    usuario: {
                        ...this.state.usuario,
                        pic: dataImage,
                        userPic: 'data:image/jpg;base64,' + base64
                    },
                    showModalImagePicker: false
                });
            })
            .catch((e) => {
                console.log(e)
                this.notificationServices.fail('No se pudo obtener foto de la camara.');
            });

    }

    _showLoading(bool) {
        this.refs.loading[bool ? 'show' : 'hide']();
    }
    _onChangeProyectos(value) {
        this.setState({ proyectos: value })
    }
    _onChangeEmpresas(value) {
        this.setState({ empresas: value })
    }

    _findData = async () => {
        this._showLoading(true);

        //Find Empresas
        let arrEmpresas = await this.empresasServices.findAllActives();
        arrEmpresas = arrEmpresas.data;
        let arrProyectos = await this.proyectosServices.findAllActives();
        arrProyectos = arrProyectos.data;


        Promise.resolve({})
            .then(() => {

                //Verificamos si es necesario realizar busqueda de informacion de usuario
                let goAndFindData = (+this.state.usuario.id && +this.state.usuario.id !== +AuthService.userLoggedData.userId);
                let userId = this.state.usuario.id || AuthService.userLoggedData.userId;

                if (goAndFindData) {
                    //Cargamos la informacion del usuario
                    this.usuariosServices.findUserData(userId)
                        .then(response => {
                            if (response.success) {

                                let empresas = response.data.empresas.map(emp => emp.empresa_id);
                                let proyectos = response.data.proyectos.map(pro => pro.proyecto_id);

                                let usuario = response.data.usuario;

                                let userSelectedEmpresas = arrEmpresas.filter(emp => empresas.indexOf(emp.id) !== -1);
                                let userSelectedProyectos = arrProyectos.filter(pro => proyectos.indexOf(pro.id) !== -1);

                                userSelectedEmpresas.sort((a, b) => a.descripcion > b.descripcion ? 1 : -1)
                                userSelectedProyectos.sort((a, b) => a.descripcion > b.descripcion ? 1 : -1)

                                //let appconfig = 
                                usuario.clave = null;
                                this.setState({
                                    empresas: empresas,
                                    proyectos: proyectos,
                                    usuario: usuario,
                                    arrEmpresas,
                                    arrProyectos,
                                    userSelectedEmpresas,
                                    userSelectedProyectos
                                });

                            } else {
                                throw Error(response.message);
                            }
                            this._showLoading(false);
                        }).catch(
                            () => {
                                this.notificationServices.fail('No se pudo obtener información del Usuario', () => {
                                    this._showLoading(false);
                                });
                            });
                } else {
                    //Si estamos en perfil y el usuario a cargar 
                    //es el mismo en session, cargamos la informacion de la sesion
                    if (this.state.action === 'profile' ||
                        +this.state.usuario.id === +AuthService.userLoggedData.userId) {
                        //Data usuario sesion
                        let empresas = AuthService.userLoggedData.userEmpresas.map(emp => emp.id);
                        let proyectos = AuthService.userLoggedData.userProyectos.map(pro => pro.id);

                        let usuario = {
                            id: AuthService.userLoggedData.userId,
                            nombre_completo: AuthService.userLoggedData.userFullName,
                            email: AuthService.userLoggedData.userEmail,
                            roles: AuthService.userLoggedData.roles,
                            identificacion: AuthService.userLoggedData.userIdentificacion,
                            usuario: AuthService.userLoggedData.userName,
                            pic: AuthService.userLoggedData.userPic,
                        };


                        let appConfig = AuthService.userLoggedData.userAppConfig;
                        let userSelectedEmpresas = arrEmpresas.filter(emp => empresas.indexOf(emp.id) !== -1);
                        let userSelectedProyectos = arrProyectos.filter(pro => proyectos.indexOf(pro.id) !== -1);

                        userSelectedEmpresas.sort((a, b) => a.descripcion > b.descripcion ? 1 : -1)
                        userSelectedProyectos.sort((a, b) => a.descripcion > b.descripcion ? 1 : -1)

                        this.setState({
                            empresas: empresas,
                            proyectos: proyectos,
                            usuario: usuario,
                            userAppConfig: appConfig,
                            arrEmpresas,
                            arrProyectos,
                            userSelectedEmpresas,
                            userSelectedProyectos
                        });
                    }

                    this._showLoading(false);
                }
            })

            .catch(() => {
                this.notificationServices.fail('No se pudo obtener información de los proyectos.', () => {
                    this._showLoading(false);
                });
            });

    }
    _animatePanel() {
        this.state.animationSlide.setValue(0)
        Animated.spring(this.state.animationSlide, {
            toValue: 1,
            velocity: 5,
            speed: 5,
            bounciness: 2,
            useNativeDriver: true
        }).start();

    }
    _crearUsuario() {
        let data = Object.assign({}, this.state.usuario);
        let appConfig = Object.assign({}, this.state.userAppConfig);

        if (!data ||
            !data['usuario'] ||
            !data['identificacion'] ||
            !data['nombre_completo'] ||
            !data['email'] ||
            (!data['id'] && !data['clave'])) {
            this.notificationServices.info('Ingrese los Datos Requeridos');
            return;
        }

        //Encriptamos la clave
        data['clave'] = data['clave'] ? sha256(data['clave']) : undefined;

        data = {
            usuario: {
                clave: data['clave'],
                usuario: data['usuario'],
                nombre_completo: data['nombre_completo'],
                email: data['email'],
                identificacion: data['identificacion'],
                id: data['id']
            },
            empresas: this.state.empresas || [],
            proyectos: this.state.proyectos || [],
            configuracion: {
                id: appConfig.configId,
                app_menu_style: appConfig.menuStyle,
                app_update_notification: appConfig.appUpdateNotification,
                app_noactivities_notification: appConfig.appNoActivitiesNotification,
                user_name: data['usuario']
            }
        }

        this._showLoading(true);
        this.usuariosServices.crear(data)
            .then(response => {
                this._showLoading(false);
                if (response.success) {
                    this.refs.toast.show('Información Registrada');
                    if ('profile' !== this.state.action) {
                        this.props.navigation.goBack(null);
                    }

                } else {
                    throw Error(response.message)
                }
            }).then(() => {
                //Si el usuario editado es el mismo que está en sesión actualizamos los datos de la sesion
                if (+data.usuario.id === +AuthService.userLoggedData.userId) {

                    AuthService.userLoggedData.userEmail = data.usuario.email;
                    AuthService.userLoggedData.userFullName = data.usuario.nombre_completo;
                    AuthService.userLoggedData.userIdentificacion = data.usuario.identificacion;
                    AuthService.userLoggedData.userEmpresas = this.state.empresas
                        .map(emp => this.state.arrEmpresas.find(empp => +empp.id === +emp));
                    AuthService.userLoggedData.userProyectos = this.state.proyectos
                        .map(pro => this.state.arrProyectos.find(proo => +proo.id === +pro));

                    AuthService.saveUserToStorage(AuthService.userLoggedData);

                }
            }).then(() => {
                //Subiendo la Imagen de perfil si hay una escogida
                let image = this.state.usuario.pic;
                if (!image) return;

                let fd = new FormData();
                fd.append('imageProfile', {
                    name: image.name,
                    type: image.type,
                    uri: image.uri
                });
                //Si es actualizacion de datos, subimos la foto al servidor
                this.usuariosServices.uploadPhoto(this.state.usuario.usuario, fd)
                    .then((result) => {
                        AuthService.userLoggedData.userPic = result.data.image_path;
                        AuthService.saveUserToStorage(AuthService.userLoggedData);
                        this.setState({
                            usuario: {
                                ...this.state.usuario,
                                userPic: AuthService.userLoggedData.userPic
                            }
                        });
                    })
                    .catch((e) => {
                        console.log(e);
                        this.notificationServices.fail('No se pudo guardar la imagen de perfil');
                    });

            }).catch((e) => {
                console.log(e)
                this.notificationServices.fail('No se pudo realizar la operación', () => {
                    this._showLoading(false);
                });

            })
    }

    _clearUsuariosForm() {
        this.setState({ usuario: {} });

    }

    _getSelectedEmpresas = () => {
        let selected = this.state.arrEmpresas
            .filter(emp => this.state.empresas.indexOf(emp.id) !== -1)
        return selected;
    }

    static propTypes = {
        action: PropTypes.string,
        navigation: PropTypes.object
    }
    static defaultProps = {
        action: 'profile'
    }

}
const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.white,
        height: '100%',
        flex: 1
    },
    header_container: {
        backgroundColor: COLORS.primaryColor,
        paddingBottom: 10,
        paddingHorizontal: containerStyles.marginHorizontal,
        flexDirection: 'row',
        height: 120,
        marginBottom: 20
    },
    header_image_container: {
        width: 90,
        justifyContent: 'center',
        //alignItems: 'center'
    },
    header_data_container: {
        flex: 1,
        //justifyContent: 'center',
    },
    photo: {
        borderRadius: 40,
        height: 80,
        width: 80,
        //overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: COLORS.white,
        borderWidth: 2,
        backgroundColor: COLORS.white
    },
    forms_main_container: {
        backgroundColor: COLORS.white,
        flex: 1,
    },
    form_container: {
        marginHorizontal: containerStyles.marginHorizontal

    },
    form_group: {
        marginBottom: 20,
        // borderTopColor: COLORS.softGrey,
        borderBottomColor: COLORS.softGrey,
        // borderTopWidth: 1,
        borderBottomWidth: 1,
        ///paddingVertical: 5
    },
    label_color: {
        color: COLORS.secondaryColor,
        fontSize: 12
    },
    border_input: {
        borderColor: 'transparent',
        borderWidth: 0,
        // borderBottomColor: COLORS.midGrey,
        // borderBottomWidth: 1,
        borderRadius: 0,
        paddingLeft: 0,
        fontSize: 15
    },
    card: {
        backgroundColor: COLORS.white,
        borderRadius: 10,
        ...boxShadow.box_shadow,
        paddingVertical: 10,
        marginBottom: 10,
        paddingHorizontal: 10
    }

});

