import React from 'react';
import { View, FlatList, TouchableOpacity } from 'react-native';
import TextView from '../../../components/TextView/TextView';
import { containerStyles } from '../../../utils/Styles';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { HeaderButtons } from '../../main/navigation.config';
import { FaIcons, MaterialIcons } from '../../../utils/Icons';
import { FaIconsEnum, MaterialIconsEnum } from '../../../utils/IconsEnum';
import { COLORS } from '../../../config/colors/colors';

export class PerfilSelectEmpresas extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            arrEmpresas: props.navigation.getParam('arrEmpresas') || [],
            arrSelected: props.navigation.getParam('arrSelected') || []
        }
    }

    componentDidMount() {
        this.state.arrEmpresas.forEach(emp => {
            let exist = this.state.arrSelected.indexOf(emp.id) !== -1
            if (exist) emp.selected = true
        })
        this.state.arrEmpresas.sort((a, b) => (a.descripcion > b.descripcion) ? 1 : -1)
        this.setState({
            arrEmpresas: [...this.state.arrEmpresas]
        })

        this.props.navigation.setParams({
            action: this._goBack
        })


    }


    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Seleccionar Empresas'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                navigation.goBack(null)
                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                    headerRight={
                        <HeaderButtons
                            action={() => {
                                let action = navigation.getParam('action')
                                action && action()
                            }}>
                            <TextView textValue={'Listo'} />
                        </HeaderButtons>
                    }
                />
        }
    }

    render() {
        return (
            <View style={{
                flex: 1,
                marginHorizontal: containerStyles.marginHorizontal
            }}>

                <FlatList
                    data={this.state.arrEmpresas}
                    renderItem={this._renderItem}
                    keyExtractor={(item) => String(item.id)}
                    showsVerticalScrollIndicator={false}
                    initialNumToRender={10}
                    maxToRenderPerBatch={3}
                />

            </View>
        )
    }


    _renderItem = ({ item }) =>
        <View style={{
            flex: 1,
            paddingVertical: 5
        }}>

            <TouchableOpacity style={{
                flexDirection: 'row',
                paddingVertical: 5,
                flexWrap: 'wrap'
            }}
                onPress={() => {
                    item.selected = !item.selected;
                    this.setState({
                        arrEmpresas: [...this.state.arrEmpresas]
                    })
                }}>
                <TextView textValue={item.descripcion}
                    styles={{
                        flex: 1
                    }}
                    numberOfLines={1} />

                <View style={{
                    width: 60,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    {
                        MaterialIcons.getIcon(item.selected ?
                            MaterialIconsEnum.CHECKBOX_FULL :
                            MaterialIconsEnum.CHECKBOX_EMPTY, 20, COLORS.contrastColor)
                    }
                </View>
            </TouchableOpacity>


        </View>

    _goBack = () => {
        let actionBack = this.props.navigation.getParam('onBack');
        actionBack && actionBack(this.state.arrEmpresas)
        this.props.navigation.goBack(null)
    }

}


export class PerfilSelectProyectos extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            arrProyectos: props.navigation.getParam('arrProyectos') || [],
            arrSelected: props.navigation.getParam('arrSelected') || []
        }
    }

    componentDidMount() {
        this.state.arrProyectos.forEach(emp => {
            let exist = this.state.arrSelected.indexOf(emp.id) !== -1
            if (exist) emp.selected = true
        })
        this.state.arrProyectos.sort((a, b) => (a.descripcion > b.descripcion) ? 1 : -1)
        this.setState({
            arrProyectos: [...this.state.arrProyectos]
        })
        this.props.navigation.setParams({
            action: this._goBack
        })
    }


    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Seleccionar Proyectos'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                navigation.goBack(null)
                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                    headerRight={
                        <HeaderButtons
                            action={() => {
                                let action = navigation.getParam('action')
                                action && action()
                            }}>
                            <TextView textValue={'Listo'} />
                        </HeaderButtons>
                    }
                />

        }
    }

    _goBack = () => {
        let actionBack = this.props.navigation.getParam('onBack');
        actionBack && actionBack(this.state.arrProyectos)
        this.props.navigation.goBack(null)
    }

    render() {
        return (
            <View style={{
                flex: 1,
                marginHorizontal: containerStyles.marginHorizontal
            }}>

                <FlatList
                    data={this.state.arrProyectos}
                    renderItem={this._renderItem}
                    keyExtractor={(item) => String(item.id)}
                    showsVerticalScrollIndicator={false}
                    initialNumToRender={10}
                    maxToRenderPerBatch={3}
                />

            </View>
        )
    }


    _renderItem = ({ item }) =>
        <View style={{
            flex: 1,
            paddingVertical: 5
        }}>

            <TouchableOpacity style={{
                flexDirection: 'row',
                paddingVertical: 5,
                flexWrap: 'wrap'
            }}
                onPress={() => {
                    item.selected = !item.selected;
                    this.setState({
                        arrProyectos: [...this.state.arrProyectos]
                    })
                }}>
                <TextView textValue={item.descripcion}
                    styles={{
                        flex: 1
                    }}
                    numberOfLines={1} />

                <View style={{
                    width: 60,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    {
                        MaterialIcons.getIcon(item.selected ?
                            MaterialIconsEnum.CHECKBOX_FULL :
                            MaterialIconsEnum.CHECKBOX_EMPTY, 20, COLORS.contrastColor)
                    }
                </View>
            </TouchableOpacity>


        </View>

}