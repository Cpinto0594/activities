import React from 'react';
import { View, Image } from 'react-native';
import TextView from '../../../components/TextView/TextView';

export class NoticesScreen extends React.Component {

    navigationOptions = {
        header: null
    }

    render() {

        return (    
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 20
            }}>
                <Image source={require('../../../../assets/blog.png')} style={{
                    height: 150,
                    width: 150
                }} />
                <TextView textValue='Noticias' />
            </View>
        )

    }

}