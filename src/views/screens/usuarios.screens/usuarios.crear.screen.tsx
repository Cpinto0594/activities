import React from 'react';
import PerfilScreen from '../perfil.screen/perfil.screen';
import { COLORS } from '../../../config/colors/colors';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { HeaderButtons } from '../../main/navigation.config';
import { NavigationActions } from 'react-navigation';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';

export default class CrearUsuariosScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    static navigationOptions = (props) => {

        return {
            tabBarVisible: false,
            header:
                <DefaultCustomHeader
                    headerTitle={'Registro de Usuarios'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                props.navigation.dispatch(NavigationActions.back())

                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
        }
    }

    render() {

        return (
            <PerfilScreen action='users' navigation={this.props.navigation} />
        )

    }
}