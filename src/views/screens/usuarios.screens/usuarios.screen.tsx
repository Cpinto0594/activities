import React from 'react';
import ListarScreen from '../listar.screen/listar.screen';
import { View } from 'react-native';
import TextView from '../../../components/TextView/TextView';
import { COLORS } from '../../../config/colors/colors';
import Utils from '../../../utils/Utils';
import FAICon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ViewsNavigation, { HeaderButtons } from '../../main/navigation.config';
import { AuthService } from '../../../services/AuthService';
import ProgressiveImage from '../../../components/Image/ProgressiveImage';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { SocialPeopleTemplateLoading } from '../../../components/loadingTemplates/loading.templates';

export default class UsuariosScreen extends React.Component {



    constructor(props) {
        super(props);
    }


    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Usuarios'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                navigation.goBack(null)
                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
        }

    }



    render() {

        return (
            <ListarScreen navigation={this.props.navigation}
                screenProps={this.props.screenProps}
                renderedChildren={(item) => {
                    return (
                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.white,
                            justifyContent: 'center',
                            //padding: 10
                        }}>
                            {
                                <View style={{
                                    width: 50,
                                    height: 50,
                                    borderRadius: 25,
                                    overflow: 'hidden',
                                    marginRight: 10,
                                    justifyContent: 'center'
                                }}>
                                    <ProgressiveImage
                                        source={
                                            item.pic ?
                                                { uri: item.pic } :
                                                require('../../../../assets/user_image.png')
                                        }
                                        style={{
                                            width: 50,
                                            height: 50
                                        }}
                                        loading
                                    />
                                </View>
                            }
                            <View style={{
                                flex: 1,
                                justifyContent: 'center'
                            }}>
                                <TextView textValue={'[ ' + item.usuario + ' ]  ' + item.nombre_completo}
                                    styles={{ fontSize: 15, fontWeight: 'bold', color: COLORS.secondaryColor }}></TextView>
                                <TextView textValue={item.identificacion}
                                    styles={{ fontSize: 12, color: COLORS.midSoftGrey }}></TextView>
                                <TextView textValue={item.email}
                                    styles={{ fontSize: 12, color: COLORS.midSoftGrey }}></TextView>
                            </View>
                        </View>
                    )
                }}
                loadingTemplate={(width) => <SocialPeopleTemplateLoading width={width} />}
            />
        )
    }

}
