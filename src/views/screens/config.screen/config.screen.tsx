import React from 'react';
import { View, StyleSheet, FlatList, Image, TouchableHighlight } from 'react-native';
import TextView from '../../../components/TextView/TextView';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { COLORS } from '../../../config/colors/colors';
import Utils from '../../../utils/Utils';
import { boxShadow, boxShadowUserProfile } from '../../../utils/Styles';
import { AuthService } from '../../../services/AuthService';
import Button from '../../../components/Button/Button';
import STRINGS_LOGIN from '../../../config/strings/login.strings';
import Separator from '../../main/separator.view';
import { IonIcons, FaIcons } from '../../../utils/Icons';
import { IonIconEnum, FaIconsEnum } from '../../../utils/IconsEnum';
import NotificationServices from '../../../services/NotificationsServices';
import { ConfigSectionComponent } from './ConfigSectionComponent';

export class ConfigurationScreen extends React.Component {

    private adminButtons: Array<any> = [
        {
            title: 'Empresas',
            action: 'Companies',
            icon: IonIconEnum.COMPANY,
            image: require('../../../../assets/company.png'),
            hide: 'ListCompany'
        },
        {
            title: 'Proyectos',
            action: 'Projects',
            image: require('../../../../assets/folder.png'),
            icon: IonIconEnum.PROJECT,
            hide: 'ListProject'
        },
        {
            title: 'Usuarios',
            action: 'Users',
            image: require('../../../../assets/user.png'),
            icon: IonIconEnum.USERS,
            hide: 'ListUser'
        },
        {
            title: 'Cargos',
            action: 'Positions',
            image: require('../../../../assets/position.png'),
            icon: IonIconEnum.KEY,
            hide: 'ListUser'
        }
    ];

    private socialButtons: Array<any> = [
        {
            title: 'Conexiones',
            action: 'Connections',
            image: require('../../../../assets/connections.png'),
            icon: IonIconEnum.USERS,
            hide: 'ListCompany'
        },
        {
            title: 'Perfil',
            action: 'UserProfile',
            image: require('../../../../assets/socialuserprofile.png'),
            icon: IonIconEnum.USER,
            hide: 'ListCompany'
        },
    ];

    private activityControlButtons: Array<any> = [
        {
            title: 'Dashboard',
            action: 'Dashboard',
            image: require('../../../../assets/dashboard.png'),
            icon: IonIconEnum.ANALITYCS,
            hide: 'ListCompany'
        },
        {
            title: 'Exportar',
            action: 'ActivitiesExport',
            image: require('../../../../assets/export.png'),
            icon: IonIconEnum.ANALITYCS,
            hide: 'ActivitiesExport'
        }
    ];

    private notificationServices: NotificationServices;

    constructor(props) {
        super(props);

        this.notificationServices = new NotificationServices;
        this.state = {
            userInfo: AuthService.userLoggedData
        }

    }
    render() {

        let arrItems = this.adminButtons.
            filter(button => !AuthService.shouldHideView(button.hide));


        return (

            <View style={styles.main_container}>
                <ScrollView showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        flexGrow: 1
                    }}>
                    {/* Social Config */}



                    <ConfigSectionComponent
                        sectionTitle={'Social'}
                        sectionItems={this.socialButtons}
                        navigation={this.props.navigation}
                    />
                    <View style={{
                        height: 10
                    }}></View>

                    {/* Admin Config */}


                    <ConfigSectionComponent
                        sectionTitle={'Recursos'}
                        sectionItems={this.adminButtons}
                        navigation={this.props.navigation}
                    />
                    <View style={{
                        height: 10
                    }}></View>


                    {/* Acticidades Config */}


                    <ConfigSectionComponent
                        sectionTitle={'Control de Actividades'}
                        sectionItems={this.activityControlButtons}
                        navigation={this.props.navigation}
                    />

                </ScrollView>
                <Separator />
                <View>
                    <Button text={STRINGS_LOGIN.CerrarSesion} type={'danger'} buttonStyles={{
                        borderRadius: 0
                    }}
                        onPress={this._logOut.bind(this)}
                    />
                </View>

            </View >

        )

    }

    _onEditProfile = () => {
        Utils.defaultNavigation(this.props.navigation, 'Profile');
    }

    _nameInitials() {
        if (!this.state.userInfo.userFullName) return '';
        let parts: string[] = this.state.userInfo.userFullName.split(" ");
        return parts[0].substring(0, 1) + parts[1].substring(0, 1);
    }

    _nameCapitalize() {
        if (!this.state.userInfo.userFullName) return '';
        let parts: string[] = this.state.userInfo.userFullName.split(" ");
        return parts.map(part => part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase()).join(" ")
    }

    _keyExtractor = (item) => item.title + '';



    _logOut() {
        this.notificationServices.confirm('¿Deseas cerrar sesión?', () => {
            AuthService.logOut()
                .then(() => {
                    Utils.navigateWithStackReset(this.props.navigation, 'Login')
                });
        })
    }

}

const styles = StyleSheet.create({

    main_container: {
        flex: 1,
        backgroundColor: COLORS.listBackgroundColor,
    },
    buttons_container: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        flex: 1,
        justifyContent: 'center'
    },
    button_container: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 30,
        borderColor: COLORS.white,
        borderWidth: 2,
        marginBottom: 10,
        backgroundColor: 'rgba(35, 133, 175, 1)',
        borderRadius: 10,
        ...boxShadow.box_shadow
    },
    text_button: {
        color: COLORS.white,
        fontSize: 25,
        marginLeft: 10
    },
    header_container: {
        width: '100%',
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 10,
        //height: 100,
        backgroundColor: COLORS.secondaryColor,
        justifyContent: 'flex-start',
        alignItems: 'center',
        ...boxShadowUserProfile.box_shadow
    },
    header_text_circle: {
        borderColor: COLORS.white,
        borderWidth: 2,
        borderRadius: 50,
        width: 70,
        height: 70,
        paddingHorizontal: 10,
        //marginLeft: 10,
        backgroundColor: COLORS.secondaryColor,
        ...boxShadowUserProfile.box_shadow,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    header_text_name: {
        fontSize: 18,
        marginBottom: 5,
        color: COLORS.white
    },
    header_text_others: {
        fontSize: 12,
        color: COLORS.white
    },

})