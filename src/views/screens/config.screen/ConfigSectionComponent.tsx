import React from 'react';
import { View, Image } from 'react-native';
import PropTypes from 'prop-types';
import { COLORS } from '../../../config/colors/colors';
import TextView from '../../../components/TextView/TextView';
import { IonIcons } from '../../../utils/Icons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Utils from '../../../utils/Utils';
import { boxShadowConfigComponent } from '../../../utils/Styles';

export class ConfigSectionComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { sectionTitle, sectionItems } = this.props;

        return (
            <View style={{
                borderBottomColor: COLORS.midSoftGrey,
                borderBottomWidth: 0.5,
                ...boxShadowConfigComponent.box_shadow
            }}>
                <View style={{
                    paddingVertical: 5,
                    paddingHorizontal: 16,
                    backgroundColor: COLORS.listBackgroundColor
                }}>
                    <TextView textValue={sectionTitle}
                        styles={{
                            color: COLORS.secondaryColor,
                            fontSize: 15,
                        }} />
                </View>
                <View style={{
                    backgroundColor: COLORS.white,
                    flexDirection: 'row',
                    paddingHorizontal: 16,
                    paddingVertical: 20,
                    alignItems: 'flex-start',
                }}>

                    {
                        sectionItems.map((item, index) =>
                            <TouchableOpacity
                                key={`Item${index}`}
                                onPress={() => {
                                    this._navigate(item.action)
                                }}
                            >
                                <View style={{
                                    marginRight: 15,
                                }}
                                >

                                    <View style={{
                                        borderRadius: 10,
                                        backgroundColor: COLORS.softGrey,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        //padding: 10,
                                        width: 50,
                                    }}>
                                        {/* {
                                            IonIcons.getIcon(item.icon, 30, COLORS.secondaryColor)
                                        } */}
                                        <Image source={item.image}
                                            style={{
                                                width: 50,
                                                height: 50
                                            }} resizeMode='contain' />
                                    </View>

                                    <TextView textValue={item.title} styles={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        textAlignVertical: 'center',
                                    }} numberOfLines={1} />
                                </View>
                            </TouchableOpacity>
                        )
                    }

                </View>
            </View >
        )
    }

    _navigate(view) {
        Utils.defaultNavigation(this.props.navigation, view);
    }

    static propTypes = {
        sectionTitle: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.node
        ]),
        sectionItems: PropTypes.array,
        navigation: PropTypes.object
    }

}