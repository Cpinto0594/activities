import React from 'react';
import { StyleSheet, View } from 'react-native';
import { COLORS } from '../../../config/colors/colors';
import ListarScreen from '../listar.screen/listar.screen';
import TextView from '../../../components/TextView/TextView';
import Utils from '../../../utils/Utils';
import FAICon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ViewsNavigation, { HeaderButtons } from '../../main/navigation.config';
import { AuthService } from '../../../services/AuthService';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';


export default class ProyectosScreen extends React.Component {

    constructor(props) {
        super(props);

    }

    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Proyectos'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                navigation.goBack(null)
                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
        }

    }


    render() {

        return (
            <ListarScreen navigation={this.props.navigation}
                screenProps={this.props.screenProps}
                renderedChildren={(item) => {
                    return (
                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.white,
                            justifyContent: 'center',
                        }}>

                            <View style={{
                                flex: 1,
                                justifyContent: 'center'
                            }}>
                                <TextView textValue={'[ ' + item.codigo + ' ]  ' + item.descripcion}
                                    styles={{ fontSize: 15, fontWeight: 'bold', color: COLORS.secondaryColor }}></TextView>
                            </View>
                        </View>
                    )
                }} />
        )
    }
}

