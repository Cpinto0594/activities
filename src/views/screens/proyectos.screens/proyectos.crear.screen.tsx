import React from 'react';
import TextView from '../../../components/TextView/TextView';
import { View, StyleSheet } from 'react-native';
import { PROYECTOS } from '../../../config/strings/proyectos.strings';
import InputText from '../../../components/InputText/InputText';
import { COLORS } from '../../../config/colors/colors';
import NotificationServices from '../../../services/NotificationsServices';
import ProyectosServices from '../../../services/proyectos.services/proyectos.services';
import { LoadingIndicator } from '../../../components/loading/loding';
import ButtonPanel from '../../../components/Button/ButtonPanel';
import { TextField } from 'react-native-material-textfield';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { HeaderButtons } from '../../main/navigation.config';
import { NavigationActions } from 'react-navigation';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { FaIcons } from '../../../utils/Icons';
import { containerStyles } from '../../../utils/Styles';


export default class CrearProyectosScreen extends React.Component {

    private notificationServices: NotificationServices;
    private proyectosServices: ProyectosServices;

    constructor(props) {
        super(props);
        this.notificationServices = new NotificationServices;
        this.proyectosServices = new ProyectosServices;
    }

    state = {
        proyecto: this.props.navigation.getParam('item') || {}
    }

    static navigationOptions = (props) => {

        return {
            tabBarVisible: false,
            header:
                <DefaultCustomHeader
                    headerTitle={'Registro de Proyectos'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                props.navigation.dispatch(NavigationActions.back())

                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
        }
    }

    componentWillUnmount() {
        this.state.proyecto = {}
    }

    render() {

        return (
            <View style={styles.main_container}>

                <View style={styles.form_container}>
                    <View style={styles.form_group}>
                        {/* <TextView textValue={PROYECTOS.Codigo} styles={styles.label_color}></TextView>
                        <InputText value={this.state.proyecto.codigo} placeholder={PROYECTOS.Codigo}
                            onChange={(value) => { this.setState({ proyecto: { ...this.state.proyecto, codigo: value } }); }}
                            styles={styles.border_input}
                            toUpper></InputText> */}
                        <TextField
                            ref='codigo'
                            label={PROYECTOS.Codigo}
                            value={this.state.proyecto.codigo}
                            tintColor={COLORS.secondaryColor}
                            baseColor={COLORS.secondaryColor}
                            animationDuration={200}
                            onChangeText={(value) => this.setState({ proyecto: { ...this.state.proyecto, codigo: value } })}
                            onBlur={() => {
                                this.refs.descripcion.focus()
                            }}
                            labelHeight={15}
                        />
                    </View>
                    <View style={styles.form_group}>
                        {/* <TextView textValue={PROYECTOS.Nombre} styles={styles.label_color}></TextView>
                        <InputText value={this.state.proyecto.descripcion} placeholder={PROYECTOS.Nombre}
                            onChange={(value) => { this.setState({ proyecto: { ...this.state.proyecto, descripcion: value } }); }}
                            styles={styles.border_input}
                            toUpper></InputText> */}
                        <TextField
                            ref='descripcion'
                            label={PROYECTOS.Nombre}
                            value={this.state.proyecto.descripcion}
                            tintColor={COLORS.secondaryColor}
                            baseColor={COLORS.secondaryColor}
                            animationDuration={200}
                            onChangeText={(value) => this.setState({ proyecto: { ...this.state.proyecto, descripcion: value } })}
                            onBlur={() => {
                                this.refs.codigo.focus()
                            }}
                            labelHeight={15}
                        />
                    </View>

                </View>
                <ButtonPanel
                    buttons={[
                        {
                            text: PROYECTOS.GuardarProyecto,
                            icon: FaIcons.getIcon(FaIconsEnum.SAVE, 20, COLORS.secondaryColor),
                            action: this._crearProyecto.bind(this)
                        }
                    ]}
                    buttonTextStyle={{
                        color: COLORS.secondaryColor
                    }} />
                <LoadingIndicator ref='loading' />
            </View>
        )

    }

    _showLoading(bool) {
        this.refs.loading[bool ? 'show' : 'hide']();
    }


    _crearProyecto() {

        let data = Object.assign({}, this.state.proyecto);
        if (!data ||
            !data['codigo'] ||
            !data['descripcion']) {
            this.notificationServices.info('Ingrese los Datos Requeridos');
            return;
        }
        this._showLoading(true);
        this.proyectosServices.save(data)
            .then(response => {

                if (response.success) {
                    this._cancelar();
                    this._clearProyectosForm();
                } else {
                    throw Error(response.message)
                }
                this._showLoading(false);
            }).catch(() => {
                this._showLoading(false);
                this.notificationServices.fail('No se pudo realizar la operación');

            })

    }

    _cancelar() {
        this.props.navigation.goBack(null);
    }

    _clearProyectosForm() {
        this.setState({ proyecto: {} });

    }

}
const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.backgroundColor,
        height: '100%',
        flex: 1
    },
    form_container: {
        marginTop: 0,
        paddingTop: 5,
        paddingBottom: 5,
        marginHorizontal: containerStyles.marginHorizontal,
        flex: 1,
        flexDirection: 'column',
    },
    form_group: {
        marginBottom: 10,
        marginTop: 10
    },
    label_color: {
        color: COLORS.secondaryColor
    },
    border_input: {
        borderColor: COLORS.secondaryColor,
        borderWidth: 1
    }
})