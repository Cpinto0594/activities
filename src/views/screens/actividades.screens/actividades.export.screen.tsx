import React from 'react';
import { StyleSheet, View, TouchableOpacity, Dimensions, Animated, ActivityIndicator, RefreshControl } from 'react-native';
import Button from '../../../components/Button/Button';
import TextView from '../../../components/TextView/TextView';
import { COLORS } from '../../../config/colors/colors';
import { AuthService } from '../../../services/AuthService';
import ActividadesServices from '../../../services/actividades.services/actividades.services';
import moment from 'moment';
import NotificationServices from '../../../services/NotificationsServices';
import { NavigationEvents } from 'react-navigation';
import { DatePickers } from '../../../components/DateTimepicker/DateAndTimePickers';
import Utils from '../../../utils/Utils';
import { LoadingIndicator } from '../../../components/loading/loding';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { boxShadow, containerStyles } from '../../../utils/Styles';
import { ACTIVIDADES } from '../../../config/strings/actividades.strings';
import { Dropdown } from 'react-native-material-dropdown';
import { ListLoadingTemplateLoading } from '../../../components/loadingTemplates/loading.templates';
import { Request } from '../../../FunctionaityProviders/http.services/http.service';
import Axios from 'axios';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { SearchBar } from '../../../components/SearchBar/SearchBar';
import { HeaderButtons } from '../../main/navigation.config';
import { CustomListComponent } from '../../../components/List/CustomList';
import DefaultCustomHeader from '../../../components/Headers/custom_header';



export default class ActivitiesExportScreen extends React.Component {
    public navigation: any;
    private notificationService: NotificationServices;
    private actividadesServices: ActividadesServices;
    private _timeoutId;
    private requestActividades: Request;


    today = moment().format('YYYY-MM-DD');
    startOfWeek = moment().startOf('week').format('YYYY-MM-DD')
    endOfWeek = moment().endOf('week').format('YYYY-MM-DD')

    defaultState = {
        arrData: [],
        searchValue: '',
        filters: {},
        applyFilters: false,
        page: 1,
        opened: false,
        hasScolled: false,
        isRefreshing: false,
        isErrorFound: false,
        noData: false,//Si no se encontraton datos
        isLoading: true, //Busqueda por descripcion o aplicar filtros
        isLoadingMore: false, //Busqueda por scroll
        listEnd: false, //Cuando la peticion no trae mas datos
        traslateXAnimation: new Animated.Value(0),
        windowWidth: Dimensions.get('window').width,
        arrProyectos: [],
        arrEmpresas: []
    }

    state = Object.assign({}, this.defaultState);


    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerLeft={
                        <HeaderButtons
                            action={() => navigation.goBack(null)}>
                            {FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)}
                        </HeaderButtons>
                    }
                    headerTitle={'Exportar Actividades'}
                />

        }

    }

    constructor(props) {
        super(props);
        this.actividadesServices = new ActividadesServices;
        this.notificationService = new NotificationServices;
    }

    async componentWillMount() {
        this.state.arrProyectos = [{ value: null, label: 'SELECCIONE' }].concat(
            AuthService.userLoggedData.userProyectos.map(proy => { return { value: proy.id, label: proy.descripcion } })
        )
        this.state.arrEmpresas = [{ value: null, label: 'SELECCIONE' }].concat(
            AuthService.userLoggedData.userEmpresas.map(empre => { return { value: empre.id, label: empre.descripcion } }))

        this._cleanFilters();
    }


    componentDidMount() {
        this._findData({
            desde: this.startOfWeek,
            hasta: this.endOfWeek,
            usuario: AuthService.userLoggedData.userName
        })
    }

    _clearData() {
        this.setState({
            ...this.defaultState
        });
    }

    _cleanFilters() {
        this.state.filters.usuario = AuthService.userLoggedData.userName;
        this.state.filters.desde = this.startOfWeek;
        this.state.filters.hasta = this.endOfWeek;
    }

    didFocus = () => {
        let item = this.props.navigation.getParam('actividad') || {}
        if (item.id) {
            this.setState({
                arrData: [...this.state.arrData, item]
            });
            this.props.navigation.setParams({
                actividad: undefined
            })
        }

    }

    render() {

        return (
            <View style={styles.main_container}>
                <NavigationEvents onWillBlur={this.componentWillUnmount}
                    onWillFocus={this.didFocus} />
                <View style={{
                    height: 50,
                    backgroundColor: COLORS.primaryColor,
                    paddingHorizontal: containerStyles.marginHorizontal,
                    borderBottomColor: COLORS.primaryColor,
                    borderBottomWidth: 1
                }}>
                    <SearchBar
                        placeHolder={'Buscar'}
                        placeHolderTextColor={COLORS.midSoftGrey}
                        style={{
                            backgroundColor: COLORS.primaryColor,
                        }}
                        inputContainerStyles={{
                            backgroundColor: COLORS.white,
                            color: COLORS.secondaryColor,
                            borderColor: 'transparent'
                        }}
                        searchingText={this.state.searchValue}
                        onChange={(value) => {
                            this.setState({
                                searchValue: value
                            })
                        }}
                        onCleanSearchText={() => {
                            this.setState({
                                searchValue: null,
                                isLoading: true
                            }, () => {
                                this._findData();
                            })
                        }}
                        onSubmit={() => {
                            this.setState({
                                isLoading: true
                            }, () => {
                                this._search()
                            })
                        }}
                        rightIcon={
                            <TouchableOpacity style={{
                                padding: 10,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                                onPress={this._toggleFilters}>
                                {
                                    FaIcons.getIcon(FaIconsEnum.FILTER, 20, COLORS.secondaryColor)
                                }
                            </TouchableOpacity>
                        }
                    />
                </View>
                <View style={[styles.containers]}>
                    <CustomListComponent
                        contentContainerStyle={{
                            //  marginHorizontal: containerStyles.marginHorizontal
                        }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrData}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onEndReached={this._handlePage}
                        onEndReachedThreshold={0.2}
                        initialNumToRender={10}
                        maxToRenderPerBatch={5}

                        refreshControl={
                            <RefreshControl
                                colors={[COLORS.secondaryColor]}
                                onRefresh={this._handleRefresh}
                                refreshing={this.state.isRefreshing}
                            />
                        }
                        refreshing={this.state.isRefreshing}
                        onMomentumScrollBegin={() => {
                            this.state.hasScolled = true;
                        }}
                        ListFooterComponent={
                            () => {
                                if (!this.state.isLoadingMore) return null;
                                return (
                                    <View style={{
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        height: 50
                                    }}>
                                        <ActivityIndicator size={20} color={COLORS.secondaryColor} />
                                    </View>
                                )

                            }
                        }
                        showLoadingTemplate={this.state.isLoading}
                        loadingTemplate={() =>
                            <View style={{
                                marginHorizontal: containerStyles.marginHorizontal
                            }}>
                                <ListLoadingTemplateLoading width={this.state.windowWidth - (containerStyles.marginHorizontal * 2)} height={120} />
                            </View>
                        }
                        stateType={this.state.isErrorFound ?
                            'ERROR' :
                            !this.state.arrData.length ?
                                'NODATA' : null}
                        refreshButton={true}
                        refreshAction={this._handleRefresh}
                        ItemSeparatorComponent={() =>
                            <View style={{ height: 5 }}></View>
                        }
                    />
                </View>
                <LoadingIndicator ref='loading' />
                {
                    this._renderFilters()
                }
            </View >
        )
    }

    _keyExtractor = (item) => item.id + '';

    _renderItem = ({ item }) => (
        <View style={{
            backgroundColor: COLORS.white,
            flex: 1
        }}>
            <Swipeable
                ref={ref => item.ref = ref}
                onSwipeableRightOpen={() => {
                    this._eliminarItem(item)
                }}
                renderRightActions={(progress, dragX) => {

                    const scale = dragX.interpolate({
                        inputRange: [-100, 0],
                        outputRange: [1, 0],
                        extrapolate: 'clamp'
                    });

                    return <View style={{
                        flex: 1,
                        backgroundColor: COLORS.red,
                    }}>
                        <TouchableOpacity style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            padding: 10,
                            paddingRight: 30,
                            height: 100 + '%'
                        }}>
                            <Animated.View style={{
                                transform: [
                                    { scale }
                                ]
                            }}>
                                {
                                    FaIcons.getIcon(FaIconsEnum.TRASH_EMPTY, 30, COLORS.white)
                                }
                            </Animated.View>
                        </TouchableOpacity>
                    </View>
                }}>
                <TouchableWithoutFeedback
                    onPress={() => this._editItem(item)
                    }
                    style={{
                        borderTopRightRadius: 10,
                        borderBottomRightRadius: 10,
                        backgroundColor: COLORS.white
                    }}>
                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: COLORS.white,
                        justifyContent: 'center',
                        paddingVertical: 10,
                        marginHorizontal: containerStyles.marginHorizontal,
                        borderRadius: 10
                    }}>
                        <View style={{
                            flex: 1,
                            justifyContent: 'center'
                        }}>
                            <TextView textValue={item.descripcion}
                                styles={{ fontSize: 18, fontWeight: 'bold', color: COLORS.secondaryColor }}></TextView>
                            <TextView textValue={item.nombreEmpresa}
                                styles={{ fontSize: 12, color: COLORS.secondaryColor }}></TextView>
                            <TextView textValue={item.nombreProyecto}
                                styles={{ fontSize: 12, color: COLORS.secondaryColor }}></TextView>
                            <View style={{ flexDirection: 'row' }}>
                                <TextView textValue={this._formatFecha(item.dia) + ' | ' + item.hora_inicio + '-' + item.hora_fin + ' | ( ' + item.cantidad_horas + ' ) Hrs'}
                                    styles={{ fontSize: 12, color: COLORS.secondaryColor }}></TextView>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Swipeable>
        </View>
    );

    _handlePage = () => {
        if (this.state.isLoadingMore || this.state.listEnd || !this.state.hasScolled) return null;

        this.setState({
            page: this.state.page + 1,
            isLoadingMore: true,
            noData: false,
        }, () => {
            this._findData({
                usuario: AuthService.userLoggedData.userName,
                descripcion: this.state.searchValue,
                ...(!this.state.searchValue ? this.state.filters : null)
            });
        })
    }

    _handleRefresh = () => {
        this.setState({
            isRefreshing: true,
            isLoading: true,
            page: 1,
            searchValue: null
        }, () => {
            this._findData({
                usuario: AuthService.userLoggedData.userName,
                desde: this.startOfWeek,
                hasta: this.endOfWeek,
            });
        })
    }

    _applyFilters = () => {
        this.setState({
            applyFilters: true,
            page: 1,
            isLoading: true,
            arrData: []
        }, () => {
            this._findData();
            this._toggleFilters();
        })
    }

    _clearFIlters = () => {
        this._cleanFilters();
        this.setState({
            filters: this.defaultState.filters,
            applyFilters: false,
            page: 1
        })
    }

    toggling = false;
    _toggleFilters = () => {
        if (this.toggling) return;


        this.toggling = true;
        Animated.spring(this.state.traslateXAnimation, {
            toValue: this.state.opened ? 0 : 1,
            tension: 50,
            velocity: 5,
            useNativeDriver: true
        }).start(() => {

            this.setState({
                opened: !this.state.opened
            }, () => {
                this.toggling = false;

            });
        })
    }

    _search = () => {

        if (!this.state.searchValue || !this.state.searchValue.trim().length) return;


        if (!this._timeoutId)
            this._timeoutId = setTimeout(() => {
                //Iniciamos la pagina 

                this.setState({
                    page: 1,
                    arrData: [],
                    isLoading: true,
                    noData: false
                }, () => {

                    this._findData({
                        usuario: AuthService.userLoggedData.userName,
                        descripcion: this.state.searchValue,
                        //...(this.state.applyFilters ? this.state.filters : {})
                    });
                })

            }, 500);
    }


    _formatFecha(fecha) {
        return moment(fecha).format('YYYY-MM-DD');
    }

    _editItem = (item) => {
        Utils.defaultNavigation(this.props.navigation, 'NuevaActividad', { item: item });
    }

    _findData(data?) {
        let request = data || this.state.filters;
        request.page = this.state.page;
        request.max_rows = 10;

        this.requestActividades = this.actividadesServices.search(request);


        this.requestActividades
            .then(res => res.data)
            .then(response => {
                if (response.success) {
                    if (response.data.length) {
                        this.setState({
                            arrData: this.state.isLoadingMore ?
                                this.state.arrData.concat(response.data) :
                                response.data,
                            isLoadingMore: false,
                            listEnd: false,
                            isLoading: false,
                            noData: false,
                            isRefreshing: false,
                            isErrorFound: false
                        })
                    } else {
                        this.setState({
                            isLoadingMore: false,
                            listEnd: true,
                            isLoading: false,
                            isRefreshing: false,
                            isErrorFound: false,
                            noData: this.state.arrData.length <= 0
                        })
                    }
                } else {

                    throw Error(response.message)
                }
            })
            .then(() => {
                this._timeoutId = null;
            })
            .catch((err) => {
                if (!Axios.isCancel(err)) {
                    this._timeoutId = null;
                    this.setState({
                        arrData: [],
                        isLoadingMore: false,
                        isLoading: false,
                        isRefreshing: false,
                        isErrorFound: true
                    })
                    this.notificationService.fail('No se pudo encontrar  información de las actividades');
                }
            });

    }
    _showLoading(bool) {
        this.refs.loading[bool ? 'show' : 'hide']();
    }
    _eliminarItem = (item) => {
        if (!item.id) return;
        this.notificationService.confirm('¿Desea Eliminar Actividad?', () => {

            this._showLoading(true);
            this.actividadesServices['delete'](item.id)
                .then(response => {
                    if (response.success) {
                        let data = this.state.arrData;
                        data.forEach((_item, index) => {
                            if (_item.id === +item.id) {
                                data.splice(index, 1);
                            }
                        })
                        this.setState({ arrData: data });
                    } else {
                        this.notificationService.fail(response.message);
                    }
                    this._showLoading(false);
                })
                .catch(() => {
                    this.notificationService.fail('No se pudo eliminar actividad.', () => {
                        this._showLoading(false);
                    });
                });

        }, () => {
            item.ref.close()
        });
    }

    _renderFilters = () =>
        <Animated.View style={
            [{
                position: 'absolute',
                top: 0,
                flex: 1,
                right: 0,
                width: this.state.windowWidth,
                height: 100 + '%',
                flexDirection: 'row'
            }, {
                transform: [{
                    translateX: this.state.traslateXAnimation.interpolate({
                        inputRange: [0, 1],
                        outputRange: [this.state.windowWidth, 0]
                    })
                }]
            }
            ]}
        >
            <View style={{
                width: 50,
                alignSelf: 'flex-start',
                height: '100%',
                zIndex: 99999
            }}>

                <TouchableWithoutFeedback style={{
                    height: 100 + '%',
                }}
                    onPress={this._toggleFilters.bind(this)}>
                </TouchableWithoutFeedback>
            </View>
            <View style={{
                width: this.state.windowWidth - 50,
                backgroundColor: COLORS.white,
                alignSelf: 'flex-end',
                height: '100%',
                zIndex: 99999,
                ...boxShadow.box_shadow
            }}>

                <View style={{
                    flex: 1,
                    padding: 20,
                    paddingTop: 30,

                }}>

                    <View style={{
                        marginBottom: 10
                    }}>

                        <TextView textValue='Fecha Inicio' />
                        <DatePickers
                            value={this.state.filters.desde}
                            inputStyles={{
                                borderColor: 'transparent',
                                borderWidth: 0,
                                borderBottomWidth: 1,
                                borderBottomColor: COLORS.secondaryColor,
                                paddingLeft: 0,
                            }}
                            onChange={(value) => {
                                this.setState({
                                    filters: {
                                        ...this.state.filters,
                                        desde: value
                                    }
                                })
                            }}
                        />
                    </View>
                    <View style={{
                        marginBottom: 10
                    }}>

                        <TextView textValue='Fecha Fin' />
                        <DatePickers
                            value={this.state.filters.hasta}
                            inputStyles={{
                                borderColor: 'transparent',
                                borderWidth: 0,
                                borderBottomWidth: 1,
                                borderBottomColor: COLORS.secondaryColor,
                                paddingLeft: 0,
                            }}
                            onChange={(value) => {
                                this.setState({
                                    filters: {
                                        ...this.state.filters,
                                        hasta: value
                                    }
                                })
                            }}
                        />
                    </View>
                    <View style={{
                        marginBottom: 10
                    }}>

                        <TextView textValue={ACTIVIDADES.Empresa } styles={{
                            color: COLORS.secondaryColor
                        }} />
                        <Dropdown data={this.state.arrEmpresas}
                            baseColor={COLORS.secondaryColor}
                            selectedItemColor={COLORS.secondaryColor}
                            value={(+this.state.filters.empresa || '')}
                            animationDuration={80}
                            inputContainerStyle={{ borderBottomColor: 'transparent' }}
                            containerStyle={{
                                borderBottomWidth: 1,
                                borderBottomColor: COLORS.secondaryColor,
                                backgroundColor: COLORS.white,
                                borderRadius: 5,
                                height: 40,
                            }}
                            rippleCentered={true}
                            dropdownOffset={{ top: 6, left: 5 }}
                            onChangeText={(value) => {
                                this.setState({
                                    filters: {
                                        ...this.state.filters,
                                        empresa: value
                                    }
                                })
                            }}
                        />
                    </View>
                    <View style={{
                        marginBottom: 10
                    }}>

                        <TextView textValue={ACTIVIDADES.Proyecto } styles={{
                            color: COLORS.secondaryColor
                        }} />
                        <Dropdown data={this.state.arrProyectos}
                            baseColor={COLORS.secondaryColor}
                            selectedItemColor={COLORS.secondaryColor}
                            value={(+this.state.filters.proyecto || '')}
                            animationDuration={80}
                            inputContainerStyle={{ borderBottomColor: 'transparent' }}
                            containerStyle={{
                                borderBottomWidth: 1,
                                borderBottomColor: COLORS.secondaryColor,
                                backgroundColor: COLORS.white,
                                borderRadius: 5,
                                height: 40,
                            }}
                            rippleCentered={true}
                            dropdownOffset={{ top: 6, left: 5 }}
                            onChangeText={(value) => {
                                this.setState({
                                    filters: {
                                        ...this.state.filters,
                                        proyecto: value
                                    }
                                })
                            }}
                        />
                    </View>
                    <View style={{
                        marginBottom: 10
                    }}>
                        <Button text='Aplicar' type='success' onPress={this._applyFilters.bind(this)} />
                        <Button text='Limpiar' type='light' onPress={this._clearFIlters.bind(this)} />
                    </View>
                </View>
            </View>
        </Animated.View>

}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.listBackgroundColor,
        height: '100%',
        flex: 1
    },
    containers: {
        marginTop: 0,
        flex: 1,
        flexDirection: 'column'
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 20,
        color: COLORS.white
    }
});