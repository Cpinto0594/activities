import React from 'react';
import { StyleSheet, View, Animated, Dimensions } from 'react-native';
import TextView from '../../../components/TextView/TextView';
import InputText from '../../../components/InputText/InputText';
import { COLORS } from '../../../config/colors/colors';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import LoggedUserModel from '../../../models/loggedUserModel';
import { AuthService } from '../../../services/AuthService';
import UsuariosServices from '../../../services/usuarios.services/usuarios.services';
import ActividadesServices from '../../../services/actividades.services/actividades.services';
import moment from 'moment';
import NotificationServices from '../../../services/NotificationsServices';
import { ACTIVIDADES } from '../../../config/strings/actividades.strings';
import { Dropdown } from 'react-native-material-dropdown';
import { DatePickers, TimePicker } from '../../../components/DateTimepicker/DateAndTimePickers';
import { LoadingIndicator } from '../../../components/loading/loding';
import ButtonPanel from '../../../components/Button/ButtonPanel';
import AutoComplete from 'react-native-autocomplete-input';
import Utils from '../../../utils/Utils';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import ViewsNavigation, { HeaderButtons } from '../../main/navigation.config';
import { NavigationActions } from 'react-navigation';
import { FaIconsEnum, MaterialIconsEnum } from '../../../utils/IconsEnum';
import { FaIcons, MaterialIcons } from '../../../utils/Icons';
import { containerStyles } from '../../../utils/Styles';


export default class CrearActividadesScreen extends React.Component {
    public navigation: any;
    private userLogged: LoggedUserModel;
    private usuariosServices: UsuariosServices;
    private actividadesServices: ActividadesServices;
    private notificationServices: NotificationServices;

    private today = moment().format('YYYY-MM-DD');
    defaultState = {
        actividad: {
            dia: this.today,
            cantidad_horas: '0'
        },
        arrProyectos: [],
        arrEmpresas: [],
        arrDescripciones: [],
        queryDescripcion: '',
        activityRanges: []
    }

    private animationHeight = new Animated.Value(0);

    state = Object.assign({}, this.defaultState)
    timeoutId: number;

    constructor(props) {
        super(props);
        this.usuariosServices = new UsuariosServices;
        this.actividadesServices = new ActividadesServices;
        this.notificationServices = new NotificationServices;
        moment.locale('es')
    }

    static navigationOptions = (props) => {

        return {
            tabBarVisible: false,
            header:
                <DefaultCustomHeader
                    headerTitle={'Registro de Actividades'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                props.navigation.dispatch(NavigationActions.back())

                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
        }
    }

    componentDidMount() {
        (async () => {
            this.userLogged = await new AuthService().userLoggedData();
            this._getUserProjectos();
        })();
    }

    componentWillUnmount() {
        this.setState({
            ...this.defaultState
        })
    }


    render() {

        const animationHeight = this.animationHeight.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 60 + styles.form_group.marginBottom],
            extrapolate: 'clamp'
        })

        const opacity = this.animationHeight.interpolate({
            inputRange: [0, 0.4, 0.8, 1],
            outputRange: [0, 0.6, 0.8, 1],
            extrapolate: 'clamp'
        })

        return (
            <View style={[
                styles.main_container
            ]}>

                <ScrollView style={[styles.form_container]}
                    showsVerticalScrollIndicator={false}>
                    <View style={[
                        styles.form_group

                    ]}>
                        <TextView textValue={ACTIVIDADES.Descripcion + ' *'} styles={styles.label_color} />
                        {/* <View style={
                            {
                                paddingTop: 25
                            }
                        }>
                            <AutoComplete
                                data={this.state.arrDescripciones}
                                defaultValue={this.state.actividad.descripcion}
                                onChangeText={text => { this._filterDescripcion(text) }}
                                keyExtractor={(item, i) => { return `Key${item.descripcion}` }}
                                renderItem={({ item }) => (
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                arrDescripciones: [],
                                                actividad: {
                                                    ...this.state.actividad,
                                                    descripcion: item.descripcion
                                                }
                                            });
                                        }}
                                        style={{
                                            padding: 10
                                        }}>
                                        <TextView textValue={item.descripcion} />
                                    </TouchableOpacity>
                                )}
                                inputContainerStyle={{
                                    borderColor: 'transparent'
                                }}
                                listStyle={{
                                    marginLeft: 1,
                                    marginRight: 1,
                                    maxHeight: 200,
                                }}
                                placeholder={ACTIVIDADES.Descripcion}
                                style={{
                                    paddingTop: 5,
                                    paddingBottom: 5,
                                    paddingLeft: 0,
                                    paddingRight: 10,
                                    // borderBottomWidth: 1,
                                    // borderBottomColor: COLORS.midGrey,
                                    borderRadius: 5,
                                    backgroundColor: '#fff',
                                    height: 35,
                                    fontSize: 18
                                }} 
                                containerStyle={{
                                    // flex: 1,
                                    // left: 0,
                                    // position: 'absolute',
                                    // right: 0,
                                    // top: 0,
                                    // zIndex: 1,
                                    // backgroundColor: 'white'
                                }}
                            />
                        </View> */}
                        <InputText value={this.state.actividad.descripcion} placeholder={ACTIVIDADES.Descripcion}
                            onChange={(value) => {
                                this._actualizarActividadProp(value, 'descripcion');
                            }} styles={styles.border_input}></InputText>
                    </View>
                    <View style={[styles.form_group,]}>
                        <TextView textValue={ACTIVIDADES.Dia + ' *'} styles={[styles.label_color]} />
                        <View style={{
                            flexDirection: 'row',
                        }}>
                            <View style={{
                                flex: 1
                            }}>
                                <DatePickers value={this.state.actividad.dia}
                                    maxDate={this.defaultState.actividad.dia}
                                    onChange={
                                        (value) => {
                                            this._actualizarActividadProp(value, 'dia');
                                            this._createActivityRange()
                                        }}
                                    inputStyles={styles.border_input}
                                    textStyles={{
                                        fontSize: 18
                                    }} />
                            </View>
                            {
                                !((this.state.actividad || {}).id) &&
                                <View style={{
                                    width: 50,
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}>
                                    <TouchableOpacity style={{
                                    }}
                                        onPress={() => {
                                            Animated.timing(this.animationHeight, {
                                                toValue: this.animationHeight._value === 1 ? 0 : 1,
                                                duration: 100
                                            }).start()
                                        }}>
                                        {
                                            MaterialIcons.getIcon(MaterialIconsEnum.EXPAND_MORE, 30, COLORS.secondaryColor)
                                        }
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>
                    </View>
                    <Animated.View style={{
                        height: animationHeight,
                        flex: 1,
                        overflow: 'hidden',
                        opacity: opacity,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <View style={[styles.form_group,{ flex: 1, alignSelf: 'flex-start' }]}>
                            <TextView textValue={'Hasta' + ' *'} styles={[styles.label_color]} />
                            <DatePickers value={this.state.actividad.dia_hasta}
                                maxDate={this.today}
                                minDate={this.state.actividad.dia}
                                onChange={
                                    (value) => {
                                        this._actualizarActividadProp(value, 'dia_hasta');
                                        this._createActivityRange();
                                    }}
                                inputStyles={styles.border_input}
                                textStyles={{
                                    fontSize: 18
                                }} />
                        </View>
                        {
                            !!this.state.actividad.dia_hasta &&
                            <TouchableOpacity style={{
                                width: 50,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                                onPress={() => {
                                    this.setState({
                                        actividad: {
                                            ...this.state.actividad,
                                            dia_hasta: null
                                        }
                                    }, () => {
                                        Animated.timing(this.animationHeight, {
                                            toValue: 0,
                                            duration: 100
                                        }).start()
                                    })
                                }}>
                                {
                                    MaterialIcons.getIcon(MaterialIconsEnum.CLEAR_CIRCLE, 25, COLORS.secondaryColor)
                                }
                            </TouchableOpacity>
                        }
                    </Animated.View>
                    <View style={styles.form_group}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ flex: 0.4, paddingRight: 5 }}>
                                <TextView textValue={ACTIVIDADES.HoraInicio + ' *'} styles={styles.label_color} />
                                <TimePicker onChange={
                                    (value) => {
                                        this._actualizarActividadProp(value, 'hora_inicio', () => {
                                            this._getCantidadHoras();
                                        });

                                    }} is24={true}
                                    inputStyles={{ ...styles.border_input }}
                                    textStyles={{
                                        fontSize: 18
                                    }}
                                    value={this.state.actividad.hora_inicio}
                                />
                            </View>
                            <View style={{ flex: 0.4, paddingLeft: 5, paddingRight: 5 }}>
                                <TextView textValue={ACTIVIDADES.HoraFin + ' *'} styles={styles.label_color} />
                                <TimePicker onChange={
                                    (value) => {
                                        this._actualizarActividadProp(value, 'hora_fin', () => {
                                            this._getCantidadHoras();
                                        });
                                    }} is24={true}
                                    inputStyles={{ ...styles.border_input }}
                                    textStyles={{
                                        fontSize: 18
                                    }}
                                    value={this.state.actividad.hora_fin}
                                />
                            </View>
                            <View style={{ flex: 0.2 }}>
                                <TextView textValue={ACTIVIDADES.Horas} styles={styles.label_color} />
                                <InputText value={this.state.actividad.cantidad_horas + ''} placeholder={ACTIVIDADES.Horas}
                                    type='numeric'
                                    onChange={(value) => {
                                        this._actualizarActividadProp(value, 'cantidad_horas');
                                    }}
                                    styles={styles.border_input} />
                            </View>
                        </View>
                    </View>

                    <View style={styles.form_group}>
                        <TextView textValue={ACTIVIDADES.Empresa + ' *'} styles={styles.label_color} />
                        {/* <Dropdown data={this.state.arrEmpresas}
                            baseColor={styles.label_color.color}
                            selectedItemColor={COLORS.secondaryColor}
                            value={(+this.state.actividad.empresa_id || '')}
                            animationDuration={100}
                            inputContainerStyle={{ borderBottomColor: 'transparent' }}
                            containerStyle={{
                                borderBottomWidth: 1,
                                borderBottomColor: COLORS.midGrey,
                                backgroundColor: COLORS.white,
                                borderRadius: 5,
                                height: 40,
                            }}
                            rippleCentered={true}
                            dropdownOffset={{ top: 6, left: 5 }}
                            onChangeText={(value) => { this._actualizarActividadProp(value, 'empresa_id') }} /> */}

                        <View style={{
                            flex: 1
                        }}>
                            <ScrollView
                                horizontal
                                contentContainerStyle={{
                                    flexGrow: 1,
                                    paddingVertical: 10
                                }}
                                ref='scrollEmpresa'
                                showsHorizontalScrollIndicator={false}>
                                {
                                    this.state.arrEmpresas
                                        .filter(emp => !!emp.value)
                                        .map(emp => (
                                            this._renderTagSelectEmpOrProj(emp, 'empresa_id')
                                        ))
                                }
                            </ScrollView>
                        </View>
                    </View>
                    <View style={styles.form_group}>
                        <TextView textValue={ACTIVIDADES.Proyecto + ' *'} styles={styles.label_color} />
                        {/* <Dropdown data={this.state.arrProyectos}
                            baseColor={styles.label_color.color}
                            selectedItemColor={COLORS.secondaryColor}
                            value={(+this.state.actividad.proyecto_id || '')}
                            animationDuration={100}
                            inputContainerStyle={{ borderBottomColor: 'transparent' }}
                            containerStyle={{
                                borderBottomWidth: 1,
                                borderBottomColor: COLORS.midGrey,
                                backgroundColor: COLORS.white,
                                borderRadius: 5,
                                height: 40,
                            }}
                            rippleCentered={true}
                            dropdownOffset={{ top: 6, left: 5 }}
                            onChangeText={(value) => { this._actualizarActividadProp(value, 'proyecto_id') }}
                        /> */}
                        <View style={{
                            flex: 1
                        }}>
                            <ScrollView
                                horizontal
                                contentContainerStyle={{
                                    flexGrow: 1,
                                    paddingVertical: 10
                                }}
                                ref='scrollProject'
                                showsHorizontalScrollIndicator={false}>
                                {
                                    this.state.arrProyectos
                                        .filter(pro => !!pro.value)
                                        .map(pro => (
                                            this._renderTagSelectEmpOrProj(pro, 'proyecto_id')
                                        ))
                                }
                            </ScrollView>
                        </View>

                    </View>


                </ScrollView>

                <ButtonPanel
                    buttons={[
                        {
                            text: ACTIVIDADES.GuardarActividad,
                            icon: FaIcons.getIcon(FaIconsEnum.SAVE, 20, COLORS.secondaryColor),
                            action: this.saveActivity.bind(this)
                        }
                    ]}
                    buttonTextStyle={{
                        color: COLORS.secondaryColor
                    }} />

                <LoadingIndicator ref='loading' />
            </View >
        )
    }

    _renderTagSelectEmpOrProj = (item, key) =>
        <View style={{
            justifyContent: 'center',
            paddingHorizontal: 5,
            marginRight: 5,
            borderRadius: 10,
            backgroundColor: this.state.actividad[key] === item.value ?
                COLORS.primaryColor :
                'transparent',
            borderColor: this.state.actividad[key] === item.value ?
                COLORS.secondaryColor :
                COLORS.headerBorderBottomColor,
            borderWidth: 1,
        }}
            ref={`${key}_${item.value}`}
            onLayout={(e) => {
                item.x = e.nativeEvent.layout.x + 50
            }}
            key={item.value}>
            <TouchableOpacity style={{
                paddingHorizontal: 5,
                paddingVertical: 5
            }}
                onPress={() => {
                    this._actualizarActividadProp(item.value, key);
                }}>
                <TextView textValue={item.label} styles={{
                    fontWeight: 'bold',
                    fontSize: 15
                }} />
            </TouchableOpacity>
        </View>


    _filterDescripcion(text) {

        if (!text) {
            this.setState({
                arrDescripciones: []
            })
            return;
        }

        if (text && text.trim().length > 4) {
            this.setState({
                query: text,
                arrDescripciones: [{ descripcion: text }],
                actividad: {
                    ...this.state.actividad,
                    descripcion: text
                }
            },
                () => {
                    if (!this.timeoutId) {
                        // this.timeoutId = setTimeout(() => {
                        if (!text) return;
                        this.actividadesServices.autoComplete(text, AuthService.userLoggedData.userName)
                            .then((response) => {
                                this.setState({
                                    arrDescripciones: [{ descripcion: text }].concat(response.data)
                                });
                                this.timeoutId = undefined;
                            })
                            .catch((e) => {
                                console.log(e);
                                this.timeoutId = undefined;
                            })


                        // }, 200)
                    }
                });


        }
    }


    _showLoading(bool) {
        this.refs.loading[bool ? 'show' : 'hide']();
    }

    _getUserProjectos() {

        this._showLoading(true);
        Promise.resolve()
            .then(() => {
                this.setState({
                    arrProyectos: [{ value: null, label: 'SELECCIONE' }].concat(
                        AuthService.userLoggedData.userProyectos.map(proy => { return { value: proy.id, label: proy.descripcion } })
                    ),
                    arrEmpresas: [{ value: null, label: 'SELECCIONE' }].concat(
                        AuthService.userLoggedData.userEmpresas.map(empre => { return { value: empre.id, label: empre.descripcion } })),
                })

                let actividad = this.props.navigation.getParam('item') || {};
                if (actividad.id) {
                    this.setState({
                        actividad: {
                            ...actividad,
                            dia: moment(actividad.dia).format('YYYY-MM-DD')
                        }
                    }, () => {
                        setTimeout(() => {
                            let width = Dimensions.get('window').width;
                            let off = this.state.arrProyectos
                                .filter(pro => pro.value === this.state.actividad.proyecto_id)[0]
                            if (off.x > width)
                                this.refs.scrollProject
                                    .scrollTo({ x: off.x, animated: true })

                        }, 200);
                    })
                }
                this._showLoading(false);
            })
            .catch(() => {
                this._showLoading(false);
            })
    }

    _actualizarActividadProp(value, key, call?) {
        let prev = this.state.actividad;
        let newValue: any = {}
        newValue[key] = value;
        let next = Object.assign({}, prev, newValue);
        this.setState({ actividad: next }, call);
    }

    _createActivityRange = () => {
        let { dia, dia_hasta } = this.state.actividad;
        if (!dia_hasta) return;
        let dateFrom = new Date(dia);
        let dateTo = new Date(dia_hasta);

        let dateAux = new Date(dateFrom);
        let activityRanges = [];
        while (dateAux.getTime() <= dateTo.getTime()) {
            dateAux.setDate(dateAux.getDate() + 1);
            activityRanges.push(new Date(dateAux));
        }

        this.state.activityRanges = activityRanges;
    }

    _cancelar(actividad) {
        Utils.defaultNavigation(this.props.navigation, 'ListaActividad', { actividad })
            ;
    }

    _clearActividadesForm() {
        this.setState({ actividad: { dia: this.today, cantidad_horas: '0' } });

    }


    saveActivity() {
        let data = this.state.actividad;

        if (!data ||
            !data['descripcion'] ||
            !data['hora_inicio'] ||
            !data['hora_fin'] ||
            !data['empresa_id'] ||
            !data['proyecto_id'] ||
            !data['dia'] ||
            !data['cantidad_horas']
        ) {
            this.notificationServices.info('Debe completar la informacion');
            return;
        }

        //data['dia'] = data['dia'];
        data['usuario'] = this.userLogged.userName;

        let arrData = [];
        var requestData: any = {
            descripcion: data['descripcion'],
            hora_inicio: data['hora_inicio'],
            hora_fin: data['hora_fin'],
            usuario: data['usuario'],
            dia: data['dia'],
            cantidad_horas: data['cantidad_horas'],
            empresa_id: data['empresa_id'],
            proyecto_id: data['proyecto_id'],
            id: data['id']
        }

        if (requestData.id) {

            this._showLoading(true);
            this.actividadesServices.saveActividad(requestData)
                .then(res => {
                    if (res.success) {

                        //Agregamos el nombre de la empresa y proyecto a la respuesta
                        let nombre_empresa = this.state.arrEmpresas.find(emp => emp.value === requestData.empresa_id).label;
                        let nombre_proyecto = this.state.arrProyectos.find(pro => pro.value === requestData.proyecto_id).label;
                        requestData.nombreEmpresa = nombre_empresa;
                        requestData.nombreProyecto = nombre_proyecto;

                        this._cancelar(requestData);
                        this._clearActividadesForm();
                    } else {
                        throw Error('No se pudo crear actividad');
                    }
                    this._showLoading(false);

                }).catch((e) => {
                    console.log(e)
                    this.notificationServices.fail('No se pudo realizar');
                    this._showLoading(false);
                });

        } else {
            let activityRanges = this.state.activityRanges;
            if (!activityRanges.length) {
                arrData = [requestData];
            } else {
                activityRanges.forEach(day => {
                    let copy = Object.assign({}, requestData);
                    copy.dia = moment(day).format('YYYY-MM-DD')
                    arrData.push(copy)
                })
            }

            this._showLoading(true);
            this.actividadesServices.createAllActividades(arrData)
                .then(res => {
                    if (res.success) {

                        //Agregamos el nombre de la empresa y proyecto a la respuesta
                        let nombre_empresa = this.state.arrEmpresas.find(emp => emp.value === requestData.empresa_id).label;
                        let nombre_proyecto = this.state.arrProyectos.find(pro => pro.value === requestData.proyecto_id).label;
                        if (res.data instanceof Array) {
                            res.data.forEach(act => {
                                act.nombreEmpresa = nombre_empresa;
                                act.nombreProyecto = nombre_proyecto;
                            })
                        }

                        this._cancelar(res.data);
                        this._clearActividadesForm();
                    } else {
                        throw Error('No se pudo crear actividad');
                    }
                    this._showLoading(false);

                }).catch(() => {
                    this.notificationServices.fail('No se pudo realizar');
                    this._showLoading(false);
                });
        }




    }

    _getCantidadHoras() {
        this.state.actividad.cantidad_horas = '0';
        if (!this.state.actividad.hora_fin || !this.state.actividad.hora_inicio) return;

        let start = moment('1994-05-09 ' + this.state.actividad.hora_inicio);
        let end = moment('1994-05-09 ' + this.state.actividad.hora_fin);
        var duration = moment.duration(end.diff(start));

        this.state.actividad.cantidad_horas = Math.round(duration.asHours()) + '';


        this.setState({
            actividad: {
                ...this.state.actividad,
            }
        });
    }

}



const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.white,
        flex: 1,
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 20,
        color: COLORS.white
    },
    //Modal Styles
    form_container: {
        paddingTop: 5,
        paddingBottom: 10,
        paddingHorizontal: containerStyles.marginHorizontal,
        flex: 1,
    },
    form_group: {
        marginBottom: 20,
        // borderTopColor: COLORS.softGrey,
        borderBottomColor: COLORS.softGrey,
        // borderTopWidth: 1,
        borderBottomWidth: 1,
        paddingVertical: 5
    },
    label_color: {
        color: COLORS.secondaryColor
    },
    border_input: {
        borderColor: 'transparent',
        borderWidth: 0,
        // borderBottomColor: COLORS.midGrey,
        // borderBottomWidth: 1,
        borderRadius: 0,
        paddingLeft: 0,
        fontSize: 18
    }
});