import React from 'react';
import { StyleSheet, View, TouchableOpacity, Dimensions, Animated, ActivityIndicator, RefreshControl } from 'react-native';
import TextView from '../../../components/TextView/TextView';
import { COLORS } from '../../../config/colors/colors';
import { AuthService } from '../../../services/AuthService';
import ActividadesServices from '../../../services/actividades.services/actividades.services';
import moment from 'moment';
import NotificationServices from '../../../services/NotificationsServices';
import { NavigationEvents } from 'react-navigation';
import Utils from '../../../utils/Utils';
import { LoadingIndicator } from '../../../components/loading/loding';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { containerStyles, boxShadowActionButton } from '../../../utils/Styles';
import { ListLoadingTemplateLoading } from '../../../components/loadingTemplates/loading.templates';
import { Request } from '../../../FunctionaityProviders/http.services/http.service';
import Axios from 'axios';
import ActionButton from 'react-native-action-button';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { SearchBar } from '../../../components/SearchBar/SearchBar';
import { HeaderButtons } from '../../main/navigation.config';
import { CustomListComponent } from '../../../components/List/CustomList';
import DefaultCustomHeader from '../../../components/Headers/custom_header';



export default class ActividadesScreen extends React.Component {
    public navigation: any;
    private notificationService: NotificationServices;
    private actividadesServices: ActividadesServices;
    private _timeoutId;
    private requestActividades: Request;


    today = moment().format('YYYY-MM-DD');
    startOfWeek = moment().startOf('week').format('YYYY-MM-DD')
    endOfWeek = moment().endOf('week').format('YYYY-MM-DD')

    defaultState = {
        arrData: [],
        searchValue: '',
        filters: {},
        applyFilters: false,
        page: 1,
        opened: false,
        hasScolled: false,
        isRefreshing: false,
        isErrorFound: false,
        noData: false,//Si no se encontraton datos
        isLoading: true, //Busqueda por descripcion o aplicar filtros
        isLoadingMore: false, //Busqueda por scroll
        listEnd: false, //Cuando la peticion no trae mas datos
        traslateXAnimation: new Animated.Value(0),
        windowWidth: Dimensions.get('window').width,
        arrProyectos: [],
        arrEmpresas: []
    }

    state = Object.assign({}, this.defaultState);


    static navigationOptions = ({ navigation }) => {
        return {
            header:
                <DefaultCustomHeader
                    headerTitle={'Control de Actividades'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                navigation.goBack(null)
                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />

        }
    }

    constructor(props) {
        super(props);
        this.actividadesServices = new ActividadesServices;
        this.notificationService = new NotificationServices;
    }

    async componentWillMount() {
        this.state.arrProyectos = [{ value: null, label: 'SELECCIONE' }].concat(
            AuthService.userLoggedData.userProyectos.map(proy => { return { value: proy.id, label: proy.nombre } })
        )
        this.state.arrEmpresas = [{ value: null, label: 'SELECCIONE' }].concat(
            AuthService.userLoggedData.userEmpresas.map(empre => { return { value: empre.id, label: empre.nombre } }))

    }


    componentDidMount() {
        this._findData({
            desde: this.startOfWeek,
            hasta: this.endOfWeek,
            usuario: AuthService.userLoggedData.userName
        })
    }

    _clearData() {
        this.setState({
            ...this.defaultState
        });
    }


    didFocus = () => {
        let item = this.props.navigation.getParam('actividad')
        if (!item) return;
        if (item instanceof Array) {
            this.setState({
                arrData: [...this.state.arrData, ...(item instanceof Array) ? item : [item]]
            });
        } else {
            let data = [...this.state.arrData];
            let exists = data.findIndex(act => +act.id === +item.id);
            data[exists] = item;
            this.setState({
                arrData: data
            });
        }
        this.props.navigation.setParams({
            actividad: undefined
        })


    }

    render() {

        return (
            <View style={styles.main_container}>
                <NavigationEvents onWillBlur={this.componentWillUnmount}
                    onWillFocus={this.didFocus} />
                <View style={{
                    height: 50,
                    backgroundColor: COLORS.primaryColor,
                    paddingHorizontal: containerStyles.marginHorizontal,
                    borderBottomColor: COLORS.primaryColor,
                    borderBottomWidth: 1
                }}>
                    <SearchBar
                        placeHolder={'Buscar'}
                        placeHolderTextColor={COLORS.midSoftGrey}
                        style={{
                            backgroundColor: COLORS.primaryColor,
                        }}
                        inputContainerStyles={{
                            backgroundColor: COLORS.white,
                            color: COLORS.secondaryColor,
                            borderColor: 'transparent'
                        }}
                        searchingText={this.state.searchValue}
                        onChange={(value) => {
                            this.setState({
                                searchValue: value
                            })
                        }}
                        onCleanSearchText={() => {
                            this.setState({
                                searchValue: null,
                                isLoading: true
                            }, () => {
                                this._findData();
                            })
                        }}
                        onSubmit={() => {
                            this.setState({
                                isLoading: true
                            }, () => {
                                this._search()
                            })
                        }}
                    />
                </View>
                <View style={[styles.containers]}>
                    <CustomListComponent
                        contentContainerStyle={{
                            //  marginHorizontal: containerStyles.marginHorizontal
                        }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrData}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onEndReached={this._handlePage}
                        onEndReachedThreshold={0.2}
                        initialNumToRender={10}
                        maxToRenderPerBatch={5}

                        refreshControl={
                            <RefreshControl
                                colors={[COLORS.secondaryColor]}
                                onRefresh={this._handleRefresh}
                                refreshing={this.state.isRefreshing}
                            />
                        }
                        refreshing={this.state.isRefreshing}
                        onMomentumScrollBegin={() => {
                            this.state.hasScolled = true;
                        }}
                        ListFooterComponent={
                            () => {
                                if (!this.state.isLoadingMore) return null;
                                return (
                                    <View style={{
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        height: 50
                                    }}>
                                        <ActivityIndicator size={20} color={COLORS.secondaryColor} />
                                    </View>
                                )

                            }
                        }
                        showLoadingTemplate={this.state.isLoading}
                        loadingTemplate={() =>
                            <View style={{
                                marginHorizontal: containerStyles.marginHorizontal
                            }}>
                                <ListLoadingTemplateLoading width={this.state.windowWidth - (containerStyles.marginHorizontal * 2)} height={120} />
                            </View>
                        }
                        stateType={this.state.isErrorFound ?
                            'ERROR' :
                            !this.state.arrData.length ?
                                'NODATA' : null}
                        refreshButton={true}
                        refreshAction={this._handleRefresh}
                        ItemSeparatorComponent={() =>
                            <View style={{ height: 5 }}></View>
                        }
                    />
                </View>


                <ActionButton
                    buttonColor={COLORS.contrastColor}
                    offsetX={containerStyles.marginHorizontal}
                    offsetY={10}
                    onPress={() => {
                        Utils.defaultNavigation(this.props.navigation, 'NuevaActividad')
                    }}
                    renderIcon={() => FaIcons.getIcon(FaIconsEnum.PENCIL, 22, COLORS.white)}
                    shadowStyle={{
                        ...boxShadowActionButton.box_shadow
                    }}
                    fixNativeFeedbackRadius={true}
                />
                <LoadingIndicator ref='loading' />

            </View >
        )
    }

    _keyExtractor = (item) => item.id + '';

    _renderItem = ({ item }) => (
        <View style={{
            backgroundColor: COLORS.white,
            flex: 1
        }}>
            <Swipeable
                ref={ref => item.ref = ref}
                // onSwipeableRightOpen={() => {
                //     this._eliminarItem(item)
                // }}
                renderRightActions={(progress, dragX) => {

                    const scale = dragX.interpolate({
                        inputRange: [-100, 0],
                        outputRange: [1, 0],
                        extrapolate: 'clamp'
                    });

                    return <View style={{
                        backgroundColor: COLORS.white,
                        justifyContent: 'flex-end',
                        flexDirection: 'row'
                    }}>
                        <TouchableOpacity style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            paddingHorizontal: 20,
                            height: 100 + '%',
                            backgroundColor: COLORS.contrastColor
                        }}
                            onPress={() => {
                                this._cloneItem(item)
                            }}>
                            <Animated.View style={{
                                transform: [
                                    { scale }
                                ]
                            }}>
                                {
                                    FaIcons.getIcon(FaIconsEnum.CLONE, 25, COLORS.white)
                                }
                            </Animated.View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingHorizontal: 20,
                            height: 100 + '%',
                            backgroundColor: COLORS.labelDanger
                        }}
                            onPress={() => {
                                this._eliminarItem(item)
                            }}
                        >
                            <Animated.View style={{
                                transform: [
                                    { scale }
                                ]
                            }}>
                                {
                                    FaIcons.getIcon(FaIconsEnum.TRASH_EMPTY, 25, COLORS.white)
                                }
                            </Animated.View>
                        </TouchableOpacity>
                    </View>
                }}>
                <TouchableOpacity
                    onPress={() => this._editItem(item)
                    }
                    activeOpacity={1}
                    style={{
                        borderTopRightRadius: 10,
                        borderBottomRightRadius: 10,
                        backgroundColor: COLORS.white
                    }}>
                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: COLORS.white,
                        justifyContent: 'center',
                        paddingVertical: 10,
                        marginHorizontal: containerStyles.marginHorizontal,
                        borderRadius: 10
                    }}>
                        <View style={{
                            flex: 1,
                            justifyContent: 'center'
                        }}>
                            <TextView textValue={item.descripcion}
                                styles={{ fontSize: 18, fontWeight: 'bold', color: COLORS.secondaryColor }}></TextView>
                            <TextView textValue={item.nombreEmpresa}
                                styles={{ fontSize: 12, color: COLORS.secondaryColor }}></TextView>
                            <TextView textValue={item.nombreProyecto}
                                styles={{ fontSize: 12, color: COLORS.secondaryColor }}></TextView>
                            <View style={{ flexDirection: 'row' }}>
                                <TextView textValue={this._formatFecha(item.dia) + ' | ' + item.hora_inicio + '-' + item.hora_fin + ' | ( ' + item.cantidad_horas + ' ) Hrs'}
                                    styles={{ fontSize: 12, color: COLORS.secondaryColor }}></TextView>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </Swipeable>
        </View>
    );

    _handlePage = () => {
        if (this.state.isLoadingMore || this.state.listEnd || !this.state.hasScolled) return null;

        this.setState({
            page: this.state.page + 1,
            isLoadingMore: true,
            noData: false,
        }, () => {
            this._findData({
                usuario: AuthService.userLoggedData.userName,
                descripcion: this.state.searchValue,
                ...(!this.state.searchValue ? this.state.filters : null)
            });
        })
    }

    _handleRefresh = () => {
        this.setState({
            isRefreshing: true,
            isLoading: true,
            page: 1,
            searchValue: null
        }, () => {
            this._findData({
                usuario: AuthService.userLoggedData.userName,
                desde: this.startOfWeek,
                hasta: this.endOfWeek,
            });
        })
    }

    _search = () => {

        if (!this.state.searchValue || !this.state.searchValue.trim().length) return;


        if (!this._timeoutId)
            this._timeoutId = setTimeout(() => {
                //Iniciamos la pagina 

                this.setState({
                    page: 1,
                    arrData: [],
                    isLoading: true,
                    noData: false
                }, () => {

                    this._findData({
                        usuario: AuthService.userLoggedData.userName,
                        descripcion: this.state.searchValue,
                        ...this.state.filters
                    });
                })

            }, 200);
    }


    _formatFecha(fecha) {
        return moment(fecha).format('YYYY-MM-DD');
    }

    _editItem = (item) => {
        Utils.defaultNavigation(this.props.navigation, 'NuevaActividad', { item: item });
    }

    _findData(data?) {
        let request = data || this.state.filters;
        request.page = this.state.page;
        request.max_rows = 10;

        this.requestActividades = this.actividadesServices.search(request);


        this.requestActividades
            .then(res => res.data)
            .then(response => {
                if (response.success) {
                    // if (response.data.length) {
                    this.setState({
                        arrData: this.state.isLoadingMore ?
                            this.state.arrData.concat(response.data) :
                            response.data,
                        isLoadingMore: false,
                        listEnd: false,
                        isLoading: false,
                        isRefreshing: false,
                        isErrorFound: false
                    })
                } else {

                    throw Error(response.message)
                }
            })
            .then(() => {
                this._timeoutId = null;
            })
            .catch((err) => {
                if (!Axios.isCancel(err)) {
                    this._timeoutId = null;
                    this.setState({
                        arrData: [],
                        isLoadingMore: false,
                        isLoading: false,
                        isRefreshing: false,
                        isErrorFound: true
                    })
                    this.notificationService.fail('No se pudo encontrar  información de las actividades');
                }
            });

    }
    _showLoading(bool) {
        this.refs.loading[bool ? 'show' : 'hide']();
    }
    _eliminarItem = (item) => {
        if (!item.id) return;
        this.notificationService.confirm('¿Desea Eliminar Actividad?', () => {
            item.ref.close()
            this._showLoading(true);
            this.actividadesServices['delete'](item.id)
                .then(response => {
                    if (response.success) {
                        let data = [...this.state.arrData];
                        data.forEach((_item, index) => {
                            if (+_item.id === +item.id) {
                                data.splice(index, 1);
                            }
                        })
                        this.setState({ arrData: data });
                    } else {
                        this.notificationService.fail(response.message);
                    }
                    this._showLoading(false);
                })
                .catch(() => {
                    this.notificationService.fail('No se pudo eliminar actividad.', () => {
                        this._showLoading(false);
                    });
                });

        }, () => {
            item.ref.close()
        });
    }

    _parseActiity = (data) => {
        if (!data) return null;
        return {
            descripcion: data['descripcion'],
            hora_inicio: data['hora_inicio'],
            hora_fin: data['hora_fin'],
            usuario: data['usuario'],
            dia: data['dia'],
            cantidad_horas: data['cantidad_horas'],
            empresa_id: data['empresa_id'],
            proyecto_id: data['proyecto_id'],
            id: data['id']
        }
    }
    _cloneItem = (item) => {
        if (!item.id) return;
        this.notificationService.confirm('¿Desea Clonar Actividad?', null, null, null, [
            {
                text: "Cancelar", onPress: () => {
                    item.ref.close()
                }
            },
            {
                text: "Fecha Actual", onPress: () => {
                    item.ref.close()
                    this._doClone(item, this.today)
                }
            },
            {
                text: "Fecha Original", onPress: () => {
                    item.ref.close()
                    this._doClone(item)
                }
            }
        ]);
    }


    _doClone = (item, newDate?) => {
        let requestObject = this._parseActiity(item);
        requestObject["dia"] = newDate || requestObject["dia"];

        delete requestObject["id"];

        this._showLoading(true);
        this.actividadesServices.saveActividad(requestObject)
            .then(response => {
                if (response.success) {
                    let result = response.data;
                    result.nombreEmpresa = item.nombreEmpresa;
                    result.nombreProyecto = item.nombreProyecto;

                    let data = [...this.state.arrData, result];
                    this.setState({
                        arrData: data
                    }, () => {
                        //this._editItem(response.data)
                    })
                } else {
                    this.notificationService.fail(response.message);
                }
                this._showLoading(false);
            })
            .catch(() => {
                this.notificationService.fail('No se pudo Clonar actividad.', () => {
                    this._showLoading(false);
                });
            });
    }
}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.listBackgroundColor,
        flex: 1
    },
    containers: {
        marginTop: 0,
        flex: 1,
        flexDirection: 'column'
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 20,
        color: COLORS.white
    }
});