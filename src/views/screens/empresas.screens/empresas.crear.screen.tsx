import React from 'react';
import TextView from '../../../components/TextView/TextView';
import { View, StyleSheet } from 'react-native';
import { EMPRESAS } from '../../../config/strings/empresas.strings';
import InputText from '../../../components/InputText/InputText';
import Button from '../../../components/Button/Button';
import NotificationServices from '../../../services/NotificationsServices';
import EmpresasServices from '../../../services/empresas.services/empresas.services';
import { COLORS } from '../../../config/colors/colors';
import { LoadingIndicator } from '../../../components/loading/loding';
import ButtonPanel from '../../../components/Button/ButtonPanel';
import { TextField } from 'react-native-material-textfield';
import DefaultCustomHeader from '../../../components/Headers/custom_header';
import { HeaderButtons } from '../../main/navigation.config';
import { NavigationActions } from 'react-navigation';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import { FaIcons } from '../../../utils/Icons';
import { containerStyles } from '../../../utils/Styles';


export default class CrearEmpresasScreen extends React.Component {

    private notificationServices: NotificationServices;
    private empresasServices: EmpresasServices;

    state = {
        empresa: this.props.navigation.getParam('item') || {}
    }

    constructor(props) {
        super(props);
        this.notificationServices = new NotificationServices;
        this.empresasServices = new EmpresasServices;
    }

    static navigationOptions = (props) => {

        return {
            tabBarVisible: false,
            header:
                <DefaultCustomHeader
                    headerTitle={'Registro de Empresas'}
                    headerLeft={
                        <HeaderButtons
                            action={() => {
                                props.navigation.dispatch(NavigationActions.back())

                            }}>
                            {
                                FaIcons.getIcon(FaIconsEnum.ARROW_BACK, 20, COLORS.secondaryColor)
                            }
                        </HeaderButtons>
                    }
                />
        }
    }

    componentWillUnmount() {
        this.state.empresa = {}
    }


    render() {

        return (
            <View style={styles.main_container}>

                <View style={styles.form_container}>
                    <View style={styles.form_group}>
                        {/* <TextView textValue={EMPRESAS.Codigo + ' *'} styles={styles.label_color}></TextView>
                        <InputText value={this.state.empresa.codigo} placeholder={EMPRESAS.Codigo}
                            onChange={(value) => { this.setState({ empresa: { ...this.state.empresa, codigo: value } }); }}
                            styles={styles.border_input}
                            toUpper></InputText> */}
                        <TextField
                            ref='codigo'
                            label={EMPRESAS.Codigo}
                            value={this.state.empresa.codigo}
                            tintColor={COLORS.secondaryColor}
                            baseColor={COLORS.secondaryColor}
                            animationDuration={200}
                            onChangeText={(value) => this.setState({ empresa: { ...this.state.empresa, codigo: value } })}
                            onBlur={() => {
                                this.refs.descripcion.focus()
                            }}
                            labelHeight={15}
                        />
                    </View>
                    <View style={styles.form_group}>
                        {/* <TextView textValue={EMPRESAS.Nombre + ' *'} styles={styles.label_color}></TextView>
                        <InputText value={this.state.empresa.descripcion} placeholder={EMPRESAS.Nombre}
                            onChange={(value) => { this.setState({ empresa: { ...this.state.empresa, descripcion: value } }); }}
                            styles={styles.border_input}
                            toUpper></InputText> */}
                        <TextField
                            ref='descripcion'
                            label={EMPRESAS.Nombre}
                            value={this.state.empresa.descripcion}
                            tintColor={COLORS.secondaryColor}
                            baseColor={COLORS.secondaryColor}
                            animationDuration={200}
                            onChangeText={(value) => this.setState({ empresa: { ...this.state.empresa, descripcion: value } })}
                            onBlur={() => {
                                this.refs.codigo.focus()
                            }}
                            labelHeight={15}
                        />
                    </View>

                </View>
                <ButtonPanel
                    buttons={[
                        {
                            text: EMPRESAS.GuardarEmpresa,
                            icon: FaIcons.getIcon(FaIconsEnum.SAVE, 20, COLORS.secondaryColor),
                            action: this._crearEmpresa.bind(this)
                        }
                    ]}
                    buttonTextStyle={{
                        color: COLORS.secondaryColor
                    }} />
                <LoadingIndicator ref='loading' />
            </View>
        )

    }

    _showLoading(bool) {
        this.refs.loading[bool ? 'show' : 'hide']();
    }


    _crearEmpresa() {

        let data = Object.assign({}, this.state.empresa);
        if (!data ||
            !data['codigo'] ||
            !data['descripcion']) {
            this.notificationServices.info('Ingrese los Datos Requeridos');
            return;
        }
        this._showLoading(true);
        this.empresasServices.save(data)
            .then(response => {

                if (response.success) {
                    this._cancelar();
                    this._clearEmpresasForm();
                } else {
                    throw Error(response.message)
                }
                this._showLoading(false);
            })
            .catch((e) => {
                console.log(e)
                this._showLoading(false);
                this.notificationServices.fail('No se pudo realizar la operación');
            })

    }

    _cancelar() {
        this.props.navigation.goBack(null);
    }

    _clearEmpresasForm() {
        this.setState({ empresa: {} });

    }

}
const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.backgroundColor,
        height: '100%',
        flex: 1
    },
    form_container: {
        marginTop: 0,
        paddingTop: 5,
        paddingBottom: 5,
        marginHorizontal: containerStyles.marginHorizontal,
        flex: 1,
        flexDirection: 'column',
    },
    form_group: {
        marginBottom: 10,
        marginTop: 10
    },
    label_color: {
        color: COLORS.secondaryColor
    },
    border_input: {
        borderColor: COLORS.secondaryColor,
        borderWidth: 1
    }
})