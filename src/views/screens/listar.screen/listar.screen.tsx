import React from 'react';
import { StyleSheet, View, TouchableOpacity, TouchableWithoutFeedback, Dimensions, Animated } from 'react-native';
import { COLORS } from '../../../config/colors/colors';
import { FlatList } from 'react-navigation';
import TextView from '../../../components/TextView/TextView';
import NotificationServices from '../../../services/NotificationsServices';
import Utils from '../../../utils/Utils';
import { LoadingIndicator } from '../../../components/loading/loding';
import PropTypes from 'prop-types';
import { FaIcons } from '../../../utils/Icons';
import { FaIconsEnum } from '../../../utils/IconsEnum';
import ActionButton from 'react-native-action-button';
import { ListLoadingTemplateLoading } from '../../../components/loadingTemplates/loading.templates';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { SearchBar } from '../../../components/SearchBar/SearchBar';
import { EventsComponent } from '../../../components/EventsComponents/EventsComponent';
import { CustomListComponent } from '../../../components/List/CustomList';
import { containerStyles, boxShadowActionButton } from '../../../utils/Styles';
import Button from '../../../components/Button/Button';


export default class ListarScreen extends React.Component {
    private notificationService: NotificationServices;
    private _optionService: any;
    private _editRoute: string;
    private _dataNotFound: string;
    private _deleteFail: string;
    private _timeoutId;

    constructor(props) {
        super(props);
        this.notificationService = new NotificationServices;
        this._editRoute = this.props.navigation.getParam('editRoute');
        this._optionService = this.props.navigation.getParam('service');
        this._dataNotFound = this.props.navigation.getParam('dataNotFound');
        this._deleteFail = this.props.navigation.getParam('deleteFail');
        this._optionService = new this._optionService;

        this.state = {
            arrData: [],
            searchValue: '',
            isSearching: true,
            windowWidth: Dimensions.get('window').width - 20,
            noData: false,
            refreshing: false,
            isErrorFound: false
        }
    }



    componentDidMount() {
        this._findData();
    }

    componentWillUnmount() {
    }


    render() {

        return (
            <View style={styles.main_container}>
                <View style={{
                    height: 50,
                    backgroundColor: COLORS.primaryColor,
                    paddingHorizontal: containerStyles.marginHorizontal,
                    borderBottomColor: COLORS.primaryColor,
                    borderBottomWidth: 1
                }}>
                    <SearchBar
                        placeHolder={'Buscar'}
                        placeHolderTextColor={COLORS.midSoftGrey}
                        style={{
                            backgroundColor: COLORS.primaryColor,
                        }}
                        inputContainerStyles={{
                            backgroundColor: COLORS.white,
                            color: COLORS.secondaryColor,
                            borderColor: 'transparent'
                        }}
                        searchingText={this.state.searchValue}
                        onChange={(value) => {
                            this.setState({
                                searchValue: value
                            })
                        }}
                        onCleanSearchText={() => {
                            this.setState({
                                searchValue: null,
                                isLoading: true
                            }, () => {
                                this._findData();
                            })
                        }}
                        onSubmit={() => {
                            this.setState({
                                isLoading: true
                            }, () => {
                                this._findData()
                            })
                        }}
                    />
                </View>

                <View style={[styles.containers]}>
                    <CustomListComponent
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrData}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onRefresh={this._onRefresh}
                        refreshing={this.state.refreshing}
                        stateType={
                            this.state.isSearching ?
                                null :
                                this.state.isErrorFound ?
                                    'ERROR' :
                                    !this.state.arrData.length ?
                                        'NODATA' : null
                        }
                        showLoadingTemplate={this.state.isSearching}
                        loadingTemplate={() =>
                            <View style={{
                                marginHorizontal: containerStyles.marginHorizontal,
                                paddingVertical: 5
                            }}>
                                {
                                    this.props.loadingTemplate(
                                        this.state.windowWidth - (containerStyles.marginHorizontal * 2)
                                    )
                                }
                            </View>
                        }
                        refreshButton={true}
                        refreshAction={this._onRefresh}
                        ItemSeparatorComponent={() =>
                            <View style={{
                                height: 5
                            }}></View>
                        }
                    />
                </View>

                <LoadingIndicator ref='loading' />
                <ActionButton
                    buttonColor={COLORS.contrastColor}
                    offsetX={containerStyles.marginHorizontal}
                    offsetY={10}
                    onPress={() => {
                        Utils.defaultNavigation(this.props.navigation, this._editRoute)
                    }}
                    renderIcon={() => FaIcons.getIcon(FaIconsEnum.PENCIL, 22, COLORS.white)}
                    shadowStyle={{
                        ...boxShadowActionButton.box_shadow
                    }}
                    fixNativeFeedbackRadius={true}
                />

            </View >
        )
    }

    _search = (value) => {
        if (value === undefined || this.state.isSearching) {
            return;
        }
        this.state.searchValue = value;
        if (!this._timeoutId)
            this._timeoutId = setTimeout(() => {
                this._findData();
            }, 200);

    }

    _onRefresh = () => {

        this.setState({
            refreshing: true,
            page: 1,
            isSearching: true
        }, () => {
            this._findData()
        })

    }

    _keyExtractor = (item) => item.id + '';

    _renderItem = ({ item }) => (
        <View style={{
            backgroundColor: COLORS.white,
            flex: 1
        }}>
            <Swipeable
                ref={ref => item.ref = ref}
                onSwipeableRightOpen={() => {
                    this._eliminarItem(item)
                }}
                renderRightActions={(progress, dragX) => {

                    const scale = dragX.interpolate({
                        inputRange: [-100, 0],
                        outputRange: [1, 0],
                        extrapolate: 'clamp'
                    });

                    return <View style={{
                        flex: 1,
                        backgroundColor: COLORS.red,
                    }}>
                        <TouchableOpacity style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            padding: 10,
                            paddingRight: 30,
                            height: 100 + '%'
                        }}>
                            <Animated.View style={{
                                transform: [
                                    { scale }
                                ]
                            }}>
                                {
                                    FaIcons.getIcon(FaIconsEnum.TRASH_EMPTY, 30, COLORS.white)
                                }
                            </Animated.View>
                        </TouchableOpacity>
                    </View>
                }}>
                <TouchableWithoutFeedback
                    onPress={() => this._editItem(item)
                    }
                >
                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: COLORS.white,
                        justifyContent: 'center',
                        paddingVertical: 15,
                        paddingHorizontal: containerStyles.marginHorizontal,
                        borderTopRightRadius: 10,
                        borderBottomRightRadius: 10,
                    }}>
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                        }}>
                            {this.props.renderedChildren(item)}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Swipeable>
        </View>
    );

    _emptyComponent = () =>
        <EventsComponent
            type='NODATA'
            legend={
                <View style={{
                    justifyContent: 'center'
                }}>

                    <TextView textValue={'No se encontraron datos.'}
                        styles={{
                            fontSize: 15,
                            fontWeight: 'bold',
                            color: COLORS.secondaryColor
                        }} />
                    <Button type={'light'}
                        onPress={() => {
                            this.setState({
                                loadingData: true,
                                page: 1
                            })
                        }}
                        text={'Toca para refrescar'}
                        icon={FaIcons.getIcon(FaIconsEnum.REFRESH, 10, COLORS.midGrey)}
                    />

                </View>
            }
        />

    _editItem = (item) => {
        Utils.defaultNavigation(this.props.navigation, this._editRoute, { item: item });
    }

    _showLoading(bool) {
        this.refs.loading[bool ? 'show' : 'hide']();
    }
    _eliminarItem = (item) => {
        if (!item.id) return;
        this.notificationService.confirm('¿Desea Eliminar Registro?', () => {
            this._showLoading(true);

            this._optionService['delete'](item.id)
                .then(response => {
                    if (response.success) {
                        let data = this.state.arrData;
                        data.forEach((_item, index) => {
                            if (_item.id === +item.id) {
                                data.splice(index, 1);
                            }
                        })
                        this.setState({ arrData: data });
                    } else {
                        throw Error(response.message);
                    }
                    this._showLoading(false);
                })
                .catch(() => {

                    this.notificationService.fail(this._deleteFail, () => {
                        this._showLoading(false);
                    });

                });
        }, () => {
            item.ref.close()
        });
    }


    _findData() {

        this._optionService['search']({ search: this.state.searchValue })
            .then(response => {
                if (response.success) {
                    this.setState({
                        arrData: response.data,
                        isSearching: false,
                        noData: response.data.length <= 0,
                        refreshing: false,
                        isErrorFound: false
                    })
                } else {
                    this.setState({
                        arrData: [],
                        isSearching: false,
                        noData: true,
                        refreshing: false,
                        isErrorFound: false
                    })
                    throw Error(response.message)
                }

            }).then(() => {
                clearTimeout(this._timeoutId);
                this._timeoutId = null;
            })
            .catch(() => {
                this.setState({
                    isSearching: false,
                    refreshing: false,
                    isErrorFound: true
                })
                this.notificationService.fail(this._dataNotFound)
                clearTimeout(this._timeoutId);
                this._timeoutId = null;
            });

    }

    static propTypes = {
        navigation: PropTypes.object,
        renderedChildren: PropTypes.func,
        screenProps: PropTypes.object,
        loadingTemplate: PropTypes.func
    }

    static defaultProps = {
        navigation: null,
        renderedChildren: () => { },
        loadingTemplate: (width, height = 60) => <ListLoadingTemplateLoading width={width} height={height} />
    }


}


const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.listBackgroundColor,
        height: '100%'
    },
    containers: {
        marginTop: 0,
        flex: 1,
        flexDirection: 'column'
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 20,
        color: COLORS.white
    }
});