
import React from 'react';
import { AuthService } from '../../services/AuthService';
import { View, Image, StyleSheet, Animated } from 'react-native';
import TextView from '../../components/TextView/TextView';
import { COLORS } from '../../config/colors/colors';
import LoggedUserModel from '../../models/loggedUserModel';
import NotificationServices from '../../services/NotificationsServices';
import UsuariosServices from '../../services/usuarios.services/usuarios.services';
import { HttpServices } from '../../FunctionaityProviders/http.services/http.service';
import Utils from '../../utils/Utils';
import DeviceInformation from '../../FunctionaityProviders/device_info/device_info';
import AppInit from '../../../App';
import { MenuStylesEnum } from '../../models/MenuStyleEnum';

export default class App extends React.Component {

    private notificationServices: NotificationServices;
    private userServices: UsuariosServices;

    static navigationOptions = {
        title: 'Bienvenidos',
        header: null
    };

    state = {
        errorVersion: null,
        errorNetwork: null,
        appName: 'App'
    }

    public navigation: any;

    constructor(props) {
        super(props);

        this.notificationServices = new NotificationServices;
        this.userServices = new UsuariosServices;
    }

    async componentWillMount() {
        //Cargamos la variable con la informacion del  Dispositivo

        let appData = await AuthService.getAppData();


        if (appData && appData.appName) {
            this.setState({
                appName: appData.appName
            })
        }

        //this._checkAppInfo();
        this._loadAsyncUser();

    }


    async _loadAsyncUser() {
        let userLogged = await this.userServices.authService.isLoggedUser();

        if (!userLogged) {
            this._goToLogin();
            return;
        }

        //Iniciamos los Headers
        HttpServices.setHeader({
            'Authorization': 'Bearer ' + AuthService.userLoggedData.userToken,
            'X-RFTK': AuthService.userLoggedData.RfTkn
        });

        this._checkToken(AuthService.userLoggedData,
            () => {
                //AppInit.reload();

                let shouldShowDefault = AuthService.userLackOfViews();
                shouldShowDefault ?
                    this._goDefault() :
                    this._goDashBoard();
            },
            () => {
                this._goToLogin();
            });
    }

    _goDashBoard() {

        Utils.navigateWithStackReset(this.props.navigation,
            'ModulesDrawer'
        );
    }

    _goDefault() {
        Utils.navigateWithStackReset(this.props.navigation, 'Default');
    }

    _goToLogin() {
        AuthService.logOut();
        Utils.navigateWithStackReset(this.props.navigation, 'Login');
    }

    _checkToken(userdata: LoggedUserModel, onSuccess: Function, onFail: Function) {
        this.userServices.checkToken(userdata.userToken)
            .then(async resp => {
                if (!resp.success) {
                    this.notificationServices.fail('Token de acceso vencido, volverá al Inicio de Sesión');
                    onFail();
                    return;
                } else {
                    //Si viene un nuevo token de Usuario, reemplazamos el anterior
                    let data = resp.data;
                    if (data.errorType && data.errorType === 'NEW_USER_TOKEN') {
                        let userData = AuthService.userLoggedData;
                        userData.userToken = data.userNewToken
                        await AuthService.saveUserToStorage(userData);

                    }
                }
                onSuccess();
            }).catch((e) => {
                this.notificationServices.fail('Token de acceso no pudo ser verificado.');
                onFail();
            })
    }




    render() {
        return (

            <View style={{ flex: 1, paddingLeft: 10, paddingRight: 10 }}>
                {
                    !(this.state.errorNetwork || this.state.errorVersion) &&
                    <View style={{
                        flex: 1, width: 100 + '%', justifyContent: 'center', alignItems: 'center'
                    }}>
                        <Animated.Image source={require('../../../assets/icon_box_blue.png')} style={styles.image} />
                        <TextView textValue={this.state.appName} styles={{ color: COLORS.secondaryColor, fontSize: 20, fontWeight: 'bold' }} />
                    </View>
                }


            </View>

        )
    }

}

const styles = StyleSheet.create({
    image: {
        width: 150,
        height: 150
    }
});