import { sha256 } from 'js-sha256';
import React from 'react';
import { Image, StyleSheet, View, ScrollView } from 'react-native';
import FAIcons from 'react-native-vector-icons/FontAwesome';
import Button from '../../components/Button/Button';
import InputText from '../../components/InputText/InputText';
import TextView from '../../components/TextView/TextView';
import { COLORS } from '../../config/colors/colors';
import STRINGS_LOGIN from '../../config/strings/login.strings';
import { AuthService } from '../../services/AuthService';
import NotificationServices from '../../services/NotificationsServices';
import UsuariosServices from '../../services/usuarios.services/usuarios.services';
import Utils from '../../utils/Utils';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { LoadingIndicator } from '../../components/loading/loding';
import DeviceInformation from '../../FunctionaityProviders/device_info/device_info';
import { NavigationActions } from 'react-navigation';
import AppInit from '../AppInit';


export default class Login extends React.Component {

    private usuariosServices: UsuariosServices;
    private notificacionesServices: NotificationServices;
    private authService: AuthService;
    public navigation: any;
    static navigationOptions = {
        title: 'Login',
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            usuario: null,
            clave: null,
            showPassword: false,
            loading: false,
            appVersion: AuthService.appData.appVersion
        };
        this.usuariosServices = new UsuariosServices;
        this.notificacionesServices = new NotificationServices;
        this.authService = this.usuariosServices.authService;
    }

    async componentDidMount() {
    }



    render() {

        const { navigation } = this.props;
        this.navigation = navigation;
        return (


            <View style={styles.main_container}>

                <View style={{
                    height: 90 + '%',
                    borderBottomLeftRadius: 180,
                    borderBottomRightRadius: 180,
                    backgroundColor: COLORS.white,
                    overflow: 'hidden',
                }}>
                    <ScrollView contentContainerStyle={{
                        flexGrow: 1,
                        justifyContent: 'space-between'
                    }}>
                        <View style={styles.header_container}>
                            <Image source={require('../../../assets/activity_icon.png')} style={styles.image} />
                        </View>
                        <View style={styles.body_container}>
                            <View style={[styles.marginsBottom10, styles.input_container]}>
                                <View style={{ flex: 0.1, alignItems: 'center' }}>
                                    <FAIcons name="user-circle-o" size={20} color={COLORS.secondaryColor} />
                                </View>
                                <View style={{ flex: 0.9, alignItems: 'center' }}>
                                    <InputText
                                        ref='usuario'
                                        onSubmit={() => {
                                            this.refs.clave.focus()
                                        }}
                                        placeholder={STRINGS_LOGIN.Usuario}
                                        styles={[styles.inputs]}
                                        value={this.state.usuario}
                                        onChange={(usuario) => {
                                            this.setState({ usuario: usuario });
                                        }}
                                        toUpper
                                        placeHolderTextColor={COLORS.secondaryColor}
                                    ></InputText>
                                </View>
                            </View>
                            <View style={[styles.marginsBottom10, styles.input_container]}>
                                <View style={{ flex: 0.1, alignItems: 'center' }}>
                                    <FAIcons name="lock" size={20} color={COLORS.secondaryColor} />

                                </View>
                                <View style={{ flex: 0.8, alignItems: 'center' }}>
                                    <InputText
                                        ref='clave'
                                        placeholder={STRINGS_LOGIN.Clave}
                                        styles={[styles.inputs]}
                                        type={this.state.showPassword ? 'text' : 'password'}
                                        value={this.state.clave}
                                        onChange={(clave) => {
                                            this.setState({ clave: clave });
                                            if (!clave)
                                                this.setState({ showPassword: false })
                                        }}
                                        placeHolderTextColor={COLORS.secondaryColor}
                                    ></InputText>
                                </View>
                                <View style={{ flex: 0.1, alignItems: 'center' }}>
                                    {this.state.clave ?
                                        <TouchableOpacity onPress={this._toggleShowPassword.bind(this)}>
                                            <FAIcons name={this.state.showPassword ? 'eye-slash' : 'eye'} size={20} color={COLORS.secondaryColor} />
                                        </TouchableOpacity> :
                                        null}
                                </View>
                            </View>
                            <View style={{ marginTop: 20 }}>
                                <Button
                                    type="light"
                                    text={STRINGS_LOGIN.Iniciar}
                                    enabled={!this.state.loading}
                                    onPress={this._onPress.bind(this)}
                                    buttonStyles={styles.button_container}
                                    buttonColorStyles={{
                                        backgroundColor: COLORS.white
                                    }}
                                    textStyles={{
                                        color: COLORS.secondaryColor
                                    }}
                                />
                            </View>
                        </View>

                    </ScrollView>
                </View>
                <View style={styles.version_container}>
                    <TextView textValue={'V' + this.state.appVersion} styles={{ color: COLORS.white }} />
                </View>
                <LoadingIndicator ref='loading' />
            </View>
        )
    }

    _toggleShowPassword() {
        this.setState({
            showPassword: !this.state.showPassword
        })
    }

    _onPress() {
        var usuario = this.state.usuario;
        var clave = this.state.clave;
        if (!usuario || !clave) {
            this.notificacionesServices.info('Debe ingresar las credenciales solicitadas.');
            return;
        }

        this.setState({ loading: true });
        this.refs.loading.show();

        this._loginUser(usuario, sha256(clave), AuthService.firebaseToken)
            .then(resp => {
                if (resp.success) {

                    //Registramos los datos de la sesion del usuario
                    this.authService.loginUser(resp.data)
                        .then(() => {
                            //Registramos los datos del dispositivo del usuario
                            let deviceInfo = DeviceInformation.deviceInfo();

                            this.usuariosServices.saveDeviceInfo(AuthService.userLoggedData.userName,
                                deviceInfo)
                                .then((respuesta) => {
                                    if (respuesta.success) {

                                        this.setState({ loading: false });
                                        this.refs.loading.hide();

                                        //Verificamos el Estilo del Home
                                        let home = AuthService.userLoggedData.userAppConfig && AuthService.userLoggedData.userAppConfig.menuStyle ?
                                            AuthService.userLoggedData.userAppConfig.menuStyle : 'DrawerNavigator';

                                        let shouldShowDefault = AuthService.userLackOfViews();
                                        shouldShowDefault ?
                                            Utils.navigateWithStackReset(this.props.navigation,
                                                'Default') :
                                            Utils.navigateWithStackReset(this.props.navigation,
                                                'ModulesDrawer')





                                    } else {
                                        throw Error(`Guardando info del dispositivo: ${respuesta.message}`)
                                    }

                                })
                                .catch((e) => {
                                    console.log(e);
                                    this.setState({ loading: false });
                                    this.refs.loading.hide();
                                    this.notificacionesServices.fail('No se pudo Iniciar sesión\nNo se enviaron datos del dispositivo.');
                                    AuthService.logOut();
                                });
                        });

                } else {
                    throw Error(resp.message);
                }
            })
            .catch((e) => {
                console.log(e);
                this.notificacionesServices.fail(e.message);
                AuthService.logOut();
                this.setState({ loading: false });
                this.refs.loading.hide();
            });


    }

    _loginUser(usuario: string, clave: string, FBtoken: string) {

        return this.usuariosServices.login(
            {
                usuario: usuario,
                clave: clave,
                movilNT: FBtoken,
                loggedFrom: 'NA_MOVIL'
            });

    }

    static propTypes = {

    }
}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: COLORS.secondaryColor,
        height: '100%',
    },
    marginsBottom10: {
        marginBottom: 10
    },
    inputs: {
        padding: 20,
        borderRadius: 20,
        borderColor: COLORS.secondaryColor,
        borderWidth: 0,
        backgroundColor: COLORS.white,
        color: COLORS.secondaryColor,
        width: '100%'
    },
    image: {
        width: 150,
        height: 150,
        backgroundColor: 'transparent'
    },
    button_container: {
        borderRadius: 50,
        borderColor: COLORS.secondaryColor,
        borderWidth: 1,
        paddingTop: 10,
        paddingBottom: 10
    },
    header_container: {
        height: '40%',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 5,
        paddingBottom: 5,
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
    },
    body_container: {
        height: '60%',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 25,
        paddingBottom: 5,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        marginLeft: 20,
        marginRight: 20
    },
    version_container: {
        height: '10%',
        justifyContent: 'flex-end',
        alignItems: 'center',
        textAlign: 'center',
    },
    input_container: {
        flexDirection: 'row',
        width: 100 + '%',
        alignItems: 'center',
        alignContent: 'center',
        borderRadius: 50,
        borderColor: COLORS.secondaryColor,
        borderWidth: 1,
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: COLORS.white
    }
});