import React from 'react';
import { View, SafeAreaView, Platform, StatusBar, StyleSheet, Image, ActivityIndicator } from 'react-native';
import { COLORS } from '../config/colors/colors';
import { FirebaseUtils } from '../FunctionaityProviders/Firebase/firebaseapp.config';
import NotificationServices from '../services/NotificationsServices';
import { AuthService } from '../services/AuthService';
import moment from 'moment';
// import RNFetchBlob from 'rn-fetch-blob';
import Modal from 'react-native-modal';
import TextView from '../components/TextView/TextView';
import Button from '../components/Button/Button';
import { ScrollView } from 'react-native-gesture-handler';
import Separator from './main/separator.view';
import { PermissionsAndroid } from 'react-native';
import Toast from 'react-native-easy-toast';
import STRINGS from '../config/strings/system.strings';
import UsuariosServices from '../services/usuarios.services/usuarios.services';
import DeviceInformation from '../FunctionaityProviders/device_info/device_info';
import { HttpServices } from '../FunctionaityProviders/http.services/http.service';
import Routes from './main/Router';


export default class AppInit extends React.Component {
    private notificationServices: NotificationServices;
    private userServices: UsuariosServices;


    state = {
        showModal: false,
        currentNotification: {},
        applicationLoaded: false,
        errorVersion: null,
        errorNetwork: null,
        algo: null
    }

    constructor(props) {
        super(props)
        this.notificationServices = new NotificationServices;
        this.userServices = new UsuariosServices;
        moment().locale('es')
    }

    async componentDidMount() {
        let userData = await AuthService.userLoggedDatas();
        let appData = await AuthService.getAppData();
        let secondaryColor = await AuthService.getsecondaryColors();
        let deviceInfo = await DeviceInformation.loadDeviceData();


        HttpServices.setHeader({
            'Content-Type': 'application/json',
            'X-Device-Id': DeviceInformation.deviceInfo().device_id,
            'X-Device-App-Version': DeviceInformation.deviceInfo().app_version,
            'X-Device-App-Key': DeviceInformation.deviceInfo().app_key,

        });

        this._checkAppInfo();

        // await this._checkPermission();

        // //Creamos Los Listeners
        // //Cuando la app esta en foreground
        // FirebaseUtils.registerForegroundNotificationCallback((message) => {
        //     AppInit._handleIncommingNotification(message);
        // })
        // //Cuando solo se envia data y la app esta en foreground
        // FirebaseUtils.registerOnMessageDataCallback(message => {
        //     AppInit._handleIncommingNotification(message);
        // });

        // //CLICK ACTION
        // //Cuando se clickea una notificacion y se abre la aplicacion estando cerrada
        // FirebaseUtils.registerApplicationClosedAndOpenedByNotification((message) => {
        //     this._handleClickUpdateNotification(message);
        // });

        // FirebaseUtils.registerNotificationClickedCallback((notificacion) => {
        //     this._handleClickUpdateNotification(notificacion)

        // });

        // //Iniciamos la escucha de mensajes
        // FirebaseUtils.createAppNotificationListeners();
        // FirebaseUtils.getFirebaseInstance().messaging().subscribeToTopic('appUpdate')
    }

    // async _checkPermission() {
    //     let permission = await FirebaseUtils.checkPermission();
    //     if (permission) {
    //         let userToken = await FirebaseUtils.getToken();
    //         AuthService.setDeviceFirebaseMessagingToken(userToken);
    //     } else {
    //         FirebaseUtils.requestPermission()
    //             .then(async () => {
    //                 let userToken = await FirebaseUtils.getToken();
    //                 AuthService.setDeviceFirebaseMessagingToken(userToken);
    //             }).catch(() => {
    //                 this.notificationServices.fail('No se permitió envio de Notificaciones');
    //             });
    //     }

    //     //Permisos de almacenamiento
    //     const granted = await PermissionsAndroid.request(
    //         PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    //         {
    //             title: 'Permisos para escribir en el dispositivo requerido.',
    //             message:
    //                 'Se necesitan permisos para escritura en el dispositivo.',
    //             buttonNegative: 'Cancelar',
    //             buttonPositive: 'OK',
    //         },
    //     );
    //     if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
    //         this.notificationServices.fail('No podras descargar las actualizaciones de la app.\nSi deseas recibir las actualizaciones debes dar permisos manualmente');
    //     }
    //     return true;
    // }

    // public static async  _handleIncommingNotification(notification) {
    //     let data = notification.data;
    //     if (data.TYPE && data.TYPE === 'APP_UPDATE') {
    //         //VALIDATE APP UPDATE NOTIFICACION
    //         const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
    //         if (granted)
    //             AppInit._handleMessageAppUpdate(notification);
    //     } else {
    //         //DEFAULT MESSAGE
    //         FirebaseUtils.configChannel();
    //         FirebaseUtils.getFirebaseNotificationInstance().displayNotification(notification);
    //     }
    // }

    // public static _handleMessageAppUpdate = (message) => {
    //     let data = message.data;
    //     if (data.TYPE === 'APP_UPDATE') {
    //         let notificacion = FirebaseUtils.createNotification({
    //             title: 'Notificación de Actualización de la Aplicación',
    //             body: 'Ha sido lanzada una nueva versión de la aplicación',
    //             data: data
    //         });

    //         //Agregamos los botones a la notificacion
    //         let firebaseInstance = FirebaseUtils.getFirebaseInstance();
    //         let action = new
    //             firebaseInstance.notifications.Android.Action('ConfirmDownload', FirebaseUtils.notificationSmallIcon, 'Descargar');
    //         notificacion.android.addAction(action);

    //         //Mostramos la notificacion
    //         FirebaseUtils.configChannel();
    //         FirebaseUtils.getFirebaseNotificationInstance().displayNotification(notificacion);
    //     }
    // }


    // _handleClickUpdateNotification(notification) {
    //     let action = notification.action;

    //     //Cerramos la notificacion
    //     FirebaseUtils.getFirebaseNotificationInstance()
    //         .removeDeliveredNotification(notification.notification.notificationId);

    //     //Si va a descargar, cargamos el modal
    //     if (action === 'ConfirmDownload') {
    //         this._handleModal(notification.notification);
    //     }
    // }

    // _handleModal(notificacion) {
    //     notificacion.data.optional = !(notificacion.data.optional === 'false');
    //     this.setState({
    //         showModal: !this.state.showModal,
    //         currentNotification: notificacion.data
    //     })
    // }

    // async _handleDownload() {
    //     let data = this.state.currentNotification;
    //     let appData = await AuthService.getAppData();
    //     this.refs.toast.show('Descargando...', 1000);
    //     this._closeModal();

    //     const android = RNFetchBlob.android
    //     let dirs = RNFetchBlob.fs.dirs

    //     RNFetchBlob
    //         .config({
    //             addAndroidDownloads: {
    //                 useDownloadManager: true,
    //                 notification: true,
    //                 title: `Descarga de ${appData.appName}V_${data.incoming_version}.apk.`,
    //                 description: 'File downloaded by download manager.',
    //                 path: dirs.DownloadDir + `/${STRINGS.appPrefix}/apks/${appData.appName}V_${data.incoming_version}.apk`,
    //                 mime: 'application/vnd.android.package-archive',

    //             }
    //         })
    //         .fetch('GET', `${data.bucket_url}/app-universal-release.apk`)
    //         .then((resp) => {
    //             // the path of downloaded file
    //             return Promise.resolve(resp);
    //         })
    //         .then((res) => {
    //             android.actionViewIntent(res.path(), 'application/vnd.android.package-archive');
    //             this.refs.toast.show(`Finaliza descarga ${appData.appName}V_${data.incoming_version}.apk.\n${dirs.DownloadDir}/${STRINGS.appPrefix}/apks/`
    //                 , 1000)
    //         })
    //         .catch((e) => {
    //             this.notificationServices.fail('No se pudo descargar apk.');
    //             console.log(e)
    //         })

    // }

    // componentWillUnmount() { 
    //     FirebaseUtils.getMessageListener() ? FirebaseUtils.getMessageListener()() : null;
    //     FirebaseUtils.getNotificationOpenedListener() ? FirebaseUtils.getNotificationOpenedListener()() : null;
    //     FirebaseUtils.getNotificationListener() ? FirebaseUtils.getNotificationListener()() : null;
    // }
 
    render() {
        return (
            <SafeAreaView style={styles.safeViewArea}>
                <StatusBar translucent backgroundColor={COLORS.primaryColor} barStyle={'dark-content'} />
                <View style={{ flex: 1, flexDirection: "column" }}>
                    {
                        !(this.state.errorNetwork || this.state.errorVersion)
                            ?
                            (
                                !this.state.applicationLoaded ?
                                    < View style={{
                                        flex: 1,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                        , paddingLeft: 10, paddingRight: 10
                                    }}>
                                        <ActivityIndicator size="small" color={COLORS.secondaryColor} />
                                        <TextView textValue="Cargando Aplicación..." />
                                    </View>
                                    :
                                    <Routes />

                            ) : null
                    }
                    {
                        (this.state.algo &&
                            <TextView textValue="Cargando Aplicación..." />)
                    }
                    {
                        this.state.errorVersion &&
                        <View style={{
                            flex: 1, width: 100 + '%', justifyContent: 'center', alignItems: 'center', paddingLeft: 10, paddingRight: 10
                        }}>
                            <View style={{
                                marginTop: 30,
                                alignItems: 'center'
                            }}>
                                <Image source={require('../../assets/no_access_cellphone.png')} style={{
                                    width: 60,
                                    height: 60
                                }} />
                                <TextView textValue={this.state.errorVersion} styles={{
                                    textAlign: 'center',
                                    fontSize: 13,
                                    color: '#455a64',
                                    marginTop: 20
                                }} />
                            </View>


                        </View>
                    }
                    {
                        this.state.errorNetwork &&
                        <View style={{
                            flex: 1, width: 100 + '%', justifyContent: 'center', alignItems: 'center', paddingLeft: 10, paddingRight: 10
                        }}>
                            <View style={{
                                marginTop: 30,
                                alignItems: 'center'
                            }}>
                                <Image source={require('../../assets/no_network.png')} style={{
                                    width: 60,
                                    height: 60
                                }} />
                                <TextView textValue={this.state.errorNetwork} styles={{
                                    textAlign: 'center',
                                    fontSize: 13,
                                    color: '#455a64',
                                    marginTop: 20
                                }} />
                            </View>


                        </View>
                    }


                </View>
                {/* <Modal isVisible={this.state.showModal}
                    backdropOpacity={0.5}>

                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{
                            backgroundColor: COLORS.white,
                            borderRadius: 20,
                            padding: 20,
                            flex: 1
                        }}>

                            <View style={{
                            }}>
                                <View style={{
                                    alignContent: 'center',
                                    justifyContent: 'center',
                                    width: 100 + '%'
                                }}>
                                    <TextView textValue="Actualización de la Aplicación" styles={{
                                        fontSize: 20,
                                        fontWeight: 'bold',
                                        textAlign: 'center'
                                    }} />
                                    <Separator />

                                    <View style={{
                                        alignItems: 'center',
                                        marginTop: 20
                                    }}>

                                        <Image source={require('../../assets/app_update.png')} style={{
                                            width: 70,
                                            height: 80
                                        }} />

                                    </View>
                                    <View style={{
                                        marginTop: 15
                                    }}>
                                        <TextView textValue="Version Nro:" styles={{
                                            fontSize: 12,
                                            fontWeight: 'bold'
                                        }} />
                                        <TextView textValue={this.state.currentNotification.incoming_version} styles={{
                                            fontSize: 12
                                        }} />
                                    </View>
                                    <View style={{
                                        marginTop: 15,
                                    }}>
                                        <TextView textValue="Cambios:" styles={{
                                            fontSize: 12,
                                            fontWeight: 'bold'
                                        }} />
                                        <TextView textValue={this.state.currentNotification.incoming_content} styles={{
                                            fontSize: 12
                                        }} />
                                    </View>
                                    <View style={{
                                        marginTop: 20,
                                        marginBottom: 20
                                    }}>
                                        <TextView textValue='* Recuerda Remover la version actual y luego Instalar la nueva app.' styles={{
                                            fontSize: 12,
                                            fontWeight: 'bold'
                                        }} />
                                    </View>
                                    <View style={{
                                        marginTop: 20,
                                        marginBottom: 20
                                    }}>
                                        {
                                            !this.state.currentNotification.optional &&
                                            <TextView textValue='* Esta actualización es Obligatoria.' styles={{
                                                color: COLORS.red,
                                                fontSize: 12,
                                                fontWeight: 'bold'
                                            }} />
                                        }
                                    </View>
                                </View>
                            </View>

                            <View style={{
                                height: 50
                            }}>
                                <Separator />
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: 100 + '%'
                                }}>
                                    <View style={{
                                        flex: 0.45
                                    }}>
                                        <Button text="Descargar" onPress={this._handleDownload.bind(this)} type='success' outlined ></Button>
                                    </View>
                                    {
                                        this.state.currentNotification.optional &&
                                        <View style={{ flex: 0.1 }}></View>
                                    }
                                    {
                                        this.state.currentNotification.optional &&
                                        <View style={{
                                            flex: 0.45
                                        }}>
                                            <Button text="Cerrar"
                                                onPress={this._closeModal.bind(this)} type='warning' outlined ></Button>
                                        </View>
                                    }
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </Modal> */}
                <Toast
                    position='bottom'
                    ref="toast"
                />
            </SafeAreaView >
        )

    }

    _checkAppInfo() {
        this.userServices.checkAppInfo()
            .then(async (response) => {
                //Si no fué succes
                if (!response.success) {
                    this.setState({
                        errorNetwork: response.message
                    })
                } else {

                    //Guardamos la informacion de este dispositivo
                    await AuthService.saveAppInfo(response.data);
                    this.setState({
                        applicationLoaded: true
                    });

                }
            })
            .catch((a) => {
                console.log('Validar App Version erorr')
                if (a.response && a.response.data) {
                    if (!a.response.data.success) {
                        this.setState({
                            errorVersion: a.response.data.message
                        })
                    }
                } else {
                    this.setState({
                        errorNetwork: 'Opss..!, Algo malo sucedió, no puedo aceder en estos momentos.'
                    })
                }
            })
    }

    _closeModal() {
        this.setState({
            showModal: false
        })
    }
}
const styles = StyleSheet.create({
    safeViewArea: {
        flex: 1,
        backgroundColor: "#fff",
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    }
});


