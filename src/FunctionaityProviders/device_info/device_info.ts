//EJECTED
//import DeviceInfo from 'react-native-device-info'
//EXPO
import Constants from 'expo-constants';
import { Platform } from 'react-native';
const app = require('../../../app.json');

export default class DeviceInformation {

    private static _DeviceInfo: any;



    static loadDeviceData() {
        return new Promise(async (succ, fail) => {

            try {

                //EJECTED
                // DeviceInformation._DeviceInfo = {
                //     so_architecture: (await DeviceInfo.supportedABIs()).join(','),
                //     device_name: await DeviceInfo.getDeviceName(),
                //     device_id: await DeviceInfo.getDeviceId(),
                //     device_year: 0,
                //     device_sdk_version: Platform.Version,
                //     device_platform: Platform.OS,
                //     device_version: await DeviceInfo.getSystemVersion(),
                //     app_version: app.appVersion,
                //     app_key: app.appKey
                // }


                //EXPO
                DeviceInformation._DeviceInfo = {
                    so_architecture: ['expo-architecture'],
                    device_name: Constants.deviceName,
                    device_id: Constants.deviceId,
                    device_year: Constants.deviceYearClass,
                    device_platform: Platform.OS,
                    device_version: Constants.systemVersion,
                    app_version: app.appVersion,
                    device_sdk_version: 1,
                    app_key: app.appKey
                }

                succ(DeviceInformation._DeviceInfo);


            } catch (e) {
                fail(e);
            }

        })
    }


    static deviceInfo(): any {
        return DeviceInformation._DeviceInfo;
    }


}