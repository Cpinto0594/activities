export class DocumentsPicker {



    async getDocument(options = {}) {
        const DocumentPicker = require('expo-document-picker');
        return await DocumentPicker.getDocumentAsync({
            ...options,
            copyToCacheDirectory: true
        });
    }






}