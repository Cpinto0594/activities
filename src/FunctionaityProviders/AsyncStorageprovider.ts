// import AsyncStorage from '@react-native-community/async-storage';
import { AsyncStorage } from "react-native";

export default class AsyncStorageProvider {

    constructor() {

    }
    static async setItem(itemKey: string, value: any = null) {
        return ( await AsyncStorage.setItem(itemKey, value) );
    }

    static async getItem(itemKey: string): Promise<any> {
        return ( await AsyncStorage.getItem(itemKey) ) ;
    }

    static async  removeItem(itemKey: string) {
        await AsyncStorage.removeItem(itemKey);
    }




}