// EXPO
import * as ImagesManipulator from 'expo-image-manipulator';


// EJECTED
// import ImageResizer from 'react-native-image-resizer';
// import ImgToBase64 from 'react-native-image-base64';



export default class ImageManipulator {


    constructor() {

    }

    // EXPO
    async resizeImage(imageUri, width, height, format = 'JPEG', quality = 100) {

        return new Promise(async (succ, fail) => {

            try {
                let _format = null;
                switch (format) {
                    case 'JPEG':
                        _format = ImagesManipulator.SaveFormat.JPEG;
                        break;
                    case 'PNG':
                        _format = ImagesManipulator.SaveFormat.PNG;
                        break;
                    case 'WEBP':
                        _format = ImagesManipulator.SaveFormat.WEBP;
                        break;
                }

                const manipResult = await ImagesManipulator.manipulateAsync(
                    imageUri,
                    [{ resize: { width: width, height: height } }],
                    { format: _format, base64: true }
                );
                succ(manipResult);
            } catch (error) {
                fail(error);
            }
        });

    }

    // resizeImage = async (imageUri, width, height, format = 'JPEG', quality = 100) => {
    //     return new Promise((succ, fail) => {

    //         try {

    //             ImageResizer.createResizedImage(imageUri, width, height,
    //                 format, quality).then((response) => {
    //                     // response.uri is the URI of the new image that can now be displayed, uploaded...
    //                     // response.path is the path of the new image
    //                     // response.name is the name of the new image with the extension
    //                     // response.size is the size of the new image

    //                     succ(response);
    //                 }).catch((err) => {
    //                     // Oops, something went wrong. Check that the filename is correct and
    //                     // inspect err to get more details.
    //                     fail(err);
    //                 });

    //         } catch (error) {
    //             fail(error);
    //         }

    //     });

    // }

    // imageUriToBase64 = async (uri) => {
    //     return new Promise(async (succ, fail) => {

    //         try {
    //             let base64 = await ImgToBase64.getBase64String(uri);
    //             succ(base64);
    //         } catch (error) {
    //             fail(error);
    //         }
    //     });
    // }




}