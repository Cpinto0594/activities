import { Platform } from "react-native";

//import ImagePicker from 'react-native-image-picker';

export default class ImageLoader {



    requestPermission = async (): Promise<Boolean> => {

        let Permissions = require('expo-permissions');

        if (Platform.OS === 'ios') {
            let response = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (response.status !== 'granted') {
                return false;
            }
            response = await Permissions.askAsync(Permissions.CAMERA);
            if (response.status !== 'granted') {
                return false;
            }
        }
        return true;
    }

    launchGallery = async (options: any): Promise<any> => {
        let ImagePicker = require('expo-image-picker');
        return await ImagePicker.launchImageLibraryAsync(options);

    }

    launchCamera = async (options: any): Promise<any> => {
        let ImagePicker = require('expo-image-picker');
        return await ImagePicker.launchCameraAsync(options);

    }


    // requestPermission = async (): Promise<Boolean> => {

    //     // let Permissions = require('expo-permissions');

    //     // if (Platform.OS === 'ios') {
    //     //     let response = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    //     //     if (response.status !== 'granted') {
    //     //         return false;
    //     //     }
    //     //     response = await Permissions.askAsync(Permissions.CAMERA);
    //     //     if (response.status !== 'granted') {
    //     //         return false;
    //     //     }
    //     // }
    //     return true;
    // }

    // launchGallery = async (options: any): Promise<any> => {
    //     return new Promise((succ, fail) => {
    //         try {
    //             ImagePicker.launchImageLibrary(options, (response) => {
    //                 if (response && response.error) {
    //                     throw Error(response.error);
    //                 }
    //                 succ(response);
    //             });
    //         } catch (error) {
    //             fail(error);
    //         }
    //     })
    // }

    // launchCamera = async (options: any): Promise<any> => {
    //     return new Promise((succ, fail) => {
    //         try {
    //             ImagePicker.launchCamera(options, (response) => {
    //                 if (response && response.error) {
    //                     throw Error(response.error);
    //                 }
    //                 succ(response);
    //             });
    //         } catch (error) {
    //             fail(error);
    //         }
    //     })
    // }



}