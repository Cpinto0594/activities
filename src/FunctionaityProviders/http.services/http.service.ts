import axios from 'axios';
import LoggedUserModel from '../../models/loggedUserModel';
import AsyncStorageProvider from '../AsyncStorageprovider';
import STRINGS from '../../config/strings/system.strings';

export class HttpServices {
    private static Headers: {};
    private requestHeaders: {};
    private apiInstance;

    constructor(apiInstance?) {
        this.apiInstance = apiInstance || axios.create({
            timeout: 60000,
            responseType: 'json'
        });
    }

    public static getHeaders(): {} {
        return HttpServices.Headers;
    }
    public static setHeader(header: any) {
        if (!header) return;

        if (!HttpServices.Headers) {
            HttpServices.Headers = {};
        }

        HttpServices.Headers = Object.assign({}, HttpServices.Headers, header);

    }

    public static removeHeader(headerKey: string) {
        if (!headerKey) return;

        if (!HttpServices.Headers) {
            return;
        }

        delete HttpServices.Headers[headerKey];

    }

    setRequestHeader(header) {
        if (!header) return;

        if (!this.requestHeaders) {
            this.requestHeaders = {};
        }

        this.requestHeaders = Object.assign({}, this.requestHeaders, header);
    }

    removeRequestHeader(headerKey) {
        if (!headerKey) return;

        if (!this.requestHeaders) {
            return;
        }



        delete this.requestHeaders[headerKey];

    }

    getAllHeaders(options: any): any {
        let objectHeaders = {};
        objectHeaders = Object.assign({}, HttpServices.Headers, this.requestHeaders, (options || { headers: {} }).headers);
        //return new Headers(objectHeaders);
        return objectHeaders;
    }

    post(url, data: any | [] = {}, options?: any): Request {
        return new Request(this.apiInstance, url, data, {
            ...options,
            headers: this.getAllHeaders(options)
        }).post();
    }

    put(url, data: any | [] = {}, options?: any) {
        return new Request(this.apiInstance, url, data, {
            ...options,
            headers: this.getAllHeaders(options)
        }).put();
    }

    delete(url, options?: any) {
        return new Request(this.apiInstance, url, null, {
            ...options,
            headers: this.getAllHeaders(options)
        }).delete();
    }

    get(url, options?: any) {
        return new Request(this.apiInstance, url, null, {
            ...options,
            headers: this.getAllHeaders(options)
        }).get();
    }

}


declare interface RequestOptions {
    headers: any
}

export class Request {

    public static JSON_MEDIATYPE = 'application/json';

    private CancelToken = axios.CancelToken;
    private cancellationSource;
    private url: any;
    private requestData: any | [];
    private requestOptions: any;
    private response: Promise<Response>;
    private apiInstance;

    get: Function;
    post: Function;
    put: Function;
    delete: Function;
    options: Function;
    then = (callback) => { return this.response.then(callback) }
    finally = (callback) => { return this.response.finally(callback) }
    catch = (callback) => { return this.response.catch(callback) }

    private methods = [
        {
            method: 'post', func: (): Request => this._request('POST')
        },
        {
            method: 'put', func: (): Request => this._request('PUT')
        },
        {
            method: 'delete', func: (): Request => this._request('DELETE')
        },
        {
            method: 'get', func: (): Request => this._request('GET')
        },
        {
            method: 'options', func: (): Request => this._request('OPTIONS')
        }
    ];



    constructor(apiInstance, url: string | Function, data?: any | [], options?: any) {

        this.url = url;
        this.requestData = data;
        this.requestOptions = options;
        this._initMethods();
        this.apiInstance = apiInstance;
        this.cancellationSource = this.CancelToken.source();


        if (this.apiInstance) {

            this.apiInstance.interceptors.response.use(
                async (response) => {
                    if (!response) return response;
                    let message = (response.data || {}).usrTokenRevalidation;
                    //Si es un refresToken guardamos el nuevo token al usuario en sesion
                    if (message) {
                        let userLoggedData = await AsyncStorageProvider.getItem(STRINGS.KeyUserData);
                        let userData: LoggedUserModel = JSON.parse(userLoggedData);
                        userData.userToken = message.newToken;

                        let stringUsr = JSON.stringify(userData);
                        AsyncStorageProvider.setItem(STRINGS.KeyUserData, stringUsr);
                    }
                    return response || {};
                },
                (error) => {
                    return Promise.reject(error);
                })
        }

    }

    private _initMethods() {
        this.methods.forEach(method => {
            this[method.method] = method.func;
        })
    }

    _getValueFromHeader(itemHeader: string) {
        return this.requestOptions.headers[itemHeader] || '';
    }
    _convertData() {
        if (
            this.requestData &&
            this.requestOptions &&
            this._getValueFromHeader('Content-Type').toLowerCase() === Request.JSON_MEDIATYPE &&
            typeof this.requestData !== 'string'
        ) {
            return JSON.stringify(this.requestData)
        }
        return this.requestData;
    }

    private _request = (method: string): any => {

        let url = (typeof this.url === 'string' ? this.url : typeof this.url === 'function' ? this.url.call() : new String(this.url));
        this.response = this.apiInstance({
            url: url,
            data: this._convertData(),
            method: method,
            cancelToken: this.cancellationSource.token,
            ...this.requestOptions
        });

        this.response['cancel'] = this.cancel;
        return this;
    }

    cancel(message): Promise<Request> {
        return new Promise((suc, fail) => {
            try {
                console.log('Se cancela petición: ', message)
                this.cancellationSource.cancel(message);
            } catch (error) {
                fail(error);
            }
        })
    }
}
