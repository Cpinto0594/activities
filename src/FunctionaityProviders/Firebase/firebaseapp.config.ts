import firebase, { RNFirebase, Firebase } from 'react-native-firebase';
import { Platform } from 'react-native';
import STRINGS from '../../config/strings/system.strings';
import { RemoteMessage } from 'react-native-firebase/messaging';
import { COLORS } from '../../config/colors/colors';
import { BackgroundEvents } from './firebase.background.events';
import AsyncStorage from '@react-native-community/async-storage';



export class FirebaseUtils {

    private static notificationListener: any;
    private static notificationOpenedListener: any;
    private static messageListener: any;

    public static int: number = 1;

    public static backgroundEvents: any = BackgroundEvents;
    public static vibrationPattern = [100, 1000, 100, 500, 100, 1000];
    public static notificationSmallIcon = '@drawable/fb_notification_icon';
    public static notificationLargeIcon = '@minmap/ic_launcher_round';
    public static CHANNEL_NOTIFICATIONS = {
        CHANNEL_ID: "fbChannelId",
        CHANNEL_NAME: "Firebase Channel Name",
        CHANNEL_DESCRIPTION: "Firebase Channel"
    }

    private static foregroundNotificationCallback: Function;
    private static notificationClickedCallback: Function;
    private static applicationClosedAndOpenedByNotification: Function;
    private static onMessageDataCallback: Function;

    static checkPermission = async () => {
        const enabled = await firebase.messaging().hasPermission();
        return enabled;
    };

    static requestPermission = () => {
        return firebase.messaging().requestPermission();
    }

    static async getToken() {
        let fcmToken = await AsyncStorage.getItem(STRINGS.cloudMessaging.TokenStorage);
        console.log(fcmToken);
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            console.log(fcmToken);
        }
        return fcmToken;
    }


    static async createAppNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
        FirebaseUtils.notificationListener = firebase.notifications().onNotification((notification) => {
            console.log('Notification')
            let _notification: RNFirebase.notifications.Notification = FirebaseUtils.createNotification(notification);
            FirebaseUtils.foregroundNotificationCallback &&
                FirebaseUtils.foregroundNotificationCallback(_notification);
        });

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        FirebaseUtils.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {

            FirebaseUtils.notificationClickedCallback &&
                FirebaseUtils.notificationClickedCallback(notificationOpen);
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {

            FirebaseUtils.applicationClosedAndOpenedByNotification &&
                FirebaseUtils.applicationClosedAndOpenedByNotification(notificationOpen);
        }
        /*
        * Triggered for data only payload in foreground and background
        * */
        FirebaseUtils.messageListener = firebase.messaging().onMessage((message) => {
            let _notification: RNFirebase.notifications.Notification = FirebaseUtils.createNotification(message);
            console.log('onMessage')
            //process data message
            FirebaseUtils.onMessageDataCallback &&
                FirebaseUtils.onMessageDataCallback(_notification)
        });
    }

    public static configChannel() {
        const channel = new firebase.notifications.Android.Channel(
            FirebaseUtils.CHANNEL_NOTIFICATIONS.CHANNEL_ID,
            FirebaseUtils.CHANNEL_NOTIFICATIONS.CHANNEL_NAME,
            firebase.notifications.Android.Importance.Max
        ).setDescription(FirebaseUtils.CHANNEL_NOTIFICATIONS.CHANNEL_DESCRIPTION)
            .setSound('default')
            .enableLights(true)
            .enableVibration(true)
            .setVibrationPattern(FirebaseUtils.vibrationPattern)
            .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
            ;
        firebase.notifications().android.createChannel(channel);
    }

    public static registerForegroundNotificationCallback(callBack: Function) {
        FirebaseUtils.foregroundNotificationCallback = callBack;
    }

    public static registerNotificationClickedCallback(callBack: Function) {
        FirebaseUtils.notificationClickedCallback = callBack;
    }

    public static registerApplicationClosedAndOpenedByNotification(callBack: Function) {
        FirebaseUtils.applicationClosedAndOpenedByNotification = callBack;
    }
    public static registerOnMessageDataCallback(callBack: Function) {
        FirebaseUtils.onMessageDataCallback = callBack;
    }

    public static registerBackgroundCallBack(callBack: {}) {
        let key = Object.keys(callBack);
        if (!key || !key.length) {
            return;
        }
        FirebaseUtils.backgroundEvents[key[0]] = callBack[key[0]];
    }

    public static unRegisterBackgroundCallBacks() {
        FirebaseUtils.backgroundEvents = {};
    }

    public static getFirebaseNotificationInstance(): RNFirebase.notifications.Notifications {
        return firebase.notifications();
    }

    public static getFirebaseInstance(): Firebase {
        return firebase;
    }

    public static getMessageListener() {
        return FirebaseUtils.messageListener;
    }

    public static getNotificationOpenedListener() {
        return FirebaseUtils.notificationOpenedListener;
    }

    public static getNotificationListener() {
        return FirebaseUtils.notificationListener;
    }

    public static createNotification(notif) {

        if (Platform.OS === 'android') {
            const localNotification = new firebase.notifications.Notification();
            //const localNotification = notif;
            notif.android = notif.android || {};
            notif.ios = notif.ios || {};
            notif.data = notif.data || {};


            localNotification
                //.setNotificationId(notif.notificationId||Math.random()*1000)
                .setTitle(notif.title || notif.data.title)
                .setSubtitle(notif.subtitle || notif.data.subtitle || '')
                .setBody(notif.body || notif.data.body)
                .setData(notif.data || {})
                .setSound(notif.sound || notif.data.sound || 'default')
                .android.setVisibility(firebase.notifications.Android.Visibility.Public)
                .android.setVibrate(FirebaseUtils.vibrationPattern)
                .android.setClickAction(notif.android.clickAction || notif.data.clickAction || '')
                .android.setBigText(notif.body || notif.data.body)
                .android.setChannelId(FirebaseUtils.CHANNEL_NOTIFICATIONS.CHANNEL_ID)
                .android.setSmallIcon(FirebaseUtils.notificationSmallIcon || notif.smallIcon || notif.data.smallIcon)
                .android.setLargeIcon(FirebaseUtils.notificationLargeIcon || notif.largeIcon || notif.data.largeIcon)
                .android.setColor(COLORS.secondaryColor)
                .android.setGroupAlertBehaviour(firebase.notifications.Android.GroupAlert.All)
                .android.setShowWhen(true)
                .android.setPriority(firebase.notifications.Android.Priority.High);
            return localNotification;
        } else if (Platform.OS === 'ios') {
            const localNotification = new firebase.notifications.Notification()
                .setTitle(notif.title || notif.data.title)
                .setSubtitle(notif.subtitle || notif.data.subtitle || '')
                .setBody(notif.body || notif.data.body)
                .setData(notif.data || {})
                .setSound(notif.sound || notif.data.sound || 'default')
                .ios.setBadge(notif.ios.badge);

            return localNotification;
        }

    }



}

export const onBackgroundHandler = async (message: RemoteMessage) => {
    // handle your message from background
    console.log('Background awakening')
    let key = Object.keys(FirebaseUtils.backgroundEvents);
    let event = null;
    (key || [])
        .forEach(key => {
            event = FirebaseUtils.backgroundEvents[key];
            if (!(typeof event === 'function')) return;
            event && event.bind(FirebaseUtils)(message);
        });

    return Promise.resolve();
} 