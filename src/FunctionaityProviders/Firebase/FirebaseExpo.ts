
import firebase from 'firebase'; // 4.8.1
import 'firebase/firestore';
import { YellowBox } from 'react-native';

class FireBaseExpo {
    public static CHAT_LIST_COLLECTION = 'social_chats';
    public static CHAT_MESSAGES_COLLECTION = 'social_chat_messages';
    public static instance: FireBaseExpo;
    public fireStoreInstance;

    constructor() {
        YellowBox.ignoreWarnings(['Setting a timer']);

        this.init();
        this.observeAuth();

    }

    init = () => {
        if (!firebase.apps.length) {
            console.log('Init App')
            firebase.initializeApp({
                apiKey: "AIzaSyDTw6WvXzm7BskokzjmdpeLoww64Mhie7s",
                authDomain: "activitycontrolapp.firebaseapp.com",
                databaseURL: "https://activitycontrolapp.firebaseio.com",
                projectId: "activitycontrolapp",
                storageBucket: "activitycontrolapp.appspot.com",
                messagingSenderId: "769463977752",
                appId: "1:769463977752:web:2a3c58febb5bdb1dd2406d",
                measurementId: "G-GZ9JZLZD5Y"
            });
            this.fireStoreInstance = firebase.firestore();

        }
    };

    observeAuth = () =>
        firebase.auth().onAuthStateChanged(this.onAuthStateChanged);

    onAuthStateChanged = user => {
        //console.log(user)
        if (!user) {
            try {
                firebase.auth().signInAnonymously();
                console.log('Auth Ok')
            } catch ({ message }) {
                alert(message);
            }
        }
    };

    firebase() {
        return firebase;
    }
    fireStore() {
        return firebase.firestore;
    }

    uid() {
        return (firebase.auth().currentUser || {}).uid;
    }

    db() {
        return this.fireStoreInstance;
    }

    timestamp() {
        return firebase.firestore.Timestamp;
    }

    serverTimestamp() {
        return firebase.firestore.FieldValue.serverTimestamp()
    }

    // close the connection to the Backend
    off() {
        //   this.db().off();
    }
}

FireBaseExpo.instance = new FireBaseExpo();
export default FireBaseExpo;

