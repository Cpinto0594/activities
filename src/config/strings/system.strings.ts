const STRINGS = {
    KeyAppData: 'appDta',
    appPrefix: 'ActivityControl',
    KeyUserData: 'usrDta',

    navigation: {
        HOME: 'Inicio',
        ACTIVIDADES: 'Actividades',
        PERFIL: 'Datos Basicos'
    },
    cloudMessaging: {
        TokenStorage: 'fcmTokenStorageKey'
    },

    //APP CONFIG
    loadingMessage: 'Cargando ...',
    NoMenu: 'No Posee menús activos.',
    MenuStyles: 'Estilo del Menú',
    Notification: 'Notificaciones',
    Notification_AppUpdate: 'Actualizaciones de Aplicación',
    Notification_NoActivities: 'Actualizaciones No Actividades',
    No_Data_Found: 'No se obtuvo datos',

    Parametros: 'Parámetros'

}

export default STRINGS;