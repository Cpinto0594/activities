const STRINGS_LOGIN = {
    Title: 'Login',
    Usuario: 'Usuario',
    Clave: 'Contraseña',
    Iniciar: 'Iniciar',
    CerrarSesion:'Cerrar Sesión'
}

export default STRINGS_LOGIN; 