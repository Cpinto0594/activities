export const ACTIVIDADES= {
    CrearTitle:'Registrar Actividades',
    ActividadesTitle:'Registro de Actividades',
    NuevaActividad:'Nueva actividad',
    Proyecto:'Proyecto',
    Empresa:'Empresa',
    Descripcion:'Descripción',
    Dia:'Dia',
    HoraInicio:'Hora Inicio',
    HoraFin:'Hora Fin',
    GuardarActividad:'Guardar',
    Horas:'Horas'
}