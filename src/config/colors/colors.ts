export const COLORS = {


    labelDanger: '#d9534f',
    labelSuccess: '#5cb85c',
    labelInfo: '#5bc0de',
    labelDefault: '#FDFFFC',
    labelWarning: '#f0ad4e',


    /////GREY
    midGrey: '#757575',
    midSoftGrey: '#9E9E9E',
    softGrey: '#E5E6E4',

    red: '#B00020',

    listBackgroundColor: '#FDFFFC',

    fontColor: '#263238',

    headerBorderBottomColor: '#f2f2f4',

    primaryColor: '#edeff7',
    secondaryColor: '#242A37',
    contrastColor: '#008ABE',
    white: '#FDFFFC',




}  