export class UserChatModel {
    user_id: string;
    user_chats: Array<ChatModel>;
}


export class ChatModel {
    chat_id?: string;
    chat_created_at: string | any;
    chat_description: string;
    chat_destination_type: string;
    chat_last_message: string;
    chat_last_message_date: string | any;
    chat_members: Array<string>;
    chat_members_detail: Array<ChatUserModel>;
    chat_owner: ChatUserModel;
    state: string;
    chat_seen: boolean;
}

export class ChatUserModel {
    name: string;
    pic: string;
    uuid: string;
}