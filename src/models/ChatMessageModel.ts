import { ChatUserModel } from "./ChatModel";

export class ChatMessageModel {
    message_id?: string;
    chat_id: string;
    message_created_at: string | any;
    message_owner: ChatUserModel;
    message_text: string;
    state: string;
}