export default class LoggedUserModel {
    userIdentifier: string;
    userName: string;
    userId: number;
    userToken: string;
    RfTkn: string;
    roles: string;
    userFullName: string;
    userPic: string;
    lastLoggedDate: string;
    userEmail: string;
    userIdentificacion: string;
    userProyectos: Array<any> = [];
    userEmpresas: Array<any> = [];
    userMenuStyle: string;
    userAppConfig: AppConfig = new AppConfig;
    userRoles: Array<UserRoles> = [];
    userRolesCode: Array<string> = [];
    userExcludedViews: Array<string> = [];
    userPosition:string;

    constructor() {

    }
}


export class AppConfig {
    menuStyle: string;
    appUpdateNotification: boolean;
    appNoActivitiesNotification: boolean;
    configId: number;
}

export class UserRoles {
    user_id: number;
    rol_id: number;
    descripcion: string;
    codigo: string;
}