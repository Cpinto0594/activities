
export enum MenuStylesEnum {

    Drawer = 'DrawerNavigator',
    BottomTab = 'BottomTabsNavigator'


}

export enum ViewsEnum {

    ListActivity = 'ListActivity',
    CreateActivity = 'CreateActivity',

    ListCompany = 'ListCompany',
    CreateCompany = 'CreateCompany',

    ListProject = 'ListProject',
    CreateProject = 'CreateProject',

    ListUser = 'ListUser',
    CreateUser = 'CreateUser',

    Social = 'Social',

    Profile = 'Profile',
    ProfileAssociate = 'ProfileAssociate',
    ProfileAppConfig = 'ProfileAppConfig',

    Parameters = 'Parameters',

    DashBoard = 'DashBoard',


}