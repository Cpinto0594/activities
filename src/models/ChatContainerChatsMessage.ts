import { ChatMessageModel } from "./ChatMessageModel"

export class ChatContainerChatsMessages {
    chat_id:string
    messages:Array<ChatMessageModel>
}