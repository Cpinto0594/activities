import { AppRegistry, Platform } from 'react-native';
//import { onBackgroundHandler } from './src/config/firebaseapp.config';
import AppInit from './src/views/AppInit';

import moment from 'moment'

//EJECTED
// require('moment/min/locales.min')

//EXPO
require( 'moment/locale/es')

if (Platform.OS === 'android') {
    AppRegistry.registerComponent(`ActivityControlApp`, () => AppInit);
    //Handler for Background Notification Service 
    //AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => onBackgroundHandler);
} else {
    AppRegistry.registerComponent(`ActivityControlApp`, () => AppInit);
}
